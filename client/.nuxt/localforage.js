import Vue from 'vue'
import VueLocalforage from 'v-localforage'

Vue.use(VueLocalforage, {"name":"nuxtJS","storeName":"nuxtLocalForage","driver":["localStorageWrapper"],"instances":[{"driver":["localStorageWrapper"],"name":"adaptive","storeName":"polls"}]})

export default (ctx, inject) => {
  inject('localForage', Vue.$localforage)
}
