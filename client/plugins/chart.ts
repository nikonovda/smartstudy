import Vue from 'vue'
import { Line, Bar, mixins, Doughnut, Radar } from 'vue-chartjs'
import 'chartjs-plugin-dragdata'
const { reactiveProp } = mixins

Vue.component('line-chart', {
  extends: Line,
  props: ['options'],
  mixins: [reactiveProp],
  mounted () {
    if(this.options.animation !== undefined) {
      Chart.Legend.prototype.afterFit = function() {
        this.height = this.height + 10;
      };
      this.options.animation.onComplete = () => {
        let ctx = this.$data._chart.ctx
        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily)
        ctx.fillStyle = 'black'
        ctx.textAlign = 'center'
        ctx.textBaseline = 'bottom'
        this.chartData.datasets.forEach((dataset, i) => {
          let meta = this.$data._chart.controller.getDatasetMeta(i)
          meta.data.forEach((bar, index) =>{
            let data = dataset.data[index] + '%'
            ctx.fillText(data, bar._model.x, bar._model.y - 5)
          })
        })
      }
    }
    this.renderChart(this.chartData, this.options)
  }
})

Vue.component('time-line-chart', {
  extends: Line,
  props: ['options'],
  mixins: [reactiveProp],
  mounted () {
    if(this.options.animation !== undefined) {
      Chart.Legend.prototype.afterFit = function() {
        this.height = this.height + 10;
      };
      this.options.animation.onComplete = () => {

        let ctx = this.$data._chart.ctx
        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily)
        ctx.fillStyle = 'black'
        ctx.textAlign = 'center'
        ctx.textBaseline = 'bottom'
        this.chartData.datasets.forEach((dataset, i) => {
          let meta = this.$data._chart.controller.getDatasetMeta(i)
          meta.data.forEach((bar, index) =>{
            let data = dataset.data[index]
            const hours = Math.floor(data / 60 / 60)
            const minutes = Math.floor(data / 60) - (hours * 60)
            const sec = data % 60
            if(hours <= 0) data = minutes.toString().padStart(2, '0') + ':' + sec.toString().padStart(2, '0')
            else data = + hours.toString().padStart(2, '0') + ':' + minutes.toString().padStart(2, '0') + ':' + sec.toString().padStart(2, '0')
            ctx.fillText(data, bar._model.x, bar._model.y - 5)
          })
        })
      }
    }
    this.renderChart(this.chartData, this.options)
  }
})

Vue.component('bar-chart', {
  extends: Bar,
  props: ['options'],
  mixins: [reactiveProp],
  mounted () {
    if(this.options.animation !== undefined) {
      Chart.Legend.prototype.afterFit = function() {
        this.height = this.height + 10;
      };
      this.options.animation.onComplete = () => {

        let ctx = this.$data._chart.ctx
        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily)
        ctx.fillStyle = 'black'
        ctx.textAlign = 'center'
        ctx.textBaseline = 'bottom'
        this.chartData.datasets.forEach((dataset, i) => {
          let meta = this.$data._chart.controller.getDatasetMeta(i)
          meta.data.forEach((bar, index) =>{
            let data = dataset.data[index]
            ctx.fillText(data, bar._model.x, bar._model.y - 5)
          })
        })
      }
    }
    this.renderChart(this.chartData, this.options)
  }
})

Vue.component('time-bar-chart', {
  extends: Bar,
  props: ['options'],
  mixins: [reactiveProp],
  mounted () {
    // console.warn(this.chartData, this.options.animation)
    if(this.options.animation !== undefined) {
      Chart.Legend.prototype.afterFit = function() {
        this.height = this.height + 10;
      };
      this.options.animation.onComplete = () => {
        let ctx = this.$data._chart.ctx
        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily)
        ctx.fillStyle = 'black'
        ctx.textAlign = 'center'
        ctx.textBaseline = 'bottom'
        this.chartData.datasets.forEach((dataset, i) => {
          let meta = this.$data._chart.controller.getDatasetMeta(i)
          meta.data.forEach((bar, index) =>{
            let data = dataset.data[index]
            const hours = Math.floor(data / 60 / 60)
            const minutes = Math.floor(data / 60) - (hours * 60)
            const sec = data % 60
            if(hours <= 0) data = minutes.toString().padStart(2, '0') + ':' + sec.toString().padStart(2, '0')
            else data = + hours.toString().padStart(2, '0') + ':' + minutes.toString().padStart(2, '0') + ':' + sec.toString().padStart(2, '0')
            ctx.fillText(data, bar._model.x, bar._model.y - 5)
          })
        })
      }
    }
    this.renderChart(this.chartData, this.options)
  }
})

Vue.component('doughnut-chart', {
  extends: Doughnut,
  props: ['options'],
  mixins: [reactiveProp],
  methods: {
    componentToHex(c) {
      var hex = Number(c).toString(16);
      return hex.length == 1 ? "0" + hex : hex;
    },
    rgbToHex(r, g, b) {
      return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
    }
  },
  mounted () {
    // console.warn(this.chartData, this.options.animation)
    Chart.pluginService.register({
      beforeDraw: function(chart) {
        if (chart.config.options.elements.center) {
          // Get ctx from string
          var ctx = chart.chart.ctx;

          // Get options from the center object in options
          var centerConfig = chart.config.options.elements.center;
          var fontStyle = centerConfig.fontStyle || 'Arial';
          var txt = centerConfig.text;
          var color = centerConfig.color || '#000';
          var maxFontSize = centerConfig.maxFontSize || 75;
          var sidePadding = centerConfig.sidePadding || 20;
          var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
          // Start with a base font of 30px
          ctx.font = "30px " + fontStyle;

          // Get the width of the string and also the width of the element minus 10 to give it 5px side padding
          var stringWidth = ctx.measureText(txt).width;
          var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

          // Find out how much the font can grow in width.
          var widthRatio = elementWidth / stringWidth;
          var newFontSize = Math.floor(30 * widthRatio);
          var elementHeight = (chart.innerRadius * 2);

          // Pick a new font size so it will not be larger than the height of label.
          var fontSizeToUse = Math.min(newFontSize, elementHeight, maxFontSize);
          var minFontSize = centerConfig.minFontSize;
          var lineHeight = centerConfig.lineHeight || 25;
          var wrapText = true;

          if (minFontSize === undefined) {
            minFontSize = 20;
          }

          if (minFontSize && fontSizeToUse < minFontSize) {
            fontSizeToUse = minFontSize;
            wrapText = true;
          }

          // Set font settings to draw it correctly.
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
          ctx.font = fontSizeToUse + "px " + fontStyle;
          ctx.fillStyle = color;

          if (!wrapText) {
            ctx.fillText(txt, centerX, centerY);
            // ctx.fillText('correct', centerX, centerY);
            return;
          }

          var words = txt.split(' ');
          var line = '';
          var lines = [];

          // Break words up into multiple lines if necessary
          for (var n = 0; n < words.length; n++) {
            var testLine = line + words[n] + ' ';
            var metrics = ctx.measureText(testLine);
            var testWidth = metrics.width;
            if (testWidth > elementWidth && n > 0) {
              lines.push(line);
              line = words[n] + ' ';
            } else {
              line = testLine;
            }
          }

          // Move the center up depending on line height and number of lines
          centerY -= (lines.length / 2) * lineHeight;

          for (var n = 0; n < lines.length; n++) {
            ctx.fillText(lines[n], centerX, centerY);
            // ctx.fillText('correct', centerX, centerY);

            centerY += lineHeight;
          }
          //Draw text in center
          // console.warn(line, 'kskskskskk');
          ctx.fillText(line, centerX, centerY);

        }
      }
    });
    let canvas = this.$refs.canvas
    this.renderChart(this.chartData, this.options)
    let chart = this.$data._chart
    canvas.onclick = (evt) => {
      let activePoints = chart.getElementsAtEvent(evt)
      if (activePoints[0]) {
        let splitColor = activePoints[0]._model.backgroundColor.split(',')
        let color = this.rgbToHex(splitColor[0].replace(/\D+/g,"").trim(), splitColor[1].replace(/\D+/g,"").trim(), splitColor[2].replace(/\D+/g,"").trim())
        if(color === '#ff2e2e' || color === '#2eb232')
          this.$emit('clickOnDoughnut', activePoints[0]['_index'] + 1);
      }
    }
  }
})


Vue.component('Radar', {
  extends: Radar,
  props: ['options'],
  mixins: [reactiveProp],
  mounted () {
    this.addPlugin({
      id: 'chartjs-plugin-dragdata'
    })
    this.renderChart(this.chartData, this.options)
  }
})