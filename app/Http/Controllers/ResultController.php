<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResultRequest;
use App\Result;
use App\Services\Statistics\ComplexityCounter;
use App\Services\Statistics\StatisticsService;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Result::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ResultRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ResultRequest $request)
    {
        $data = $request->all();
        $time = 0;
        foreach ($data['answers'] as $answer) {
            $time += $answer['time'];
        }
        $data['time'] = $time;
        $result = auth()->user()->results()->create($data);

        // Update user simple rating
        $complexity = $result->poll->hops->reduce(function ($acc, $item) {
            return $acc += intval($item->complexity);
        }, 0);
        $rating = auth()->user()->rating ? auth()->user()->rating : 0;
        // auth()->user()->rating = $rating + $complexity;
        auth()->user()->rating = $rating + $result->getCorrectCount();
        
        auth()->user()->save();

        // Update hops complexity in poll
        (new ComplexityCounter())->recalc($result->poll);

        //Update user's corectness, timings and ratings
        
        $statistics = (new \App\Services\Statistics\StatisticsService)->byUserCommon(auth()->user());
        // dd($statistics);
        $timing = [
            'math_calc' => 0,
            'math_nocalc' => 0,
            'personal' => 0,
            'olympics' => 0
        ];
        $countHops = [
            'math_calc' => 38,
            'math_nocalc' => 20,
            'personal' => 20,
            'olympics' => 25
        ];
        $correctness = [
            'math_calc' => 0,
            'math_nocalc' => 0,
            'personal' => 0,
            'olympics' => 0
        ];

        $stats = [];
        foreach ($statistics as $answer) {
            $stats[$answer['section']][] = $answer;
        }

        foreach ($stats as $section => $items) {
            $count = count($items);
            if ($count > 0) {
                foreach ($items as $item) {
                    $timing[$section] += $item['time'];
                    $correctness[$section] += intval($item['isCorrect']);
                }
                $timing[$section] = $timing[$section] / $count * $countHops[$section];
                $correctness[$section . '_percent'] = $correctness[$section] / $count * 100;
                $correctness[$section] = $correctness[$section];
            }
        }


        auth()->user()->timing = $timing;
        auth()->user()->correctness = $correctness;
        auth()->user()->save();

        return response()->json([ 'status' => 'OK', 'result_id' => $result->_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ResultRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function storeUnauthorized(ResultRequest $request)
    {
        $data = $request->all();
        $time = 0;
        foreach ($data['answers'] as $answer) {
            $time += $answer['time'];
        }
        $data['time'] = $time;
        
        $result = Result::create($data);
        return response()->json([ 'status' => 'OK', 'data' => $result ]);
    }

    public function updateUnauthorized(ResultRequest $request, Result $result)
    {
        $data = $request->all();
        $time = 0;
        foreach ($data['answers'] as $answer) {
            $time += $answer['time'];
        }
        $data['time'] = $time;

        $result->update($data);
        return response()->json([ 'status' => 'OK']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function show(Result $result)
    {
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ResultRequest  $request
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function update(ResultRequest $request, Result $result)
    {
        $data = $request->all();
        $time = 0;
        foreach ($data['answers'] as $answer) {
            $time += $answer['time'];
        }
        $data['time'] = $time;

        $result->update($data);

        // Update user rating
        $complexity = $result->poll->hops->reduce(function ($acc, $item) {
            return $acc += intval($item->complexity);
        }, 0);
        $rating = auth()->user()->rating ? auth()->user()->rating : 0;
        auth()->user()->rating = $rating + $complexity;
        auth()->user()->save();
        // Update hops complexity in poll
        (new ComplexityCounter())->recalc($result->poll);

        return response()->json([ 'status' => 'OK', 'result_id' => $result->_id ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function destroy(Result $result)
    {
        $result->delete();
        return response()->json([ 'status' => 'OK' ]);
    }
}
