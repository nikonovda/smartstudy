import { getUserFromCookie, getUserFromLocalStorage } from "../utils/auth"
import axios from '../plugins/axios';

export default async function ({ req, store }) {
  const loggedUser = process.server ? getUserFromCookie(req) : getUserFromLocalStorage()
  if(process.server && loggedUser) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + loggedUser
  }
  if(loggedUser !== undefined) {
    await store.dispatch('polls/init')
    // await store.commit('auth/SET_USER')
    // console.warn(store);
    if(!store.state.auth.user.commonStatistics)
      await store.dispatch('auth/getUser')
  } else {
    await store.dispatch('polls/init')
    if(!store.state.auth.user.commonStatistics)
      await store.dispatch('auth/getTempUser')
  }
}