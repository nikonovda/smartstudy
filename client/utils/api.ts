import { NuxtAxiosInstance } from '@nuxtjs/axios'

let api: NuxtAxiosInstance

export function initializeAxios(axiosInstance: NuxtAxiosInstance) {
  api = axiosInstance
}

export { api }