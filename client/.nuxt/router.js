import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _99e42026 = () => interopDefault(import('..\\pages\\constructor\\index.vue' /* webpackChunkName: "pages/constructor/index" */))
const _d6870ba4 = () => interopDefault(import('..\\pages\\lk\\index.vue' /* webpackChunkName: "pages/lk/index" */))
const _2bea1ede = () => interopDefault(import('..\\pages\\login\\index.vue' /* webpackChunkName: "pages/login/index" */))
const _4b23c3c3 = () => interopDefault(import('..\\pages\\logout\\index.vue' /* webpackChunkName: "pages/logout/index" */))
const _4c5226f4 = () => interopDefault(import('..\\pages\\registration\\index.vue' /* webpackChunkName: "pages/registration/index" */))
const _9b3445b4 = () => interopDefault(import('..\\pages\\tests\\index.vue' /* webpackChunkName: "pages/tests/index" */))
const _267ad690 = () => interopDefault(import('..\\pages\\constructor\\calc\\index.vue' /* webpackChunkName: "pages/constructor/calc/index" */))
const _d591c012 = () => interopDefault(import('..\\pages\\constructor\\noCalc\\index.vue' /* webpackChunkName: "pages/constructor/noCalc/index" */))
const _7af88522 = () => interopDefault(import('..\\pages\\constructor\\оlympics\\index.vue' /* webpackChunkName: "pages/constructor/оlympics/index" */))
const _175b5d92 = () => interopDefault(import('..\\pages\\lk\\dashboard\\index.vue' /* webpackChunkName: "pages/lk/dashboard/index" */))
const _4de26098 = () => interopDefault(import('..\\pages\\lk\\issues\\index.vue' /* webpackChunkName: "pages/lk/issues/index" */))
const _b5921e3c = () => interopDefault(import('..\\pages\\lk\\messages\\index.vue' /* webpackChunkName: "pages/lk/messages/index" */))
const _73218961 = () => interopDefault(import('..\\pages\\lk\\progress\\index.vue' /* webpackChunkName: "pages/lk/progress/index" */))
const _2b1c23ab = () => interopDefault(import('..\\pages\\lk\\statistics\\index.vue' /* webpackChunkName: "pages/lk/statistics/index" */))
const _bb47c878 = () => interopDefault(import('..\\pages\\lk\\TestReview\\index.vue' /* webpackChunkName: "pages/lk/TestReview/index" */))
const _01a059fa = () => interopDefault(import('..\\pages\\lk\\user\\index.vue' /* webpackChunkName: "pages/lk/user/index" */))
const _0edb2de6 = () => interopDefault(import('..\\pages\\lk\\issues\\_id\\index.vue' /* webpackChunkName: "pages/lk/issues/[_]id/index" */))
const _656cb7b4 = () => interopDefault(import('..\\pages\\tests\\_id\\index.vue' /* webpackChunkName: "pages/tests/[_]id/index" */))
const _083b446a = () => interopDefault(import('..\\pages\\tests\\_id\\edit\\index.vue' /* webpackChunkName: "pages/tests/[_]id/edit/index" */))
const _70b6ef83 = () => interopDefault(import('..\\pages\\tests\\_id\\exec\\index.vue' /* webpackChunkName: "pages/tests/[_]id/exec/index" */))
const _1d6cb412 = () => interopDefault(import('..\\pages\\tests\\_id\\result\\index.vue' /* webpackChunkName: "pages/tests/[_]id/result/index" */))
const _04cf420d = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/constructor",
    component: _99e42026,
    name: "constructor"
  }, {
    path: "/lk",
    component: _d6870ba4,
    name: "lk"
  }, {
    path: "/login",
    component: _2bea1ede,
    name: "login"
  }, {
    path: "/logout",
    component: _4b23c3c3,
    name: "logout"
  }, {
    path: "/registration",
    component: _4c5226f4,
    name: "registration"
  }, {
    path: "/tests",
    component: _9b3445b4,
    name: "tests"
  }, {
    path: "/constructor/calc",
    component: _267ad690,
    name: "constructor-calc"
  }, {
    path: "/constructor/noCalc",
    component: _d591c012,
    name: "constructor-noCalc"
  }, {
    path: "/constructor/оlympics",
    component: _7af88522,
    name: "constructor-оlympics"
  }, {
    path: "/lk/dashboard",
    component: _175b5d92,
    name: "lk-dashboard"
  }, {
    path: "/lk/issues",
    component: _4de26098,
    name: "lk-issues"
  }, {
    path: "/lk/messages",
    component: _b5921e3c,
    name: "lk-messages"
  }, {
    path: "/lk/progress",
    component: _73218961,
    name: "lk-progress"
  }, {
    path: "/lk/statistics",
    component: _2b1c23ab,
    name: "lk-statistics"
  }, {
    path: "/lk/TestReview",
    component: _bb47c878,
    name: "lk-TestReview"
  }, {
    path: "/lk/user",
    component: _01a059fa,
    name: "lk-user"
  }, {
    path: "/lk/issues/:id",
    component: _0edb2de6,
    name: "lk-issues-id"
  }, {
    path: "/tests/:id",
    component: _656cb7b4,
    name: "tests-id"
  }, {
    path: "/tests/:id/edit",
    component: _083b446a,
    name: "tests-id-edit"
  }, {
    path: "/tests/:id/exec",
    component: _70b6ef83,
    name: "tests-id-exec"
  }, {
    path: "/tests/:id/result",
    component: _1d6cb412,
    name: "tests-id-result"
  }, {
    path: "/",
    component: _04cf420d,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
