import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import axios from '../plugins/axios'
import { auth } from '../store'
import Vue from 'vue'
import { Group, Users } from '../interfaces/interfaceLk'
@Module({
  name: 'lk',
  stateFactory: true,
  namespaced: true
})
export default class Lk extends VuexModule {
  group: Group = {}
  groups: Group[] = []
  users: Users[] = []
  fuckUp: boolean = true

  @Mutation
  SET_GROUP (payload: Group) {
    this.group = payload
  }
  
  @Mutation
  SET_USERS ( payload: Users[] ) {
    this.users = payload
    this.fuckUp = false
  }

  @Mutation
  SET_ALL_GROUPS ( payload: Group[] ) {
    this.groups = payload
  }

  @Mutation
  CHANGE_OWNER ( payload: string ) {
    Vue.set(this.group, 'user_id', payload)
    this.fuckUp = true
  }

  @Mutation
  ADD_USER_IN_GROUP_AND_USERS ( payload: any ) {
    if(payload.group_id === auth.user.group_id) {
      this.group.members.push(payload)
      Vue.set(this.group, 'members', this.group.members)
    }

    if(auth.isRoot) {
      let indexUser = this.users.findIndex( item => item._id === payload._id)
      let indexGroup = this.groups.findIndex( item => item._id === payload.group_id )
      this.users[indexUser].roles = payload.roles
      this.users[indexUser].group = this.groups[indexGroup] || null
      Vue.set(this.users, indexUser, this.users[indexUser])
    }
  }

  @Mutation
  DELETE_USER_FROM_GROUP ( payload: string ) {
    if(this.group._id === auth.user.group_id){
      let index = this.group.members.findIndex( item => item._id === payload)
      index ? this.group.members.splice(index, 1) : false
    }
    this.fuckUp = true
  }

  @Mutation
  DELETE_USER_FROM_USERS (payload: string) {
    let indexUser = this.users.findIndex( item => item._id === payload)
    indexUser ? this.users.splice(indexUser, 1) : false
  }

  @Action({ rawError: true })
  async getGroupByUser() {
    await axios.get('/api/profile/group')
      .then( (response) => {
        this.SET_GROUP(response.data)
      })
      .catch( e => {
        // alert(e)
      })
  }

  @Action({ rawError: true })
  async getAllgroup() {
    await axios.get('api/groups')
      .then( (response) => {
        this.SET_ALL_GROUPS(response.data)
      })
      .catch( e => {
        // alert(e)
      })
  }

  @Action({ rawError: true })
  async getAllUsers() {
    await axios.get('/api/users/domain/all')
      .then( (response) => {
        this.SET_USERS(response.data)
      })
      .catch( e => {
        // alert(e)
      })
  }

  @Action({ rawError: true })
  createRoot( userId: string ) {
    axios.patch(`/api/users/domain/${userId}/addRole`, { role: 'root' })
      .then( (response) => {
        this.ADD_USER_IN_GROUP_AND_USERS(response.data.data)
      })
      .catch( e => {
        // alert(e)
      })
  }
  
  @Action({ rawError: true })
  changeOwner( userId: string ) {
    axios.patch(`/api/groups/${this.group._id}/owner`, { user_id: userId })
      .then( (response) => {
        this.CHANGE_OWNER(userId)
      })
      .catch( e => {
        // alert(e)
      })
  }

  @Action({ rawError: true })
  addUserInGroupById( payload: any ) {
    let group_id = payload.IdGroup === undefined ? this.group._id : payload.IdGroup

    axios.post(`/api/groups/${group_id}/members`, { user_id: payload.userId } )
      .then( (response) => {
        this.ADD_USER_IN_GROUP_AND_USERS(response.data.data)
      })
      .catch( e => {
        // alert(e)
      })
  }

  @Action({ rawError: true })
  addUserWithNewGroup( payload: any ) {
    axios.post('/api/groups', { name: payload.name, user_id: payload.userId })
      .then( async (response) => {
        await this.getAllUsers()
        await this.getAllgroup()
        this.addUserInGroupById({ IdGroup: response.data.data._id, userId: payload.userId })
      })
  }

  @Action({ rawError: true })
  deleteUserFromGroup( payload: any ) {
    axios.delete(`/api/groups/${payload.groupId}/members/${payload.userId}`)
      .then( () => {
        this.DELETE_USER_FROM_GROUP(payload.userId)
      })
      .catch( e => {
      // alert(e)
    })
  }

  @Action({ rawError: true })
  async deleteUser( userId: string ) {
    await axios.delete(`/api/users/${userId}`)
      .then( () => {
        this.DELETE_USER_FROM_USERS(userId)
      })
      .catch( e => {
        // alert(e)
      })
  }

  @Action({ rawError: true })
  removeRole( payload: any ) {
    axios.patch(`api/users/domain/${payload.userId}/removeRole`, { role: payload.role})
      .then( () => {
        this.getAllUsers()
        this.getAllgroup()
      })
      .catch( (e) => {
        // alert(e)
      })
  }

  get getIdGroupByName () {
    return (name: string) => {
      let index = this.groups.findIndex(item => item.name === name)
      return index !== -1 ? this.groups[index]._id : ''
    }
  }

  get getNameGroupById () {
    return (id: string) => {
      let index = this.groups.findIndex(item => item._id === id)
      return index !== -1 ? this.groups[index].name : ''
    }
  }

  get getOwnerIdGroupByName () {
    return (name: string) => {
      let index = this.groups.findIndex(item => item.name === name)
      return index !== -1 ? this.groups[index].user_id : ''
    }
  }
}