<?php

namespace App\Exports;

use App\Poll;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

class PollResultsExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	protected $poll;
    protected $dataCollection;
    protected $headingsArray;

    public function __construct(Poll $poll) 
    {
    	$poll->loadSortedHops();
        $this->poll = $poll;
        $this->makeCollection();
        $this->makeHeadings();
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->dataCollection;
    }

    public function headings(): array
    {
        return $this->headingsArray;
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        $ranges = [];
        $firstColIndex = 1;
        $firstRowIndex = 1;
        $lastColIndex = $this->calcWidth();
        $lastRowIndex = $this->calcHeight();

        // Диапазон и стили для всей таблицы
        $rangeAll = [
            Coordinate::stringFromColumnIndex($firstColIndex),
            $firstRowIndex,
            ':',
            Coordinate::stringFromColumnIndex($lastColIndex),
            $lastRowIndex
        ];
        $styleAll = [
            'alignment' => [
                'wrapText' => true,
                'horizontal' => 'left',
                'vertical' => 'top',
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ]
        ];
        $ranges[] = [ 'range' => implode('', $rangeAll), 'style' => $styleAll ];

        // Диапазон и стили для заголовков
        $rangeHeadings = [
            Coordinate::stringFromColumnIndex($firstColIndex),
            $firstRowIndex,
            ':',
            Coordinate::stringFromColumnIndex($lastColIndex),
            $firstRowIndex
        ];
        $styleHeadings = [
            'font' => [
                'bold' => true
            ],
        ];
        $ranges[] = [ 'range' => implode('', $rangeHeadings), 'style' => $styleHeadings ];


        // Регистрируем 
        return [
            AfterSheet::class => function(AfterSheet $event) use ($ranges) {
                foreach ($ranges as $item) {
                    $event->getDelegate()->getStyle($item['range'])->applyFromArray($item['style']);
                }
            },
        ];
    }

    /**
     * Формирует коллекцию для отображения результатов
     * 
     * @return [type] [description]
     */
    protected function makeCollection()
    {
        $hopsData = $this->poll->sorted->map(function ($hop) {
            return collect([
                '_id' => $hop->_id,
                'content' => $hop->content, 
                'options' => collect($hop->options)
            ]);
        });

        $this->dataCollection = $this->poll->results->map(function ($result) use ($hopsData) {
            $answers = collect($result->answers);

            $formattedAnswers = $hopsData->map(function ($hop) use ($answers) {
                $answerKey = $answers->search(function ($answer) use ($hop) {
                    return $answer['hop_id'] === $hop['_id'];
                });

                // Если вопрос был добавлен после того, как кто-то уже успел пройти тест
                if ($answerKey === false) return '';

                $answer = $answers[$answerKey];

                // Выбрано несколько опций
                if (count($answer['options']) > 1) {
                    $parsedOptions = array_map(function ($option) use ($hop) {
                        return $this->parseOption($option, $hop['options']);
                    }, $answer['options']);
                    return implode('; ', $parsedOptions);
                }
                // Выбрана одна опция
                else if (count($answer['options']) === 1) {
                    if (!array_key_exists(0, $answer['options'])) dd($answer);
                    return $this->parseOption($answer['options'][0], $hop['options']);
                }
                // Не выбрана ни одна опция
                else {
                    return '';
                }
            });
            return $formattedAnswers;
        });
    }

    /**
     * Формирует массив заголовков.
     * 
     * @return [type] [description]
     */
    protected function makeHeadings()
    {
        $this->headingsArray = $this->poll->sorted->map(function ($hop) {
            return $hop->content;
        })->toArray();
    }

    /**
     * Возвращает полную высоту таблицы.
     * 
     * @return [type] [description]
     */
    protected function calcHeight()
    {
        return $this->dataCollection->count() + 1;
    }

    /**
     * Возвращает ширину таблицы.
     * 
     * @return [type] [description]
     */
    protected function calcWidth()
    {
        return count($this->headingsArray);
    }

    /**
     * Возвращает значение для ячейки из переданной опции и коллекции опций хопа.
     *   
     * @param  array      $option     [description]
     * @param  Collection $hopOptions [description]
     * @return [type]                 [description]
     */
    protected function parseOption(array $option, Collection $hopOptions)
    {
    	if ($option['value']) return $option['value'];

    	$key = $hopOptions->search(function ($item) use ($option) {
    		return $item['id'] === $option['id'];
    	});
	    return $key !== false ? $hopOptions->get($key)['value'] : 'НЕИЗВЕСТНОЕ ЗНАЧЕНИЕ';
    }
}
