import Vue from "vue"
import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import { HopIds, OptionIds, Options, PollFull, Option, HopStore, HopData } from '../interfaces/interfaceStore'
import { InterfacePoll } from '../interfaces/interfacePoll'
import { Hop } from '../interfaces/interfaceHops'
import { auth } from '../store';
import axios from '../plugins/axios'

interface Obj {
  [key: string]: any
}

interface PayloadOption {
  options: Option[],
  hopId: String
}

@Module({
  name: 'poll',
  stateFactory: true,
  namespaced: true
})
export default class Poll extends VuexModule {
  poll: PollFull = {}
  hops: HopStore = {}
  options: Options = {}
  hopIds: HopIds = []
  optionIds: OptionIds = []
  tmpHopId: number = 1

  @Mutation
  INCR_TMPHOPID () {
    this.tmpHopId++
  }


  @Mutation
  SET_POLL (poll: InterfacePoll) {
    this.poll = {
      _id: poll._id,
      name: poll.name,
      description: poll.description,
      type: poll.type,
      created_at: poll.created_at,
      published_at: poll.published_at,
      closed_at: poll.closed_at,
      hops: poll.sort,
      isPublished: poll.isPublished,
      isActive: poll.isActive,
      section: poll.section,
      user_id: poll.user_id,
      timeToSolution: poll.timeToSolution
    }
  }

  @Mutation
  SET_FIELD (payload: any) {
    Vue.set(this.poll, payload.key, payload.value)
  }

  @Mutation
  SET_HOP_IDS (hopIds: string[]) {
    this.poll.hops = hopIds
  }

  @Mutation
  SET_HOPS (hops: Array<Hop>) {
    this.hops = hops.reduce((obj: Obj, item) => {
      obj[item._id] = {
        _id: item._id,
        content: item.content,
        options: item.options.map( function (option) {
          return option.id
        }),
        image: item.image,
        explanation: item.explanation,
        correctAnswer: item.correctAnswer,
        tags: item.tags,
        subTags: item.subTags,
        complexity: item.complexity
      }
      return obj
    }, {})
    this.hopIds = hops.map(item => item._id)
  }

  @Mutation
  SET_HOP ( hop: HopData) {
    Vue.set(this.hops, this.tmpHopId.toString(), {})
    const obj: any = {
      _id: this.tmpHopId.toString(),
      content: hop.content,
      options: hop.options.map((option) => option.id),
      image: hop.image,
      explanation: hop.explanation,
      correctAnswer: hop.correctAnswer,
      tags: hop.tags,
      subTags: hop.subTags,
      complexity: hop.complexity
    }
    Vue.set(this.hops, this.tmpHopId.toString(), obj)
    if (!this.hopIds.includes(this.tmpHopId.toString())) {
      this.hopIds.push(this.tmpHopId.toString())
    }
  }

  @Mutation
  UPDATE_HOP (hop: HopStore) {
    Vue.delete(this.hops, hop._id)
    Vue.set(this.hops, hop._id, {})
    const obj: Obj = {
      _id: hop._id,
      content: hop.content,
      options: hop.options.map((option) => option),
      image: hop.image,
      correctAnswer: hop.correctAnswer,
      explanation: hop.explanation,
      tags: hop.tags,
      subTags: hop.subTags,
      complexity: hop.complexity
    }
    if (hop.condition !== undefined) {
      obj.condition = hop.condition.trim()
    }
    Vue.set(this.hops, hop._id, obj)
    if (!this.hopIds.includes(hop._id)) {
      this.hopIds.push(hop._id)
    }
  }

  @Mutation
  SET_OPTIONS (payload: PayloadOption) {
    payload.options.forEach((item: Option) => {
      const id: string = payload.hopId + '_' + item.id
      const obj = {
        id: item.id,
        value: item.value,
        type: item.type,
        image: item.image
      }
    Vue.set(this.options, id, obj)
    })
    const hopOptionIds = payload.options.map((item: Option) => (`${payload.hopId}_${item.id}`))
    this.optionIds.push(...hopOptionIds)
  }

  @Mutation
  SET_OPTION (payload: any) {
    const id = payload.hopId + '_' + payload.option.id
    const obj = {
      id: payload.option.id,
      value: payload.option.value,
      type: payload.option.type,
      image: payload.option.image
    }
    Vue.set(this.options, id, obj)
    if (!this.optionIds.includes(id)) {
      this.optionIds.push(id)
      this.hops[payload.hopId].options.push(payload.option.id)
    }
  }

  @Mutation
  REMOVE_HOP (hop: HopStore) {
    this.hops[hop._id] = undefined
    this.optionIds.forEach((item: string) => {
      if (item[0] === hop._id) {
        const idx: Number = this.optionIds.indexOf(item)
        this.optionIds.splice(idx, 1)
      }
    })
    const index: Number = this.hopIds.indexOf(hop._id)
    if (index !== -1) {
      this.hopIds.splice(index, 1)
    }
  }

  @Mutation
  REMOVE_OPTION (payload: Option) {
    const id: string = payload.hopId + '_' + payload.option.id
    this.options[id] = undefined

    const indexHop: number = this.hops[payload.hopId].options.indexOf(payload.option.id)
    if (indexHop !== -1) {
      this.hops[payload.hopId].options.splice(indexHop, 1)
    }

    const index: number = this.optionIds.indexOf(id)
    if (index !== -1) {
      this.optionIds.splice(index, 1)
    }
  }

  @Mutation
  REFRESH_STATE () {
    this.hopIds = []
    this.optionIds = []
    this.poll = {}
    this.hops = {}
    this.options = {}
  }

  @Action({ rawError: true })
  async fetch (id: String) {
    let url: string
    if(auth.isAdmin || auth.isRoot) url = '/api/polls/' + id
    else url = '/api/polls/published/' + id 
    await axios.get(url)
                  .then((response) => {
                    const poll: InterfacePoll = response.data
                    this.SET_POLL(poll)
                    this.SET_HOPS(poll.sorted)
                    poll.sorted.forEach((hop: Hop) => this.SET_OPTIONS({ options: hop.options, hopId: hop._id }))
                  })
                  .catch((e) => {
                  })
  }

  @Action({ rawError: true })
  createHop (payload) {
    this.SET_POLL(payload)
    this.SET_HOPS(payload.sorted)
    payload.sorted.forEach((hop: Hop) => this.SET_OPTIONS({ options: hop.options, hopId: hop._id }))
  }

  @Action({ rawError: true })
  updateField (payload) {
    this.SET_FIELD(payload)
  }

  @Action({ rawError: true })
  addTemplates(hop: HopData) {
    this.SET_HOP(hop)
    const hops = [ ...this.poll.hops, this.tmpHopId.toString() ]
    this.SET_HOP_IDS(hops)
    this.SET_OPTIONS({ options: hop.options, hopId: this.tmpHopId.toString() })
  }

  @Action({ rawError: true })
  addHop (hop: HopData) {
    this.SET_HOP(hop)
    const hops = [ ...this.poll.hops, hop._id ]
    this.SET_HOP_IDS(hops)
  }

  @Action({ rawError: true })
  updateHop (upd) {
    if (this.hopIds.includes(upd._id)) {
      if ( upd.options ) {
        upd.options = upd.options.map(option => typeof option === 'number' ? option : option.id)
      }
      const hop = { ...this.hops[upd._id], ...upd }
      this.UPDATE_HOP(hop)
    }
  }

  @Action({ rawError: true })
  removeHop (hop: HopStore) {
    if (typeof hop === 'string') {
      hop = this.hopById(hop)
      if (!hop) { return false }
    }
    this.REMOVE_HOP(hop)
    const hops = this.poll.hops.filter(id => id !== hop._id)
    this.SET_HOP_IDS(hops)
  }

  @Action({ rawError: true })
  addOption (payload) {
    this.SET_OPTION(payload)
  }

  @Action({ rawError: true })
  updateOption (payload) {
    const id = payload.hopId + '_' + payload.option.id
    if (this.optionIds.includes(id)) {
      const option = { ...this.options[id], ...payload.option }
      this.SET_OPTION({ hopId: payload.hopId, option })
    }
  }

  @Action({ rawError: true })
  removeOption (payload) {
    this.REMOVE_OPTION(payload)
  }

  @Action({ rawError: true })
  async savePoll (source: any) {
    try {
      const resp = await axios.post('/api/polls', source)
      this.REFRESH_STATE()
      return resp.data._id
    } catch (e) {
      return false
    }
  }

  @Action({ rawError: true })
  async updatePoll (source: any) {
    try {
      const resp = await axios.patch('/api/polls/' + source._id, source)
      this.REFRESH_STATE()
      return resp.data._id
    } catch (e) {
      return false
    }
  }

  get full () {
    const hops = []
    if (!this.poll.hops) return this.poll
    this.poll.hops.forEach((hopId) => {
      if (this.hopIds.includes(hopId)) {
        const options = []
        this.hops[hopId].options.forEach((optionId) => {
          const id = hopId + '_' + optionId
          if (this.optionIds.includes(id)) { options.push(this.options[id]) }
        })
        const hop = {
          ...this.hops[hopId],
          options
        }
        hops.push(hop)
      }
    })
    return { ...this.poll, hops }
  }

  get conditionByHopId () {
    return (id: string) => this.hops[id].condition === undefined ? false : true
  }

  get hopById () {
    return (id: string) => this.hops[id]
  }

  get hopByIdWithOptions () {
    return (id: string) => {
      const options = []
      this.hops[id].options.forEach((optionId) => {
        const realId = id + '_' + optionId
        if (this.optionIds.includes(realId)) { options.push(this.options[realId]) }
      })
      return { ...this.hops[id], options}
    }
  }

  get getHops (): HopStore {
    if (this.hops !== {}) return this.hops
  }

  get contentById () {
    return (id: string) => this.hops[id].content
  }

  get getPoll () {
    return this.poll
  }
  get optionById () {
    return (id: string) => this.options[id]
  }

}
