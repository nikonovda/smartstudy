<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserDomainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        return User::with(['polls:_id,user_id'])->get();
    }


    public function addRole(Request $request, User $user)
    {
        if (!$request->has('role')) {
            return response()->json([ 'status' => 'error', 'message' => 'No role.' ]);
        }
        $user->addRole($request->role);
        return response()->json([ 'status' => 'OK', 'data' => $user ]);
    }


    public function removeRole(Request $request, User $user)
    {
        if (!$request->has('role')) {
            return response()->json([ 'status' => 'error', 'message' => 'No role.' ]);
        }
        $user->removeRole($request->role);
        return response()->json([ 'status' => 'OK', 'data' => $user ]);
    }

    public function activate(User $user)
    {
        $user->activate();
        return response()->json([ 'status' => 'OK', 'data' => $user ]);
    }

    public function deactivate(User $user)
    {
        $user->deactivate();
        return response()->json([ 'status' => 'OK', 'data' => $user ]);
    }
}
