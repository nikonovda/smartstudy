exports.ids = [19];
exports.modules = {

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VCardActions; });
/* unused harmony export VCardSubtitle */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return VCardText; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return VCardTitle; });
/* harmony import */ var _VCard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(298);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);


const VCardActions = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__actions');
const VCardSubtitle = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__subtitle');
const VCardText = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__text');
const VCardTitle = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__title');

/* unused harmony default export */ var _unused_webpack_default_export = ({
  $_vuetify_subcomponents: {
    VCard: _VCard__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"],
    VCardActions,
    VCardSubtitle,
    VCardText,
    VCardTitle
  }
});

/***/ }),

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// UNUSED EXPORTS: VInput

// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VInput/VInput.sass
var VInput = __webpack_require__(328);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/index.js
var VIcon = __webpack_require__(23);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VLabel/index.js + 1 modules
var VLabel = __webpack_require__(321);

// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VMessages/VMessages.sass
var VMessages = __webpack_require__(332);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/colorable/index.js
var colorable = __webpack_require__(12);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/themeable/index.js
var themeable = __webpack_require__(11);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/mixins.js
var mixins = __webpack_require__(4);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/helpers.js
var helpers = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VMessages/VMessages.js
// Styles
 // Mixins



 // Utilities


/* @vue/component */

/* harmony default export */ var VMessages_VMessages = (Object(mixins["a" /* default */])(colorable["a" /* default */], themeable["a" /* default */]).extend({
  name: 'v-messages',
  props: {
    value: {
      type: Array,
      default: () => []
    }
  },
  methods: {
    genChildren() {
      return this.$createElement('transition-group', {
        staticClass: 'v-messages__wrapper',
        attrs: {
          name: 'message-transition',
          tag: 'div'
        }
      }, this.value.map(this.genMessage));
    },

    genMessage(message, key) {
      return this.$createElement('div', {
        staticClass: 'v-messages__message',
        key
      }, Object(helpers["s" /* getSlot */])(this, 'default', {
        message,
        key
      }) || [message]);
    }

  },

  render(h) {
    return h('div', this.setTextColor(this.color, {
      staticClass: 'v-messages',
      class: this.themeClasses
    }), [this.genChildren()]);
  }

}));
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VMessages/index.js


/* harmony default export */ var components_VMessages = (VMessages_VMessages);
// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/binds-attrs/index.js
var binds_attrs = __webpack_require__(36);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/validatable/index.js
var validatable = __webpack_require__(323);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/mergeData.js
var mergeData = __webpack_require__(31);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VInput/VInput.js
// Styles
 // Components



 // Mixins


 // Utilities




const baseMixins = Object(mixins["a" /* default */])(binds_attrs["a" /* default */], validatable["a" /* default */]);
/* @vue/component */

/* harmony default export */ var VInput_VInput = (baseMixins.extend().extend({
  name: 'v-input',
  inheritAttrs: false,
  props: {
    appendIcon: String,
    backgroundColor: {
      type: String,
      default: ''
    },
    dense: Boolean,
    height: [Number, String],
    hideDetails: [Boolean, String],
    hint: String,
    id: String,
    label: String,
    loading: Boolean,
    persistentHint: Boolean,
    prependIcon: String,
    value: null
  },

  data() {
    return {
      lazyValue: this.value,
      hasMouseDown: false
    };
  },

  computed: {
    classes() {
      return {
        'v-input--has-state': this.hasState,
        'v-input--hide-details': !this.showDetails,
        'v-input--is-label-active': this.isLabelActive,
        'v-input--is-dirty': this.isDirty,
        'v-input--is-disabled': this.isDisabled,
        'v-input--is-focused': this.isFocused,
        // <v-switch loading>.loading === '' so we can't just cast to boolean
        'v-input--is-loading': this.loading !== false && this.loading != null,
        'v-input--is-readonly': this.isReadonly,
        'v-input--dense': this.dense,
        ...this.themeClasses
      };
    },

    computedId() {
      return this.id || `input-${this._uid}`;
    },

    hasDetails() {
      return this.messagesToDisplay.length > 0;
    },

    hasHint() {
      return !this.hasMessages && !!this.hint && (this.persistentHint || this.isFocused);
    },

    hasLabel() {
      return !!(this.$slots.label || this.label);
    },

    // Proxy for `lazyValue`
    // This allows an input
    // to function without
    // a provided model
    internalValue: {
      get() {
        return this.lazyValue;
      },

      set(val) {
        this.lazyValue = val;
        this.$emit(this.$_modelEvent, val);
      }

    },

    isDirty() {
      return !!this.lazyValue;
    },

    isLabelActive() {
      return this.isDirty;
    },

    messagesToDisplay() {
      if (this.hasHint) return [this.hint];
      if (!this.hasMessages) return [];
      return this.validations.map(validation => {
        if (typeof validation === 'string') return validation;
        const validationResult = validation(this.internalValue);
        return typeof validationResult === 'string' ? validationResult : '';
      }).filter(message => message !== '');
    },

    showDetails() {
      return this.hideDetails === false || this.hideDetails === 'auto' && this.hasDetails;
    }

  },
  watch: {
    value(val) {
      this.lazyValue = val;
    }

  },

  beforeCreate() {
    // v-radio-group needs to emit a different event
    // https://github.com/vuetifyjs/vuetify/issues/4752
    this.$_modelEvent = this.$options.model && this.$options.model.event || 'input';
  },

  methods: {
    genContent() {
      return [this.genPrependSlot(), this.genControl(), this.genAppendSlot()];
    },

    genControl() {
      return this.$createElement('div', {
        staticClass: 'v-input__control'
      }, [this.genInputSlot(), this.genMessages()]);
    },

    genDefaultSlot() {
      return [this.genLabel(), this.$slots.default];
    },

    genIcon(type, cb, extraData = {}) {
      const icon = this[`${type}Icon`];
      const eventName = `click:${Object(helpers["w" /* kebabCase */])(type)}`;
      const hasListener = !!(this.listeners$[eventName] || cb);
      const data = Object(mergeData["a" /* default */])({
        attrs: {
          'aria-label': hasListener ? Object(helpers["w" /* kebabCase */])(type).split('-')[0] + ' icon' : undefined,
          color: this.validationState,
          dark: this.dark,
          disabled: this.isDisabled,
          light: this.light
        },
        on: !hasListener ? undefined : {
          click: e => {
            e.preventDefault();
            e.stopPropagation();
            this.$emit(eventName, e);
            cb && cb(e);
          },
          // Container has g event that will
          // trigger menu open if enclosed
          mouseup: e => {
            e.preventDefault();
            e.stopPropagation();
          }
        }
      }, extraData);
      return this.$createElement('div', {
        staticClass: `v-input__icon`,
        class: type ? `v-input__icon--${Object(helpers["w" /* kebabCase */])(type)}` : undefined
      }, [this.$createElement(VIcon["a" /* default */], data, icon)]);
    },

    genInputSlot() {
      return this.$createElement('div', this.setBackgroundColor(this.backgroundColor, {
        staticClass: 'v-input__slot',
        style: {
          height: Object(helpers["g" /* convertToUnit */])(this.height)
        },
        on: {
          click: this.onClick,
          mousedown: this.onMouseDown,
          mouseup: this.onMouseUp
        },
        ref: 'input-slot'
      }), [this.genDefaultSlot()]);
    },

    genLabel() {
      if (!this.hasLabel) return null;
      return this.$createElement(VLabel["a" /* default */], {
        props: {
          color: this.validationState,
          dark: this.dark,
          disabled: this.isDisabled,
          focused: this.hasState,
          for: this.computedId,
          light: this.light
        }
      }, this.$slots.label || this.label);
    },

    genMessages() {
      if (!this.showDetails) return null;
      return this.$createElement(components_VMessages, {
        props: {
          color: this.hasHint ? '' : this.validationState,
          dark: this.dark,
          light: this.light,
          value: this.messagesToDisplay
        },
        attrs: {
          role: this.hasMessages ? 'alert' : null
        },
        scopedSlots: {
          default: props => Object(helpers["s" /* getSlot */])(this, 'message', props)
        }
      });
    },

    genSlot(type, location, slot) {
      if (!slot.length) return null;
      const ref = `${type}-${location}`;
      return this.$createElement('div', {
        staticClass: `v-input__${ref}`,
        ref
      }, slot);
    },

    genPrependSlot() {
      const slot = [];

      if (this.$slots.prepend) {
        slot.push(this.$slots.prepend);
      } else if (this.prependIcon) {
        slot.push(this.genIcon('prepend'));
      }

      return this.genSlot('prepend', 'outer', slot);
    },

    genAppendSlot() {
      const slot = []; // Append icon for text field was really
      // an appended inner icon, v-text-field
      // will overwrite this method in order to obtain
      // backwards compat

      if (this.$slots.append) {
        slot.push(this.$slots.append);
      } else if (this.appendIcon) {
        slot.push(this.genIcon('append'));
      }

      return this.genSlot('append', 'outer', slot);
    },

    onClick(e) {
      this.$emit('click', e);
    },

    onMouseDown(e) {
      this.hasMouseDown = true;
      this.$emit('mousedown', e);
    },

    onMouseUp(e) {
      this.hasMouseDown = false;
      this.$emit('mouseup', e);
    }

  },

  render(h) {
    return h('div', this.setTextColor(this.validationState, {
      staticClass: 'v-input',
      class: this.classes
    }), this.genContent());
  }

}));
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VInput/index.js


/* harmony default export */ var components_VInput = __webpack_exports__["a"] = (VInput_VInput);

/***/ }),

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// UNUSED EXPORTS: VLabel

// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VLabel/VLabel.sass
var VLabel = __webpack_require__(330);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/colorable/index.js
var colorable = __webpack_require__(12);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/themeable/index.js
var themeable = __webpack_require__(11);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/mixins.js
var mixins = __webpack_require__(4);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/helpers.js
var helpers = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VLabel/VLabel.js
// Styles
 // Mixins



 // Helpers


/* @vue/component */

/* harmony default export */ var VLabel_VLabel = (Object(mixins["a" /* default */])(themeable["a" /* default */]).extend({
  name: 'v-label',
  functional: true,
  props: {
    absolute: Boolean,
    color: {
      type: String,
      default: 'primary'
    },
    disabled: Boolean,
    focused: Boolean,
    for: String,
    left: {
      type: [Number, String],
      default: 0
    },
    right: {
      type: [Number, String],
      default: 'auto'
    },
    value: Boolean
  },

  render(h, ctx) {
    const {
      children,
      listeners,
      props
    } = ctx;
    const data = {
      staticClass: 'v-label',
      class: {
        'v-label--active': props.value,
        'v-label--is-disabled': props.disabled,
        ...Object(themeable["b" /* functionalThemeClasses */])(ctx)
      },
      attrs: {
        for: props.for,
        'aria-hidden': !props.for
      },
      on: listeners,
      style: {
        left: Object(helpers["g" /* convertToUnit */])(props.left),
        right: Object(helpers["g" /* convertToUnit */])(props.right),
        position: props.absolute ? 'absolute' : 'relative'
      },
      ref: 'label'
    };
    return h('label', colorable["a" /* default */].options.methods.setTextColor(props.focused && props.color, data), children);
  }

}));
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VLabel/index.js


/* harmony default export */ var components_VLabel = __webpack_exports__["a"] = (VLabel_VLabel);

/***/ }),

/***/ 323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _colorable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(12);
/* harmony import */ var _themeable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(11);
/* harmony import */ var _registrable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(56);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);
/* harmony import */ var _util_console__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6);
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4);
// Mixins


 // Utilities




const baseMixins = Object(_util_mixins__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"])(_colorable__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"], Object(_registrable__WEBPACK_IMPORTED_MODULE_2__[/* inject */ "a"])('form'), _themeable__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"]);
/* @vue/component */

/* harmony default export */ __webpack_exports__["a"] = (baseMixins.extend({
  name: 'validatable',
  props: {
    disabled: Boolean,
    error: Boolean,
    errorCount: {
      type: [Number, String],
      default: 1
    },
    errorMessages: {
      type: [String, Array],
      default: () => []
    },
    messages: {
      type: [String, Array],
      default: () => []
    },
    readonly: Boolean,
    rules: {
      type: Array,
      default: () => []
    },
    success: Boolean,
    successMessages: {
      type: [String, Array],
      default: () => []
    },
    validateOnBlur: Boolean,
    value: {
      required: false
    }
  },

  data() {
    return {
      errorBucket: [],
      hasColor: false,
      hasFocused: false,
      hasInput: false,
      isFocused: false,
      isResetting: false,
      lazyValue: this.value,
      valid: false
    };
  },

  computed: {
    computedColor() {
      if (this.isDisabled) return undefined;
      if (this.color) return this.color; // It's assumed that if the input is on a
      // dark background, the user will want to
      // have a white color. If the entire app
      // is setup to be dark, then they will
      // like want to use their primary color

      if (this.isDark && !this.appIsDark) return 'white';else return 'primary';
    },

    hasError() {
      return this.internalErrorMessages.length > 0 || this.errorBucket.length > 0 || this.error;
    },

    // TODO: Add logic that allows the user to enable based
    // upon a good validation
    hasSuccess() {
      return this.internalSuccessMessages.length > 0 || this.success;
    },

    externalError() {
      return this.internalErrorMessages.length > 0 || this.error;
    },

    hasMessages() {
      return this.validationTarget.length > 0;
    },

    hasState() {
      if (this.isDisabled) return false;
      return this.hasSuccess || this.shouldValidate && this.hasError;
    },

    internalErrorMessages() {
      return this.genInternalMessages(this.errorMessages);
    },

    internalMessages() {
      return this.genInternalMessages(this.messages);
    },

    internalSuccessMessages() {
      return this.genInternalMessages(this.successMessages);
    },

    internalValue: {
      get() {
        return this.lazyValue;
      },

      set(val) {
        this.lazyValue = val;
        this.$emit('input', val);
      }

    },

    isDisabled() {
      return this.disabled || !!this.form && this.form.disabled;
    },

    isInteractive() {
      return !this.isDisabled && !this.isReadonly;
    },

    isReadonly() {
      return this.readonly || !!this.form && this.form.readonly;
    },

    shouldValidate() {
      if (this.externalError) return true;
      if (this.isResetting) return false;
      return this.validateOnBlur ? this.hasFocused && !this.isFocused : this.hasInput || this.hasFocused;
    },

    validations() {
      return this.validationTarget.slice(0, Number(this.errorCount));
    },

    validationState() {
      if (this.isDisabled) return undefined;
      if (this.hasError && this.shouldValidate) return 'error';
      if (this.hasSuccess) return 'success';
      if (this.hasColor) return this.computedColor;
      return undefined;
    },

    validationTarget() {
      if (this.internalErrorMessages.length > 0) {
        return this.internalErrorMessages;
      } else if (this.successMessages.length > 0) {
        return this.internalSuccessMessages;
      } else if (this.messages.length > 0) {
        return this.internalMessages;
      } else if (this.shouldValidate) {
        return this.errorBucket;
      } else return [];
    }

  },
  watch: {
    rules: {
      handler(newVal, oldVal) {
        if (Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* deepEqual */ "j"])(newVal, oldVal)) return;
        this.validate();
      },

      deep: true
    },

    internalValue() {
      // If it's the first time we're setting input,
      // mark it with hasInput
      this.hasInput = true;
      this.validateOnBlur || this.$nextTick(this.validate);
    },

    isFocused(val) {
      // Should not check validation
      // if disabled
      if (!val && !this.isDisabled) {
        this.hasFocused = true;
        this.validateOnBlur && this.$nextTick(this.validate);
      }
    },

    isResetting() {
      setTimeout(() => {
        this.hasInput = false;
        this.hasFocused = false;
        this.isResetting = false;
        this.validate();
      }, 0);
    },

    hasError(val) {
      if (this.shouldValidate) {
        this.$emit('update:error', val);
      }
    },

    value(val) {
      this.lazyValue = val;
    }

  },

  beforeMount() {
    this.validate();
  },

  created() {
    this.form && this.form.register(this);
  },

  beforeDestroy() {
    this.form && this.form.unregister(this);
  },

  methods: {
    genInternalMessages(messages) {
      if (!messages) return [];else if (Array.isArray(messages)) return messages;else return [messages];
    },

    /** @public */
    reset() {
      this.isResetting = true;
      this.internalValue = Array.isArray(this.internalValue) ? [] : undefined;
    },

    /** @public */
    resetValidation() {
      this.isResetting = true;
    },

    /** @public */
    validate(force = false, value) {
      const errorBucket = [];
      value = value || this.internalValue;
      if (force) this.hasInput = this.hasFocused = true;

      for (let index = 0; index < this.rules.length; index++) {
        const rule = this.rules[index];
        const valid = typeof rule === 'function' ? rule(value) : rule;

        if (valid === false || typeof valid === 'string') {
          errorBucket.push(valid || '');
        } else if (typeof valid !== 'boolean') {
          Object(_util_console__WEBPACK_IMPORTED_MODULE_4__[/* consoleError */ "b"])(`Rules should return a string or boolean, received '${typeof valid}' instead`, this);
        }
      }

      this.errorBucket = errorBucket;
      this.valid = errorBucket.length === 0;
      return this.valid;
    }

  }
}));

/***/ }),

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);


/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_0___default.a.extend({
  name: 'comparable',
  props: {
    valueComparator: {
      type: Function,
      default: _util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* deepEqual */ "j"]
    }
  }
}));

/***/ }),

/***/ 328:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(329);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(10).default("22487aae", content, true)

/***/ }),

/***/ 329:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".theme--light.v-input,.theme--light.v-input input,.theme--light.v-input textarea{color:rgba(0,0,0,.87)}.theme--light.v-input input::-moz-placeholder,.theme--light.v-input textarea::-moz-placeholder{color:rgba(0,0,0,.38)}.theme--light.v-input input:-ms-input-placeholder,.theme--light.v-input textarea:-ms-input-placeholder{color:rgba(0,0,0,.38)}.theme--light.v-input input::-ms-input-placeholder,.theme--light.v-input textarea::-ms-input-placeholder{color:rgba(0,0,0,.38)}.theme--light.v-input input::placeholder,.theme--light.v-input textarea::placeholder{color:rgba(0,0,0,.38)}.theme--light.v-input--is-disabled,.theme--light.v-input--is-disabled input,.theme--light.v-input--is-disabled textarea{color:rgba(0,0,0,.38)}.theme--dark.v-input,.theme--dark.v-input input,.theme--dark.v-input textarea{color:#fff}.theme--dark.v-input input::-moz-placeholder,.theme--dark.v-input textarea::-moz-placeholder{color:hsla(0,0%,100%,.5)}.theme--dark.v-input input:-ms-input-placeholder,.theme--dark.v-input textarea:-ms-input-placeholder{color:hsla(0,0%,100%,.5)}.theme--dark.v-input input::-ms-input-placeholder,.theme--dark.v-input textarea::-ms-input-placeholder{color:hsla(0,0%,100%,.5)}.theme--dark.v-input input::placeholder,.theme--dark.v-input textarea::placeholder{color:hsla(0,0%,100%,.5)}.theme--dark.v-input--is-disabled,.theme--dark.v-input--is-disabled input,.theme--dark.v-input--is-disabled textarea{color:hsla(0,0%,100%,.5)}.v-input{align-items:flex-start;display:flex;flex:1 1 auto;font-size:16px;letter-spacing:normal;max-width:100%;text-align:left}.v-input .v-progress-linear{top:calc(100% - 1px);left:0}.v-input input{max-height:32px}.v-input input:invalid,.v-input textarea:invalid{box-shadow:none}.v-input input:active,.v-input input:focus,.v-input textarea:active,.v-input textarea:focus{outline:none}.v-input .v-label{height:20px;line-height:20px}.v-input__append-outer,.v-input__prepend-outer{display:inline-flex;margin-bottom:4px;margin-top:4px;line-height:1}.v-input__append-outer .v-icon,.v-input__prepend-outer .v-icon{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.v-application--is-ltr .v-input__append-outer{margin-left:9px}.v-application--is-ltr .v-input__prepend-outer,.v-application--is-rtl .v-input__append-outer{margin-right:9px}.v-application--is-rtl .v-input__prepend-outer{margin-left:9px}.v-input__control{display:flex;flex-direction:column;height:auto;flex-grow:1;flex-wrap:wrap;min-width:0;width:100%}.v-input__icon{align-items:center;display:inline-flex;height:24px;flex:1 0 auto;justify-content:center;min-width:24px;width:24px}.v-input__icon--clear{border-radius:50%}.v-input__icon--clear .v-icon--disabled{visibility:hidden}.v-input__slot{align-items:center;color:inherit;display:flex;margin-bottom:8px;min-height:inherit;position:relative;transition:.3s cubic-bezier(.25,.8,.5,1);width:100%}.v-input--dense>.v-input__control>.v-input__slot{margin-bottom:4px}.v-input--is-disabled:not(.v-input--is-readonly){pointer-events:none}.v-input--is-loading>.v-input__control>.v-input__slot:after,.v-input--is-loading>.v-input__control>.v-input__slot:before{display:none}.v-input--hide-details>.v-input__control>.v-input__slot{margin-bottom:0}.v-input--has-state.error--text .v-label{-webkit-animation:v-shake .6s cubic-bezier(.25,.8,.5,1);animation:v-shake .6s cubic-bezier(.25,.8,.5,1)}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 330:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(331);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(10).default("371f82d0", content, true)

/***/ }),

/***/ 331:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".theme--light.v-label{color:rgba(0,0,0,.6)}.theme--light.v-label--is-disabled{color:rgba(0,0,0,.38)}.theme--dark.v-label{color:hsla(0,0%,100%,.7)}.theme--dark.v-label--is-disabled{color:hsla(0,0%,100%,.5)}.v-label{font-size:16px;line-height:1;min-height:8px;transition:.3s cubic-bezier(.25,.8,.5,1)}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 332:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(333);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(10).default("2bb34da4", content, true)

/***/ }),

/***/ 333:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".theme--light.v-messages{color:rgba(0,0,0,.6)}.theme--dark.v-messages{color:hsla(0,0%,100%,.7)}.v-messages{flex:1 1 auto;font-size:12px;min-height:14px;min-width:1px;position:relative}.v-application--is-ltr .v-messages{text-align:left}.v-application--is-rtl .v-messages{text-align:right}.v-messages__message{line-height:12px;word-break:break-word;word-wrap:break-word;-webkit-hyphens:auto;-ms-hyphens:auto;hyphens:auto}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(131);
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _util_mergeData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);



 // no xs

const breakpoints = ['sm', 'md', 'lg', 'xl'];
const ALIGNMENT = ['start', 'end', 'center'];

function makeProps(prefix, def) {
  return breakpoints.reduce((props, val) => {
    props[prefix + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = def();
    return props;
  }, {});
}

const alignValidator = str => [...ALIGNMENT, 'baseline', 'stretch'].includes(str);

const alignProps = makeProps('align', () => ({
  type: String,
  default: null,
  validator: alignValidator
}));

const justifyValidator = str => [...ALIGNMENT, 'space-between', 'space-around'].includes(str);

const justifyProps = makeProps('justify', () => ({
  type: String,
  default: null,
  validator: justifyValidator
}));

const alignContentValidator = str => [...ALIGNMENT, 'space-between', 'space-around', 'stretch'].includes(str);

const alignContentProps = makeProps('alignContent', () => ({
  type: String,
  default: null,
  validator: alignContentValidator
}));
const propMap = {
  align: Object.keys(alignProps),
  justify: Object.keys(justifyProps),
  alignContent: Object.keys(alignContentProps)
};
const classMap = {
  align: 'align',
  justify: 'justify',
  alignContent: 'align-content'
};

function breakpointClass(type, prop, val) {
  let className = classMap[type];

  if (val == null) {
    return undefined;
  }

  if (prop) {
    // alignSm -> Sm
    const breakpoint = prop.replace(type, '');
    className += `-${breakpoint}`;
  } // .align-items-sm-center


  className += `-${val}`;
  return className.toLowerCase();
}

const cache = new Map();
/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_1___default.a.extend({
  name: 'v-row',
  functional: true,
  props: {
    tag: {
      type: String,
      default: 'div'
    },
    dense: Boolean,
    noGutters: Boolean,
    align: {
      type: String,
      default: null,
      validator: alignValidator
    },
    ...alignProps,
    justify: {
      type: String,
      default: null,
      validator: justifyValidator
    },
    ...justifyProps,
    alignContent: {
      type: String,
      default: null,
      validator: alignContentValidator
    },
    ...alignContentProps
  },

  render(h, {
    props,
    data,
    children
  }) {
    // Super-fast memoization based on props, 5x faster than JSON.stringify
    let cacheKey = '';

    for (const prop in props) {
      cacheKey += String(props[prop]);
    }

    let classList = cache.get(cacheKey);

    if (!classList) {
      classList = []; // Loop through `align`, `justify`, `alignContent` breakpoint props

      let type;

      for (type in propMap) {
        propMap[type].forEach(prop => {
          const value = props[prop];
          const className = breakpointClass(type, prop, value);
          if (className) classList.push(className);
        });
      }

      classList.push({
        'no-gutters': props.noGutters,
        'row--dense': props.dense,
        [`align-${props.align}`]: props.align,
        [`justify-${props.justify}`]: props.justify,
        [`align-content-${props.alignContent}`]: props.alignContent
      });
      cache.set(cacheKey, classList);
    }

    return h(props.tag, Object(_util_mergeData__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(data, {
      staticClass: 'row',
      class: classList
    }), children);
  }

}));

/***/ }),

/***/ 343:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(131);
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _util_mergeData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);



 // no xs

const breakpoints = ['sm', 'md', 'lg', 'xl'];

const breakpointProps = (() => {
  return breakpoints.reduce((props, val) => {
    props[val] = {
      type: [Boolean, String, Number],
      default: false
    };
    return props;
  }, {});
})();

const offsetProps = (() => {
  return breakpoints.reduce((props, val) => {
    props['offset' + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = {
      type: [String, Number],
      default: null
    };
    return props;
  }, {});
})();

const orderProps = (() => {
  return breakpoints.reduce((props, val) => {
    props['order' + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = {
      type: [String, Number],
      default: null
    };
    return props;
  }, {});
})();

const propMap = {
  col: Object.keys(breakpointProps),
  offset: Object.keys(offsetProps),
  order: Object.keys(orderProps)
};

function breakpointClass(type, prop, val) {
  let className = type;

  if (val == null || val === false) {
    return undefined;
  }

  if (prop) {
    const breakpoint = prop.replace(type, '');
    className += `-${breakpoint}`;
  } // Handling the boolean style prop when accepting [Boolean, String, Number]
  // means Vue will not convert <v-col sm></v-col> to sm: true for us.
  // Since the default is false, an empty string indicates the prop's presence.


  if (type === 'col' && (val === '' || val === true)) {
    // .col-md
    return className.toLowerCase();
  } // .order-md-6


  className += `-${val}`;
  return className.toLowerCase();
}

const cache = new Map();
/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_1___default.a.extend({
  name: 'v-col',
  functional: true,
  props: {
    cols: {
      type: [Boolean, String, Number],
      default: false
    },
    ...breakpointProps,
    offset: {
      type: [String, Number],
      default: null
    },
    ...offsetProps,
    order: {
      type: [String, Number],
      default: null
    },
    ...orderProps,
    alignSelf: {
      type: String,
      default: null,
      validator: str => ['auto', 'start', 'end', 'center', 'baseline', 'stretch'].includes(str)
    },
    tag: {
      type: String,
      default: 'div'
    }
  },

  render(h, {
    props,
    data,
    children,
    parent
  }) {
    // Super-fast memoization based on props, 5x faster than JSON.stringify
    let cacheKey = '';

    for (const prop in props) {
      cacheKey += String(props[prop]);
    }

    let classList = cache.get(cacheKey);

    if (!classList) {
      classList = []; // Loop through `col`, `offset`, `order` breakpoint props

      let type;

      for (type in propMap) {
        propMap[type].forEach(prop => {
          const value = props[prop];
          const className = breakpointClass(type, prop, value);
          if (className) classList.push(className);
        });
      }

      const hasColClasses = classList.some(className => className.startsWith('col-'));
      classList.push({
        // Default to .col if no other col-{bp}-* classes generated nor `cols` specified.
        col: !hasColClasses || !props.cols,
        [`col-${props.cols}`]: props.cols,
        [`offset-${props.offset}`]: props.offset,
        [`order-${props.order}`]: props.order,
        [`align-self-${props.alignSelf}`]: props.alignSelf
      });
      cache.set(cacheKey, classList);
    }

    return h(props.tag, Object(_util_mergeData__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(data, {
      class: classList
    }), children);
  }

}));

/***/ }),

/***/ 352:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(353);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(10).default("5e62c9d0", content, true)

/***/ }),

/***/ 353:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".theme--light.v-radio--is-disabled label{color:rgba(0,0,0,.38)}.theme--light.v-radio--is-disabled .v-icon{color:rgba(0,0,0,.26)!important}.theme--dark.v-radio--is-disabled label{color:hsla(0,0%,100%,.5)}.theme--dark.v-radio--is-disabled .v-icon{color:hsla(0,0%,100%,.3)!important}.v-radio{align-items:center;display:flex;height:auto;outline:none}.v-radio--is-disabled{pointer-events:none}.v-input--radio-group.v-input--radio-group--row .v-radio{margin-right:16px}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 354:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(355);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(10).default("2e2bc7da", content, true)

/***/ }),

/***/ 355:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".theme--light.v-input--selection-controls.v-input--is-disabled:not(.v-input--indeterminate) .v-icon{color:rgba(0,0,0,.26)!important}.theme--dark.v-input--selection-controls.v-input--is-disabled:not(.v-input--indeterminate) .v-icon{color:hsla(0,0%,100%,.3)!important}.v-input--selection-controls{margin-top:16px;padding-top:4px}.v-input--selection-controls>.v-input__append-outer,.v-input--selection-controls>.v-input__prepend-outer{margin-top:0;margin-bottom:0}.v-input--selection-controls:not(.v-input--hide-details)>.v-input__slot{margin-bottom:12px}.v-input--selection-controls .v-input__slot>.v-label,.v-input--selection-controls .v-radio>.v-label{align-items:center;display:inline-flex;flex:1 1 auto;height:auto}.v-input--selection-controls__input{color:inherit;display:inline-flex;flex:0 0 auto;height:24px;position:relative;transition:.3s cubic-bezier(.25,.8,.5,1);transition-property:transform;width:24px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.v-input--selection-controls__input .v-icon{width:100%}.v-application--is-ltr .v-input--selection-controls__input{margin-right:8px}.v-application--is-rtl .v-input--selection-controls__input{margin-left:8px}.v-input--selection-controls__input input[role=checkbox],.v-input--selection-controls__input input[role=radio],.v-input--selection-controls__input input[role=switch]{position:absolute;opacity:0;width:100%;height:100%;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.v-input--selection-controls__input+.v-label{cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.v-input--selection-controls__ripple{border-radius:50%;cursor:pointer;height:34px;position:absolute;transition:inherit;width:34px;left:-12px;top:calc(50% - 24px);margin:7px}.v-input--selection-controls__ripple:before{border-radius:inherit;bottom:0;content:\"\";position:absolute;opacity:.2;left:0;right:0;top:0;transform-origin:center center;transform:scale(.2);transition:inherit}.v-input--selection-controls__ripple>.v-ripple__container{transform:scale(1.2)}.v-input--selection-controls.v-input--dense .v-input--selection-controls__ripple{width:28px;height:28px;left:-9px}.v-input--selection-controls.v-input--dense:not(.v-input--switch) .v-input--selection-controls__ripple{top:calc(50% - 21px)}.v-input--selection-controls.v-input{flex:0 1 auto}.v-input--selection-controls.v-input--is-focused .v-input--selection-controls__ripple:before,.v-input--selection-controls .v-radio--is-focused .v-input--selection-controls__ripple:before{background:currentColor;transform:scale(1.2)}.v-input--selection-controls .v-input--selection-controls__input:hover .v-input--selection-controls__ripple:before{background:currentColor;transform:scale(1.2);transition:none}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 356:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(357);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(10).default("999cb8a8", content, true)

/***/ }),

/***/ 357:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-input--radio-group__input{border:none;display:flex;width:100%}.v-input--radio-group--column .v-input--radio-group__input>.v-label{padding-bottom:8px}.v-input--radio-group--row .v-input--radio-group__input>.v-label{padding-right:8px}.v-input--radio-group--row legend{-ms-grid-row-align:center;align-self:center;display:inline-block}.v-input--radio-group--row .v-input--radio-group__input{flex-direction:row;flex-wrap:wrap}.v-input--radio-group--column .v-radio:not(:last-child):not(:only-child){margin-bottom:8px}.v-input--radio-group--column .v-input--radio-group__input{flex-direction:column}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_styles_components_selection_controls_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(354);
/* harmony import */ var _src_styles_components_selection_controls_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_styles_components_selection_controls_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _src_components_VRadioGroup_VRadioGroup_sass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(356);
/* harmony import */ var _src_components_VRadioGroup_VRadioGroup_sass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_src_components_VRadioGroup_VRadioGroup_sass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _VInput__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(320);
/* harmony import */ var _VItemGroup_VItemGroup__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(19);
/* harmony import */ var _mixins_comparable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(326);
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4);
// Styles

 // Extensions


 // Mixins

 // Types


const baseMixins = Object(_util_mixins__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"])(_mixins_comparable__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], _VItemGroup_VItemGroup__WEBPACK_IMPORTED_MODULE_3__[/* BaseItemGroup */ "a"], _VInput__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"]);
/* @vue/component */

/* harmony default export */ __webpack_exports__["a"] = (baseMixins.extend({
  name: 'v-radio-group',

  provide() {
    return {
      radioGroup: this
    };
  },

  props: {
    column: {
      type: Boolean,
      default: true
    },
    height: {
      type: [Number, String],
      default: 'auto'
    },
    name: String,
    row: Boolean,
    // If no value set on VRadio
    // will match valueComparator
    // force default to null
    value: null
  },
  computed: {
    classes() {
      return { ..._VInput__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"].options.computed.classes.call(this),
        'v-input--selection-controls v-input--radio-group': true,
        'v-input--radio-group--column': this.column && !this.row,
        'v-input--radio-group--row': this.row
      };
    }

  },
  methods: {
    genDefaultSlot() {
      return this.$createElement('div', {
        staticClass: 'v-input--radio-group__input',
        attrs: {
          id: this.id,
          role: 'radiogroup',
          'aria-labelledby': this.computedId
        }
      }, _VInput__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"].options.methods.genDefaultSlot.call(this));
    },

    genInputSlot() {
      const render = _VInput__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"].options.methods.genInputSlot.call(this);
      delete render.data.on.click;
      return render;
    },

    genLabel() {
      const label = _VInput__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"].options.methods.genLabel.call(this);
      if (!label) return null;
      label.data.attrs.id = this.computedId; // WAI considers this an orphaned label

      delete label.data.attrs.for;
      label.tag = 'legend';
      return label;
    },

    onClick: _VItemGroup_VItemGroup__WEBPACK_IMPORTED_MODULE_3__[/* BaseItemGroup */ "a"].options.methods.onClick
  }
}));

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VRadioGroup/VRadio.sass
var VRadio = __webpack_require__(352);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VLabel/index.js + 1 modules
var VLabel = __webpack_require__(321);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/index.js
var VIcon = __webpack_require__(23);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VInput/index.js + 3 modules
var VInput = __webpack_require__(320);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/binds-attrs/index.js
var binds_attrs = __webpack_require__(36);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/colorable/index.js
var colorable = __webpack_require__(12);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/groupable/index.js
var groupable = __webpack_require__(25);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/directives/ripple/index.js
var ripple = __webpack_require__(35);

// EXTERNAL MODULE: external "vue"
var external_vue_ = __webpack_require__(2);
var external_vue_default = /*#__PURE__*/__webpack_require__.n(external_vue_);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/mixins/rippleable/index.js
// Directives
 // Types


/* harmony default export */ var rippleable = (external_vue_default.a.extend({
  name: 'rippleable',
  directives: {
    ripple: ripple["a" /* default */]
  },
  props: {
    ripple: {
      type: [Boolean, Object],
      default: true
    }
  },
  methods: {
    genRipple(data = {}) {
      if (!this.ripple) return null;
      data.staticClass = 'v-input--selection-controls__ripple';
      data.directives = data.directives || [];
      data.directives.push({
        name: 'ripple',
        value: {
          center: true
        }
      });
      data.on = Object.assign({
        click: this.onChange
      }, this.$listeners);
      return this.$createElement('div', data);
    },

    onChange() {}

  }
}));
// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/themeable/index.js
var themeable = __webpack_require__(11);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/comparable/index.js
var comparable = __webpack_require__(326);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/mixins.js
var mixins = __webpack_require__(4);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/mixins/selectable/index.js
// Components
 // Mixins


 // Utilities


/* @vue/component */

/* harmony default export */ var selectable = (Object(mixins["a" /* default */])(VInput["a" /* default */], rippleable, comparable["a" /* default */]).extend({
  name: 'selectable',
  model: {
    prop: 'inputValue',
    event: 'change'
  },
  props: {
    id: String,
    inputValue: null,
    falseValue: null,
    trueValue: null,
    multiple: {
      type: Boolean,
      default: null
    },
    label: String
  },

  data() {
    return {
      hasColor: this.inputValue,
      lazyValue: this.inputValue
    };
  },

  computed: {
    computedColor() {
      if (!this.isActive) return undefined;
      if (this.color) return this.color;
      if (this.isDark && !this.appIsDark) return 'white';
      return 'primary';
    },

    isMultiple() {
      return this.multiple === true || this.multiple === null && Array.isArray(this.internalValue);
    },

    isActive() {
      const value = this.value;
      const input = this.internalValue;

      if (this.isMultiple) {
        if (!Array.isArray(input)) return false;
        return input.some(item => this.valueComparator(item, value));
      }

      if (this.trueValue === undefined || this.falseValue === undefined) {
        return value ? this.valueComparator(value, input) : Boolean(input);
      }

      return this.valueComparator(input, this.trueValue);
    },

    isDirty() {
      return this.isActive;
    },

    rippleState() {
      return !this.isDisabled && !this.validationState ? undefined : this.validationState;
    }

  },
  watch: {
    inputValue(val) {
      this.lazyValue = val;
      this.hasColor = val;
    }

  },
  methods: {
    genLabel() {
      const label = VInput["a" /* default */].options.methods.genLabel.call(this);
      if (!label) return label;
      label.data.on = {
        click: e => {
          // Prevent label from
          // causing the input
          // to focus
          e.preventDefault();
          this.onChange();
        }
      };
      return label;
    },

    genInput(type, attrs) {
      return this.$createElement('input', {
        attrs: Object.assign({
          'aria-checked': this.isActive.toString(),
          disabled: this.isDisabled,
          id: this.computedId,
          role: type,
          type
        }, attrs),
        domProps: {
          value: this.value,
          checked: this.isActive
        },
        on: {
          blur: this.onBlur,
          change: this.onChange,
          focus: this.onFocus,
          keydown: this.onKeydown
        },
        ref: 'input'
      });
    },

    onBlur() {
      this.isFocused = false;
    },

    onChange() {
      if (!this.isInteractive) return;
      const value = this.value;
      let input = this.internalValue;

      if (this.isMultiple) {
        if (!Array.isArray(input)) {
          input = [];
        }

        const length = input.length;
        input = input.filter(item => !this.valueComparator(item, value));

        if (input.length === length) {
          input.push(value);
        }
      } else if (this.trueValue !== undefined && this.falseValue !== undefined) {
        input = this.valueComparator(input, this.trueValue) ? this.falseValue : this.trueValue;
      } else if (value) {
        input = this.valueComparator(input, value) ? null : value;
      } else {
        input = !input;
      }

      this.validate(true, input);
      this.internalValue = input;
      this.hasColor = input;
    },

    onFocus() {
      this.isFocused = true;
    },

    /** @abstract */
    onKeydown(e) {}

  }
}));
// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/helpers.js
var helpers = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VRadioGroup/VRadio.js
// Styles



 // Mixins






 // Utilities



const baseMixins = Object(mixins["a" /* default */])(binds_attrs["a" /* default */], colorable["a" /* default */], rippleable, Object(groupable["a" /* factory */])('radioGroup'), themeable["a" /* default */]);
/* @vue/component */

/* harmony default export */ var VRadioGroup_VRadio = __webpack_exports__["a"] = (baseMixins.extend().extend({
  name: 'v-radio',
  inheritAttrs: false,
  props: {
    disabled: Boolean,
    id: String,
    label: String,
    name: String,
    offIcon: {
      type: String,
      default: '$radioOff'
    },
    onIcon: {
      type: String,
      default: '$radioOn'
    },
    readonly: Boolean,
    value: {
      default: null
    }
  },
  data: () => ({
    isFocused: false
  }),
  computed: {
    classes() {
      return {
        'v-radio--is-disabled': this.isDisabled,
        'v-radio--is-focused': this.isFocused,
        ...this.themeClasses,
        ...this.groupClasses
      };
    },

    computedColor() {
      return selectable.options.computed.computedColor.call(this);
    },

    computedIcon() {
      return this.isActive ? this.onIcon : this.offIcon;
    },

    computedId() {
      return VInput["a" /* default */].options.computed.computedId.call(this);
    },

    hasLabel: VInput["a" /* default */].options.computed.hasLabel,

    hasState() {
      return (this.radioGroup || {}).hasState;
    },

    isDisabled() {
      return this.disabled || !!this.radioGroup && this.radioGroup.isDisabled;
    },

    isReadonly() {
      return this.readonly || !!this.radioGroup && this.radioGroup.isReadonly;
    },

    computedName() {
      if (this.name || !this.radioGroup) {
        return this.name;
      }

      return this.radioGroup.name || `radio-${this.radioGroup._uid}`;
    },

    rippleState() {
      return selectable.options.computed.rippleState.call(this);
    },

    validationState() {
      return (this.radioGroup || {}).validationState || this.computedColor;
    }

  },
  methods: {
    genInput(args) {
      // We can't actually use the mixin directly because
      // it's made for standalone components, but its
      // genInput method is exactly what we need
      return selectable.options.methods.genInput.call(this, 'radio', args);
    },

    genLabel() {
      if (!this.hasLabel) return null;
      return this.$createElement(VLabel["a" /* default */], {
        on: {
          click: e => {
            // Prevent label from
            // causing the input
            // to focus
            e.preventDefault();
            this.onChange();
          }
        },
        attrs: {
          for: this.computedId
        },
        props: {
          color: this.validationState,
          focused: this.hasState
        }
      }, Object(helpers["s" /* getSlot */])(this, 'label') || this.label);
    },

    genRadio() {
      return this.$createElement('div', {
        staticClass: 'v-input--selection-controls__input'
      }, [this.$createElement(VIcon["a" /* default */], this.setTextColor(this.validationState, {
        props: {
          dense: this.radioGroup && this.radioGroup.dense
        }
      }), this.computedIcon), this.genInput({
        name: this.computedName,
        value: this.value,
        ...this.attrs$
      }), this.genRipple(this.setTextColor(this.rippleState))]);
    },

    onFocus(e) {
      this.isFocused = true;
      this.$emit('focus', e);
    },

    onBlur(e) {
      this.isFocused = false;
      this.$emit('blur', e);
    },

    onChange() {
      if (this.isDisabled || this.isReadonly || this.isActive) return;
      this.toggle();
    },

    onKeydown: () => {}
  },

  render(h) {
    const data = {
      staticClass: 'v-radio',
      class: this.classes
    };
    return h('div', data, [this.genRadio(), this.genLabel()]);
  }

}));

/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(tslib__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_property_decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8);
/* harmony import */ var vue_property_decorator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_property_decorator__WEBPACK_IMPORTED_MODULE_1__);
// Общие методы, используемые в компонентах прохождения опроса
// Возможно, на будущее - разнести методы по отдельным миксинам типа WithUserValue, WithMultiple ...

/* eslint no-use-before-define: 0 */


let MixinHops = class MixinHops extends vue_property_decorator__WEBPACK_IMPORTED_MODULE_1__["Vue"] {
  constructor() {
    super(...arguments);
    this.userValue = '';
    this.choised = [];
  }

  get hopId() {
    return this.hop._id;
  } // Флаг обязательности заполнения хопа


  get required() {
    return this.minChoiseCount > 0;
  } // Возвращает обтримленный пользовательский ввод


  get trimUserValue() {
    return this.userValue !== undefined ? this.userValue.trim() : '';
  } // Признак заполненности пользовательского ввода


  get userValueFilled() {
    return this.trimUserValue.length > 0;
  } // Возвращает опцию с пользовательским вводом


  get userOption() {
    return this.hop.options.find(option => option.type === 'user');
  }

  get clearUserValue() {
    return this.isChoisedUserOption ? this.trimUserValue : '';
  }

  get isChoisedUserOption() {
    return this.userOption ? this.isChoised(this.userOption) : false;
  }

  isChoised(option) {
    return this.choised.includes(option.id);
  } // Вызывает событие "changed"


  fireChanged() {
    const data = {
      id: this.hopId,
      payload: {
        choised: this.clearChoised,
        userValue: this.clearUserValue
      }
    };
    this.$emit('changed', data);
  } // Проверяет опцию на наличие пользовательского ввода


  isUserValue(option) {
    return option.type === 'user';
  }

};

Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(vue_property_decorator__WEBPACK_IMPORTED_MODULE_1__["Prop"])()], MixinHops.prototype, "hop", void 0);

MixinHops = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(vue_property_decorator__WEBPACK_IMPORTED_MODULE_1__["Component"])({})], MixinHops);
/* harmony default export */ __webpack_exports__["a"] = (MixinHops);

/***/ }),

/***/ 446:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(485);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("4fb62398", content, true, context)
};

/***/ }),

/***/ 447:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(487);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("47a2fb48", content, true, context)
};

/***/ }),

/***/ 484:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_radio2_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(446);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_radio2_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_radio2_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_radio2_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_radio2_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_radio2_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 485:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "textarea{margin-bottom:10px}#silentbox-gallery{display:flex}#silentbox-gallery .silentbox-item{text-decoration:none;display:flex}#silentbox-gallery .silentbox-item img{width:260px;height:260px;-o-object-fit:contain;object-fit:contain;flex:none;margin-right:8px;border:2px solid #00abba;border-radius:20px;background-color:#fff}.v-radio>.v-label{font-size:16px;font-weight:400px;color:#000}.theme--light.v-label{color:#000!important}.v-input--selection-controls .v-input__control{width:100%}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 486:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index2_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(447);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index2_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index2_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index2_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index2_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index2_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 487:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".nav-footer{position:fixed;bottom:0;width:100%;height:60px;border-top:1px solid #ddd;padding:0;background:#fff}.nav-footer .btn-complete,.nav-footer .btn-next,.nav-footer .btn-prev{height:36px!important;text-transform:none;font-size:.9rem}.nav-footer .v-slide-group__wrapper{align-items:center;padding:0 3px}.nav-footer .progress-box-caret{left:50%;position:absolute;top:-15px}.nav-footer .progress-box-caret:before{content:\"\";margin-left:-15px;top:100%;left:0;height:0;width:0;border:13px solid transparent;border-top-color:#ddd;position:absolute}.nav-footer__actions{display:flex;align-items:center;max-width:1000px;width:100%;margin:0 auto;height:100%}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 488:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(489);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(10).default("63c9496b", content, true)

/***/ }),

/***/ 489:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".theme--light.v-snack__wrapper{color:rgba(0,0,0,.87)}.theme--dark.v-snack__wrapper{color:#fff}.v-sheet.v-snack__wrapper{border-radius:4px}.v-sheet.v-snack__wrapper:not(.v-sheet--outlined){box-shadow:0 3px 5px -1px rgba(0,0,0,.2),0 6px 10px 0 rgba(0,0,0,.14),0 1px 18px 0 rgba(0,0,0,.12)}.v-sheet.v-snack__wrapper.v-sheet--shaped{border-radius:24px 4px}.v-snack{bottom:0;display:flex;font-size:.875rem;justify-content:center;left:0;pointer-events:none;right:0;top:0;width:100%}.v-snack:not(.v-snack--absolute){height:100vh;position:fixed;z-index:1000}.v-snack:not(.v-snack--centered):not(.v-snack--top){align-items:flex-end}.v-snack__wrapper{align-items:center;border-color:currentColor!important;display:flex;margin:8px;max-width:672px;min-height:48px;min-width:344px;padding:0;pointer-events:auto;position:relative;transition-duration:.15s;transition-property:opacity,transform;transition-timing-function:cubic-bezier(0,0,.2,1)}.v-snack__wrapper.theme--dark{background-color:#333;color:hsla(0,0%,100%,.87)}.v-snack__content{flex-grow:1;font-size:.875rem;font-weight:400;letter-spacing:.0178571429em;line-height:1.25rem;margin-right:auto;padding:14px 16px;text-align:left;text-align:initial}.v-snack__action{align-items:center;align-self:center;display:flex}.v-snack__action .v-ripple__container{display:none}.v-application--is-ltr .v-snack__action{margin-right:8px}.v-application--is-rtl .v-snack__action{margin-left:8px}.v-snack__action>.v-snack__btn.v-btn{padding:0 8px}.v-snack__btn{margin:0;min-width:auto}.v-snack--absolute{height:100%;position:absolute;z-index:1}.v-snack--centered{align-items:center}.v-snack--left{justify-content:flex-start}.v-snack--multi-line .v-snack__wrapper{min-height:68px}.v-snack--right{justify-content:flex-end}.v-snack:not(.v-snack--has-background) .v-snack__wrapper{background:transparent!important;box-shadow:none!important}.v-snack--text .v-snack__wrapper:before{background-color:currentColor;border-radius:inherit;bottom:0;content:\"\";left:0;opacity:.12;pointer-events:none;position:absolute;right:0;top:0}.v-snack--top{align-items:flex-start}.v-snack--vertical .v-snack__wrapper{flex-direction:column}.v-snack--vertical .v-snack__wrapper .v-snack__action{align-self:flex-end;margin-bottom:8px}.v-snack-transition-enter.v-snack__wrapper{transform:scale(.8)}.v-snack-transition-enter.v-snack__wrapper,.v-snack-transition-leave-to.v-snack__wrapper{opacity:0}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 513:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/tests/_id/exec/index.vue?vue&type=template&id=075e5a47&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-container',[(_vm.type === "SAT" || _vm.type === "olympics")?[_c('PollClassic',{attrs:{"previousResult":_vm.result}})]:[_c('v-row',{staticClass:"d-flex"},[_c('v-col',{staticClass:"d-flex justify-center"},[_c('p',{staticClass:"display-1"},[_vm._v("Erorr")])])],1)]],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/tests/_id/exec/index.vue?vue&type=template&id=075e5a47&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// EXTERNAL MODULE: ./store/index.ts + 1 modules
var store_0 = __webpack_require__(5);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/pollClassic/index2.vue?vue&type=template&id=5b744dec&lang=pug&
var index2vue_type_template_id_5b744dec_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('client-only',[_c('div',{staticClass:"d-flex justify-center mt-10"},[_c('div',{staticClass:"d-flex flex-column",staticStyle:{"width":"650px"}},[_c('div',{staticClass:"d-flex"},[_c('div',{staticClass:"d-flex align-center"},[_c('v-icon',{staticClass:"mr-2",attrs:{"color":"black"}},[_vm._v("mdi-calculator")]),_c('span',{staticClass:"display-1 font-weight-light"},[_vm._v("QUESTION "+_vm._s(_vm.step + 1)+" OF "+_vm._s(_vm.poll.sorted.length))])],1),_c('v-spacer'),_c('div',{staticClass:"d-flex align-center"},[_c('v-icon',{staticClass:"mr-2",attrs:{"small":""}},[_vm._v("mdi-cloud-question")]),_c('span',{staticClass:"subtitle-2"},[_vm._v("Report")])],1),_c('div',{staticClass:"d-flex align-center ml-2"},[(!_vm.isBookmark)?_c('v-tooltip',{attrs:{"top":""},scopedSlots:_vm._u([{key:"activator",fn:function(ref){
var on = ref.on;
var attrs = ref.attrs;
return [_c('v-icon',_vm._g(_vm._b({staticStyle:{"font-size":"32px"},attrs:{"ripple":false},on:{"click":function($event){return _vm.addToBookmark()}}},'v-icon',attrs,false),on),[_vm._v("mdi-bookmark-outline")])]}}],null,false,2741185912)},[_c('span',[_vm._v("Add this Question to bookmark")])]):_c('v-tooltip',{attrs:{"top":""},scopedSlots:_vm._u([{key:"activator",fn:function(ref){
var on = ref.on;
var attrs = ref.attrs;
return [_c('v-icon',_vm._g(_vm._b({staticStyle:{"font-size":"32px"},attrs:{"ripple":false},on:{"click":function($event){return _vm.deleteFromBookmark()}}},'v-icon',attrs,false),on),[_vm._v("mdi-bookmark")])]}}])},[_c('span',[_vm._v("Remove this Question from bookmarks")])])],1)],1),_c('v-divider',{staticClass:"mt-2"}),_c('client-only',[_c('radioHop',{key:_vm.step,attrs:{"hop":_vm.currentHop,"step":_vm.step,"readonly":false,"choisedPrevious":_vm.getPreviousValue(_vm.currentHop)},on:{"changed":_vm.registerChoise}})],1)],1),_c('div',{staticClass:"nav-footer"},[_c('div',{staticClass:"nav-footer__actions"},[_c('div',{staticClass:"fill-height",staticStyle:{"max-width":"300px","align-self":"center"}},[_c('v-slide-group',{staticClass:"fill-height",attrs:{"center-active":"","mandatory":"","show-arrows":""},model:{value:(_vm.step),callback:function ($$v) {_vm.step=$$v},expression:"step"}},_vm._l((_vm.poll.sorted),function(hop,index){return _c('v-slide-item',{key:index,scopedSlots:_vm._u([{key:"default",fn:function(ref){
var active = ref.active;
var toggle = ref.toggle;
return [_c('div',{staticStyle:{"position":"relative"}},[(active)?_c('span',{staticClass:"progress-box-caret"}):_vm._e(),_c('v-card',{staticClass:"ma-0 my-1 mr-1 text-center",attrs:{"color":active || _vm.getPreviousValue(hop) ? 'primary' : 'blue-grey lighten-5',"width":"22","height":"22"},on:{"click":toggle}},[_c('span',{class:active || _vm.getPreviousValue(hop) ? "white--text" : "white-text"},[_vm._v(_vm._s(index + 1))])])],1)]}}],null,true)})}),1)],1),(_vm.bookmarks.length !== 0)?_c('v-menu',{attrs:{"top":"","offset-y":""},scopedSlots:_vm._u([{key:"activator",fn:function(ref){
var on = ref.on;
var attrs = ref.attrs;
return [_c('v-btn',_vm._g(_vm._b({staticStyle:{"color":"#0084ce"},attrs:{"text":""}},'v-btn',attrs,false),on),[_vm._v("Bookmarks")])]}}],null,false,2928482106)},[_c('v-list',_vm._l((_vm.bookmarks),function(bookmark,index){return (_vm.bookmarks.length !== 0)?_c('v-list-item',{key:index,attrs:{"dense":""},on:{"click":function($event){_vm.step = bookmark.step}}},[_c('v-list-item-title',[_vm._v(_vm._s('Qustion ' + (bookmark.step + 1)))])],1):_c('v-list-item',{key:index,attrs:{"dense":""}},[_c('v-list-item-title',[_vm._v("No bookmarked questions.")])],1)}),1)],1):_vm._e(),_c('v-spacer'),(_vm.currentHop !== _vm.poll.sorted[0])?_c('v-btn',{staticClass:"ma-0 mr-2 btn-prev",attrs:{"ripple":false,"text":""},on:{"click":function($event){return _vm.previousHop()}}},[_vm._v("Previous Question")]):_vm._e(),(_vm.currentHop !== _vm.poll.sorted[_vm.poll.sorted.length - 1])?_c('v-btn',{staticClass:"ma-0 btn-next",attrs:{"color":"#0084ce"},on:{"click":function($event){return _vm.nextHop()}}},[_vm._v(_vm._s(_vm.choised ? 'Next Question' : 'Skip Question'))]):_vm._e(),(_vm.fullAnswers)?_c('v-btn',{staticClass:"ma-0 btn-complete",attrs:{"color":"#0084ce"},on:{"click":function($event){return _vm.apply('complete')}}},[_vm._v("Complete the test")]):_vm._e()],1)]),_c('v-snackbar',{attrs:{"shaped":"","right":"","timeout":2300},model:{value:(_vm.snackbar),callback:function ($$v) {_vm.snackbar=$$v},expression:"snackbar"}},[_vm._v(_vm._s(_vm.snackBarText))])],1)])}
var index2vue_type_template_id_5b744dec_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./components/pollClassic/index2.vue?vue&type=template&id=5b744dec&lang=pug&

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/hops/Classic/radio2.vue?vue&type=template&id=54758fc6&lang=pug&
var radio2vue_type_template_id_54758fc6_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticStyle:{"width":"100%"}},[_c('v-card',{attrs:{"elevation":"0"}},[_c('v-card-title',{staticClass:"d-flex justify-center"},[_c('div',{staticStyle:{"background-color":"white"}},[_c('div',{directives:[{name:"katex",rawName:"v-katex:auto",value:({ delimiters: [ {left: "$$", right: "$$", display: true}, {left: "\[", right: "\]", display: true}, {left: "$", right: "$", display: false}, {left: "\(", right: "\)", display: false} ], trust: true}),expression:"{ delimiters: [ {left: \"$$\", right: \"$$\", display: true}, {left: \"\\[\", right: \"\\]\", display: true}, {left: \"$\", right: \"$\", display: false}, {left: \"\\(\", right: \"\\)\", display: false} ], trust: true}",arg:"auto"}],staticStyle:{"word-break":"break-word","text-align":"center"},domProps:{"textContent":_vm._s(_vm.hop.content)}}),(_vm.hop.image.length !== 0 )?_c('v-col',{staticClass:"d-flex align-center justify-center",attrs:{"cols":"12"}},[_c('silent-box',{attrs:{"gallery":_vm.galleryImage,"thumbnailHeight":"126","thumbnailWidth":"126","inner":true}})],1):_vm._e()],1)]),_c('v-card-text',{staticClass:"pb-0",staticStyle:{"background-color":"white"}},[_c('v-row',[_c('v-col',{staticClass:"pl-0",attrs:{"cols":"12"}},[_c('v-radio-group',{staticStyle:{"width":"100%"},attrs:{"column":"","rules":_vm.validaitonRadio,"readonly":_vm.readonly},on:{"change":_vm.fireChanged},model:{value:(_vm.choisedRadio),callback:function ($$v) {_vm.choisedRadio=$$v},expression:"choisedRadio"}},[_vm._l((_vm.options),function(option,index){return [_c('v-radio',{key:option.id,staticClass:"custom-radio ma-4",staticStyle:{"width":"100%","word-break":"break-word"},attrs:{"value":option.id,"label":option.value,"ripple":false,"off-icon":_vm.icons[index][0],"hide-details":"","on-icon":_vm.icons[index][1]},scopedSlots:_vm._u([{key:"label",fn:function(){return [_c('div',{directives:[{name:"katex",rawName:"v-katex:auto",value:({ delimiters: [ {left: "$$", right: "$$", display: true}, {left: "\[", right: "\]", display: true}, {left: "$", right: "$", display: false}, {left: "\(", right: "\)", display: false} ], trust: true}),expression:"{ delimiters: [ {left: \"$$\", right: \"$$\", display: true}, {left: \"\\[\", right: \"\\]\", display: true}, {left: \"$\", right: \"$\", display: false}, {left: \"\\(\", right: \"\\)\", display: false} ], trust: true}",arg:"auto"}],staticClass:"ml-4",domProps:{"textContent":_vm._s(option.value)}}),(option.image)?_c('silent-box',{staticClass:"ml-9 mt-1",attrs:{"image":{ src: option.image },"thumbnailHeight":"126","thumbnailWidth":"126","inner":true}}):_vm._e()]},proxy:true}],null,true)}),_c('v-divider')]})],2)],1)],1)],1),_c('v-card-actions',{staticClass:"mb-12 pt-0",staticStyle:{"background-color":"white"}},[_c('div',{staticClass:"d-flex align-start",staticStyle:{"width":"100%"}},[_c('div',{staticClass:"mr-1"},[_c('span',[_vm._v(_vm._s(_vm.percentageOfCorrectlyStudents + "% of students solved this problem correctly"))])]),_c('div',{staticClass:"mr-1"},[_c('span',[_vm._v(_vm._s("Average time spent: " + _vm.averageTimeSpent))])]),_c('div',[_c('span',[_vm._v(_vm._s("Correct solutions, on average, took: " + _vm.averageTimeCorrectAnswer))])])])])],1)],1)}
var radio2vue_type_template_id_54758fc6_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./components/hops/Classic/radio2.vue?vue&type=template&id=54758fc6&lang=pug&

// EXTERNAL MODULE: ./components/hops/__mixinCommonMethods.ts
var _mixinCommonMethods = __webpack_require__(409);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/hops/Classic/radio2.vue?vue&type=script&lang=ts&




let radio2vue_type_script_lang_ts_RadioTS = class RadioTS extends Object(external_vue_property_decorator_["Mixins"])(_mixinCommonMethods["a" /* default */]) {
  constructor() {
    super(...arguments);
    this.choisedRadio = '';
    this.icons = [['mdi-36px mdi-alpha-a-circle-outline', 'mdi-36px mdi-alpha-a-circle'], ['mdi-36px mdi-alpha-b-circle-outline', 'mdi-36px mdi-alpha-b-circle'], ['mdi-36px mdi-alpha-c-circle-outline', 'mdi-36px mdi-alpha-c-circle'], ['mdi-36px mdi-alpha-d-circle-outline', 'mdi-36px mdi-alpha-d-circle'], ['mdi-36px mdi-alpha-e-circle-outline', 'mdi-36px mdi-alpha-e-circle']];
  }

  get options() {
    return this.hop.options;
  }

  get clearUserValue() {
    return this.isChoisedUserOption ? this.trimUserValue : '';
  }

  get clearChoised() {
    return !this.choisedRadio ? [] : [this.choisedRadio];
  }

  get validaitonRadio() {
    return [() => !this.required || !!this.choisedRadio || 'Пожалуйста, выберите один из вариантов'];
  }

  get isChoisedUserOption() {
    return this.userOption ? this.isChoised(this.userOption) : false;
  } // Флаг заполненности пользовательского поля в случае его выбора


  get userValueApprove() {
    return this.isChoisedUserOption ? this.userValueFilled : true;
  }

  get galleryImage() {
    let images = this.hop.image.map(img => {
      return {
        src: img,
        description: this.hop.content
      };
    });
    return images;
  }

  get hopFilled() {
    if (this.required) {
      this.$emit('filled', !!this.choisedRadio && this.userValueApprove);
      return !!this.choisedRadio && this.userValueApprove;
    } else {
      this.$emit('filled', this.userValueApprove);
      return this.userValueApprove;
    }
  }

  get percentageOfCorrectlyStudents() {
    if (store_0["auth"].user.commonStatistics !== undefined) {
      const resultHops = store_0["auth"].user.commonStatistics.filter(item => {
        return item.hop_id === this.hop._id;
      });
      const answersHop = resultHops.filter(item => item.isCorrect);
      if (resultHops.length === 0) return 0;
      return Math.floor(answersHop.length / resultHops.length * 100);
    }
  }

  get averageTimeSpent() {
    if (store_0["auth"].user.commonStatistics !== undefined) {
      const resultHops = store_0["auth"].user.commonStatistics.filter(item => {
        return item.hop_id === this.hop._id;
      });
      const answersHop = resultHops.reduce((acc, item) => {
        return item.time + acc;
      }, 0);
      if (resultHops.length === 0) return this.$moment.utc(0).format('mm [min] ss [sec]');
      return this.$moment.utc(Math.floor(answersHop / resultHops.length) * 1000).format('mm [min] ss [sec]');
    }
  }

  get averageTimeCorrectAnswer() {
    if (store_0["auth"].user.commonStatistics !== undefined) {
      const resultHops = store_0["auth"].user.commonStatistics.filter(item => {
        return item.hop_id === this.hop._id;
      });
      const answersHop = resultHops.reduce((acc, item) => {
        return item.isCorrect ? item.time + acc : 0 + acc;
      }, 0);
      if (resultHops.length === 0) return this.$moment.utc(0).format('mm [min] ss [sec]');
      return this.$moment.utc(Math.floor(answersHop / resultHops.length) * 1000).format('mm [min] ss [sec]');
    }
  }

  mounted() {
    this.init();
  }

  init() {
    this.choisedRadio = this.choisedPrevious !== undefined ? this.choisedPrevious : '';
  }

  isChoised(option) {
    return Number(this.choisedRadio) === option.id;
  }

};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Number,
  required: true
})], radio2vue_type_script_lang_ts_RadioTS.prototype, "step", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({})], radio2vue_type_script_lang_ts_RadioTS.prototype, "choisedPrevious", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  required: true,
  type: Boolean
})], radio2vue_type_script_lang_ts_RadioTS.prototype, "readonly", void 0);

radio2vue_type_script_lang_ts_RadioTS = Object(external_tslib_["__decorate"])([external_vue_property_decorator_["Component"]], radio2vue_type_script_lang_ts_RadioTS);
/* harmony default export */ var radio2vue_type_script_lang_ts_ = (radio2vue_type_script_lang_ts_RadioTS);
// CONCATENATED MODULE: ./components/hops/Classic/radio2.vue?vue&type=script&lang=ts&
 /* harmony default export */ var Classic_radio2vue_type_script_lang_ts_ = (radio2vue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(298);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(296);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(343);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(301);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VRadioGroup/VRadio.js + 2 modules
var VRadio = __webpack_require__(403);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VRadioGroup/VRadioGroup.js
var VRadioGroup = __webpack_require__(402);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(341);

// CONCATENATED MODULE: ./components/hops/Classic/radio2.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(484)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  Classic_radio2vue_type_script_lang_ts_,
  radio2vue_type_template_id_54758fc6_lang_pug_render,
  radio2vue_type_template_id_54758fc6_lang_pug_staticRenderFns,
  false,
  injectStyles,
  null,
  "2efa8bf5"
  
)

/* harmony default export */ var radio2 = (component.exports);

/* vuetify-loader */










installComponents_default()(component, {VCard: VCard["a" /* default */],VCardActions: components_VCard["a" /* VCardActions */],VCardText: components_VCard["b" /* VCardText */],VCardTitle: components_VCard["c" /* VCardTitle */],VCol: VCol["a" /* default */],VDivider: VDivider["a" /* default */],VRadio: VRadio["a" /* default */],VRadioGroup: VRadioGroup["a" /* default */],VRow: VRow["a" /* default */]})

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/pollClassic/index2.vue?vue&type=script&lang=ts&




let index2vue_type_script_lang_ts_PollClassic = class PollClassic extends external_vue_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.currentPollsFromLS = {};
    this.bookmarks = [];
    this.step = 0;
    this.fullAnswers = false;
    this.result = {};
    this.choised = false;
    this.snackbar = false;
    this.startTime = 0;
    this.startPauseTime = 0;
    this.stopPauseTime = 0;
    this.snackBarText = '';
    this.showStatistics = false;
    this.timeToSolution = '00:00:00';
    this.timerInterval = '';
    this.isPause = false;
  }

  async changeStep(val, oldVal) {
    this.closeSnackbar();
    this.currentPollsFromLS[this.poll._id].step = this.step;
    await this.$localForage.polls.setItem('pollsWithResults', this.currentPollsFromLS);
    if (!this.showStatistics) this.timerHop(oldVal);
  }

  get poll() {
    return store_0["resultPollStore"].poll;
  }

  get currentHop() {
    return store_0["resultPollStore"].poll.sorted[this.step];
  }

  get isBookmark() {
    return !!this.bookmarks.find(item => item.step === this.step);
  }

  async beforeMount() {
    if ((await this.$localForage.polls.getItem('historyLS')) === null) {
      await this.$localForage.polls.setItem('historyLS', []);
    }

    const pollsWithResults = await this.$localForage.polls.getItem('pollsWithResults');

    if (pollsWithResults === null) {
      await this.$localForage.polls.setItem('pollsWithResults', {});
      this.addNewPollLS().finally(() => {
        this.init();
        this.timer();
      });
    } else {
      let results = pollsWithResults[this.poll._id];

      if (results !== undefined && results.complete === false) {
        this.currentPollsFromLS = pollsWithResults;
        this.init();
        this.timer();
      } else {
        this.addNewPollLS().finally(() => {
          this.init();
          this.timer();
        });
      }
    }
  }

  init() {
    store_0["resultPollStore"].poll.sorted.forEach(item => {
      this.result[item._id.toLowerCase()] = {
        choised: item.choised,
        userValue: '',
        time: 0,
        wasShown: false
      };
    });
    this.startTime = Date.now();

    if (this.currentPollsFromLS[this.poll._id].results !== undefined) {
      this.result = this.currentPollsFromLS[this.poll._id].results;
      this.step = this.currentPollsFromLS[this.poll._id].step;
      let allTime = 0;

      for (let key in this.currentPollsFromLS[this.poll._id].results) {
        allTime += this.currentPollsFromLS[this.poll._id].results[key].time;
      }

      this.setTimeToResolve(allTime);
    }
  }

  timer() {
    const parseTimeToSolution = this.timeToSolution.split(':');
    let hours = parseTimeToSolution[0];
    let minutes = parseTimeToSolution[1];
    let seconds = parseTimeToSolution[2]; // let timeLeft = document.getElementById('time-spent')

    this.timerInterval = setInterval(() => {
      if (!this.isPause) {
        if (seconds < 59) {
          seconds++;
        } else if (minutes < 59) {
          minutes++;
          seconds = 0;
        } else if (hours < 59) {
          hours++;
          seconds = 0;
          minutes = 0;
        }

        if (hours.toString().length === 1) hours = '0' + hours;
        if (minutes.toString().length === 1) minutes = '0' + minutes;
        if (seconds.toString().length === 1) seconds = '0' + seconds;
        this.timeToSolution = hours + ':' + minutes + ':' + seconds;
      }
    }, 1000);
    return;
  }

  async timerHop(oldStep) {
    this.snackbar = false; // this.getCompleteProgress()

    const hopId = this.poll.sorted[oldStep]._id;

    if (this.result[hopId] !== undefined) {
      let time = Date.now() - this.startTime + this.result[hopId].time - (this.stopPauseTime - this.startPauseTime);
      this.result[hopId].time = time > 0 ? time : 0;
      this.startTime = Date.now();
      this.stopPauseTime = 0;
      this.startPauseTime = 0;

      if (this.result[hopId].choised.length !== 0 && !this.result[hopId].wasShown) {
        let convertToNormalTime = this.$moment.duration(this.result[hopId].time, 'milliseconds');
        this.snackBarText = 'Question #' + (oldStep + 1).toString() + '' + ' was completed in ' + convertToNormalTime.minutes() + ' min ' + convertToNormalTime.seconds() + ' sec ';
        this.result[hopId].wasShown = true;
        this.snackbar = true;
      }
    }

    this.currentPollsFromLS[this.poll._id].results[hopId] = this.result[hopId];
    await this.$localForage.polls.setItem('pollsWithResults', this.currentPollsFromLS);
  }

  async registerChoise(e) {
    this.result[e.id.toLowerCase()].choised = e.payload.choised;
    this.result[e.id.toLowerCase()].userValue = e.payload.userValue;
    let countAnsweredHops = 0;

    for (const hopId in this.result) {
      this.result[hopId].choised.length !== 0 ? countAnsweredHops++ : '';
    }

    if (countAnsweredHops === this.poll.sorted.length) this.fullAnswers = true;else this.fullAnswers = false;
    this.choised = true;
    this.currentPollsFromLS[this.poll._id].results = this.result;
    await this.$localForage.polls.setItem('pollsWithResults', this.currentPollsFromLS);
  }

  setTimeToResolve(allTime) {
    let hours = Math.floor(allTime / 1000 / 60 / 60);
    let minutes = Math.floor(allTime / 1000 / 60) - hours * 60;
    let sec = Math.floor(allTime / 1000) % 60;
    if (hours.toString().length === 1) hours = '0' + hours;
    if (minutes.toString().length === 1) minutes = '0' + minutes;
    if (sec.toString().length === 1) sec = '0' + sec;
    this.timeToSolution = hours + ':' + minutes + ':' + sec;
  }

  async addNewPollLS() {
    let historyLS = await this.$localForage.polls.getItem('historyLS');
    let pollsLS = await this.$localForage.polls.getItem('pollsWithResults');
    historyLS.push({ ...this.poll,
      complete: false,
      history_id: historyLS.length + 1,
      date: Date.now(),
      step: 0
    });
    pollsLS[this.poll._id] = {
      results: {},
      complete: false,
      history_id: historyLS.length,
      date: Date.now(),
      step: 0,
      bookmarks: []
    };
    store_0["resultPollStore"].poll.sorted.forEach(item => {
      pollsLS[this.poll._id].results[item._id.toLowerCase()] = {
        choised: item.choised,
        userValue: '',
        time: 0,
        wasShown: false
      };
    });
    this.currentPollsFromLS = pollsLS;
    await this.$localForage.polls.setItem('historyLS', historyLS);
    await this.$localForage.polls.setItem('pollsWithResults', pollsLS);
  }

  getPreviousValue(hop) {
    if (this.result[hop._id]) {
      return this.result[hop._id].choised[0];
    } else {
      return [];
    }
  }

  async addToBookmark() {
    this.bookmarks.push({
      step: this.step
    });
    this.currentPollsFromLS[this.poll._id].bookmarks = this.bookmarks;
    await this.$localForage.polls.setItem('pollsWithResults', this.currentPollsFromLS);
  }

  async deleteFromBookmark() {
    let index = this.bookmarks.findIndex(item => item.step === this.step);

    if (index !== -1) {
      this.bookmarks.splice(index, 1);
    }

    this.currentPollsFromLS[this.poll._id].bookmarks = this.bookmarks;
    await this.$localForage.polls.setItem('pollsWithResults', this.currentPollsFromLS);
  }

  closeSnackbar() {
    this.snackbar = false;
  }

  nextHop() {
    this.step++;
    if (this.result[this.currentHop._id].choised.length !== 0) this.choised = true;else this.choised = false;
  }

  previousHop() {
    this.step--;
    if (this.result[this.currentHop._id].choised.length !== 0) this.choised = true;else this.choised = false;
  }

  async apply(event) {
    this.timerHop(this.step);
    const answers = [];
    let fullAnwswers = true;

    for (const hopId in this.result) {
      const answer = this.result[hopId];
      const hop = this.poll.sorted.find(item => item._id === hopId);
      const options = answer.choised.map(optionId => {
        optionId === undefined ? fullAnwswers = false : false;
        return {
          id: optionId
        };
      });
      answers.push({
        hop_id: hopId,
        options,
        time: this.result[hopId].time
      });
    }

    const dataResp = {
      poll_id: store_0["resultPollStore"].poll._id,
      answers
    };
    let historyLS = await this.$localForage.polls.getItem('historyLS');
    let pollFromHistory = historyLS.find(item => item.history_id === this.currentPollsFromLS[this.poll._id].history_id);
    console.warn(pollFromHistory, 'pollFromHistory');

    if (event === 'complete') {
      if (this.previousResult || !fullAnwswers) {
        store_0["resultsStore"].save({
          dataResp,
          resultId: this.previousResult._id
        }).then(res => {
          clearTimeout(this.timerInterval);
        });
      } else {
        store_0["resultsStore"].save(dataResp).then(async res => {
          clearTimeout(this.timerInterval);
          this.currentPollsFromLS[this.poll._id].complete = true;
          pollFromHistory.answers = dataResp.answers;
          pollFromHistory.complete = true;
          pollFromHistory.completed_at = this.$moment().format();
          await this.$localForage.polls.setItem('historyLS', historyLS);
          await this.$localForage.polls.setItem('pollsWithResults', this.currentPollsFromLS);
        });
      }
    }
  }

};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  required: true
})], index2vue_type_script_lang_ts_PollClassic.prototype, "previousResult", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Watch"])('step')], index2vue_type_script_lang_ts_PollClassic.prototype, "changeStep", null);

index2vue_type_script_lang_ts_PollClassic = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'pollClassic',
  components: {
    radioHop: radio2
  }
})], index2vue_type_script_lang_ts_PollClassic);
/* harmony default export */ var index2vue_type_script_lang_ts_ = (index2vue_type_script_lang_ts_PollClassic);
// CONCATENATED MODULE: ./components/pollClassic/index2.vue?vue&type=script&lang=ts&
 /* harmony default export */ var pollClassic_index2vue_type_script_lang_ts_ = (index2vue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/VBtn.js
var VBtn = __webpack_require__(81);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(50);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/VList.js
var VList = __webpack_require__(92);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/VListItem.js
var VListItem = __webpack_require__(51);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/index.js
var components_VList = __webpack_require__(13);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VMenu/VMenu.js
var VMenu = __webpack_require__(300);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSlideGroup/VSlideGroup.js
var VSlideGroup = __webpack_require__(52);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/groupable/index.js
var groupable = __webpack_require__(25);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/mixins.js
var mixins = __webpack_require__(4);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/console.js
var util_console = __webpack_require__(6);

// EXTERNAL MODULE: external "vue"
var external_vue_ = __webpack_require__(2);
var external_vue_default = /*#__PURE__*/__webpack_require__.n(external_vue_);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VItemGroup/VItem.js
// Mixins
 // Utilities


 // Types


/* @vue/component */

const BaseItem = external_vue_default.a.extend({
  props: {
    activeClass: String,
    value: {
      required: false
    }
  },
  data: () => ({
    isActive: false
  }),
  methods: {
    toggle() {
      this.isActive = !this.isActive;
    }

  },

  render() {
    if (!this.$scopedSlots.default) {
      Object(util_console["c" /* consoleWarn */])('v-item is missing a default scopedSlot', this);
      return null;
    }

    let element;
    /* istanbul ignore else */

    if (this.$scopedSlots.default) {
      element = this.$scopedSlots.default({
        active: this.isActive,
        toggle: this.toggle
      });
    }

    if (Array.isArray(element) && element.length === 1) {
      element = element[0];
    }

    if (!element || Array.isArray(element) || !element.tag) {
      Object(util_console["c" /* consoleWarn */])('v-item should only contain a single element', this);
      return element;
    }

    element.data = this._b(element.data || {}, element.tag, {
      class: {
        [this.activeClass]: this.isActive
      }
    });
    return element;
  }

});
/* harmony default export */ var VItem = (Object(mixins["a" /* default */])(BaseItem, Object(groupable["a" /* factory */])('itemGroup', 'v-item', 'v-item-group')).extend({
  name: 'v-item'
}));
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VSlideGroup/VSlideItem.js
// Extensions
 // Mixins



/* harmony default export */ var VSlideItem = (Object(mixins["a" /* default */])(BaseItem, Object(groupable["a" /* factory */])('slideGroup')
/* @vue/component */
).extend({
  name: 'v-slide-item'
}));
// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VSnackbar/VSnackbar.sass
var VSnackbar = __webpack_require__(488);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSheet/VSheet.js
var VSheet = __webpack_require__(24);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/colorable/index.js
var colorable = __webpack_require__(12);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/themeable/index.js
var themeable = __webpack_require__(11);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/toggleable/index.js
var toggleable = __webpack_require__(15);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/positionable/index.js
var positionable = __webpack_require__(22);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/helpers.js
var helpers = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VSnackbar/VSnackbar.js
// Styles
 // Components

 // Mixins




 // Utilities




/* harmony default export */ var VSnackbar_VSnackbar = (Object(mixins["a" /* default */])(VSheet["a" /* default */], colorable["a" /* default */], toggleable["a" /* default */], Object(positionable["b" /* factory */])(['absolute', 'bottom', 'left', 'right', 'top'])
/* @vue/component */
).extend({
  name: 'v-snackbar',
  props: {
    app: Boolean,
    centered: Boolean,
    contentClass: {
      type: String,
      default: ''
    },
    multiLine: Boolean,
    text: Boolean,
    timeout: {
      type: [Number, String],
      default: 5000
    },
    transition: {
      type: [Boolean, String],
      default: 'v-snack-transition',
      validator: v => typeof v === 'string' || v === false
    },
    vertical: Boolean
  },
  data: () => ({
    activeTimeout: -1
  }),
  computed: {
    classes() {
      return {
        'v-snack--absolute': this.absolute,
        'v-snack--active': this.isActive,
        'v-snack--bottom': this.bottom || !this.top,
        'v-snack--centered': this.centered,
        'v-snack--has-background': this.hasBackground,
        'v-snack--left': this.left,
        'v-snack--multi-line': this.multiLine && !this.vertical,
        'v-snack--right': this.right,
        'v-snack--text': this.text,
        'v-snack--top': this.top,
        'v-snack--vertical': this.vertical
      };
    },

    // Text and outlined styles both
    // use transparent backgrounds
    hasBackground() {
      return !this.text && !this.outlined;
    },

    // Snackbar is dark by default
    // override themeable logic.
    isDark() {
      return this.hasBackground ? !this.light : themeable["a" /* default */].options.computed.isDark.call(this);
    },

    styles() {
      // Styles are not needed when
      // using the absolute prop.
      if (this.absolute) return {};
      const {
        bar,
        bottom,
        footer,
        insetFooter,
        left,
        right,
        top
      } = this.$vuetify.application; // Should always move for y-axis
      // applicationable components.

      return {
        paddingBottom: Object(helpers["g" /* convertToUnit */])(bottom + footer + insetFooter),
        paddingLeft: !this.app ? undefined : Object(helpers["g" /* convertToUnit */])(left),
        paddingRight: !this.app ? undefined : Object(helpers["g" /* convertToUnit */])(right),
        paddingTop: Object(helpers["g" /* convertToUnit */])(bar + top)
      };
    }

  },
  watch: {
    isActive: 'setTimeout',
    timeout: 'setTimeout'
  },

  mounted() {
    if (this.isActive) this.setTimeout();
  },

  created() {
    /* istanbul ignore next */
    if (this.$attrs.hasOwnProperty('auto-height')) {
      Object(util_console["e" /* removed */])('auto-height', this);
    }
    /* istanbul ignore next */
    // eslint-disable-next-line eqeqeq


    if (this.timeout == 0) {
      Object(util_console["d" /* deprecate */])('timeout="0"', '-1', this);
    }
  },

  methods: {
    genActions() {
      return this.$createElement('div', {
        staticClass: 'v-snack__action '
      }, [Object(helpers["s" /* getSlot */])(this, 'action', {
        attrs: {
          class: 'v-snack__btn'
        }
      })]);
    },

    genContent() {
      return this.$createElement('div', {
        staticClass: 'v-snack__content',
        class: {
          [this.contentClass]: true
        },
        attrs: {
          role: 'status',
          'aria-live': 'polite'
        }
      }, [Object(helpers["s" /* getSlot */])(this)]);
    },

    genWrapper() {
      const setColor = this.hasBackground ? this.setBackgroundColor : this.setTextColor;
      const data = setColor(this.color, {
        staticClass: 'v-snack__wrapper',
        class: VSheet["a" /* default */].options.computed.classes.call(this),
        directives: [{
          name: 'show',
          value: this.isActive
        }]
      });
      return this.$createElement('div', data, [this.genContent(), this.genActions()]);
    },

    genTransition() {
      return this.$createElement('transition', {
        props: {
          name: this.transition
        }
      }, [this.genWrapper()]);
    },

    setTimeout() {
      window.clearTimeout(this.activeTimeout);
      const timeout = Number(this.timeout);

      if (!this.isActive || // TODO: remove 0 in v3
      [0, -1].includes(timeout)) {
        return;
      }

      this.activeTimeout = window.setTimeout(() => {
        this.isActive = false;
      }, timeout);
    }

  },

  render(h) {
    return h('div', {
      staticClass: 'v-snack',
      class: this.classes,
      style: this.styles
    }, [this.transition !== false ? this.genTransition() : this.genWrapper()]);
  }

}));
// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VSpacer.js
var VSpacer = __webpack_require__(315);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VTooltip/VTooltip.js
var VTooltip = __webpack_require__(316);

// CONCATENATED MODULE: ./components/pollClassic/index2.vue



function index2_injectStyles (context) {
  
  var style0 = __webpack_require__(486)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var index2_component = Object(componentNormalizer["a" /* default */])(
  pollClassic_index2vue_type_script_lang_ts_,
  index2vue_type_template_id_5b744dec_lang_pug_render,
  index2vue_type_template_id_5b744dec_lang_pug_staticRenderFns,
  false,
  index2_injectStyles,
  null,
  "3fc00ed7"
  
)

/* harmony default export */ var index2 = (index2_component.exports);

/* vuetify-loader */














installComponents_default()(index2_component, {VBtn: VBtn["a" /* default */],VCard: VCard["a" /* default */],VDivider: VDivider["a" /* default */],VIcon: VIcon["a" /* default */],VList: VList["a" /* default */],VListItem: VListItem["a" /* default */],VListItemTitle: components_VList["c" /* VListItemTitle */],VMenu: VMenu["a" /* default */],VSlideGroup: VSlideGroup["b" /* default */],VSlideItem: VSlideItem,VSnackbar: VSnackbar_VSnackbar,VSpacer: VSpacer["a" /* default */],VTooltip: VTooltip["a" /* default */]})

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/tests/_id/exec/index.vue?vue&type=script&lang=ts&


 // import PollClassic from '~/components/pollClassic/index.vue'


let execvue_type_script_lang_ts_Polls_Id_Exec = class Polls_Id_Exec extends external_vue_property_decorator_["Vue"] {
  get result() {
    if (store_0["auth"].isAuthenticated) return store_0["auth"].user.results.find(item => item._id === this.$route.query.resultId);else return undefined;
  }

  get isActive() {
    return store_0["resultPollStore"].poll.isActive;
  }

  get type() {
    return store_0["resultPollStore"].poll.type;
  }

  get name() {
    return store_0["resultPollStore"].poll.name;
  }

};
execvue_type_script_lang_ts_Polls_Id_Exec = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'execClassic',
  layout: 'poll',
  components: {
    PollClassic: index2
  },

  async fetch({
    params,
    redirect,
    store
  }) {
    await store_0["resultPollStore"].init(params.id);
  }

})], execvue_type_script_lang_ts_Polls_Id_Exec);
/* harmony default export */ var execvue_type_script_lang_ts_ = (execvue_type_script_lang_ts_Polls_Id_Exec);
// CONCATENATED MODULE: ./pages/tests/_id/exec/index.vue?vue&type=script&lang=ts&
 /* harmony default export */ var _id_execvue_type_script_lang_ts_ = (execvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VContainer.js
var VContainer = __webpack_require__(314);

// CONCATENATED MODULE: ./pages/tests/_id/exec/index.vue



function exec_injectStyles (context) {
  
  
}

/* normalize component */

var exec_component = Object(componentNormalizer["a" /* default */])(
  _id_execvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  exec_injectStyles,
  null,
  "f84256f8"
  
)

/* harmony default export */ var exec = __webpack_exports__["default"] = (exec_component.exports);

/* vuetify-loader */




installComponents_default()(exec_component, {VCol: VCol["a" /* default */],VContainer: VContainer["a" /* default */],VRow: VRow["a" /* default */]})


/***/ })

};;
//# sourceMappingURL=index.js.map