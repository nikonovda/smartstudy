<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Hop;
use Faker\Generator as Faker;

$factory->define(Hop::class, function (Faker $faker) {
    return [
		'content' => $faker->city,
		'view' => 'checkbox',
		'maxChoise' => 2,
		'choised' => [ 'a' ],
		'options' => [
			[ 'id' => 'a', 'value' => 'Да', 'type' => 'simple' ],
			[ 'id' => 'b', 'value' => 'Нет', 'type' => 'simple' ],
			[ 'id' => 'c', 'value' => 'Свой вариант', 'type' => 'user' ]
		]
    ];
});

