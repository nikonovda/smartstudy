import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'

import { Hop } from '../interfaces/interfaceHops'

import axios from '../plugins/axios'

@Module({
  name: 'templates',
  stateFactory: true,
  namespaced: true
})
export default class Templates extends VuexModule {
  templates: Hop[] = []
  generalTemplates: Hop[] = []
  userTemplates: Hop[] = []
  sharedTemplates: Hop[] = []

  @Mutation
  SET_TEMPLATES (payload) {
    this.templates = payload
    payload.forEach((item) => {
      switch (item.typeTemplate) {
        case 'general':
          this.generalTemplates.push(item)
          break;
          
        case 'user':
          this.userTemplates.push(item)
          break;

        case 'shared':
          this.sharedTemplates.push(item)
          break;
      }
    })
  }

  @Mutation
  REFRESH_STATE () {
    this.generalTemplates = []
    this.userTemplates = []
    this.sharedTemplates = []
  }

  @Action({ rawError: true })
  getTemplates () {
    axios.get('/api/profile/templates')
      .then((response) => {
        this.SET_TEMPLATES(response.data)
      })
      .catch((e) => {
        console.log(e)
      })
  }

  @Action({ rawError: true })
  addUserTemplate (hop: Hop) {
    axios.post('/api/profile/templates', hop)
    .then((response) => {
      hop._id = response.data._id
      this.SET_TEMPLATES([ hop ])
    })
    .catch((e) => {
      console.log(e)
    })
  }
}
