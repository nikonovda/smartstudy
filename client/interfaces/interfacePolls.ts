
export interface Polls {
  _id: String,
  name: String,
  description: String,
  isOpenResults: Boolean,
  published_at: String,
  closed_at: String,
  updated_at: String,
  created_at: String,
  sort: Array<String>,
  isPublished: Boolean,
  isActive: Boolean,
  type: string
}