import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import axios from '../plugins/axios'
import { setToken, unsetToken } from '../utils/auth'
import { User } from '../interfaces/interfaceUser'
import { lk } from '../store'
interface formData {
  username: string
  password: string
}

@Module({
  name: 'auth',
  stateFactory: true,
  namespaced: true
})
export default class Auth extends VuexModule {
  isAuthenticated: boolean = false
  errorMessage: string = ''
  user: User = {}
  isAnonimous: boolean = false

  @Mutation
  SET_USER() {
    this.isAuthenticated = true
  }

  @Mutation
  SET_ANONIMOUS(payload: boolean) {
    this.isAnonimous = payload
  }

  @Mutation
  UNSET_USER() {
    this.isAuthenticated = false
  }

  @Mutation
  SET_ERROR(payload: string) {
    this.errorMessage = payload
  }

  @Mutation
  SET_ABOUT_USER( payload: Object) {
    this.user = payload
  }

  @Action({ rawError: true })
  setToken(token: string): void {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
  }

  @Action({ rawError: true })
  async getUser() {
    await axios.get('/api/profile')
      .then( (response) => {
        this.SET_ABOUT_USER( response.data )
        this.SET_ANONIMOUS( response.data.isAnonimous )
        if(!response.data.isAnonimous) this.SET_USER()
      })
      .catch (e => {
        console.warn(e)
      })
  }

  @Action({ rawError: true })
  async getTempUser() {
    await axios.get('/api/tempprofile')
      .then((response) => {
        this.SET_ABOUT_USER( response.data )
      })
      .catch(e => {
        console.log(e)
      })
  }

  @Action({ rawError: true })
  async generatePersonalTest () {
    await axios.patch('/api/polls/generate/byLastResult')
      .then((response) => {
      })
      .catch((e: ExceptionInformation) => {
        alert(e)
      })
  }

  @Action({ rawError: true })
  async signin(payload: formData) {
    const formData = {
      username: payload.username,
      password: payload.password
    }

    return await axios.post('/api/login', formData)
      .then( async (response) => {
        // axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.access_token
        await this.setToken(response.data.access_token)
        this.SET_USER()
        setToken(response.data.access_token)
        await this.getUser()
        // if(this.isRoot || this.isAdmin) {
          // lk.getGroupByUser()
        // }
        return true
      })
      .catch((e) => {
        switch (e.response.data.status) {
          case 422:
            this.SET_ERROR(e.response.data.type)
            return e.response.data.message
          default:
            // alert("Проблемы на стороне сервера, попробуйте перезагрузить страницу")
            return false
        }
      })
  }

  @Action({ rawError: true })
  async logout() {
    return await axios.post('/api/logout')
      .then( async (response) => {
        if (response.data.status === 200) {
          delete axios.defaults.headers.common['Authorization']
          await this.UNSET_USER()
          await this.SET_ABOUT_USER({})
          await unsetToken()
          await this.SET_ANONIMOUS(false)
          return true
        }
      })
      .catch((e) => {
        console.warn(e.responce.data)
      })
  }

  @Action({ rawError: true })
  async registrationAnonimous () {
    return await axios.post('/api/registerAnonimous', {})
      .then( async (response) => {
        this.SET_ANONIMOUS(true)
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.access_token
        setToken(response.data.access_token)
        await this.getUser()
      })
      .catch( e => {
        alert('Sorry, we have some problems... Try later')
      })
  }

  @Action({ rawError: true })
  async registration (payload) {
    return await axios.post('/api/register', payload)
      .then( (response) => {
        if(response.data.status === 201) {
          return true
        }
        else {
          return response
        }
      })
      .catch( (e) => {
        console.warn(e);
      })
  }

  get isRoot () {
    if(this.user.roles !== undefined)
      return this.user.roles.find(item => item === 'root') ? true : false
    else
      return false
  }

  get isAdmin () {
    if(this.user.roles !== undefined)
      return this.user.roles.find(item => item === 'admin') ? true : false
    else
      return false
  }

  get stsError () {
    return this.errorMessage
  }
}
