exports.ids = [22];
exports.modules = {

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VCardActions; });
/* unused harmony export VCardSubtitle */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return VCardText; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return VCardTitle; });
/* harmony import */ var _VCard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(298);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);


const VCardActions = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__actions');
const VCardSubtitle = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__subtitle');
const VCardText = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__text');
const VCardTitle = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__title');

/* unused harmony default export */ var _unused_webpack_default_export = ({
  $_vuetify_subcomponents: {
    VCard: _VCard__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"],
    VCardActions,
    VCardSubtitle,
    VCardText,
    VCardTitle
  }
});

/***/ }),

/***/ 322:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(345);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("0db8c820", content, true, context)
};

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreCard.vue?vue&type=template&id=0a0f4055&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-card',_vm._b({staticClass:"v-card--material pa-3",class:_vm.classes},'v-card',_vm.$attrs,false),[_c('div',{staticClass:"d-flex grow flex-wrap"},[(_vm.avatar)?_c('v-avatar',{staticClass:"mx-auto v-card--material__avatar elevation-6",attrs:{"size":"128","color":"grey"}},[_c('v-img',{attrs:{"src":_vm.avatar}})],1):(_vm.$slots.image || _vm.place || _vm.icon || _vm.$slots.heading)?_c('v-sheet',{staticClass:"text-start v-card--material__heading mb-n6",class:{ 'pa-7': !_vm.$slots.image && !_vm.place },style:(_vm.place ? "display: flex;" : ""),attrs:{"color":_vm.color,"max-height":_vm.icon || _vm.place ? 90 : undefined,"width":_vm.icon || _vm.place ? '' : '100%',"elevation":_vm.place ? 0 : 6,"dark":""}},[(_vm.$slots.heading)?_vm._t("heading"):(_vm.$slots.image)?_vm._t("image"):(_vm.title && !_vm.icon)?_c('div',{staticClass:"display-1 font-weight-light",domProps:{"textContent":_vm._s(_vm.title)}}):(_vm.icon)?_c('v-icon',{attrs:{"size":"32"},domProps:{"textContent":_vm._s(_vm.icon)}}):(_vm.place)?[_c('div',{staticClass:"text-center",style:("padding: 15px 20px; background-color:" + _vm.colorPlace + "; border-radius: 4px; width: 115px")},[_c('span',{staticClass:"display-1"},[_vm._v("Rank:")]),_c('h1',{staticClass:"display-3"},[_vm._v(_vm._s(_vm.place))])]),_c('div',{staticClass:"text-center ml-4",style:("padding: 15px 20px; background-color:" + _vm.colorPlace + "; border-radius: 4px; width: 115px")},[_c('span',{staticClass:"display-1"},[_vm._v("Top:")]),_c('h1',{staticClass:"display-3"},[_vm._v(_vm._s(_vm.placePercentage + "%"))])])]:_vm._e(),(_vm.text)?_c('div',{staticClass:"headline font-weight-thin",domProps:{"textContent":_vm._s(_vm.text)}}):_vm._e()],2):_vm._e(),(_vm.$slots['after-heading'])?_c('div',{staticClass:"justify-center align-center",staticStyle:{"height":"calc(100% - 85px)"}},[_vm._t("after-heading")],2):(_vm.icon && _vm.title)?_c('div',{staticClass:"ml-4"},[_c('div',{staticClass:"card-title font-weight-light",domProps:{"textContent":_vm._s(_vm.title)}})]):_vm._e()],1),_vm._t("default"),(_vm.$slots.actions)?[_c('v-divider',{staticClass:"mt-2"}),_c('v-card-actions',{staticClass:"pb-0"},[_vm._t("actions")],2)]:_vm._e()],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/core/coreCard.vue?vue&type=template&id=0a0f4055&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreCard.vue?vue&type=script&lang=ts&


let coreCardvue_type_script_lang_ts_CoreCard = class CoreCard extends external_vue_property_decorator_["Vue"] {
  get classes() {
    return {
      'v-card--material--has-heading': this.hasHeading
    };
  }

  get hasHeading() {
    return Boolean(this.$slots.heading || this.title || this.icon);
  }

  get hasAltHeading() {
    return Boolean(this.$slots.heading || this.title && this.icon);
  }

};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "avatar", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: 'success'
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "color", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: 'success'
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "colorPlace", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "icon", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Boolean,
  default: false
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "image", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "text", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "title", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "place", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "placePercentage", void 0);

coreCardvue_type_script_lang_ts_CoreCard = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'CoreCard'
})], coreCardvue_type_script_lang_ts_CoreCard);
/* harmony default export */ var coreCardvue_type_script_lang_ts_ = (coreCardvue_type_script_lang_ts_CoreCard);
// CONCATENATED MODULE: ./components/core/coreCard.vue?vue&type=script&lang=ts&
 /* harmony default export */ var core_coreCardvue_type_script_lang_ts_ = (coreCardvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VAvatar/VAvatar.js
var VAvatar = __webpack_require__(96);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(298);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(296);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(301);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(50);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(72);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSheet/VSheet.js
var VSheet = __webpack_require__(24);

// CONCATENATED MODULE: ./components/core/coreCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(344)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_coreCardvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "f4daf264"
  
)

/* harmony default export */ var coreCard = __webpack_exports__["a"] = (component.exports);

/* vuetify-loader */








installComponents_default()(component, {VAvatar: VAvatar["a" /* default */],VCard: VCard["a" /* default */],VCardActions: components_VCard["a" /* VCardActions */],VDivider: VDivider["a" /* default */],VIcon: VIcon["a" /* default */],VImg: VImg["a" /* default */],VSheet: VSheet["a" /* default */]})


/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(131);
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _util_mergeData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);



 // no xs

const breakpoints = ['sm', 'md', 'lg', 'xl'];
const ALIGNMENT = ['start', 'end', 'center'];

function makeProps(prefix, def) {
  return breakpoints.reduce((props, val) => {
    props[prefix + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = def();
    return props;
  }, {});
}

const alignValidator = str => [...ALIGNMENT, 'baseline', 'stretch'].includes(str);

const alignProps = makeProps('align', () => ({
  type: String,
  default: null,
  validator: alignValidator
}));

const justifyValidator = str => [...ALIGNMENT, 'space-between', 'space-around'].includes(str);

const justifyProps = makeProps('justify', () => ({
  type: String,
  default: null,
  validator: justifyValidator
}));

const alignContentValidator = str => [...ALIGNMENT, 'space-between', 'space-around', 'stretch'].includes(str);

const alignContentProps = makeProps('alignContent', () => ({
  type: String,
  default: null,
  validator: alignContentValidator
}));
const propMap = {
  align: Object.keys(alignProps),
  justify: Object.keys(justifyProps),
  alignContent: Object.keys(alignContentProps)
};
const classMap = {
  align: 'align',
  justify: 'justify',
  alignContent: 'align-content'
};

function breakpointClass(type, prop, val) {
  let className = classMap[type];

  if (val == null) {
    return undefined;
  }

  if (prop) {
    // alignSm -> Sm
    const breakpoint = prop.replace(type, '');
    className += `-${breakpoint}`;
  } // .align-items-sm-center


  className += `-${val}`;
  return className.toLowerCase();
}

const cache = new Map();
/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_1___default.a.extend({
  name: 'v-row',
  functional: true,
  props: {
    tag: {
      type: String,
      default: 'div'
    },
    dense: Boolean,
    noGutters: Boolean,
    align: {
      type: String,
      default: null,
      validator: alignValidator
    },
    ...alignProps,
    justify: {
      type: String,
      default: null,
      validator: justifyValidator
    },
    ...justifyProps,
    alignContent: {
      type: String,
      default: null,
      validator: alignContentValidator
    },
    ...alignContentProps
  },

  render(h, {
    props,
    data,
    children
  }) {
    // Super-fast memoization based on props, 5x faster than JSON.stringify
    let cacheKey = '';

    for (const prop in props) {
      cacheKey += String(props[prop]);
    }

    let classList = cache.get(cacheKey);

    if (!classList) {
      classList = []; // Loop through `align`, `justify`, `alignContent` breakpoint props

      let type;

      for (type in propMap) {
        propMap[type].forEach(prop => {
          const value = props[prop];
          const className = breakpointClass(type, prop, value);
          if (className) classList.push(className);
        });
      }

      classList.push({
        'no-gutters': props.noGutters,
        'row--dense': props.dense,
        [`align-${props.align}`]: props.align,
        [`justify-${props.justify}`]: props.justify,
        [`align-content-${props.alignContent}`]: props.alignContent
      });
      cache.set(cacheKey, classList);
    }

    return h(props.tag, Object(_util_mergeData__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(data, {
      staticClass: 'row',
      class: classList
    }), children);
  }

}));

/***/ }),

/***/ 343:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(131);
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _util_mergeData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);



 // no xs

const breakpoints = ['sm', 'md', 'lg', 'xl'];

const breakpointProps = (() => {
  return breakpoints.reduce((props, val) => {
    props[val] = {
      type: [Boolean, String, Number],
      default: false
    };
    return props;
  }, {});
})();

const offsetProps = (() => {
  return breakpoints.reduce((props, val) => {
    props['offset' + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = {
      type: [String, Number],
      default: null
    };
    return props;
  }, {});
})();

const orderProps = (() => {
  return breakpoints.reduce((props, val) => {
    props['order' + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = {
      type: [String, Number],
      default: null
    };
    return props;
  }, {});
})();

const propMap = {
  col: Object.keys(breakpointProps),
  offset: Object.keys(offsetProps),
  order: Object.keys(orderProps)
};

function breakpointClass(type, prop, val) {
  let className = type;

  if (val == null || val === false) {
    return undefined;
  }

  if (prop) {
    const breakpoint = prop.replace(type, '');
    className += `-${breakpoint}`;
  } // Handling the boolean style prop when accepting [Boolean, String, Number]
  // means Vue will not convert <v-col sm></v-col> to sm: true for us.
  // Since the default is false, an empty string indicates the prop's presence.


  if (type === 'col' && (val === '' || val === true)) {
    // .col-md
    return className.toLowerCase();
  } // .order-md-6


  className += `-${val}`;
  return className.toLowerCase();
}

const cache = new Map();
/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_1___default.a.extend({
  name: 'v-col',
  functional: true,
  props: {
    cols: {
      type: [Boolean, String, Number],
      default: false
    },
    ...breakpointProps,
    offset: {
      type: [String, Number],
      default: null
    },
    ...offsetProps,
    order: {
      type: [String, Number],
      default: null
    },
    ...orderProps,
    alignSelf: {
      type: String,
      default: null,
      validator: str => ['auto', 'start', 'end', 'center', 'baseline', 'stretch'].includes(str)
    },
    tag: {
      type: String,
      default: 'div'
    }
  },

  render(h, {
    props,
    data,
    children,
    parent
  }) {
    // Super-fast memoization based on props, 5x faster than JSON.stringify
    let cacheKey = '';

    for (const prop in props) {
      cacheKey += String(props[prop]);
    }

    let classList = cache.get(cacheKey);

    if (!classList) {
      classList = []; // Loop through `col`, `offset`, `order` breakpoint props

      let type;

      for (type in propMap) {
        propMap[type].forEach(prop => {
          const value = props[prop];
          const className = breakpointClass(type, prop, value);
          if (className) classList.push(className);
        });
      }

      const hasColClasses = classList.some(className => className.startsWith('col-'));
      classList.push({
        // Default to .col if no other col-{bp}-* classes generated nor `cols` specified.
        col: !hasColClasses || !props.cols,
        [`col-${props.cols}`]: props.cols,
        [`offset-${props.offset}`]: props.offset,
        [`order-${props.order}`]: props.order,
        [`align-self-${props.alignSelf}`]: props.alignSelf
      });
      cache.set(cacheKey, classList);
    }

    return h(props.tag, Object(_util_mergeData__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(data, {
      class: classList
    }), children);
  }

}));

/***/ }),

/***/ 344:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(322);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 345:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-card--material__avatar{position:relative;top:-64px;margin-bottom:-32px}.v-card--material__heading{position:relative;top:-40px;transition:.3s ease;z-index:1}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 405:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(406);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(10).default("2065bca8", content, true)

/***/ }),

/***/ 406:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-dialog{border-radius:4px;margin:24px;overflow-y:auto;pointer-events:auto;transition:.3s cubic-bezier(.25,.8,.25,1);width:100%;z-index:inherit;box-shadow:0 11px 15px -7px rgba(0,0,0,.2),0 24px 38px 3px rgba(0,0,0,.14),0 9px 46px 8px rgba(0,0,0,.12)}.v-dialog:not(.v-dialog--fullscreen){max-height:90%}.v-dialog>*{width:100%}.v-dialog>.v-card>.v-card__title{font-size:.75rem;font-weight:500;letter-spacing:.0125em;padding:16px 24px 10px}.v-dialog>.v-card>.v-card__subtitle,.v-dialog>.v-card>.v-card__text{padding:0 24px 20px}.v-dialog__content{align-items:center;display:flex;height:100%;justify-content:center;left:0;pointer-events:none;position:fixed;top:0;transition:.2s cubic-bezier(.25,.8,.25,1),z-index 1ms;width:100%;z-index:6;outline:none}.v-dialog__container{display:none}.v-dialog__container--attached{display:inline}.v-dialog--animated{-webkit-animation-duration:.15s;animation-duration:.15s;-webkit-animation-name:animate-dialog;animation-name:animate-dialog;-webkit-animation-timing-function:cubic-bezier(.25,.8,.25,1);animation-timing-function:cubic-bezier(.25,.8,.25,1)}.v-dialog--fullscreen{border-radius:0;margin:0;height:100%;position:fixed;overflow-y:auto;top:0;left:0}.v-dialog--fullscreen>.v-card{min-height:100%;min-width:100%;margin:0!important;padding:0!important}.v-dialog--scrollable,.v-dialog--scrollable>form{display:flex}.v-dialog--scrollable>.v-card,.v-dialog--scrollable>form>.v-card{display:flex;flex:1 1 100%;flex-direction:column;max-height:100%;max-width:100%}.v-dialog--scrollable>.v-card>.v-card__actions,.v-dialog--scrollable>.v-card>.v-card__title,.v-dialog--scrollable>form>.v-card>.v-card__actions,.v-dialog--scrollable>form>.v-card>.v-card__title{flex:0 0 auto}.v-dialog--scrollable>.v-card>.v-card__text,.v-dialog--scrollable>form>.v-card>.v-card__text{-webkit-backface-visibility:hidden;backface-visibility:hidden;flex:1 1 auto;overflow-y:auto}@-webkit-keyframes animate-dialog{0%{transform:scale(1)}50%{transform:scale(1.03)}to{transform:scale(1)}}@keyframes animate-dialog{0%{transform:scale(1)}50%{transform:scale(1.03)}to{transform:scale(1)}}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 421:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(464);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("4805f846", content, true, context)
};

/***/ }),

/***/ 450:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/registrationModal.vue?vue&type=template&id=7de117c9&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-dialog',{attrs:{"max-width":"390px"},on:{"input":_vm.emitClose},model:{value:(_vm.status),callback:function ($$v) {_vm.status=$$v},expression:"status"}},[_c('v-card',[_c('v-card-title',{staticStyle:{"word-break":"break-word"}},[_c('div',[_vm._v("How to keep your progress and improve your knowledge?"),_c('v-spacer'),_c('v-icon',{attrs:{"tag":"button"},on:{"click":_vm.emitClose}},[_vm._v("mdi-close")])],1)]),_c('v-divider',{staticClass:"mt-4"}),_c('v-card-text',{staticClass:"body-1 text-center"},[_c('v-row',[_c('v-col',{attrs:{"cols":"12"}},[_c('div',[_c('div',[_c('strong',[_vm._v("1. Register")])]),_c('div',{staticClass:"grey--text"}),_vm._v("The first step is to create an account at Smart Study. You can choose a social network or go for the classic version, whatever works best for you.")])]),_c('v-col',{attrs:{"cols":"12"}},[_c('div',[_c('div',[_c('strong',[_vm._v("2. Try tests")])]),_c('div',{staticClass:"grey--text"}),_vm._v("Learn your weak topics in mathematics, get personalized tests, watch your development progress.")])])],1),_c('v-divider',{staticClass:"mt-4"}),_c('v-btn',{staticClass:"mt-6 mr-0",attrs:{"depressed":"","rounded":"","color":"success","to":"/registration"}},[_vm._v("Registration")])],1)],1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/core/registrationModal.vue?vue&type=template&id=7de117c9&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/registrationModal.vue?vue&type=script&lang=ts&


let registrationModalvue_type_script_lang_ts_RegistrationModal = class RegistrationModal extends external_vue_property_decorator_["Vue"] {
  emitClose() {
    this.$emit('changeStatus', this.status);
  }

};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  required: true,
  type: Boolean
})], registrationModalvue_type_script_lang_ts_RegistrationModal.prototype, "status", void 0);

registrationModalvue_type_script_lang_ts_RegistrationModal = Object(external_tslib_["__decorate"])([external_vue_property_decorator_["Component"]], registrationModalvue_type_script_lang_ts_RegistrationModal);
/* harmony default export */ var registrationModalvue_type_script_lang_ts_ = (registrationModalvue_type_script_lang_ts_RegistrationModal);
// CONCATENATED MODULE: ./components/core/registrationModal.vue?vue&type=script&lang=ts&
 /* harmony default export */ var core_registrationModalvue_type_script_lang_ts_ = (registrationModalvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/VBtn.js
var VBtn = __webpack_require__(81);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(298);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(296);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(343);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDialog/VDialog.js
var VDialog = __webpack_require__(455);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(301);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(50);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(341);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VSpacer.js
var VSpacer = __webpack_require__(315);

// CONCATENATED MODULE: ./components/core/registrationModal.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_registrationModalvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "a531a4f6"
  
)

/* harmony default export */ var registrationModal = __webpack_exports__["a"] = (component.exports);

/* vuetify-loader */











installComponents_default()(component, {VBtn: VBtn["a" /* default */],VCard: VCard["a" /* default */],VCardText: components_VCard["b" /* VCardText */],VCardTitle: components_VCard["c" /* VCardTitle */],VCol: VCol["a" /* default */],VDialog: VDialog["a" /* default */],VDivider: VDivider["a" /* default */],VIcon: VIcon["a" /* default */],VRow: VRow["a" /* default */],VSpacer: VSpacer["a" /* default */]})


/***/ }),

/***/ 455:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VDialog_VDialog_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(405);
/* harmony import */ var _src_components_VDialog_VDialog_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VDialog_VDialog_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _VThemeProvider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(295);
/* harmony import */ var _mixins_activatable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(33);
/* harmony import */ var _mixins_dependent__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(38);
/* harmony import */ var _mixins_detachable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(60);
/* harmony import */ var _mixins_overlayable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(107);
/* harmony import */ var _mixins_returnable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(106);
/* harmony import */ var _mixins_stackable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(105);
/* harmony import */ var _mixins_toggleable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(15);
/* harmony import */ var _directives_click_outside__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(58);
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(4);
/* harmony import */ var _util_console__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(6);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(0);
// Styles
 // Components

 // Mixins







 // Directives

 // Helpers




const baseMixins = Object(_util_mixins__WEBPACK_IMPORTED_MODULE_10__[/* default */ "a"])(_mixins_activatable__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], _mixins_dependent__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], _mixins_detachable__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], _mixins_overlayable__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"], _mixins_returnable__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], _mixins_stackable__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], _mixins_toggleable__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"]);
/* @vue/component */

/* harmony default export */ __webpack_exports__["a"] = (baseMixins.extend({
  name: 'v-dialog',
  directives: {
    ClickOutside: _directives_click_outside__WEBPACK_IMPORTED_MODULE_9__[/* default */ "a"]
  },
  props: {
    dark: Boolean,
    disabled: Boolean,
    fullscreen: Boolean,
    light: Boolean,
    maxWidth: {
      type: [String, Number],
      default: 'none'
    },
    noClickAnimation: Boolean,
    origin: {
      type: String,
      default: 'center center'
    },
    persistent: Boolean,
    retainFocus: {
      type: Boolean,
      default: true
    },
    scrollable: Boolean,
    transition: {
      type: [String, Boolean],
      default: 'dialog-transition'
    },
    width: {
      type: [String, Number],
      default: 'auto'
    }
  },

  data() {
    return {
      activatedBy: null,
      animate: false,
      animateTimeout: -1,
      isActive: !!this.value,
      stackMinZIndex: 200
    };
  },

  computed: {
    classes() {
      return {
        [`v-dialog ${this.contentClass}`.trim()]: true,
        'v-dialog--active': this.isActive,
        'v-dialog--persistent': this.persistent,
        'v-dialog--fullscreen': this.fullscreen,
        'v-dialog--scrollable': this.scrollable,
        'v-dialog--animated': this.animate
      };
    },

    contentClasses() {
      return {
        'v-dialog__content': true,
        'v-dialog__content--active': this.isActive
      };
    },

    hasActivator() {
      return Boolean(!!this.$slots.activator || !!this.$scopedSlots.activator);
    }

  },
  watch: {
    isActive(val) {
      if (val) {
        this.show();
        this.hideScroll();
      } else {
        this.removeOverlay();
        this.unbind();
      }
    },

    fullscreen(val) {
      if (!this.isActive) return;

      if (val) {
        this.hideScroll();
        this.removeOverlay(false);
      } else {
        this.showScroll();
        this.genOverlay();
      }
    }

  },

  created() {
    /* istanbul ignore next */
    if (this.$attrs.hasOwnProperty('full-width')) {
      Object(_util_console__WEBPACK_IMPORTED_MODULE_11__[/* removed */ "e"])('full-width', this);
    }
  },

  beforeMount() {
    this.$nextTick(() => {
      this.isBooted = this.isActive;
      this.isActive && this.show();
    });
  },

  beforeDestroy() {
    if (typeof window !== 'undefined') this.unbind();
  },

  methods: {
    animateClick() {
      this.animate = false; // Needed for when clicking very fast
      // outside of the dialog

      this.$nextTick(() => {
        this.animate = true;
        window.clearTimeout(this.animateTimeout);
        this.animateTimeout = window.setTimeout(() => this.animate = false, 150);
      });
    },

    closeConditional(e) {
      const target = e.target; // Ignore the click if the dialog is closed or destroyed,
      // if it was on an element inside the content,
      // if it was dragged onto the overlay (#6969),
      // or if this isn't the topmost dialog (#9907)

      return !(this._isDestroyed || !this.isActive || this.$refs.content.contains(target) || this.overlay && target && !this.overlay.$el.contains(target)) && this.activeZIndex >= this.getMaxZIndex();
    },

    hideScroll() {
      if (this.fullscreen) {
        document.documentElement.classList.add('overflow-y-hidden');
      } else {
        _mixins_overlayable__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"].options.methods.hideScroll.call(this);
      }
    },

    show() {
      !this.fullscreen && !this.hideOverlay && this.genOverlay();
      this.$nextTick(() => {
        this.$refs.content.focus();
        this.bind();
      });
    },

    bind() {
      window.addEventListener('focusin', this.onFocusin);
    },

    unbind() {
      window.removeEventListener('focusin', this.onFocusin);
    },

    onClickOutside(e) {
      this.$emit('click:outside', e);

      if (this.persistent) {
        this.noClickAnimation || this.animateClick();
      } else {
        this.isActive = false;
      }
    },

    onKeydown(e) {
      if (e.keyCode === _util_helpers__WEBPACK_IMPORTED_MODULE_12__[/* keyCodes */ "x"].esc && !this.getOpenDependents().length) {
        if (!this.persistent) {
          this.isActive = false;
          const activator = this.getActivator();
          this.$nextTick(() => activator && activator.focus());
        } else if (!this.noClickAnimation) {
          this.animateClick();
        }
      }

      this.$emit('keydown', e);
    },

    // On focus change, wrap focus to stay inside the dialog
    // https://github.com/vuetifyjs/vuetify/issues/6892
    onFocusin(e) {
      if (!e || !this.retainFocus) return;
      const target = e.target;

      if (!!target && // It isn't the document or the dialog body
      ![document, this.$refs.content].includes(target) && // It isn't inside the dialog body
      !this.$refs.content.contains(target) && // We're the topmost dialog
      this.activeZIndex >= this.getMaxZIndex() && // It isn't inside a dependent element (like a menu)
      !this.getOpenDependentElements().some(el => el.contains(target)) // So we must have focused something outside the dialog and its children
      ) {
          // Find and focus the first available element inside the dialog
          const focusable = this.$refs.content.querySelectorAll('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
          const el = [...focusable].find(el => !el.hasAttribute('disabled'));
          el && el.focus();
        }
    },

    genContent() {
      return this.showLazyContent(() => [this.$createElement(_VThemeProvider__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], {
        props: {
          root: true,
          light: this.light,
          dark: this.dark
        }
      }, [this.$createElement('div', {
        class: this.contentClasses,
        attrs: {
          role: 'document',
          tabindex: this.isActive ? 0 : undefined,
          ...this.getScopeIdAttrs()
        },
        on: {
          keydown: this.onKeydown
        },
        style: {
          zIndex: this.activeZIndex
        },
        ref: 'content'
      }, [this.genTransition()])])]);
    },

    genTransition() {
      const content = this.genInnerContent();
      if (!this.transition) return content;
      return this.$createElement('transition', {
        props: {
          name: this.transition,
          origin: this.origin,
          appear: true
        }
      }, [content]);
    },

    genInnerContent() {
      const data = {
        class: this.classes,
        ref: 'dialog',
        directives: [{
          name: 'click-outside',
          value: {
            handler: this.onClickOutside,
            closeConditional: this.closeConditional,
            include: this.getOpenDependentElements
          }
        }, {
          name: 'show',
          value: this.isActive
        }],
        style: {
          transformOrigin: this.origin
        }
      };

      if (!this.fullscreen) {
        data.style = { ...data.style,
          maxWidth: this.maxWidth === 'none' ? undefined : Object(_util_helpers__WEBPACK_IMPORTED_MODULE_12__[/* convertToUnit */ "g"])(this.maxWidth),
          width: this.width === 'auto' ? undefined : Object(_util_helpers__WEBPACK_IMPORTED_MODULE_12__[/* convertToUnit */ "g"])(this.width)
        };
      }

      return this.$createElement('div', data, this.getContentSlot());
    }

  },

  render(h) {
    return h('div', {
      staticClass: 'v-dialog__container',
      class: {
        'v-dialog__container--attached': this.attach === '' || this.attach === true || this.attach === 'attach'
      },
      attrs: {
        role: 'dialog'
      }
    }, [this.genActivator(), this.genContent()]);
  }

}));

/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(421);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 464:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "body{overflow:hidden}::-webkit-scrollbar{width:0}.card-poll-0,.card-poll-1,.card-poll-2,.card-poll-3,.card-poll-4,.card-poll-5,.card-poll-6,.card-poll-7,.card-poll-8,.card-poll-9,.card-poll-10,.card-poll-11,.card-poll-12,.card-poll-13,.card-poll-14,.card-poll-15{transition:background-color 1s;-webkit-animation-duration:.4s;animation-duration:.3s;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-name:fadeInRightBig;animation-name:fadeInRightBig}.card-poll-0:hover,.card-poll-1:hover,.card-poll-2:hover,.card-poll-3:hover,.card-poll-4:hover,.card-poll-5:hover,.card-poll-6:hover,.card-poll-7:hover,.card-poll-8:hover,.card-poll-9:hover,.card-poll-10:hover,.card-poll-11:hover,.card-poll-12:hover,.card-poll-13:hover,.card-poll-14:hover,.card-poll-15:hover{background-color:#f66f8d}.card-poll-0{-webkit-animation-delay:0s;animation-delay:0s}.card-poll-1{-webkit-animation-delay:.3s;animation-delay:.3s}.card-poll-2{-webkit-animation-delay:.6s;animation-delay:.6s}.card-poll-3{-webkit-animation-delay:.9s;animation-delay:.9s}.card-poll-4{-webkit-animation-delay:1.2s;animation-delay:1.2s}.card-poll-5{-webkit-animation-delay:1.5s;animation-delay:1.5s}.card-poll-6{-webkit-animation-delay:1.8s;animation-delay:1.8s}.card-poll-7{-webkit-animation-delay:2.1s;animation-delay:2.1s}.card-poll-8{-webkit-animation-delay:2.4s;animation-delay:2.4s}.card-poll-9{-webkit-animation-delay:2.7s;animation-delay:2.7s}.card-poll-10{-webkit-animation-delay:3s;animation-delay:3s}.card-poll-11{-webkit-animation-delay:3.3s;animation-delay:3.3s}.card-poll-12{-webkit-animation-delay:3.6s;animation-delay:3.6s}.card-poll-13{-webkit-animation-delay:3.9s;animation-delay:3.9s}.card-poll-14{-webkit-animation-delay:4.2s;animation-delay:4.2s}.card-poll-15{-webkit-animation-delay:4.5s;animation-delay:4.5s}@-webkit-keyframes fadeInRightBig{0%{opacity:0;transform:translateX(1000px)}to{opacity:1;transform:translateX(0)}}@keyframes fadeInRightBig{0%{opacity:0;transform:translateX(1000px)}to{opacity:1;transform:translateX(0)}}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/tests/index.vue?vue&type=template&id=c2021a02&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-container',{staticClass:"d-flex fill-height"},[_c('v-row',{staticClass:"d-flex justify-center ma-6"},[_c('h1',{staticClass:"display-5 text-center font-weight-light"},[_vm._v("SAT simulation")])]),_c('v-row',{staticClass:"d-flex justify-center ma-6",attrs:{"dark":""}},[_c('span',{staticClass:"display-4 text-center font-weight-light"},[_vm._v("Tests with calculator (section 4)")])]),_c('v-row',{staticClass:"d-flex"},[_c('v-col',{staticClass:"d-flex flex-wrap justify-center"},[_vm._l((_vm.calc),function(poll,index){return (_vm.calc.length !== 0)?_c('v-lazy',{key:poll._id,attrs:{"options":{ threshold: .1 },"min-height":"200","transition":"slide-y-transition"},model:{value:(_vm.isActive),callback:function ($$v) {_vm.isActive=$$v},expression:"isActive"}},[_c('coreCard',{staticClass:"ml-5 mb-5",class:"card-poll-" + index,style:(index === 0 && !_vm.authenticated ? 'height: 87%': ''),attrs:{"width":"250","color":index !== 0 && !_vm.authenticated ? "greу" : "green"},scopedSlots:_vm._u([{key:"heading",fn:function(){return [_c('div',{staticClass:"display-1 font-weight-light"},[_vm._v(_vm._s("Modified " + poll.name))]),_c('div',{staticClass:"subtitle-1 font-weight-light"},[_vm._v(_vm._s(poll.type))]),_c('div',{staticClass:"subtitle-1 font-weight-light"},[_vm._v(_vm._s(index !== 0 && !_vm.authenticated ? 'Available for registered users only' : ''))])]},proxy:true}],null,true)},[_c('div',{staticClass:"d-flex flex-column justify-center align-center",staticStyle:{"height":"180px"}},[(_vm.isRootOrIsAdmin)?_c('v-btn',{staticClass:"mx-5 my-3",attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px"},on:{"click":function($event){return _vm.removePoll(poll)}}},[_vm._v("Delete")]):_vm._e(),(_vm.isRootOrIsAdmin)?_c('v-btn',{staticClass:"mx-5 my-3",attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px","to":_vm.urlPollEdit(poll)}},[_vm._v("Edit")]):_vm._e(),_c('v-btn',{staticClass:"mx-5 my-3",staticStyle:{"text-transform":"none","font-size":"1rem"},attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px"},on:{"click":function($event){index !== 0 && !_vm.authenticated ? _vm.alertToRegistation() : _vm.goUrlPollExec(poll)}}},[_vm._v("Start")]),(_vm.isHasPollInLS(poll))?_c('v-btn',{staticClass:"mx-5 my-3",attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px"},on:{"click":function($event){index !== 0 && !_vm.authenticated ? _vm.alertToRegistation() : _vm.goUrlPollContinue(poll)}}},[_vm._v("Continue")]):_vm._e()],1)])],1):_vm._e()}),(_vm.calc.length === 0)?_c('div',[_c('span',{staticClass:"display-3"},[_vm._v("Will be added soon")])]):_vm._e()],2)],1),_c('v-row',{staticClass:"d-flex justify-center ma-6",attrs:{"dark":""}},[_c('span',{staticClass:"display-4 text-center font-weight-light"},[_vm._v("Tests without calculator (section 3)")])]),_c('v-row',{staticClass:"d-flex"},[_c('v-col',{staticClass:"d-flex flex-wrap justify-center"},_vm._l((_vm.noCalc),function(poll,index){return _c('v-lazy',{key:poll._id,attrs:{"options":{ threshold: .1 },"min-height":"200","transition":"slide-y-transition"},model:{value:(_vm.isActive),callback:function ($$v) {_vm.isActive=$$v},expression:"isActive"}},[_c('coreCard',{staticClass:"ml-5 mb-5",class:"card-poll-" + index,style:(index === 0 && !_vm.authenticated ? 'height: 87%': ''),attrs:{"width":"250","color":index !== 0 && !_vm.authenticated ? "grey" : "green"},scopedSlots:_vm._u([{key:"heading",fn:function(){return [_c('div',{staticClass:"display-1 font-weight-light"},[_vm._v(_vm._s("Modified " + poll.name))]),_c('div',{staticClass:"subtitle-1 font-weight-light"},[_vm._v(_vm._s(index !== 0 && !_vm.authenticated ? 'Available for registered users only' : ''))])]},proxy:true}],null,true)},[_c('div',{staticClass:"d-flex flex-column justify-center align-center",staticStyle:{"height":"180px"}},[(_vm.isRootOrIsAdmin)?_c('v-btn',{staticClass:"mx-5 my-3",attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px"},on:{"click":function($event){return _vm.removePoll(poll)}}},[_vm._v("Delete")]):_vm._e(),(_vm.isRootOrIsAdmin)?_c('v-btn',{staticClass:"mx-5 my-3",attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px","to":_vm.urlPollEdit(poll)}},[_vm._v("Edit")]):_vm._e(),_c('v-btn',{staticClass:"mx-5 my-3",staticStyle:{"text-transform":"none","font-size":"1rem"},attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px"},on:{"click":function($event){index !== 0 && !_vm.authenticated ? _vm.alertToRegistation() : _vm.goUrlPollExec(poll)}}},[_vm._v("Start")]),(_vm.isHasPollInLS(poll))?_c('v-btn',{staticClass:"mx-5 my-3",attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px"},on:{"click":function($event){index !== 0 && !_vm.authenticated ? _vm.alertToRegistation() : _vm.goUrlPollContinue(poll)}}},[_vm._v("Continue")]):_vm._e()],1)])],1)}),1)],1),(_vm.personal.length !== 0)?_c('v-row',{staticClass:"d-flex justify-center ma-6",attrs:{"dark":""}},[_c('span',{staticClass:"display-4 text-center font-weight-light"},[_vm._v("Personal tests")])]):_vm._e(),_c('v-row',{staticClass:"d-flex"},[_c('v-col',{staticClass:"d-flex flex-wrap justify-center"},_vm._l((_vm.personal),function(poll,index){return _c('v-lazy',{key:poll._id,attrs:{"options":{ threshold: .1 },"min-height":"200","transition":"slide-y-transition"},model:{value:(_vm.isActive),callback:function ($$v) {_vm.isActive=$$v},expression:"isActive"}},[_c('coreCard',{staticClass:"ml-5 mb-5",class:"card-poll-" + index,style:(index === 0 && !_vm.authenticated ? 'height: 87%': ''),attrs:{"width":"250","color":index !== 0 && !_vm.authenticated ? "grey" : "green"},scopedSlots:_vm._u([{key:"heading",fn:function(){return [_c('div',{staticClass:"display-1 font-weight-light"},[_vm._v(_vm._s(poll.name))]),_c('div',{staticClass:"subtitle-1 font-weight-light"},[_vm._v(_vm._s(index !== 0 && !_vm.authenticated ? 'Available for registered users only' : ''))])]},proxy:true}],null,true)},[_c('div',{staticClass:"d-flex flex-column justify-center align-center",staticStyle:{"height":"180px"}},[(_vm.isRootOrIsAdmin)?_c('v-btn',{staticClass:"mx-5 my-3",attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px"},on:{"click":function($event){return _vm.removePoll(poll)}}},[_vm._v("Delete")]):_vm._e(),(_vm.isRootOrIsAdmin)?_c('v-btn',{staticClass:"mx-5 my-3",attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px","to":_vm.urlPollEdit(poll)}},[_vm._v("Edit")]):_vm._e(),_c('v-btn',{staticClass:"mx-5 my-3",staticStyle:{"text-transform":"none","font-size":"1rem"},attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px"},on:{"click":function($event){index !== 0 && !_vm.authenticated ? _vm.alertToRegistation() : _vm.goUrlPollExec(poll)}}},[_vm._v("Start")]),(_vm.isHasPollInLS(poll))?_c('v-btn',{staticClass:"mx-5 my-3",attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px"},on:{"click":function($event){index !== 0 && !_vm.authenticated ? _vm.alertToRegistation() : _vm.goUrlPollContinue(poll)}}},[_vm._v("Continue")]):_vm._e()],1)])],1)}),1)],1),(_vm.olympics.length !== 0)?_c('v-row',{staticClass:"d-flex justify-center ma-6",attrs:{"dark":""}},[_c('span',{staticClass:"display-4 text-center font-weight-light"},[_vm._v("Math Olympiads")])]):_vm._e(),_c('v-row',{staticClass:"d-flex"},[_c('v-col',{staticClass:"d-flex flex-wrap justify-center"},_vm._l((_vm.olympics),function(poll,index){return _c('v-lazy',{key:poll._id,attrs:{"options":{ threshold: .1 },"min-height":"200","transition":"slide-y-transition"},model:{value:(_vm.isActive),callback:function ($$v) {_vm.isActive=$$v},expression:"isActive"}},[_c('coreCard',{staticClass:"ml-5 mb-5",class:"card-poll-" + index,style:(index === 0 && !_vm.authenticated ? 'height: 87%': ''),attrs:{"width":"250","color":index !== 0 && !_vm.authenticated ? "grey" : "green"},scopedSlots:_vm._u([{key:"heading",fn:function(){return [_c('div',{staticClass:"display-1 font-weight-light"},[_vm._v(_vm._s("Modified " + poll.name))]),_c('div',{staticClass:"subtitle-1 font-weight-light"},[_vm._v(_vm._s(index !== 0 && !_vm.authenticated ? 'Available for registered users only' : ''))])]},proxy:true}],null,true)},[_c('div',{staticClass:"d-flex flex-column justify-center align-center",staticStyle:{"height":"180px"}},[(_vm.isRootOrIsAdmin)?_c('v-btn',{staticClass:"mx-5 my-3",attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px"},on:{"click":function($event){return _vm.removePoll(poll)}}},[_vm._v("Delete")]):_vm._e(),(_vm.isRootOrIsAdmin)?_c('v-btn',{staticClass:"mx-5 my-3",attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px","to":_vm.urlPollEdit(poll)}},[_vm._v("Edit")]):_vm._e(),_c('v-btn',{staticClass:"mx-5 my-3",staticStyle:{"text-transform":"none","font-size":"1rem"},attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px"},on:{"click":function($event){index !== 0 && !_vm.authenticated ? _vm.alertToRegistation() : _vm.goUrlPollExec(poll)}}},[_vm._v("Start")]),(_vm.isHasPollInLS(poll))?_c('v-btn',{staticClass:"mx-5 my-3",attrs:{"outlined":"","rounded":"","color":"deep purple accent-4","width":"100px"},on:{"click":function($event){index !== 0 && !_vm.authenticated ? _vm.alertToRegistation() : _vm.goUrlPollContinue(poll)}}},[_vm._v("Continue")]):_vm._e()],1)])],1)}),1)],1),(!_vm.authenticated)?_c('registrationModal',{attrs:{"status":_vm.saveTestDialog},on:{"changeStatus":_vm.closeDialog}}):_vm._e()],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/tests/index.vue?vue&type=template&id=c2021a02&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// EXTERNAL MODULE: ./store/index.ts + 1 modules
var store = __webpack_require__(5);

// EXTERNAL MODULE: ./components/core/registrationModal.vue + 4 modules
var registrationModal = __webpack_require__(450);

// EXTERNAL MODULE: ./components/core/coreCard.vue + 4 modules
var coreCard = __webpack_require__(324);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/tests/index.vue?vue&type=script&lang=ts&





let testsvue_type_script_lang_ts_Polls_Id = class Polls_Id extends external_vue_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.isActive = false;
    this.saveTestDialog = false;
  }

  isHasPollInLS(poll) {
    if (!this.$nuxt.$isServer) {
      let LSPolls = localStorage.getItem('polls');

      if (LSPolls !== null) {
        LSPolls = JSON.parse(LSPolls);
        return LSPolls[poll._id] !== undefined && !LSPolls[poll._id].complete ? true : false;
      } else return false;
    }
  }

  get personal() {
    return store["pollsStore"].personal;
  }

  get olympics() {
    return store["pollsStore"].olympics;
  }

  get noCalc() {
    return store["pollsStore"].noCalc;
  }

  get calc() {
    return store["pollsStore"].calc;
  }

  get authenticated() {
    return store["auth"].isAuthenticated;
  }

  get isRootOrIsAdmin() {
    return store["auth"].isRoot || store["auth"].isAdmin;
  }

  closeDialog() {
    this.saveTestDialog = false;
  }

  goUrlPollExec(poll) {
    let LSPolls = localStorage.getItem('polls');

    if (LSPolls !== null) {
      LSPolls = JSON.parse(LSPolls);

      if (LSPolls[poll._id] !== undefined) {
        delete LSPolls[poll._id];
        localStorage.setItem('polls', JSON.stringify(LSPolls));
      }
    }

    window.location.replace(`/tests/${poll._id}/exec`); // this.$router.push(`/tests/${poll._id}/exec`)
  }

  goUrlPollContinue(poll) {
    window.location.replace(`/tests/${poll._id}/exec`); // this.$router.push(`/tests/${poll._id}/exec`)
  }

  urlPollEdit(poll) {
    return `/tests/${poll._id}/edit`;
  }

  alertToRegistation() {
    this.saveTestDialog = true;
  }

  removePoll(poll) {
    if (confirm('Вы уверены, что хотите удалить опрос?')) {
      store["pollsStore"].remove(poll._id);
    }
  }

};
testsvue_type_script_lang_ts_Polls_Id = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  transition: 'page',
  components: {
    coreCard: coreCard["a" /* default */],
    registrationModal: registrationModal["a" /* default */]
  },

  beforeMount() {
    store["pollsStore"].init();
  }

})], testsvue_type_script_lang_ts_Polls_Id);
/* harmony default export */ var testsvue_type_script_lang_ts_ = (testsvue_type_script_lang_ts_Polls_Id);
// CONCATENATED MODULE: ./pages/tests/index.vue?vue&type=script&lang=ts&
 /* harmony default export */ var pages_testsvue_type_script_lang_ts_ = (testsvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/VBtn.js
var VBtn = __webpack_require__(81);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(343);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VContainer.js
var VContainer = __webpack_require__(314);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/measurable/index.js
var measurable = __webpack_require__(41);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/toggleable/index.js
var toggleable = __webpack_require__(15);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/directives/intersect/index.js
var intersect = __webpack_require__(103);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/mixins.js
var mixins = __webpack_require__(4);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/helpers.js
var helpers = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VLazy/VLazy.js
// Mixins

 // Directives

 // Utilities



/* harmony default export */ var VLazy = (Object(mixins["a" /* default */])(measurable["a" /* default */], toggleable["a" /* default */]).extend({
  name: 'VLazy',
  directives: {
    intersect: intersect["a" /* default */]
  },
  props: {
    options: {
      type: Object,
      // For more information on types, navigate to:
      // https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API
      default: () => ({
        root: undefined,
        rootMargin: undefined,
        threshold: undefined
      })
    },
    tag: {
      type: String,
      default: 'div'
    },
    transition: {
      type: String,
      default: 'fade-transition'
    }
  },
  computed: {
    styles() {
      return { ...this.measurableStyles
      };
    }

  },
  methods: {
    genContent() {
      const slot = Object(helpers["s" /* getSlot */])(this);
      /* istanbul ignore if */

      if (!this.transition) return slot;
      const children = [];
      if (this.isActive) children.push(slot);
      return this.$createElement('transition', {
        props: {
          name: this.transition
        }
      }, children);
    },

    onObserve(entries, observer, isIntersecting) {
      if (this.isActive) return;
      this.isActive = isIntersecting;
    }

  },

  render(h) {
    return h(this.tag, {
      staticClass: 'v-lazy',
      attrs: this.$attrs,
      directives: [{
        name: 'intersect',
        value: {
          handler: this.onObserve,
          options: this.options
        }
      }],
      on: this.$listeners,
      style: this.styles
    }, [this.genContent()]);
  }

}));
// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(341);

// CONCATENATED MODULE: ./pages/tests/index.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(463)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_testsvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "5362d9c7"
  
)

/* harmony default export */ var tests = __webpack_exports__["default"] = (component.exports);

/* vuetify-loader */






installComponents_default()(component, {VBtn: VBtn["a" /* default */],VCol: VCol["a" /* default */],VContainer: VContainer["a" /* default */],VLazy: VLazy,VRow: VRow["a" /* default */]})


/***/ })

};;
//# sourceMappingURL=index.js.map