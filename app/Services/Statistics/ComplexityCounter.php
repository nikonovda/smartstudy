<?php

namespace App\Services\Statistics;

use App\Poll;
use Illuminate\Database\Eloquent\Collection;

/**
 * 
 */
class ComplexityCounter
{
	
	// function __construct()
	// {
	// 	# code...
	// }
	

	/**
	 * Recalc all hops complexity by poll
	 *  
	 * @param  Poll   $poll [description]
	 * @return [type]       [description]
	 */
	public function recalc(Poll $poll)
	{
		$hops = $poll->hops;

		// Get all answers
		$answers = [];
		foreach ($poll->results as $result) {
			$answers = array_merge($answers, $result->answers);
		}
		$answers = collect($answers);

		// Compose stats
		$allHopsStats = collect([]);
		foreach ($hops as $hop) {
			$hopAnswers = $answers->where('hop_id', $hop->_id);

			$allHopsStats[$hop->_id] = $hopAnswers->map(function ($answer) use ($hop) {
				$option_id = count($answer['options']) > 0 ? $answer['options'][0]['id'] : null;
				return collect([
					'isCorrect' => in_array($option_id, $hop->correctAnswer),
					'time' => array_key_exists('time', $answer) ? $answer['time'] : 0
				]);
			});
		}

		// dd($allHopsStats);
		// Calc complexity and update hops
		$allHopsStats->each(function ($stats, $key) use ($hops) {
			$complexity = 0;
			if ($stats->count() > 0) {
				$correctAnswers = $stats->where('isCorrect', true)->count();
				$correctness = $correctAnswers / $stats->count() * 100;
				
				$fullTime = $stats->reduce(function ($acc, $item) {
					return $acc + $item['time'];
				}, 0);

				$timing = $fullTime / $stats->count();
				if ($timing > 0) {
					// Calc vector
					$complexity = sqrt( $timing**2 + ($correctness - 100)**2 );
				}
				// dump('BEG');
				// dump($correctness);
				// dump($timing);
				// dump($complexity);
			}
			$hop = $hops->find($key);
			$hop->update(['complexity' => $complexity]);
		});
	}
}
