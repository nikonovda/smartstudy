import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'

import Results from '../store/results'
import Poll from '../store/poll'
import ResultsPoll from '../store/resultPoll'
import Polls from '../store/polls'
import Auth from '../store/auth'
import Templates from '../store/templates'
import Lk from '../store/lk'
import App from '../store/app'
import Issues from '../store/issues'

let resultsStore: Results
let pollState: Poll
let resultPollStore: ResultsPoll
let pollsStore: Polls
let auth: Auth
let templates: Templates
let lk: Lk
let app: App
let issues: Issues

function initialiseStores(store: Store<any>): void {
  resultsStore = getModule(Results, store)
  pollState = getModule(Poll, store)
  resultPollStore = getModule(ResultsPoll, store)
  pollsStore = getModule(Polls, store)
  auth = getModule(Auth, store)
  templates = getModule(Templates, store)
  lk = getModule(Lk, store)
  app = getModule(App, store)
  issues = getModule(Issues, store)
}

export {
  initialiseStores,
  resultsStore,
  issues,
  pollState,
  resultPollStore,
  pollsStore,
  auth,
  templates,
  lk,
  app
}