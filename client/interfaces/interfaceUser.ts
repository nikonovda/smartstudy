import { InterfacePoll } from "./interfacePoll";

export interface User {
  _id?: string
  created_at?: string
  email?: string
  group_id?: string | null
  name?: string
  roles?: string[]
  updated_at?: string
  results?: Array<ResultUser>
  statistics?: TypeStatictics
  rating?: number
  rank?: number
  commonStatistics?: CommonStatistics[]
}

interface TypeStatictics {
  [key: string]: any
  math_nocalc: Statistic[]
  math_calc: Statistic[]
  personal: Statistic[]
}

export interface CommonStatistics {
  complexity: string
  created_at: string
  isCorrect: boolean
  recipient_id: null | string
  section: string
  tag: string
  time: number
  user_id: string
}

export interface Statistic {
  [key: string]: any
  complexity: string
  created_at: string
  isCorrect: boolean
  tag: string
  time: number
}

export interface ResultUser {
  _id: string
  answers: Array<Answer>
  created_at: string
  time: number
  poll: InterfacePoll
  poll_id: string
  updated_at: string
  user_id: string
}

export interface Answer {
  hop_id: string
  options: Array<Options>
  time: number
}

export interface Options {
  id: number
  value: null | string
}
