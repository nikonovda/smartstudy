// Общие методы, используемые в компонентах прохождения опроса
// Возможно, на будущее - разнести методы по отдельным миксинам типа WithUserValue, WithMultiple ...
/* eslint no-use-before-define: 0 */

import { Component, Vue, Prop } from 'vue-property-decorator'
import { Hop, Option } from '../../interfaces/interfaceHops'

import { resultPollStore } from '../../store'

@Component({})
export default class MixinHops extends Vue {
  [x: string]: any

  @Prop() readonly hop!: Hop

  userValue: string = ''
  choised: Array<Number> = []

  get hopId (): string {
    return this.hop._id
  }

  // Флаг обязательности заполнения хопа
  get required (): Boolean {
    return this.minChoiseCount > 0
  }

  // Возвращает обтримленный пользовательский ввод
  get trimUserValue (): string {
    return this.userValue !== undefined ? this.userValue.trim() : ''
  }

  // Признак заполненности пользовательского ввода
  get userValueFilled (): Boolean {
    return this.trimUserValue.length > 0
  }

  // Возвращает опцию с пользовательским вводом
  get userOption (): Option {
    return this.hop.options.find(option => option.type === 'user')
  }

  get clearUserValue (): string {
    return this.isChoisedUserOption ? this.trimUserValue : ''
  }

  get isChoisedUserOption (): Boolean {
    return this.userOption ? this.isChoised(this.userOption) : false
  }

  isChoised (option: Option): Boolean {
    return this.choised.includes(option.id)
  }
  // Вызывает событие "changed"
  fireChanged (): void {
    const data = {
      id: this.hopId,
      payload: {
        choised: this.clearChoised,
        userValue: this.clearUserValue
      }
    }
    this.$emit('changed', data)
  }

  // Проверяет опцию на наличие пользовательского ввода
  isUserValue (option: Option) {
    return option.type === 'user'
  }
}
