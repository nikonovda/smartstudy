<?php

namespace App\Services\PollBuilder\Builders;

use App\Poll;

/**
 * 
 */
class PollBuilder
{
	
	protected $source;

	public function make($source)
	{
		$this->source = $source;
		$model = Poll::make();

		$model->_id = array_key_exists('_id', $source) ? $source['_id'] : '0';
		$model->name = $source['name'];
		$model->description = $source['description'];
		$model->type = $source['type'];
		$model->section = $source['section'];
		$model->timeToSolution = $source['timeToSolution'];
		$model->published_at = $source['published_at'];
		$model->closed_at = $source['closed_at'];
		$model->hops = $this->mapHops();

		if ( array_key_exists('user', $source) ) {
			$model->user()->associate($source['user']);
		}
		if ( array_key_exists('updatedByUser', $source) ) {
			$model->updatedByUser()->associate($source['updatedByUser']);
		}
		if ( array_key_exists('recipient', $source) ) {
			$model->recipient()->associate($source['recipient']);
		}

		return $model;
	}

	protected function mapHops()
	{
		$hopIds = [];
		foreach ($this->source['hops'] as $source) {
			$hopIds[] = $source['_id'];
		}
		return $hopIds;
	}
}
