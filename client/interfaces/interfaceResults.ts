import { Option } from './interfaceHops';

export interface Result {
  _id: String
  poll_id: String
  answers: Answer[]
  updated_at: String
  created_at: String
  user_id: string
  time: string
}

export interface Answer {
  hop_id: String,
  options: Option[]
  time: number
}

export interface Option {
  id: Number,
  value: String | null
}