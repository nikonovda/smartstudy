<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PollRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:500',
            'description' => 'required|string|max:5000',
            'type' => 'required|string|in:SAT,olympics',
            'published_at' => 'required|date',
            'closed_at' => 'required|date',
            'section' => 'required|string|in:math_calc,math_nocalc,olympics',
            'timeToSolution' => 'required|string',

            'hops' => 'required|array',
            'hops.*.content' => 'required|string|max:1000',
            'hops.*.image' => 'array',
            'hops.*.correctAnswer' => 'required|array',
            'hops.*.tags' => 'required|string',
            'hops.*.subTags' => 'required|string',
            // 'hops.*.complexity' => 'required|string',
            'hops.*.explanation' => 'required|string|max:5000',
            
            'hops.*.options' => 'required|array',
            'hops.*.options.*.id' => 'required|integer|min:0',
            'hops.*.options.*.value' => 'required|string|max:500',
        ];
    }
}
