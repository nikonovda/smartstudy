<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Poll;
use Faker\Generator as Faker;

$factory->define(Poll::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
		'status' => 'active',
		'description' => $faker->city,
		'type' => 'classic',
		'isPublished' => 'true',
    ];
});
