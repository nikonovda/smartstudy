<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register', 'API\AuthController@register');
Route::post('registerAnonimous', 'API\AuthController@registerAnonimous');
Route::post('login', 'API\AuthController@login');
Route::middleware('auth:api')->group(function () {
	Route::post('logout', 'API\AuthController@logout');
	Route::post('registerAnonimousFinally', 'API\AuthController@registerAnonimousFinally')->name('registerAnonimousFinally');
});


/**
 * Работа с пользователями
 */
Route::middleware(['auth:api', 'role:root'])->group(function () {
	// Показать домены пользователей
	Route::get('users/domain/all', 'UserDomainController@all')->name('users.domain.all');
	// Добавить роль
	Route::patch('users/domain/{user}/addRole', 'UserDomainController@addRole')->name('users.domain.addRole');
	// Удалить роль
	Route::patch('users/domain/{user}/removeRole', 'UserDomainController@removeRole')->name('users.domain.removeRole');
	// Активировать
	Route::patch('users/domain/{user}/activate', 'UserDomainController@activate')->name('users.domain.activate');
	// Деактивировать
	Route::patch('users/domain/{user}/deactivate', 'UserDomainController@deactivate')->name('users.domain.deactivate');
});


/**
 * Работа с профилем пользователя
 */
Route::middleware('auth:api')->group(function () {
	// Route::resource('results', 'ResultController')->except([ 'create', 'edit' ]);
	Route::get('profile', 'ProfileController@show')->name('profile.show');
});
Route::get('tempprofile', 'ProfileController@tempProfile')->name('tempProfile.tempProfile');


/**
 * Работа с опросами
 */
// Скачивание экспортного файла
Route::get('download', 'PollAdminController@onceLoadFile');

Route::get('polls/published', 'PollPublishedController@index')->name('polls.published.index');
Route::get('polls/published/{id}', 'PollPublishedController@show')->name('polls.published.show');

Route::patch('polls/generate/byLastResult', 'PersonalTestController@makeTestByLastUserResult')->middleware('auth:api')->name('polls.generate.byLastResult');

Route::middleware(['auth:api', 'role:admin'])->group(function () {
	Route::get('polls/{poll}/exportResults', 'PollAdminController@export')->name('polls.exportResults');
	Route::post('polls', 'PollAdminController@store')->name('polls.store');
	Route::get('polls/{poll}', 'PollAdminController@show')->name('polls.show');
	Route::patch('polls/{poll}', 'PollAdminController@update')->name('polls.update');
	Route::delete('polls/{poll}', 'PollAdminController@destroy')->name('polls.destroy');

	Route::get('profile/polls', 'PollAdminController@profile')->name('polls.admin.profile');
});
Route::middleware(['auth:api', 'role:root'])->group(function () {
	Route::get('polls', 'PollController@index')->name('polls.index');
});


/**
 * Работа с результатами
 */
Route::post('results/noauth', 'ResultController@storeUnauthorized')->name('results.storeUnauthorized');
Route::patch('results/noauth/{result}', 'ResultController@updateUnauthorized')->name('results.updateUnauthorized');

// Route::get('results/export', 'ResultController@export')->name('results.export');

Route::middleware('auth:api')->group(function () {
	Route::resource('results', 'ResultController')->except([ 'create', 'edit' ]);
});
Route::middleware(['auth:api', 'role:root'])->group(function () {
	#
});




/**
 * Ресурсные роуты для суперадмина
 */
Route::middleware(['auth:api', 'role:root'])->group(function() {
	Route::resource('users', 'UserController')->except([ 'create', 'edit' ]);
});

/**
 * !TEST ROUTES. DELETE ON PRODUCTION!
 */
// Route::get('autobuild/{poll}', 'PollAdminController@show')->name('polls.show');
// Route::get('autobuild/{user}', 'ProfileController@show')->name('asdf.show');
Route::get('/autobuild', function (Request $request) {
    // return abort(403, 'Forbidden');
    return \App\User::registered()->get();
    $user = \App\User::first();
	// $user = \App\User::find('5eb45b7c49530000900049b4'); //Denis
    // return (new \App\Services\PersonalPollBuilder\PersonalPollBuilder)->byLastResultByUser($user);
    $poll = \App\Poll::find('5e6f228c7f89e9369539e8d2');
    $service = new \App\Services\Statistics\StatisticsService();
    // return (new \App\Services\Statistics\StatisticsService)->byUser($user);
	// $statistics = (new \App\Services\Statistics\StatisticsService)->byUser($user);
	// dump($statistics);
    $section = 'math_calc';
	
    // return $service->predictiveOfCorrectAnswers($section, $user);
    return $service->predictiveTimeToResolveTests($section, $user);
    return $service->percentageCorrectAnwsersOfTopicAverage($section);
    return $service->averageTimeSpendToTopicValueUsers($section);
    return $service->percentageOfCorrectAnswersOnEechThemeAverage($section);
    return $service->allDepersonized($user);




	$statistics = (new \App\Services\Statistics\StatisticsService)->byUserCommon($user);
	// dd($statistics);
        $timing = [
            'math_calc' => 0,
            'math_nocalc' => 0,
            'personal' => 0
        ];
        $correctness = [
            'math_calc' => 0,
            'math_nocalc' => 0,
            'personal' => 0
        ];

        $stats = [];
        foreach ($statistics as $answer) {
        	$stats[$answer['section']][] = $answer;
        }

        foreach ($stats as $section => $items) {
        	$count = count($items);
        	if ($count > 0) {
        		foreach ($items as $item) {
	                $timing[$section] += $item['time'];
	                $correctness[$section] += intval($item['isCorrect']);
        		}
	            $timing[$section] = $timing[$section] / $count;
	            $correctness[$section] = $correctness[$section] / $count * 100;
        	}
        }


        auth()->user()->timing = $timing;
        auth()->user()->correctness = $correctness;

        return auth()->user();



    return (new \App\Services\Statistics\StatisticsService)->all();
    return (new \App\Services\Statistics\ComplexityCounter)->recalc($poll);
});
// Route::get('testtoken', 'API\AuthController@registerAnonimous');
Route::get('/autobuild/recalc', function (Request $request) {

    // return abort(403, 'Forbidden');
    $users = \App\User::all();
    foreach ($users as $user) {
        $correctness = [
            'math_calc' => $user->calcRatingBySection('math_calc'),
            'math_nocalc' => $user->calcRatingBySection('math_nocalc'),
            'personal' => $user->calcRatingBySection('personal'),
            'olympics' => $user->calcRatingBySection('olympics')
        ];

        $countCorrectness = [
            'math_calc' => $user->calcCountBySection('math_calc'),
            'math_nocalc' => $user->calcCountBySection('math_nocalc'),
            'personal' => $user->calcCountBySection('personal'),
            'olympics' => $user->calcCountBySection('olympics')
        ];

        $timing = [
            'math_calc' => $user->calcTimingBySection('math_calc'),
            'math_nocalc' => $user->calcTimingBySection('math_nocalc'),
            'personal' => $user->calcTimingBySection('personal'),
            'olympics' => $user->calcTimingBySection('olympics')
        ];

        $counts = [
            'math_calc' => 38,
            'math_nocalc' => 20,
            'personal' => 20,
            'olympics' => 25
        ];
        foreach (['math_calc', 'math_nocalc', 'personal', 'olympics'] as $section) {
            $results = $user->results()->with('poll')->get();
            $count = $results->filter(function ($item) use ($section) {
                return $item->poll->section === $section;
            })->count();
            $correctness[$section . '_percent'] = $count > 0
                ? $countCorrectness[$section] / ($counts[$section] * $count) * 100
                : 0;
        }

        // $correctness['math_calc_percent'] = $correctness['math_calc'] / 38 * 100;
        // $correctness['math_nocalc_percent'] = $correctness['math_nocalc'] / 20 * 100;
        // $correctness['personal_percent'] = $correctness['personal'] / 20 * 100;
        // $correctness['olympics_percent'] = $correctness['olympics'] / 25 * 100;

        $user->correctness = $correctness;
        $user->timing = $timing;

        $user->rating = $user->calcRating();
        $user->save();
        dump('User: ' . $user->name);
        dump('Rating: ' . $user->rating);
        dump($user->correctness);
        dump($user->timing);

    }
});
