import Cookie from 'js-cookie'
import { Request } from 'express-serve-static-core'

export const setToken = (token) => {
  if (process.SERVER_BUILD) return
  localStorage.setItem('token', token)
  Cookie.set('token', token)
}

export const unsetToken = () => {
  if (process.SERVER_BUILD) return
  localStorage.removeItem('token')
  Cookie.remove('token')
}

export const getUserFromCookie = (req: any = {}) => {
  if (!req.headers.cookie) return
  const tokenCookie: string = req.headers.cookie.split(';').find((s: string) => s.trim().startsWith('token='))
  if (!tokenCookie) return
  return tokenCookie.split('=')[1]
}

export const getUserFromLocalStorage = () => {
  const token = localStorage.token
  return token ? token : undefined
}