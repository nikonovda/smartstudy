<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use DesignMyNight\Mongodb\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $collection = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'lastname', 'middlename', 'roles', 'activated_at', 'rating', 'correctness', 'timing', 'correctnessRank', 'timingRank'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'activated_at' => 'datetime'
    ];

    protected $appends = [
        'isAnonimous'
    ];

    // public function group()
    // {
    //     return $this->belongsTo('App\Group');
    // }

    // public function ownedGroup()
    // {
    //     return $this->hasOne('App\Group', 'user_id');
    // }

    public function polls()
    {
        return $this->hasMany('App\Poll');
    }

    public function personalPolls()
    {
        return $this->hasMany('App\Poll', 'recipient_id');
    }

    // public function templates()
    // {
    //     return $this->hasMany('App\Template');
    // }

    public function results()
    {
        return $this->hasMany('App\Result');
    }

    public function getIsAnonimousAttribute()
    {
        return !$this->password;
    }

    public function scopeRegistered($query)
    {
        return $query->where('password', '<>', null);
    }


    public function getCompletedPolls()
    {
        $results = $this->results()->with('poll')->get();
        $polls = $results->map(function ($item) {
            if (!$item->poll) {
                return null;
            }
            $poll = $item->poll;
            $poll->completed_at = $item->created_at->format('Y-m-d H:i:s');
            return $poll;
        });
        return $polls->filter(function ($item) { return !!$item; })->values();
    }

    public function hasRole($role)
    {
        return in_array($role, $this->roles) || in_array('root', $this->roles);
    }

    protected function checkRole($role)
    {
        if (!in_array($role, [ 'admin', 'root' ])) {
            return abort(418, 'Wrong role: ' . $role);
        }
    }

    public function addRole($role)
    {
        $this->checkRole($role);

        if (!$this->hasRole($role)) {
            $updated = $this->roles;
            $updated[] = $role;
            $this->roles = $updated;
            $this->save();
        }
    }

    public function removeRole($role)
    {
        $this->checkRole($role);

        if ($this->hasRole($role)) {
            $filtered = [];
            foreach ($this->roles as $item) {
                if ($item !== $role) $filtered[] = $item;
            }
            $this->roles = $filtered;
            $this->save();
        }
    }


    public function activate()
    {
        if ($this->activated_at) return true;

        $this->activated_at = Carbon::now();
        return $this->save();
    }

    public function deactivate()
    {
        if (!$this->activated_at) return true;

        $this->activated_at = null;
        return $this->save();
    }


    public function calcRating()
    {
        return $this->results->reduce(function ($acc, $item) {
            return $acc += $item->getRatioCorrect();
        }, 0);
    }

    public function calcRatingBySection(string $section)
    {
        // $results = $this->results()->with(['poll' => function ($query) use ($section) {
        //     $query->where('section', $section);
        // }])->get();

        $results = $this->results()->with('poll')->get();
        $results = $results->filter(function ($item) use ($section) {
            return $item->poll->section === $section;
        });

        return $results->reduce(function ($acc, $item) {
            return $acc += $item->getRatioCorrect();
        }, 0);
    }

    public function calcCountBySection(string $section)
    {
        // $results = $this->results()->with(['poll' => function ($query) use ($section) {
        //     $query->where('section', $section);
        // }])->get();

        $results = $this->results()->with('poll')->get();
        $results = $results->filter(function ($item) use ($section) {
            return $item->poll->section === $section;
        });

        return $results->reduce(function ($acc, $item) {
            return $acc += $item->getCorrectCount();
        }, 0);
    }

    public function calcTimingBySection(string $section)
    {
        // $results = $this->results()->with(['poll' => function ($query) use ($section) {
        //     $query->where('section', $section);
        // }])->get();

        $results = $this->results()->with('poll')->get();
        $results = $results->filter(function ($item) use ($section) {
            return $item->poll->section === $section;
        });

        $fullTime = $results->reduce(function ($acc, $item) {
            return $acc += $item->time;
        }, 0);

        return $fullTime <= 0 ? 0 : $fullTime / $results->count();
    }
}
