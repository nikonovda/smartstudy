<?php

namespace App\Services\PollBuilder;

use App\Hop;
use App\Poll;

/**
 * 
 */
class PollSaver
{
	
	protected $fromUpd;
	protected $fromDB;
	protected $build;

	protected $equality;

	function __construct(array $fromUpd)
	{
		$this->fromUpd = $fromUpd;
		$this->initFromDB();
	}

	public function save()
	{
		$this->build();

		$this->saveCascade();
		
		return $this->build['poll']->_id;
		// return $this->fromUpd;
		// return $this->build;
		// return $this->fromDB;
	}

	protected function initFromDB()
	{
		$this->fromDB = [
			'poll' => Poll::whereIn('_id', $this->fromUpd['poll'])
				->get(),
			'hops' => Hop::whereIn('_id', array_keys($this->fromUpd['hops']))
				->get(),
			'options' => collect([])
		];
		foreach ($this->fromDB['hops'] as $hop) {
			foreach ($hop->options as $option) {
				$fetched = new \StdClass($option);
				$fetched->fullId = $hop->_id . '_' . $option['id'];
				$this->fromDB['options']->push($fetched);
			}
		}
	}

	protected function build()
	{
		$model = $this->findFromDB('poll');
		if (!$model) {
			$model = new Poll();
		}
		$model->fill($this->fromUpd['poll']->toArray());
		$this->build['poll'] = $model;


		$this->build['hops'] = collect([]);
		foreach ($this->fromUpd['hops'] as $src) {
			$model = $this->findFromDB('hops', $src->_id);
			if (!$model) {
				$model = new Hop();
			}
			$model->fill($src->toArray());
			$this->build['hops']->push($model);
		}

		return $this->build;
	}

	protected function saveCascade()
	{
		$poll = $this->build['poll'];
		$poll->save();

		$poll->hops()->saveMany($this->build['hops']);

        $poll->sort = $this->build['hops']->pluck('_id')->toArray();
        $poll->save();
        $poll->syncSortedHops();
	}

	protected function findFromDB($value, $id = '')
	{
		if ($value === 'poll') {
			return $this->fromDB['poll']->first();
		} else if ($value === 'hop') {
			return $this->fromDB[$value]->firstWhere('_id', $id);
		} else {
			return $this->fromDB[$value]->firstWhere('id', $id);
		}
	}

	// Не использовать?	
	protected function findFromUpd($value, $id = '')
	{
		switch ($value) {
			case 'poll':
				return $this->fromUpd['poll'];
			case 'hop':
				return $this->fromUpd['hops'][$id] ? $this->fromUpd['hops'][$id]['model'] : null;
			case 'option':
				return $this->fromUpd['options'][$id];			
			default:
				return null;
		}
	}

}
