<?php

namespace App\Http\Controllers;

use App\Poll;
use Illuminate\Http\Request;

class PollPublishedController extends Controller
{
    /**
     * Возвращает коллекцию опубликованных тестов.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Not all published, except not owned personal
        return Poll::published()->get();
    }


    /**
     * Возвращает тест, если он доступен для пользователя.
     *
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Poll::getForUser($id);
    }
}
