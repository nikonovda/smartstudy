import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'

import { auth } from '../store'

import axios from '../plugins/axios'
@Module({
  name: 'polls',
  stateFactory: true,
  namespaced: true
})
export default class Polls extends VuexModule {
  items: any = []

  @Mutation
  SET_POLLS (payload) {
    this.items = payload
  }

  @Mutation
  DELETE_POLL (id) {
    const index = this.items.findIndex(item => item._id === id)
    if (index !== -1) { this.items.splice(index, 1) }
  }

  @Action({ rawError: true })
  async init () {
    await axios.get('/api/polls/published')
      .then((resp) => {
        this.SET_POLLS(resp.data)
      })
      .catch((e) => {
        console.warn(e.data)
        console.warn(e)
      })
  }

  @Action({ rawError: true })
  async getAdminPolls () {
    let url: string
    if(auth.isRoot) url = 'api/polls'
    else if(auth.isAdmin) url = '/api/profile/polls'
    await axios.get(url)
      .then((resp) => {
        this.SET_POLLS(resp.data)
      })
  }

  @Action({ rawError: true })
  async remove (id) {
    await axios.delete('/api/polls/' + id)
      .then(() => {
        alert('Удалено!')
        this.DELETE_POLL(id)
      })
      .catch(() => {
        alert('Ошибка удаления')
      })
  }

  get all () {
    return this.items
  }

  get noCalc () {
   return this.items.filter(item => item.section === 'math_nocalc')
  }
  
  get calc () {
    return this.items.filter(item => item.section === 'math_calc')
  }

  get personal () {
    return this.items.filter(item => item.section === 'personal' && item.recipient_id !== null ? item.recipient_id === auth.user._id : false)
  }

  get olympics () {
    return this.items.filter(item => item.section === 'olympics')
  }

  get byId () {
    return (id: string) => this.items.find(item => item._id === id)
  }

  get activeById () {
    return (id: string) => {
      const item = this.items.find(item => item._id === id)
      return (item === undefined || !item.isPublished || !item.isActive) ? undefined : item
    }
  }
}
