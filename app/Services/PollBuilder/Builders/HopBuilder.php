<?php

namespace App\Services\PollBuilder\Builders;

use App\Hop;

/**
 * 
 */
class HopBuilder
{
	
	protected $source;

	public function make($source)
	{
		$this->source = $source;
		$model = Hop::make();
		$model->_id = $source['_id'];
		$model->origin_id = array_key_exists('origin_id', $source) ? $source['origin_id'] : null;
		$model->content = $source['content'];
		$model->image = $source['image'];
		$model->correctAnswer = $source['correctAnswer'];
		$model->tags = $source['tags'];
		$model->subTags = $source['subTags'];
		$model->complexity = $source['complexity'] ? $source['complexity'] : 0;
		$model->explanation = $source['explanation'];
		$model->options = $source['options'];
		$model->optionIds = $this->mapOptions();

		return $model;
	}

	protected function mapOptions()
	{
		$optionIds = [];
		foreach ($this->source['options'] as $source) {
			$option = $this->source['_id'] . '_' . $source['id'];
			$optionIds[] = $option;
		}
		return $optionIds;
	}
}
