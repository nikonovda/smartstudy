<?php

use Illuminate\Database\Seeder;

class TestPolls extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Poll::class, 6)->create()->each(function ($poll) {
	        $poll->hops()->save(factory(App\Hop::class)->make());
	    });
    }
}
