export interface Group {
  _id?: string
  name?: string
  updated_at?: string
  created_at?: string
  user_id?: string
  members?: Members[]
}

export interface Members {
  _id: string
  name: string
  email: string
  roles: string[]
  updated_at: string
  created_at: string
  group_id: string
}

export interface Users {
  _id: string
  name: string
  email: string
  roles: string[]
  updated_at: string
  created_at: string
  group_id: string
  group: GroupUser
}

export interface GroupUser {
  _id?: string
  name?: string
  updated_at?: string
  created_at?: string
  owner_id?: string
}
