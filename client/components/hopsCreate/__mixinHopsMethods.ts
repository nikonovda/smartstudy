import Vue from 'vue'
import { Component, Prop } from 'vue-property-decorator'
import { Option } from '../../interfaces/interfaceHops'
import { pollState } from '../../store'
import { HopStore } from '../../interfaces/interfaceStore'

@Component({
  props: {
    hopId: {
      type: String,
      required: true
    }
  }
})
export default class MixinHopsMethods extends Vue {
  @Prop({ required: true }) readonly hopId!: string

  optionsInit: Option[] = []

  validationContent = [
    v => !!v.trim() || 'Please enter a question name'
  ]

  get explanation (): string {
    return this.hop.explanation
  }

  set explanation (value: string) {
    this.updateHop('explanation', value)
  }

  get subTags (): string {
    return this.hop.complexity
  }

  set subTags (value: string) {
    this.updateHop('subTags', value)
  }

  get tags (): string {
    return this.hop.tags
  }

  set tags (value) {
    this.updateHop('tags', value)
  }

  get options (): Option[] {
    return this.optionsInit
  }

  set options (value) {
    this.updateHop('options', value)
    this.initOptions()
  }

  get hop (): HopStore {
   return pollState.hopById(this.hopId)
  }

  get image (): string {
    return this.hop.image
  }

  set image (value: string) {
    this.updateHop('image', value)
  }

  get content (): string {
    return this.hop.content
  }

  set content (value: string) {
    this.updateHop('content', value)
  }

  get correctAnswer (): Array<Number> {
    return this.hop.correctAnswer
  }

  set correctAnswer (value: Array<Number>) {
    this.updateHop('correctAnswer', value)
  }

  get optionIds (): number[] {
    return this.hop.options
  }

  set optionIds (value: number[]) {
    this.updateHop('options', value)
    this.initOptions()
  }

  get panelProps () {
    return {
      optionsCount: this.options.length,
    }
  }

  mounted () {
    this.initOptions()
  }

  updateHop (key: string, value: String | String[] | Array<Number> | Option[]) {
    const upd: Object = { _id: this.hopId }
    upd[key] = value
    pollState.updateHop(upd)
  }

  initOptions (): void {
    this.optionsInit = this.optionIds.map((optionId) => {
      const id = this.hopId + '_' + optionId
      const option = pollState.optionById(id)
      if (!option) {
        return { id: 0, value: 'ERROR', type: 'simple', image: '' }
      }
      return {
        id: option.id,
        value: option.value,
        type: option.type,
        image: option.image
      }
    })
  }

  toValue (some: any) {
    return Array.isArray(some) ? some[0] : some
  }

  toArray (some: String[] | Number[] | String | Number): Array<String | Number> {
    return Array.isArray(some)
      ? some
      : !some
        ? []
        : [ some ]
  }
}
