exports.ids = [5];
exports.modules = {

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VCardActions; });
/* unused harmony export VCardSubtitle */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return VCardText; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return VCardTitle; });
/* harmony import */ var _VCard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(298);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);


const VCardActions = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__actions');
const VCardSubtitle = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__subtitle');
const VCardText = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__text');
const VCardTitle = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__title');

/* unused harmony default export */ var _unused_webpack_default_export = ({
  $_vuetify_subcomponents: {
    VCard: _VCard__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"],
    VCardActions,
    VCardSubtitle,
    VCardText,
    VCardTitle
  }
});

/***/ }),

/***/ 322:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(345);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("0db8c820", content, true, context)
};

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreCard.vue?vue&type=template&id=0a0f4055&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-card',_vm._b({staticClass:"v-card--material pa-3",class:_vm.classes},'v-card',_vm.$attrs,false),[_c('div',{staticClass:"d-flex grow flex-wrap"},[(_vm.avatar)?_c('v-avatar',{staticClass:"mx-auto v-card--material__avatar elevation-6",attrs:{"size":"128","color":"grey"}},[_c('v-img',{attrs:{"src":_vm.avatar}})],1):(_vm.$slots.image || _vm.place || _vm.icon || _vm.$slots.heading)?_c('v-sheet',{staticClass:"text-start v-card--material__heading mb-n6",class:{ 'pa-7': !_vm.$slots.image && !_vm.place },style:(_vm.place ? "display: flex;" : ""),attrs:{"color":_vm.color,"max-height":_vm.icon || _vm.place ? 90 : undefined,"width":_vm.icon || _vm.place ? '' : '100%',"elevation":_vm.place ? 0 : 6,"dark":""}},[(_vm.$slots.heading)?_vm._t("heading"):(_vm.$slots.image)?_vm._t("image"):(_vm.title && !_vm.icon)?_c('div',{staticClass:"display-1 font-weight-light",domProps:{"textContent":_vm._s(_vm.title)}}):(_vm.icon)?_c('v-icon',{attrs:{"size":"32"},domProps:{"textContent":_vm._s(_vm.icon)}}):(_vm.place)?[_c('div',{staticClass:"text-center",style:("padding: 15px 20px; background-color:" + _vm.colorPlace + "; border-radius: 4px; width: 115px")},[_c('span',{staticClass:"display-1"},[_vm._v("Rank:")]),_c('h1',{staticClass:"display-3"},[_vm._v(_vm._s(_vm.place))])]),_c('div',{staticClass:"text-center ml-4",style:("padding: 15px 20px; background-color:" + _vm.colorPlace + "; border-radius: 4px; width: 115px")},[_c('span',{staticClass:"display-1"},[_vm._v("Top:")]),_c('h1',{staticClass:"display-3"},[_vm._v(_vm._s(_vm.placePercentage + "%"))])])]:_vm._e(),(_vm.text)?_c('div',{staticClass:"headline font-weight-thin",domProps:{"textContent":_vm._s(_vm.text)}}):_vm._e()],2):_vm._e(),(_vm.$slots['after-heading'])?_c('div',{staticClass:"justify-center align-center",staticStyle:{"height":"calc(100% - 85px)"}},[_vm._t("after-heading")],2):(_vm.icon && _vm.title)?_c('div',{staticClass:"ml-4"},[_c('div',{staticClass:"card-title font-weight-light",domProps:{"textContent":_vm._s(_vm.title)}})]):_vm._e()],1),_vm._t("default"),(_vm.$slots.actions)?[_c('v-divider',{staticClass:"mt-2"}),_c('v-card-actions',{staticClass:"pb-0"},[_vm._t("actions")],2)]:_vm._e()],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/core/coreCard.vue?vue&type=template&id=0a0f4055&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreCard.vue?vue&type=script&lang=ts&


let coreCardvue_type_script_lang_ts_CoreCard = class CoreCard extends external_vue_property_decorator_["Vue"] {
  get classes() {
    return {
      'v-card--material--has-heading': this.hasHeading
    };
  }

  get hasHeading() {
    return Boolean(this.$slots.heading || this.title || this.icon);
  }

  get hasAltHeading() {
    return Boolean(this.$slots.heading || this.title && this.icon);
  }

};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "avatar", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: 'success'
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "color", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: 'success'
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "colorPlace", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "icon", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Boolean,
  default: false
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "image", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "text", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "title", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "place", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "placePercentage", void 0);

coreCardvue_type_script_lang_ts_CoreCard = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'CoreCard'
})], coreCardvue_type_script_lang_ts_CoreCard);
/* harmony default export */ var coreCardvue_type_script_lang_ts_ = (coreCardvue_type_script_lang_ts_CoreCard);
// CONCATENATED MODULE: ./components/core/coreCard.vue?vue&type=script&lang=ts&
 /* harmony default export */ var core_coreCardvue_type_script_lang_ts_ = (coreCardvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VAvatar/VAvatar.js
var VAvatar = __webpack_require__(96);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(298);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(296);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(301);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(50);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(72);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSheet/VSheet.js
var VSheet = __webpack_require__(24);

// CONCATENATED MODULE: ./components/core/coreCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(344)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_coreCardvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "f4daf264"
  
)

/* harmony default export */ var coreCard = __webpack_exports__["a"] = (component.exports);

/* vuetify-loader */








installComponents_default()(component, {VAvatar: VAvatar["a" /* default */],VCard: VCard["a" /* default */],VCardActions: components_VCard["a" /* VCardActions */],VDivider: VDivider["a" /* default */],VIcon: VIcon["a" /* default */],VImg: VImg["a" /* default */],VSheet: VSheet["a" /* default */]})


/***/ }),

/***/ 334:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(371);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("91e81038", content, true, context)
};

/***/ }),

/***/ 335:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(373);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("09b645a2", content, true, context)
};

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(131);
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _util_mergeData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);



 // no xs

const breakpoints = ['sm', 'md', 'lg', 'xl'];
const ALIGNMENT = ['start', 'end', 'center'];

function makeProps(prefix, def) {
  return breakpoints.reduce((props, val) => {
    props[prefix + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = def();
    return props;
  }, {});
}

const alignValidator = str => [...ALIGNMENT, 'baseline', 'stretch'].includes(str);

const alignProps = makeProps('align', () => ({
  type: String,
  default: null,
  validator: alignValidator
}));

const justifyValidator = str => [...ALIGNMENT, 'space-between', 'space-around'].includes(str);

const justifyProps = makeProps('justify', () => ({
  type: String,
  default: null,
  validator: justifyValidator
}));

const alignContentValidator = str => [...ALIGNMENT, 'space-between', 'space-around', 'stretch'].includes(str);

const alignContentProps = makeProps('alignContent', () => ({
  type: String,
  default: null,
  validator: alignContentValidator
}));
const propMap = {
  align: Object.keys(alignProps),
  justify: Object.keys(justifyProps),
  alignContent: Object.keys(alignContentProps)
};
const classMap = {
  align: 'align',
  justify: 'justify',
  alignContent: 'align-content'
};

function breakpointClass(type, prop, val) {
  let className = classMap[type];

  if (val == null) {
    return undefined;
  }

  if (prop) {
    // alignSm -> Sm
    const breakpoint = prop.replace(type, '');
    className += `-${breakpoint}`;
  } // .align-items-sm-center


  className += `-${val}`;
  return className.toLowerCase();
}

const cache = new Map();
/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_1___default.a.extend({
  name: 'v-row',
  functional: true,
  props: {
    tag: {
      type: String,
      default: 'div'
    },
    dense: Boolean,
    noGutters: Boolean,
    align: {
      type: String,
      default: null,
      validator: alignValidator
    },
    ...alignProps,
    justify: {
      type: String,
      default: null,
      validator: justifyValidator
    },
    ...justifyProps,
    alignContent: {
      type: String,
      default: null,
      validator: alignContentValidator
    },
    ...alignContentProps
  },

  render(h, {
    props,
    data,
    children
  }) {
    // Super-fast memoization based on props, 5x faster than JSON.stringify
    let cacheKey = '';

    for (const prop in props) {
      cacheKey += String(props[prop]);
    }

    let classList = cache.get(cacheKey);

    if (!classList) {
      classList = []; // Loop through `align`, `justify`, `alignContent` breakpoint props

      let type;

      for (type in propMap) {
        propMap[type].forEach(prop => {
          const value = props[prop];
          const className = breakpointClass(type, prop, value);
          if (className) classList.push(className);
        });
      }

      classList.push({
        'no-gutters': props.noGutters,
        'row--dense': props.dense,
        [`align-${props.align}`]: props.align,
        [`justify-${props.justify}`]: props.justify,
        [`align-content-${props.alignContent}`]: props.alignContent
      });
      cache.set(cacheKey, classList);
    }

    return h(props.tag, Object(_util_mergeData__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(data, {
      staticClass: 'row',
      class: classList
    }), children);
  }

}));

/***/ }),

/***/ 343:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(131);
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _util_mergeData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);



 // no xs

const breakpoints = ['sm', 'md', 'lg', 'xl'];

const breakpointProps = (() => {
  return breakpoints.reduce((props, val) => {
    props[val] = {
      type: [Boolean, String, Number],
      default: false
    };
    return props;
  }, {});
})();

const offsetProps = (() => {
  return breakpoints.reduce((props, val) => {
    props['offset' + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = {
      type: [String, Number],
      default: null
    };
    return props;
  }, {});
})();

const orderProps = (() => {
  return breakpoints.reduce((props, val) => {
    props['order' + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = {
      type: [String, Number],
      default: null
    };
    return props;
  }, {});
})();

const propMap = {
  col: Object.keys(breakpointProps),
  offset: Object.keys(offsetProps),
  order: Object.keys(orderProps)
};

function breakpointClass(type, prop, val) {
  let className = type;

  if (val == null || val === false) {
    return undefined;
  }

  if (prop) {
    const breakpoint = prop.replace(type, '');
    className += `-${breakpoint}`;
  } // Handling the boolean style prop when accepting [Boolean, String, Number]
  // means Vue will not convert <v-col sm></v-col> to sm: true for us.
  // Since the default is false, an empty string indicates the prop's presence.


  if (type === 'col' && (val === '' || val === true)) {
    // .col-md
    return className.toLowerCase();
  } // .order-md-6


  className += `-${val}`;
  return className.toLowerCase();
}

const cache = new Map();
/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_1___default.a.extend({
  name: 'v-col',
  functional: true,
  props: {
    cols: {
      type: [Boolean, String, Number],
      default: false
    },
    ...breakpointProps,
    offset: {
      type: [String, Number],
      default: null
    },
    ...offsetProps,
    order: {
      type: [String, Number],
      default: null
    },
    ...orderProps,
    alignSelf: {
      type: String,
      default: null,
      validator: str => ['auto', 'start', 'end', 'center', 'baseline', 'stretch'].includes(str)
    },
    tag: {
      type: String,
      default: 'div'
    }
  },

  render(h, {
    props,
    data,
    children,
    parent
  }) {
    // Super-fast memoization based on props, 5x faster than JSON.stringify
    let cacheKey = '';

    for (const prop in props) {
      cacheKey += String(props[prop]);
    }

    let classList = cache.get(cacheKey);

    if (!classList) {
      classList = []; // Loop through `col`, `offset`, `order` breakpoint props

      let type;

      for (type in propMap) {
        propMap[type].forEach(prop => {
          const value = props[prop];
          const className = breakpointClass(type, prop, value);
          if (className) classList.push(className);
        });
      }

      const hasColClasses = classList.some(className => className.startsWith('col-'));
      classList.push({
        // Default to .col if no other col-{bp}-* classes generated nor `cols` specified.
        col: !hasColClasses || !props.cols,
        [`col-${props.cols}`]: props.cols,
        [`offset-${props.offset}`]: props.offset,
        [`order-${props.order}`]: props.order,
        [`align-self-${props.alignSelf}`]: props.alignSelf
      });
      cache.set(cacheKey, classList);
    }

    return h(props.tag, Object(_util_mergeData__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(data, {
      class: classList
    }), children);
  }

}));

/***/ }),

/***/ 344:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(322);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 345:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-card--material__avatar{position:relative;top:-64px;margin-bottom:-32px}.v-card--material__heading{position:relative;top:-40px;transition:.3s ease;z-index:1}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreChartCard.vue?vue&type=template&id=0a3012ae&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('coreCard',_vm._g(_vm._b({staticClass:"v-card--material-chart"},'coreCard',_vm.$attrs,false),_vm.$listeners),[_vm._t("title"),(_vm.type === "Line")?_c('line-chart',{attrs:{"chart-data":_vm.data,"options":_vm.options,"height":_vm.heigntChart}}):_vm._e(),(_vm.type === "Bar")?_c('bar-chart',{attrs:{"chart-data":_vm.data,"options":_vm.options,"height":_vm.heigntChart}}):_vm._e(),(_vm.type === "Time-Bar")?_c('time-bar-chart',{attrs:{"chart-data":_vm.data,"options":_vm.options,"height":_vm.heigntChart}}):_vm._e(),(_vm.type === "Time-Line")?_c('time-line-chart',{attrs:{"chart-data":_vm.data,"options":_vm.options,"height":_vm.heigntChart}}):_vm._e(),(_vm.type === "Radar")?_c('Radar',{attrs:{"chart-data":_vm.data,"options":_vm.options,"height":_vm.heigntChart}}):_vm._e(),_vm._t("reveal-actions",null,{"slot":"reveal-actions"}),_vm._t("actions",null,{"slot":"actions"})],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/core/coreChartCard.vue?vue&type=template&id=0a3012ae&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// EXTERNAL MODULE: ./components/core/coreCard.vue + 4 modules
var coreCard = __webpack_require__(324);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreChartCard.vue?vue&type=script&lang=ts&



let coreChartCardvue_type_script_lang_ts_CoreChartCard = class CoreChartCard extends external_vue_property_decorator_["Vue"] {};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Object,
  default: () => ({})
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "data", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Array,
  default: () => []
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "eventHandlers", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Object,
  default: () => ({})
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "options", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "ratio", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Array,
  default: () => []
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "responsiveOptions", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  required: true,
  validator: v => ['Bar', 'Line', 'Time-Bar', 'Time-Line', 'Radar'].includes(v)
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "type", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Number,
  default: 100
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "heigntChart", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "backgroundChart", void 0);

coreChartCardvue_type_script_lang_ts_CoreChartCard = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'CoreChartCard',
  inheritAttrs: false,
  components: {
    coreCard: coreCard["a" /* default */]
  }
})], coreChartCardvue_type_script_lang_ts_CoreChartCard);
/* harmony default export */ var coreChartCardvue_type_script_lang_ts_ = (coreChartCardvue_type_script_lang_ts_CoreChartCard);
// CONCATENATED MODULE: ./components/core/coreChartCard.vue?vue&type=script&lang=ts&
 /* harmony default export */ var core_coreChartCardvue_type_script_lang_ts_ = (coreChartCardvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// CONCATENATED MODULE: ./components/core/coreChartCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(372)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_coreChartCardvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "a027eb80"
  
)

/* harmony default export */ var coreChartCard = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(298);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/Card.vue?vue&type=script&lang=js&

/* harmony default export */ var Cardvue_type_script_lang_js_ = ({
  name: 'Card',
  extends: VCard["a" /* default */]
});
// CONCATENATED MODULE: ./components/core/Card.vue?vue&type=script&lang=js&
 /* harmony default export */ var core_Cardvue_type_script_lang_js_ = (Cardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// CONCATENATED MODULE: ./components/core/Card.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_Cardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "48139ca2"
  
)

/* harmony default export */ var Card = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(334);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 371:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-card--material-stats{display:flex;flex-wrap:wrap;position:relative}.v-card--material-stats>div:first-child{justify-content:space-between}.v-card--material-stats .v-card{border-radius:4px;flex:0 1 auto}.v-card--material-stats .v-card__text{display:inline-block;flex:1 0 calc(100% - 120px);position:absolute;top:0;right:0;width:100%}.v-card--material-stats .v-card__actions{flex:1 0 100%}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 372:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreChartCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(335);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreChartCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreChartCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreChartCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreChartCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreChartCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 373:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-card--material-chart p{color:#999}.v-card--material-chart .v-card--material__heading{max-height:185px}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreStatsCard.vue?vue&type=template&id=8d7c3be4&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('coreCard',_vm._g(_vm._b({staticClass:"v-card--material-stats",attrs:{"icon":_vm.icon},scopedSlots:_vm._u([{key:"after-heading",fn:function(){return [_c('div',{staticClass:"ml-auto text-right"},[_c('div',{staticClass:"display-2 grey--text font-weight-light",domProps:{"textContent":_vm._s(_vm.title)}}),_c('h3',{staticClass:"display-3 font-weight-light text--primary"},[_vm._v(_vm._s(_vm.value)+" "),_c('small',[_vm._v(" "+_vm._s(_vm.smallValue)+" ")])])])]},proxy:true}])},'coreCard',_vm.$attrs,false),_vm.$listeners),[_c('v-col',{staticClass:"px-0",attrs:{"cols":"12"}},[_c('v-divider')],1),_c('v-icon',{staticClass:"ml-2 mr-1",attrs:{"color":_vm.subIconColor,"size":"16"}},[_vm._v(_vm._s(_vm.subIcon))]),_c('span',{staticClass:"caption grey--text font-weight-light",class:_vm.subTextColor,domProps:{"textContent":_vm._s(_vm.subText)}})],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/core/coreStatsCard.vue?vue&type=template&id=8d7c3be4&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// EXTERNAL MODULE: ./components/core/Card.vue + 2 modules
var Card = __webpack_require__(361);

// EXTERNAL MODULE: ./components/core/coreCard.vue + 4 modules
var coreCard = __webpack_require__(324);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreStatsCard.vue?vue&type=script&lang=ts&




let coreStatsCardvue_type_script_lang_ts_ComponentName = class ComponentName extends external_vue_property_decorator_["Vue"] {};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  required: true
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "icon", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "subIcon", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "subIconColor", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "subTextColor", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "subText", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "title", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "value", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "smallValue", void 0);

coreStatsCardvue_type_script_lang_ts_ComponentName = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'CoreStatsCard',
  inheritAttrs: false,
  components: {
    coreCard: coreCard["a" /* default */]
  },
  props: { ...Card["a" /* default */].props
  }
})], coreStatsCardvue_type_script_lang_ts_ComponentName);
/* harmony default export */ var coreStatsCardvue_type_script_lang_ts_ = (coreStatsCardvue_type_script_lang_ts_ComponentName);
// CONCATENATED MODULE: ./components/core/coreStatsCard.vue?vue&type=script&lang=ts&
 /* harmony default export */ var core_coreStatsCardvue_type_script_lang_ts_ = (coreStatsCardvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(343);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(301);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(50);

// CONCATENATED MODULE: ./components/core/coreStatsCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(370)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_coreStatsCardvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "48bea601"
  
)

/* harmony default export */ var coreStatsCard = __webpack_exports__["a"] = (component.exports);

/* vuetify-loader */




installComponents_default()(component, {VCol: VCol["a" /* default */],VDivider: VDivider["a" /* default */],VIcon: VIcon["a" /* default */]})


/***/ }),

/***/ 449:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(505);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("60287856", content, true, context)
};

/***/ }),

/***/ 494:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/ForAll.98ef4c6.svg";

/***/ }),

/***/ 495:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/MachineLearning.d0b7f94.svg";

/***/ }),

/***/ 496:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/howWork1.1527076.svg";

/***/ }),

/***/ 497:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/howWork2.cbd9986.svg";

/***/ }),

/***/ 498:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/howWork3.0b179f2.svg";

/***/ }),

/***/ 499:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/about.3075301.svg";

/***/ }),

/***/ 500:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/sheme.338a05f.svg";

/***/ }),

/***/ 501:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/upgrade.f857631.svg";

/***/ }),

/***/ 502:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/Compete.77e972b.svg";

/***/ }),

/***/ 503:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/collage.4c97da2.svg";

/***/ }),

/***/ 504:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(449);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 505:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(136);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(506);
exports = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
exports.push([module.i, "::-webkit-scrollbar{width:0}.list-unstyled{padding-left:0;list-style:none}.center{text-align:center;text-align:-moz-center;text-align:-webkit-center}.button-main{box-shadow:0 -8px 83px -9px #ee5050;-webkit-animation:fadeInRight 1s;animation:fadeInRight 1s}.button-main:hover{transform:translateY(-10px)}.about-main{display:inline-block;word-wrap:break-word;text-align:center;width:300px;color:hsla(0,0%,100%,.7)!important;-webkit-animation:fade 1s;animation:fade 1s}.title-bar{font-size:1.25rem;color:#fff}.wrapper{background-color:#0648b3!important}.wrapper-adaptive,.wrapper-formula{background-color:#fff!important;padding:80px 0}.wrapper-adaptive .adaptive-education{border-radius:30px!important}.wrapper-adaptive .adaptive-education__title{justify-content:center;height:150px}.wrapper-adaptive .adaptive-education__text{height:300px}.wrapper-pricing-plans{background-color:#0648b3!important;padding-bottom:75px!important}.wrapper-pricing-plans th{text-align:left;border-right:1px solid rgba(0,0,0,.16078)}.wrapper-pricing-plans th:last-child{border-right:none}.wrapper-pricing-plans td{border-right:1px solid rgba(0,0,0,.16078)}.wrapper-pricing-plans td:last-child{border-right:none}.wrapper-pricing-plans .card-pricing{box-shadow:0 2px 48px 0 rgba(0,0,0,.13);border-radius:10px;background-color:#fff;margin-top:15px;margin-bottom:15px}.wrapper-pricing-plans .card-pricing .pricing-title{margin-bottom:40px}.wrapper-pricing-plans .card-pricing .pricing-title .pricing-subtitle{color:#333;font-size:30px;margin-bottom:10px;margin-top:20px;min-height:54px}.wrapper-pricing-plans .card-pricing .pricing-description{font-size:1rem;padding:5px 55px;font-weight:500;margin-bottom:0;line-height:2}.wrapper-pricing-plans .card-pricing td,.wrapper-pricing-plans .card-pricing th{width:20%;padding:0 20px}.wrapper-pricing-plans .card-pricing thead{margin-top:10px}.wrapper-pricing-plans .card-pricing thead tr th{padding-top:10px}.wrapper-pricing-plans .card-pricing thead th{background-color:#fff;position:-webkit-sticky;position:sticky;top:0}.wrapper-pricing-plans .card-pricing thead th,.wrapper-pricing-plans thead{text-align:center;border-top-left-radius:10px;border-bottom-left-radius:10px}.wrapper-pricing-plans tbody{margin-bottom:10px}.wrapper-pricing-plans tbody tr{border-top:1px solid rgba(0,0,0,.16078)}.wrapper-pricing-plans tbody tr td{text-align:center;line-height:2.75;font-size:12px;height:250px}.wrapper table{border-collapse:collapse}.wrapper table trtd:first-child{width:7%}.wrapper-about,.wrapper-why-apative{background-color:#fff!important;padding:80px 0}.wrapper-why-apative .info-block{max-width:360px;border-radius:.25rem;transition:all .5s;display:flex;flex-direction:column}.wrapper-why-apative .info-block__icon-shape{padding:12px;align-self:center;text-align:center;display:inline-flex;align-items:center;justify-content:center;border-radius:50%;width:5rem;height:5rem;transition:transform .4s,box-shadow .4s}.wrapper-why-apative .info-block__info-title{margin:25px 0 15px;font-size:1.3rem;font-family:inherit;font-weight:300;line-height:1.5}.wrapper-why-apative .info-block__description{font-size:1.2rem;opacity:.8!important;font-weight:351;line-height:1.5}.wrapper-description,.wrapper-howWork{background-color:#fff!important}.wrapper__container{max-width:1294px}.icon-shape-lg{width:5.5rem;height:5.5rem;color:#ee5050;text-align:center;display:inline-flex;align-items:center;justify-content:center;transition:all .2s ease 0s;background-color:#fff!important;margin-bottom:1.5rem!important;box-shadow:0 1rem 3rem rgba(13,20,49,.176)!important;border-radius:50%!important;border-color:#eaedf2!important}.lead{font-size:1.5rem;font-weight:300}.logo-text{-webkit-animation:fadeInDown 1s;animation:fadeInDown 1s}.pattern{position:absolute;bottom:0;width:100%;background-image:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");background-repeat:repeat-x;background-position:bottom;background-size:1450px 160px;height:0;padding:0 0 140px}.pattern.bottom{bottom:-10px;transform:rotate(180deg)}.pattern.top{top:-10px}@-webkit-keyframes fade{0%{opacity:0}to{opacity:1}}@keyframes fade{0%{opacity:0}to{opacity:1}}@-webkit-keyframes fadeInDown{0%{opacity:0;transform:translateY(-20px)}to{opacity:1;transform:translateY(0)}}@keyframes fadeInDown{0%{opacity:0;transform:translateY(-20px)}to{opacity:1;transform:translateY(0)}}@-webkit-keyframes fadeInRight{0%{opacity:0;transform:translateX(40px)}to{opacity:1;transform:translateX(0)}}@keyframes fadeInRight{0%{opacity:0;transform:translateX(40px)}to{opacity:1;transform:translateX(0)}}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 506:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/wave.14db5dd.svg";

/***/ }),

/***/ 507:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(508);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(10).default("bb35a8d6", content, true)

/***/ }),

/***/ 508:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".theme--light.v-footer{background-color:#f5f5f5;color:rgba(0,0,0,.87)}.theme--dark.v-footer{background-color:#272727;color:#fff}.v-sheet.v-footer{border-radius:0}.v-sheet.v-footer:not(.v-sheet--outlined){box-shadow:0 0 0 0 rgba(0,0,0,.2),0 0 0 0 rgba(0,0,0,.14),0 0 0 0 rgba(0,0,0,.12)}.v-sheet.v-footer.v-sheet--shaped{border-radius:24px 0}.v-footer{align-items:center;display:flex;flex:0 1 auto!important;flex-wrap:wrap;padding:6px 16px;position:relative;transition-duration:.2s;transition-property:background-color,left,right;transition-timing-function:cubic-bezier(.4,0,.2,1)}.v-footer:not([data-booted=true]){transition:none!important}.v-footer--absolute,.v-footer--fixed{z-index:3}.v-footer--absolute{position:absolute}.v-footer--absolute:not(.v-footer--inset){width:100%}.v-footer--fixed{position:fixed}.v-footer--padless{padding:0}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 519:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/index.vue?vue&type=template&id=073f46fa&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-app',[_c('v-app-bar',{attrs:{"id":"main--app-bar","app":"","hide-on-scroll":"","color":"#0648b3","flat":"","height":"75"}},[_c('v-container',{staticClass:"wrapper__container"},[_c('div',{staticClass:"d-flex align-center"},[_c('span',{staticClass:"title-bar"},[_vm._v("Adaptive Education")]),_c('v-spacer'),_c('v-btn',{staticClass:"ma-5",attrs:{"dark":"","text":"","depressed":"","rounded":"","to":"/login"}},[_vm._v("Login")]),_c('v-btn',{staticClass:"ma-5 mr-0",attrs:{"dark":"","color":"#ee5050","rounded":"","to":"/registration"}},[_vm._v("Registration")])],1)])],1),_c('v-main',{staticClass:"wrapper",staticStyle:{"height":"800px"},attrs:{"tag":"section"}},[_c('v-container',{staticClass:"wrapper__container"},[_c('v-row',{staticClass:"d-flex",attrs:{"no-gutters":""}},[_c('v-col',{staticClass:"d-flex flex-column align-center",staticStyle:{"height":"500px","place-content":"center"},attrs:{"cols":"12"}},[_c('h1',{staticClass:"white--text logo-text text-center",staticStyle:{"font-weight":"700","font-size":"5rem !important"}},[_vm._v("Adaptive Education")]),_c('span',{staticClass:"display-2 about-main"},[_vm._v("Empowering Your Education With AI and Machine Learning")]),_c('v-btn',{staticClass:"button-main mt-10",staticStyle:{"font-size":"1.8rem","background-color":"#ee5050"},attrs:{"rounded":"","text":"","color":"white","min-height":"70px","min-width":"255px"},on:{"click":function($event){return _vm.generateNewAnonimousUser()}}},[_vm._v("Start")])],1)],1)],1),_c('div',{staticClass:"pattern bottom"})],1),_c('v-main',{staticClass:"wrapper-description",attrs:{"tag":"section"}},[_c('v-container',{staticClass:"wrapper__container"},[_c('v-row',{staticClass:"d-flex",attrs:{"no-gutters":""}},[_c('v-col',{staticClass:"d-flex flex-column align-center mb-12",attrs:{"cols":"12"}},[_c('h2',{staticClass:"display-4 text-center mb-5 mb-lg-7"},[_vm._v("What is Adaptive Education?")])])],1),_c('v-row',{staticClass:"mb-10 d-flex"},[_c('v-col',{staticStyle:{"align-self":"center"},attrs:{"md":"5","cols":"12"}},[_c('h2',{staticClass:"font-weight-bold mb-4"},[_vm._v("Personalized Learning For All")]),_c('p',{staticClass:"lead"},[_vm._v("A test prep and study tool for math that responds in real-time to each student’s strengths and weaknesses, Adaptive Education is a learning ecosystem for the 21st century.")])]),_c('v-col',{staticClass:"ml-md-auto",attrs:{"md":"6","cols":"12"}},[_c('img',{attrs:{"src":__webpack_require__(494)}})])],1),_c('v-row',{staticClass:"mb-10 d-flex",class:_vm.$vuetify.breakpoint.mdAndUp ? "" : "flex-wrap-reverse"},[_c('v-col',{staticClass:"mr-md-auto",attrs:{"md":"6","cols":"12"}},[_c('img',{attrs:{"src":__webpack_require__(495)}})]),_c('v-col',{staticStyle:{"align-self":"center"},attrs:{"md":"5","cols":"12"}},[_c('h2',{staticClass:"font-weight-bold mb-4"},[_vm._v("Proprietary Machine Learning Engine")]),_c('p',{staticClass:"lead"},[_vm._v("Using a proprietary machine learning engine, the Adaptive Education platform creates a study program matched to each user’s unique profile. Programs update dynamically in line with a student’s progress, so learning time is always spent where it is needed most.")]),_c('p',{staticClass:"lead"},[_vm._v("Whether prepping for college admissions tests, training for various math competitions/Olympiads, or simply striving for the top grades in class, Adaptive Education is the tool for learning acceleration.")])])],1)],1)],1),_c('v-main',{staticClass:"wrapper-howWork",attrs:{"tag":"section"}},[_c('v-container',{staticClass:"wrapper__container"},[_c('v-row',{staticClass:"d-flex",attrs:{"no-gutters":""}},[_c('v-col',{staticClass:"d-flex flex-column align-center mb-12",attrs:{"cols":"12"}},[_c('h2',{staticClass:"display-4 text-center mb-5 mb-lg-7"},[_vm._v("How Does Adaptive Education Work?")])])],1),_c('v-row',{staticClass:"mb-10 d-flex"},[_c('v-col',{staticStyle:{"align-self":"center"},attrs:{"md":"5","cols":"12"}},[_c('p',{staticClass:"lead mt-md-12"},[_vm._v("The Adaptive Education platform works using a continuous feedback loop. Every time a student engages with the platform, their performance is tracked and evaluated. On the basis of that data, the platform identifies the student’s weak spots and generates content that will lead to improvement in those areas.")])]),_c('v-col',{staticClass:"ml-md-auto text-center",attrs:{"md":"6","cols":"12"}},[_c('img',{staticStyle:{"width":"75%"},attrs:{"src":__webpack_require__(496)}})])],1),_c('v-row',{staticClass:"mb-md-10 d-flex",class:_vm.$vuetify.breakpoint.mdAndUp ? "" : "flex-wrap-reverse"},[_c('v-col',{staticClass:"mr-md-auto text-center",attrs:{"md":"6","cols":"12"}},[_c('img',{staticStyle:{"width":"75%"},attrs:{"src":__webpack_require__(497)}})]),_c('v-col',{staticStyle:{"align-self":"center"},attrs:{"md":"5","cols":"12"}},[_c('p',{staticClass:"lead mt-md-12"},[_vm._v("Thanks to Adaptive Education’s unique machine learning engine, this process is entirely automated. As a result, users of the platform are empowered to learn independently and organically at a pace that is right for them.")])])],1),_c('v-row',{staticClass:"mb-md-10 d-flex"},[_c('v-col',{staticStyle:{"align-self":"center"},attrs:{"md":"5","cols":"12"}},[_c('p',{staticClass:"lead mt-md-12"},[_vm._v("The personalized nature of the platform means better student test scores and academic performance are guaranteed.")])]),_c('v-col',{staticClass:"ml-md-auto text-center",attrs:{"md":"6","cols":"12"}},[_c('img',{staticStyle:{"width":"75%"},attrs:{"src":__webpack_require__(498)}})])],1)],1)],1),_c('v-main',{staticClass:"wrapper-why-apative",attrs:{"tag":"section"}},[_c('v-container',{staticClass:"wrapper__container"},[_c('v-row',{staticClass:"d-flex",attrs:{"no-gutters":""}},[_c('v-col',{staticClass:"d-flex flex-column align-center mb-12",attrs:{"cols":"12"}},[_c('h2',{staticClass:"display-4 text-center mb-5 mb-lg-7"},[_vm._v("Why Adaptive Education?")])])],1),_c('v-row',{staticClass:"mb-10 d-flex"},[_c('v-col',{attrs:{"cols":"12","md":"6","lg":"4"}},[_c('div',{staticClass:"info-block text-justify",style:(_vm.$vuetify.breakpoint.mdAndUp ? "padding: 1.5rem; margin: 0 auto" : "")},[_c('div',{staticClass:"info-block__icon-shape",staticStyle:{"background-color":"rgba(138, 152, 235, 0.5)"}},[_c('v-icon',{attrs:{"large":"","color":"#3855ff"}},[_vm._v("mdi-tools")])],1),_c('h6',{staticClass:"info-block__info-title text-uppercase",staticStyle:{"color":"#3855ff"}},[_vm._v("Lorem ipsum dolor sit amet, consectetur adipisicing elit")]),_c('p',{staticClass:"info-block__description"},[_vm._v("A next-generation study and test prep tool for mathematics, Adaptive Education brings together the latest advances in artificial intelligence and remote learning. Fully online and accessible from anywhere, the platform democratizes access to personalized education.")])])]),_c('v-col',{attrs:{"cols":"12","md":"6","lg":"4"}},[_c('div',{staticClass:"info-block text-justify",style:(_vm.$vuetify.breakpoint.mdAndUp ? "margin-top: -50px; padding: 1.5rem; margin: 0 auto": "")},[_c('div',{staticClass:"info-block__icon-shape",staticStyle:{"background-color":"rgba(238, 80, 80, 0.5)"}},[_c('v-icon',{attrs:{"large":"","color":"#fd0000"}},[_vm._v("mdi-chart-bell-curve-cumulative")])],1),_c('h6',{staticClass:"info-block__info-title text-uppercase",staticStyle:{"color":"#fd0000"}},[_vm._v("Lorem ipsum dolor sit amet, consectetur adipisicing elit")]),_c('p',{staticClass:"info-block__description"},[_vm._v("For a fraction of the cost of personal tutors, platform users receive a study course that is not only tailored to their specific needs, but which adapts in lockstep with their progress. This ensures a structured learning experience in which students, parents, and educators can have complete confidence.")])])]),_c('v-col',{attrs:{"cols":"12","md":"6","lg":"4"}},[_c('div',{staticClass:"info-block text-justify",style:(_vm.$vuetify.breakpoint.mdAndUp ? "padding: 1.5rem; margin: 0 auto" : "")},[_c('div',{staticClass:"info-block__icon-shape",staticStyle:{"background-color":"rgba(84, 218, 161, 0.5)"}},[_c('v-icon',{attrs:{"large":"","color":"#1aae6f"}},[_vm._v("mdi-account-group")])],1),_c('h6',{staticClass:"info-block__info-title text-uppercase",staticStyle:{"color":"#1aae6f"}},[_vm._v("Lorem ipsum dolor sit amet, consectetur adipisicing elit")]),_c('p',{staticClass:"info-block__description"},[_vm._v("The Adaptive Education platform is suitable for both independent learners and as a tool to be used alongside traditional classroom instruction.")])])])],1)],1)],1),_c('v-main',{staticClass:"wrapper-about",attrs:{"tag":"section"}},[_c('v-container',{staticClass:"wrapper__container"},[_c('v-row',{staticClass:"d-flex",attrs:{"no-gutters":""}},[_c('v-col',{staticClass:"d-flex flex-column align-center mb-12",attrs:{"cols":"12"}},[_c('h2',{staticClass:"display-4 text-center mb-5 mb-lg-7"},[_vm._v("About Us")])])],1),_c('v-row',{staticClass:"d-flex mb-10"},[_c('v-col',{staticStyle:{"align-self":"center"},attrs:{"md":"5","cols":"12"}},[_c('p',{staticClass:"lead mt-md-12"},[_vm._v("The mission of Adaptive Education is to leverage emerging technologies to reimagine the learning experience. Through innovation in teaching, we believe we can make incredible strides forward in how the world learns, and in the process help students everywhere reach their full potential.")])]),_c('v-col',{staticClass:"text-center",attrs:{"md":"6","cols":"12"}},[_c('img',{staticStyle:{"text-align":"center"},attrs:{"src":__webpack_require__(499)}})])],1),_c('v-row',{staticClass:"d-flex justify-center mb-5 mb-lg-7"},[_c('v-col',{staticClass:"text-center mb-4",attrs:{"cols":"6","md":"3"}},[_c('div',{staticClass:"icon-shape-lg"},[_c('v-icon',{attrs:{"large":"","color":"#ee5050"}},[_vm._v("mdi-account")])],1),_c('h3',{staticClass:"font-weight-bold display-3"},[_vm._v("1000+")]),_c('p',{staticClass:"text--disabled"},[_vm._v("Created Accounts")])]),_c('v-col',{staticClass:"text-center mb-4",attrs:{"cols":"6","md":"3"}},[_c('div',{staticClass:"icon-shape-lg"},[_c('v-icon',{attrs:{"large":"","color":"#ee5050"}},[_vm._v("mdi-square-root")])],1),_c('h3',{staticClass:"font-weight-bold display-3"},[_vm._v(_vm._s(_vm.countPolls))]),_c('p',{staticClass:"text--disabled"},[_vm._v("Tests")])]),_c('v-col',{staticClass:"text-center mb-4",attrs:{"cols":"6","md":"3"}},[_c('div',{staticClass:"icon-shape-lg"},[_c('v-icon',{attrs:{"large":"","color":"#ee5050"}},[_vm._v("mdi-progress-check")])],1),_c('h3',{staticClass:"font-weight-bold display-3"},[_vm._v(_vm._s(_vm.results))]),_c('p',{staticClass:"text--disabled"},[_vm._v("Processed results")])]),_c('v-col',{staticClass:"text-center mb-4",attrs:{"cols":"6","md":"3"}},[_c('div',{staticClass:"icon-shape-lg"},[_c('v-icon',{attrs:{"large":"","color":"#ee5050"}})],1),_c('h3',{staticClass:"font-weight-bold display-3"}),_c('p',{staticClass:"text--disabled"})])],1)],1)],1),_c('v-main',{staticClass:"wrapper-formula",attrs:{"tag":"section"}},[_c('v-container',{staticClass:"wrapper__container"},[_c('v-row',{staticClass:"d-flex",attrs:{"no-gutters":""}},[_c('v-col',{staticClass:"d-flex flex-column align-center mb-12",attrs:{"cols":"12"}},[_c('h2',{staticClass:"display-4 text-center mb-5 mb-lg-7"},[_vm._v("The Unique Adaptive Education Formula")]),_c('h3',{staticClass:"display-3 text-center mb-5 mb-lg-7"},[_vm._v("The Adaptive Education platform creates personalized, auto-updating study programs by applying machine learning to a range of live data source including:")]),_c('ul',{staticClass:"list-unstyled mt-6"},[_c('li',{staticClass:"py-3 d-flex"},[_c('div',{staticClass:"d-flex justify-center align-center"},[_c('div',{staticClass:"d-flex justify-center align-center mr-7",staticStyle:{"background-color":"#eaecfb","border-radius":"50%","min-width":"64px","min-height":"64px"}},[_c('v-icon',{attrs:{"large":"","color":"#2643e9"}},[_vm._v("mdi-chart-areaspline")])],1),_c('span',{staticClass:"display-2"},[_vm._v("Student performance while using the platform")])])]),_c('li',{staticClass:"py-3 d-flex"},[_c('div',{staticClass:"d-flex justify-center align-center"},[_c('div',{staticClass:"d-flex justify-center align-center mr-7",staticStyle:{"background-color":"#eaecfb","border-radius":"50%","min-width":"64px","min-height":"64px"}},[_c('v-icon',{attrs:{"large":"","color":"#2643e9"}},[_vm._v("mdi-chart-areaspline")])],1),_c('span',{staticClass:"display-2"},[_vm._v("School curriculum data")])])]),_c('li',{staticClass:"py-3 d-flex"},[_c('div',{staticClass:"d-flex justify-center align-center"},[_c('div',{staticClass:"d-flex justify-center align-center mr-7",staticStyle:{"background-color":"#eaecfb","border-radius":"50%","min-width":"64px","min-height":"64px"}},[_c('v-icon',{attrs:{"large":"","color":"#2643e9"}},[_vm._v("mdi-chart-areaspline")])],1),_c('span',{staticClass:"display-2"},[_vm._v("Student test scores")])])]),_c('li',{staticClass:"py-3 d-flex"},[_c('div',{staticClass:"d-flex justify-center align-center"},[_c('div',{staticClass:"d-flex justify-center align-center mr-7",staticStyle:{"background-color":"#eaecfb","border-radius":"50%","min-width":"64px","min-height":"64px"}},[_c('v-icon',{attrs:{"large":"","color":"#2643e9"}},[_vm._v("mdi-chart-areaspline")])],1),_c('span',{staticClass:"display-2"},[_vm._v("School-specific and standardized test papers")])])])])])],1),_c('v-row',[_c('v-col',{staticClass:"my-10",attrs:{"cols":"12"}},[_c('img',{attrs:{"src":__webpack_require__(500)}})])],1)],1)],1),_c('v-main',{staticClass:"wrapper-adaptive",attrs:{"tag":"section"}},[_c('v-container',{staticClass:"wrapper__container"},[_c('v-row',{staticClass:"d-flex",attrs:{"no-gutters":""}},[_c('v-col',{staticClass:"d-flex flex-column align-center mb-12",attrs:{"cols":"12"}},[_c('h2',{staticClass:"display-4 text-center mb-5 mb-lg-7"},[_vm._v("Adaptive Education")])])],1),_c('v-row',{staticClass:"d-flex justify-center mb-5 mb-lg-7"},[_c('v-col',{staticClass:"text-center mb-4",attrs:{"cols":"12","md":"4"}},[_c('v-card',{staticClass:"adaptive-education"},[_c('v-card-title',{staticClass:"adaptive-education__title"},[_c('h2',{staticClass:"font-weight-bold"},[_vm._v("SAT/ACT Prep")])]),_c('v-divider'),_c('v-card-text',{staticClass:"adaptive-education__text"},[_c('p',[_vm._v("Ace your college admissions tests. We guarantee an improvement in your score")]),_c('img',{staticStyle:{"width":"70%"},attrs:{"src":__webpack_require__(501)}})])],1)],1),_c('v-col',{staticClass:"text-center mb-4",attrs:{"cols":"12","md":"4"}},[_c('v-card',{staticClass:"adaptive-education"},[_c('v-card-title',{staticClass:"adaptive-education__title"},[_c('h2',{staticClass:"font-weight-bold"},[_vm._v("Olympiads and Competition")])]),_c('v-divider'),_c('v-card-text',{staticClass:"adaptive-education__text"},[_c('p',[_vm._v("Ace your college admissions tests. We guarantee an improvement in your score")]),_c('img',{staticStyle:{"width":"55%"},attrs:{"src":__webpack_require__(502)}})])],1)],1),_c('v-col',{staticClass:"text-center mb-4",attrs:{"cols":"12","md":"4"}},[_c('v-card',{staticClass:"adaptive-education"},[_c('v-card-title',{staticClass:"adaptive-education__title"},[_c('h2',{staticClass:"font-weight-bold"},[_vm._v("Continuous Learning")])]),_c('v-divider'),_c('v-card-text',{staticClass:"adaptive-education__text"},[_c('p',[_vm._v("Ace your college admissions tests. We guarantee an improvement in your score")]),_c('img',{staticStyle:{"width":"55%"},attrs:{"src":__webpack_require__(503)}})])],1)],1)],1)],1)],1),_c('v-footer',{staticClass:"pl-0",staticStyle:{"padding-top":"286px"},attrs:{"color":"#0648b3"}},[_c('div',{staticClass:"pattern top"}),_c('v-container',{staticClass:"wrapper__container"},[_c('v-row',[_c('v-col')],1),_c('v-divider'),_c('v-row',[_c('v-col',{staticClass:"pb-4 mb-md-0"},[_c('div',{staticClass:"d-flex text-center justify-center align-center"},[_c('p',{staticClass:"font-weight-medium mb-0 white--text"},[_vm._v("© Adaptive Education 2020. All rights reserved.")])])])],1)],1)],1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/index.vue?vue&type=template&id=073f46fa&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// EXTERNAL MODULE: ./components/core/coreCard.vue + 4 modules
var coreCard = __webpack_require__(324);

// EXTERNAL MODULE: ./components/core/coreStatsCard.vue + 4 modules
var coreStatsCard = __webpack_require__(374);

// EXTERNAL MODULE: ./components/core/coreChartCard.vue + 4 modules
var coreChartCard = __webpack_require__(360);

// EXTERNAL MODULE: ./store/index.ts + 1 modules
var store = __webpack_require__(5);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/index.vue?vue&type=script&lang=ts&






let lib_vue_loader_options_pagesvue_type_script_lang_ts_MainPage = class MainPage extends external_vue_property_decorator_["Vue"] {
  get countPolls() {
    return store["pollsStore"].items.filter(item => item.section !== `personal`).length;
  }

  get results() {
    return store["auth"].user.commonStatistics ? store["auth"].user.commonStatistics.length : 0;
  }

  async generateNewAnonimousUser() {
    this.$router.push('/tests'); // await auth.registrationAnonimous()
    // .then( () => {
    // this.$router.push('/tests')
    // })
  }

};
lib_vue_loader_options_pagesvue_type_script_lang_ts_MainPage = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  transition: 'page',
  layout: 'startLayout',
  components: {
    coreCard: coreCard["a" /* default */],
    coreStatsCard: coreStatsCard["a" /* default */],
    coreChartCard: coreChartCard["a" /* default */]
  },

  fetch({
    redirect
  }) {// if(auth.isAuthenticated || auth.isAnonimous) redirect('/lk')
  }

})], lib_vue_loader_options_pagesvue_type_script_lang_ts_MainPage);
/* harmony default export */ var lib_vue_loader_options_pagesvue_type_script_lang_ts_ = (lib_vue_loader_options_pagesvue_type_script_lang_ts_MainPage);
// CONCATENATED MODULE: ./pages/index.vue?vue&type=script&lang=ts&
 /* harmony default export */ var pagesvue_type_script_lang_ts_ = (lib_vue_loader_options_pagesvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VApp/VApp.js
var VApp = __webpack_require__(313);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VAppBar/VAppBar.js + 2 modules
var VAppBar = __webpack_require__(319);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/VBtn.js
var VBtn = __webpack_require__(81);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(298);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(296);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(343);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VContainer.js
var VContainer = __webpack_require__(314);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(301);

// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VFooter/VFooter.sass
var VFooter = __webpack_require__(507);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSheet/VSheet.js
var VSheet = __webpack_require__(24);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/applicationable/index.js
var applicationable = __webpack_require__(69);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/ssr-bootable/index.js
var ssr_bootable = __webpack_require__(27);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/mixins.js
var mixins = __webpack_require__(4);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/helpers.js
var helpers = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VFooter/VFooter.js
// Styles
 // Components

 // Mixins


 // Utilities



/* @vue/component */

/* harmony default export */ var VFooter_VFooter = (Object(mixins["a" /* default */])(VSheet["a" /* default */], Object(applicationable["a" /* default */])('footer', ['height', 'inset']), ssr_bootable["a" /* default */]).extend({
  name: 'v-footer',
  props: {
    height: {
      default: 'auto',
      type: [Number, String]
    },
    inset: Boolean,
    padless: Boolean
  },
  computed: {
    applicationProperty() {
      return this.inset ? 'insetFooter' : 'footer';
    },

    classes() {
      return { ...VSheet["a" /* default */].options.computed.classes.call(this),
        'v-footer--absolute': this.absolute,
        'v-footer--fixed': !this.absolute && (this.app || this.fixed),
        'v-footer--padless': this.padless,
        'v-footer--inset': this.inset
      };
    },

    computedBottom() {
      if (!this.isPositioned) return undefined;
      return this.app ? this.$vuetify.application.bottom : 0;
    },

    computedLeft() {
      if (!this.isPositioned) return undefined;
      return this.app && this.inset ? this.$vuetify.application.left : 0;
    },

    computedRight() {
      if (!this.isPositioned) return undefined;
      return this.app && this.inset ? this.$vuetify.application.right : 0;
    },

    isPositioned() {
      return Boolean(this.absolute || this.fixed || this.app);
    },

    styles() {
      const height = parseInt(this.height);
      return { ...VSheet["a" /* default */].options.computed.styles.call(this),
        height: isNaN(height) ? height : Object(helpers["g" /* convertToUnit */])(height),
        left: Object(helpers["g" /* convertToUnit */])(this.computedLeft),
        right: Object(helpers["g" /* convertToUnit */])(this.computedRight),
        bottom: Object(helpers["g" /* convertToUnit */])(this.computedBottom)
      };
    }

  },
  methods: {
    updateApplication() {
      const height = parseInt(this.height);
      return isNaN(height) ? this.$el ? this.$el.clientHeight : 0 : height;
    }

  },

  render(h) {
    const data = this.setBackgroundColor(this.color, {
      staticClass: 'v-footer',
      class: this.classes,
      style: this.styles
    });
    return h('footer', data, this.$slots.default);
  }

}));
// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(50);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VMain/VMain.js
var VMain = __webpack_require__(317);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(341);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VSpacer.js
var VSpacer = __webpack_require__(315);

// CONCATENATED MODULE: ./pages/index.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(504)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pagesvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "0bacbbc2"
  
)

/* harmony default export */ var pages = __webpack_exports__["default"] = (component.exports);

/* vuetify-loader */















installComponents_default()(component, {VApp: VApp["a" /* default */],VAppBar: VAppBar["a" /* default */],VBtn: VBtn["a" /* default */],VCard: VCard["a" /* default */],VCardText: components_VCard["b" /* VCardText */],VCardTitle: components_VCard["c" /* VCardTitle */],VCol: VCol["a" /* default */],VContainer: VContainer["a" /* default */],VDivider: VDivider["a" /* default */],VFooter: VFooter_VFooter,VIcon: VIcon["a" /* default */],VMain: VMain["a" /* default */],VRow: VRow["a" /* default */],VSpacer: VSpacer["a" /* default */]})


/***/ })

};;
//# sourceMappingURL=index.js.map