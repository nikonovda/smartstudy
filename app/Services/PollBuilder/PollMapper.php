<?php

namespace App\Services\PollBuilder;

use App\Services\PollBuilder\Builders\HopBuilder;
use App\Services\PollBuilder\Builders\OptionBuilder;
use App\Services\PollBuilder\Builders\PollBuilder;


/**
 * 
 */
class PollMapper
{
	
	protected $source;

	protected $poll;
	protected $hops;
	protected $options;
	protected $conditions;

	function __construct($source)
	{
		$this->source = $source;
	}

	public function getModels()
	{
		return [
			'poll' => $this->poll,
			'hops' => $this->hops,
			'options' => $this->options
		];
	}

	public function save()
	{
		$this->build();

		return (new PollSaver($this->getModels()))->save();
	}

	public function build()
	{
		$builders = [
			'poll' => new PollBuilder(),
			'hop' => new HopBuilder(),
			'option' => new OptionBuilder()
		];

		$this->poll = $builders['poll']->make($this->source);

		foreach ($this->source['hops'] as $key => $srcHop) {
			$hop = $builders['hop']->make($srcHop);
			$hop->sortKey = $key;
			$this->hops[$srcHop['_id']] = $hop;

			foreach ($srcHop['options'] as $srcOption) {
				$option = $builders['option']->make($srcOption);
				$optionId = $srcHop['_id'] . '_' . $option->id;
				$this->options[$optionId] = $option;
			}
		}
	}
}
