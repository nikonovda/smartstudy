export default ({ store, redirect }) => {
  if(!store.getters['auth/isRoot']) {
    redirect('/')
  }
}