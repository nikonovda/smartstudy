import { Vue, Component, Mixins } from 'vue-property-decorator'
import MixinHopsMethods from './__mixinHopsMethods'
import { pollState } from '../../store'

@Component({
  mixins: [MixinHopsMethods]
})
export default class MixinOptionMethods extends Mixins(MixinHopsMethods, Vue) {
  // Локальная копия массива вопросов из стора
  hopId: string
  // Правила валидации вопроса
  validationOption = [
    v => !!v || 'Please enter a response option'
  ]

  get userOption () {
    return this.optionsInit.find(option => option.type === 'user')
  }

  get maxOptionId () {
    const foo = this.optionsInit.reduce((previousValue, item) => {
      return item.id > previousValue ? item.id : previousValue
    }, 0)
    return foo
  }

  addOption (typeOption = 'simple', valueOption = '') {
    const option = {
      id: this.maxOptionId + 1,
      value: valueOption,
      type: typeOption,
      image: ''
    }
    pollState.addOption({ hopId: this.hopId, option })
    this.optionsInit.push(option)
  }

  updateOption (option) {
    const data = {
      hopId: this.hopId,
      option
    }
    pollState.updateOption(data)
  }

  removeOption (option) {
    const index = this.optionsInit.indexOf(option)
    if (index !== -1) {
      pollState.removeOption({ hopId: this.hopId, option })
      this.optionsInit.splice(index, 1)
    }
  }

  // Проверяет опцию на наличие пользовательского ввода
  isUserValue = option => option.type === 'user'
}
