import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import { Result } from '../interfaces/interfaceResults'
import { auth } from '../store'
import axios from '../plugins/axios'
import { ResultPoll } from '../interfaces/interfaceStore'


@Module({
  stateFactory: true,
  namespaced: true,
  name: 'results'
})
export default class Results extends VuexModule {
  pollId: String = ''
  items: Result[] = []
  resultPoll: ResultPoll[] = []

  @Mutation
  SET_RESULT_POLL( payload: ResultPoll ) {
    this.resultPoll.unshift(payload)
  }

  @Mutation
  SET_POLL_ID (payload: String) {
    this.pollId = payload
  }
  
  @Mutation
  SET_ITEMS (payload: Result[]) {
    this.items = payload
  }

  @Action({ rawError: true })
  async get (pollId: String) {
    if (this.pollId === pollId) { return true }
    const data: Result[] = await axios.get('/api/polls/' + pollId)
    this.SET_POLL_ID(pollId)
    this.SET_ITEMS(data.data)
    return data.data
  }

  @Action({ rawError: true })
  async save (data: any) {
    if(auth.isAuthenticated || auth.isAnonimous) {
      return await axios.post('/api/results', data)
      .then(() => {
        auth.getUser()
        return true
      })
      .catch(() => {
        alert('Сервис временно недоступен...')
      })
    } else {
      return await axios.post('/api/results/noauth', data)
        .then((response) => {
          this.SET_RESULT_POLL(response.data.data)
          return response.data.data._id
        })
        .catch(() => {
          alert('Сервис временно недоступен...')
        })
    }
  }

  @Action({ rawError: true })
  async update (data: any) {
    if(auth.isAuthenticated || auth.isAnonimous) {
      return await axios.patch(`/api/results/${data.resultId}`, data.dataResp)
      .then((res) => {
        auth.getUser()
        return true
      })
      .catch(() => {
        alert('Сервис временно недоступен...')
      })
    } else {
      return await axios.patch(`/api/results/noauth/${data.resultId}`, data.dataResp)
      .then((res) => {
        return true
      })
      .catch(() => {
        alert('Сервис временно недоступен...')
      })
    }
  }


  get all () {
    return this.items
  }

  get getById () {
    return (id: String) => {
      const result = this.items.find(item => item._id === id)
      return !result ? null : result
    }
  } 
}