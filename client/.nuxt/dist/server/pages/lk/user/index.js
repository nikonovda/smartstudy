exports.ids = [14];
exports.modules = {

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VCardActions; });
/* unused harmony export VCardSubtitle */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return VCardText; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return VCardTitle; });
/* harmony import */ var _VCard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(298);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);


const VCardActions = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__actions');
const VCardSubtitle = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__subtitle');
const VCardText = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__text');
const VCardTitle = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__title');

/* unused harmony default export */ var _unused_webpack_default_export = ({
  $_vuetify_subcomponents: {
    VCard: _VCard__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"],
    VCardActions,
    VCardSubtitle,
    VCardText,
    VCardTitle
  }
});

/***/ }),

/***/ 322:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(345);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("0db8c820", content, true, context)
};

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreCard.vue?vue&type=template&id=0a0f4055&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-card',_vm._b({staticClass:"v-card--material pa-3",class:_vm.classes},'v-card',_vm.$attrs,false),[_c('div',{staticClass:"d-flex grow flex-wrap"},[(_vm.avatar)?_c('v-avatar',{staticClass:"mx-auto v-card--material__avatar elevation-6",attrs:{"size":"128","color":"grey"}},[_c('v-img',{attrs:{"src":_vm.avatar}})],1):(_vm.$slots.image || _vm.place || _vm.icon || _vm.$slots.heading)?_c('v-sheet',{staticClass:"text-start v-card--material__heading mb-n6",class:{ 'pa-7': !_vm.$slots.image && !_vm.place },style:(_vm.place ? "display: flex;" : ""),attrs:{"color":_vm.color,"max-height":_vm.icon || _vm.place ? 90 : undefined,"width":_vm.icon || _vm.place ? '' : '100%',"elevation":_vm.place ? 0 : 6,"dark":""}},[(_vm.$slots.heading)?_vm._t("heading"):(_vm.$slots.image)?_vm._t("image"):(_vm.title && !_vm.icon)?_c('div',{staticClass:"display-1 font-weight-light",domProps:{"textContent":_vm._s(_vm.title)}}):(_vm.icon)?_c('v-icon',{attrs:{"size":"32"},domProps:{"textContent":_vm._s(_vm.icon)}}):(_vm.place)?[_c('div',{staticClass:"text-center",style:("padding: 15px 20px; background-color:" + _vm.colorPlace + "; border-radius: 4px; width: 115px")},[_c('span',{staticClass:"display-1"},[_vm._v("Rank:")]),_c('h1',{staticClass:"display-3"},[_vm._v(_vm._s(_vm.place))])]),_c('div',{staticClass:"text-center ml-4",style:("padding: 15px 20px; background-color:" + _vm.colorPlace + "; border-radius: 4px; width: 115px")},[_c('span',{staticClass:"display-1"},[_vm._v("Top:")]),_c('h1',{staticClass:"display-3"},[_vm._v(_vm._s(_vm.placePercentage + "%"))])])]:_vm._e(),(_vm.text)?_c('div',{staticClass:"headline font-weight-thin",domProps:{"textContent":_vm._s(_vm.text)}}):_vm._e()],2):_vm._e(),(_vm.$slots['after-heading'])?_c('div',{staticClass:"justify-center align-center",staticStyle:{"height":"calc(100% - 85px)"}},[_vm._t("after-heading")],2):(_vm.icon && _vm.title)?_c('div',{staticClass:"ml-4"},[_c('div',{staticClass:"card-title font-weight-light",domProps:{"textContent":_vm._s(_vm.title)}})]):_vm._e()],1),_vm._t("default"),(_vm.$slots.actions)?[_c('v-divider',{staticClass:"mt-2"}),_c('v-card-actions',{staticClass:"pb-0"},[_vm._t("actions")],2)]:_vm._e()],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/core/coreCard.vue?vue&type=template&id=0a0f4055&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreCard.vue?vue&type=script&lang=ts&


let coreCardvue_type_script_lang_ts_CoreCard = class CoreCard extends external_vue_property_decorator_["Vue"] {
  get classes() {
    return {
      'v-card--material--has-heading': this.hasHeading
    };
  }

  get hasHeading() {
    return Boolean(this.$slots.heading || this.title || this.icon);
  }

  get hasAltHeading() {
    return Boolean(this.$slots.heading || this.title && this.icon);
  }

};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "avatar", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: 'success'
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "color", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: 'success'
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "colorPlace", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "icon", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Boolean,
  default: false
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "image", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "text", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "title", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "place", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "placePercentage", void 0);

coreCardvue_type_script_lang_ts_CoreCard = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'CoreCard'
})], coreCardvue_type_script_lang_ts_CoreCard);
/* harmony default export */ var coreCardvue_type_script_lang_ts_ = (coreCardvue_type_script_lang_ts_CoreCard);
// CONCATENATED MODULE: ./components/core/coreCard.vue?vue&type=script&lang=ts&
 /* harmony default export */ var core_coreCardvue_type_script_lang_ts_ = (coreCardvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VAvatar/VAvatar.js
var VAvatar = __webpack_require__(96);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(298);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(296);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(301);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(50);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(72);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSheet/VSheet.js
var VSheet = __webpack_require__(24);

// CONCATENATED MODULE: ./components/core/coreCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(344)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_coreCardvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "f4daf264"
  
)

/* harmony default export */ var coreCard = __webpack_exports__["a"] = (component.exports);

/* vuetify-loader */








installComponents_default()(component, {VAvatar: VAvatar["a" /* default */],VCard: VCard["a" /* default */],VCardActions: components_VCard["a" /* VCardActions */],VDivider: VDivider["a" /* default */],VIcon: VIcon["a" /* default */],VImg: VImg["a" /* default */],VSheet: VSheet["a" /* default */]})


/***/ }),

/***/ 334:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(371);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("91e81038", content, true, context)
};

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(131);
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _util_mergeData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);



 // no xs

const breakpoints = ['sm', 'md', 'lg', 'xl'];
const ALIGNMENT = ['start', 'end', 'center'];

function makeProps(prefix, def) {
  return breakpoints.reduce((props, val) => {
    props[prefix + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = def();
    return props;
  }, {});
}

const alignValidator = str => [...ALIGNMENT, 'baseline', 'stretch'].includes(str);

const alignProps = makeProps('align', () => ({
  type: String,
  default: null,
  validator: alignValidator
}));

const justifyValidator = str => [...ALIGNMENT, 'space-between', 'space-around'].includes(str);

const justifyProps = makeProps('justify', () => ({
  type: String,
  default: null,
  validator: justifyValidator
}));

const alignContentValidator = str => [...ALIGNMENT, 'space-between', 'space-around', 'stretch'].includes(str);

const alignContentProps = makeProps('alignContent', () => ({
  type: String,
  default: null,
  validator: alignContentValidator
}));
const propMap = {
  align: Object.keys(alignProps),
  justify: Object.keys(justifyProps),
  alignContent: Object.keys(alignContentProps)
};
const classMap = {
  align: 'align',
  justify: 'justify',
  alignContent: 'align-content'
};

function breakpointClass(type, prop, val) {
  let className = classMap[type];

  if (val == null) {
    return undefined;
  }

  if (prop) {
    // alignSm -> Sm
    const breakpoint = prop.replace(type, '');
    className += `-${breakpoint}`;
  } // .align-items-sm-center


  className += `-${val}`;
  return className.toLowerCase();
}

const cache = new Map();
/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_1___default.a.extend({
  name: 'v-row',
  functional: true,
  props: {
    tag: {
      type: String,
      default: 'div'
    },
    dense: Boolean,
    noGutters: Boolean,
    align: {
      type: String,
      default: null,
      validator: alignValidator
    },
    ...alignProps,
    justify: {
      type: String,
      default: null,
      validator: justifyValidator
    },
    ...justifyProps,
    alignContent: {
      type: String,
      default: null,
      validator: alignContentValidator
    },
    ...alignContentProps
  },

  render(h, {
    props,
    data,
    children
  }) {
    // Super-fast memoization based on props, 5x faster than JSON.stringify
    let cacheKey = '';

    for (const prop in props) {
      cacheKey += String(props[prop]);
    }

    let classList = cache.get(cacheKey);

    if (!classList) {
      classList = []; // Loop through `align`, `justify`, `alignContent` breakpoint props

      let type;

      for (type in propMap) {
        propMap[type].forEach(prop => {
          const value = props[prop];
          const className = breakpointClass(type, prop, value);
          if (className) classList.push(className);
        });
      }

      classList.push({
        'no-gutters': props.noGutters,
        'row--dense': props.dense,
        [`align-${props.align}`]: props.align,
        [`justify-${props.justify}`]: props.justify,
        [`align-content-${props.alignContent}`]: props.alignContent
      });
      cache.set(cacheKey, classList);
    }

    return h(props.tag, Object(_util_mergeData__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(data, {
      staticClass: 'row',
      class: classList
    }), children);
  }

}));

/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VSubheader_VSubheader_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(358);
/* harmony import */ var _src_components_VSubheader_VSubheader_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VSubheader_VSubheader_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mixins_themeable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(11);
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4);
// Styles
 // Mixins



/* harmony default export */ __webpack_exports__["a"] = (Object(_util_mixins__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(_mixins_themeable__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"]
/* @vue/component */
).extend({
  name: 'v-subheader',
  props: {
    inset: Boolean
  },

  render(h) {
    return h('div', {
      staticClass: 'v-subheader',
      class: {
        'v-subheader--inset': this.inset,
        ...this.themeClasses
      },
      attrs: this.$attrs,
      on: this.$listeners
    }, this.$slots.default);
  }

}));

/***/ }),

/***/ 343:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(131);
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _util_mergeData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);



 // no xs

const breakpoints = ['sm', 'md', 'lg', 'xl'];

const breakpointProps = (() => {
  return breakpoints.reduce((props, val) => {
    props[val] = {
      type: [Boolean, String, Number],
      default: false
    };
    return props;
  }, {});
})();

const offsetProps = (() => {
  return breakpoints.reduce((props, val) => {
    props['offset' + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = {
      type: [String, Number],
      default: null
    };
    return props;
  }, {});
})();

const orderProps = (() => {
  return breakpoints.reduce((props, val) => {
    props['order' + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = {
      type: [String, Number],
      default: null
    };
    return props;
  }, {});
})();

const propMap = {
  col: Object.keys(breakpointProps),
  offset: Object.keys(offsetProps),
  order: Object.keys(orderProps)
};

function breakpointClass(type, prop, val) {
  let className = type;

  if (val == null || val === false) {
    return undefined;
  }

  if (prop) {
    const breakpoint = prop.replace(type, '');
    className += `-${breakpoint}`;
  } // Handling the boolean style prop when accepting [Boolean, String, Number]
  // means Vue will not convert <v-col sm></v-col> to sm: true for us.
  // Since the default is false, an empty string indicates the prop's presence.


  if (type === 'col' && (val === '' || val === true)) {
    // .col-md
    return className.toLowerCase();
  } // .order-md-6


  className += `-${val}`;
  return className.toLowerCase();
}

const cache = new Map();
/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_1___default.a.extend({
  name: 'v-col',
  functional: true,
  props: {
    cols: {
      type: [Boolean, String, Number],
      default: false
    },
    ...breakpointProps,
    offset: {
      type: [String, Number],
      default: null
    },
    ...offsetProps,
    order: {
      type: [String, Number],
      default: null
    },
    ...orderProps,
    alignSelf: {
      type: String,
      default: null,
      validator: str => ['auto', 'start', 'end', 'center', 'baseline', 'stretch'].includes(str)
    },
    tag: {
      type: String,
      default: 'div'
    }
  },

  render(h, {
    props,
    data,
    children,
    parent
  }) {
    // Super-fast memoization based on props, 5x faster than JSON.stringify
    let cacheKey = '';

    for (const prop in props) {
      cacheKey += String(props[prop]);
    }

    let classList = cache.get(cacheKey);

    if (!classList) {
      classList = []; // Loop through `col`, `offset`, `order` breakpoint props

      let type;

      for (type in propMap) {
        propMap[type].forEach(prop => {
          const value = props[prop];
          const className = breakpointClass(type, prop, value);
          if (className) classList.push(className);
        });
      }

      const hasColClasses = classList.some(className => className.startsWith('col-'));
      classList.push({
        // Default to .col if no other col-{bp}-* classes generated nor `cols` specified.
        col: !hasColClasses || !props.cols,
        [`col-${props.cols}`]: props.cols,
        [`offset-${props.offset}`]: props.offset,
        [`order-${props.order}`]: props.order,
        [`align-self-${props.alignSelf}`]: props.alignSelf
      });
      cache.set(cacheKey, classList);
    }

    return h(props.tag, Object(_util_mergeData__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(data, {
      class: classList
    }), children);
  }

}));

/***/ }),

/***/ 344:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(322);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 345:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-card--material__avatar{position:relative;top:-64px;margin-bottom:-32px}.v-card--material__heading{position:relative;top:-40px;transition:.3s ease;z-index:1}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 358:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(359);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(10).default("e8b41e5e", content, true)

/***/ }),

/***/ 359:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".theme--light.v-subheader{color:rgba(0,0,0,.6)}.theme--dark.v-subheader{color:hsla(0,0%,100%,.7)}.v-subheader{align-items:center;display:flex;height:48px;font-size:.875rem;font-weight:400;padding:0 16px}.v-subheader--inset{margin-left:56px}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(298);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/Card.vue?vue&type=script&lang=js&

/* harmony default export */ var Cardvue_type_script_lang_js_ = ({
  name: 'Card',
  extends: VCard["a" /* default */]
});
// CONCATENATED MODULE: ./components/core/Card.vue?vue&type=script&lang=js&
 /* harmony default export */ var core_Cardvue_type_script_lang_js_ = (Cardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// CONCATENATED MODULE: ./components/core/Card.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_Cardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "48139ca2"
  
)

/* harmony default export */ var Card = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(334);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 371:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-card--material-stats{display:flex;flex-wrap:wrap;position:relative}.v-card--material-stats>div:first-child{justify-content:space-between}.v-card--material-stats .v-card{border-radius:4px;flex:0 1 auto}.v-card--material-stats .v-card__text{display:inline-block;flex:1 0 calc(100% - 120px);position:absolute;top:0;right:0;width:100%}.v-card--material-stats .v-card__actions{flex:1 0 100%}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreStatsCard.vue?vue&type=template&id=8d7c3be4&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('coreCard',_vm._g(_vm._b({staticClass:"v-card--material-stats",attrs:{"icon":_vm.icon},scopedSlots:_vm._u([{key:"after-heading",fn:function(){return [_c('div',{staticClass:"ml-auto text-right"},[_c('div',{staticClass:"display-2 grey--text font-weight-light",domProps:{"textContent":_vm._s(_vm.title)}}),_c('h3',{staticClass:"display-3 font-weight-light text--primary"},[_vm._v(_vm._s(_vm.value)+" "),_c('small',[_vm._v(" "+_vm._s(_vm.smallValue)+" ")])])])]},proxy:true}])},'coreCard',_vm.$attrs,false),_vm.$listeners),[_c('v-col',{staticClass:"px-0",attrs:{"cols":"12"}},[_c('v-divider')],1),_c('v-icon',{staticClass:"ml-2 mr-1",attrs:{"color":_vm.subIconColor,"size":"16"}},[_vm._v(_vm._s(_vm.subIcon))]),_c('span',{staticClass:"caption grey--text font-weight-light",class:_vm.subTextColor,domProps:{"textContent":_vm._s(_vm.subText)}})],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/core/coreStatsCard.vue?vue&type=template&id=8d7c3be4&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// EXTERNAL MODULE: ./components/core/Card.vue + 2 modules
var Card = __webpack_require__(361);

// EXTERNAL MODULE: ./components/core/coreCard.vue + 4 modules
var coreCard = __webpack_require__(324);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreStatsCard.vue?vue&type=script&lang=ts&




let coreStatsCardvue_type_script_lang_ts_ComponentName = class ComponentName extends external_vue_property_decorator_["Vue"] {};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  required: true
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "icon", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "subIcon", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "subIconColor", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "subTextColor", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "subText", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "title", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "value", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "smallValue", void 0);

coreStatsCardvue_type_script_lang_ts_ComponentName = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'CoreStatsCard',
  inheritAttrs: false,
  components: {
    coreCard: coreCard["a" /* default */]
  },
  props: { ...Card["a" /* default */].props
  }
})], coreStatsCardvue_type_script_lang_ts_ComponentName);
/* harmony default export */ var coreStatsCardvue_type_script_lang_ts_ = (coreStatsCardvue_type_script_lang_ts_ComponentName);
// CONCATENATED MODULE: ./components/core/coreStatsCard.vue?vue&type=script&lang=ts&
 /* harmony default export */ var core_coreStatsCardvue_type_script_lang_ts_ = (coreStatsCardvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(343);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(301);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(50);

// CONCATENATED MODULE: ./components/core/coreStatsCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(370)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_coreStatsCardvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "48bea601"
  
)

/* harmony default export */ var coreStatsCard = __webpack_exports__["a"] = (component.exports);

/* vuetify-loader */




installComponents_default()(component, {VCol: VCol["a" /* default */],VDivider: VDivider["a" /* default */],VIcon: VIcon["a" /* default */]})


/***/ }),

/***/ 444:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(481);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("80a734bc", content, true, context)
};

/***/ }),

/***/ 480:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(444);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 481:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".relative{position:relative!important}#user-profile .background-profile{border-radius:6px;background-image:url(https://source.unsplash.com/1600x900);background-size:cover;width:100%;height:240px}#user-profile .avatar{position:absolute;bottom:-28px;left:25px;display:flex;border-radius:50%;border:4px solid #fff}#user-profile .user-content,#user-profile .user-nav{background-color:#fff;border-radius:10px;border:1px solid hsla(0,0%,54.9%,.32157);min-height:500px}#user-profile .user-content{width:80%}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 531:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/lk/user/index.vue?vue&type=template&id=9fce510a&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-main',[_c('v-container',{attrs:{"id":"user-profile","fluid":"","tag":"section"}},[_c('v-row',{attrs:{"justify":"center"}},[_c('v-col',{attrs:{"cols":"12"}},[_c('div',{staticClass:"background-profile relative"},[_c('div',{staticClass:"d-flex fill-height align-end"},[_c('div',{staticClass:"d-flex relative"},[_c('div',{staticClass:"avatar"},[_c('v-avatar',{attrs:{"size":"108"}},[_c('v-img',{attrs:{"src":"http://i.pravatar.cc/108"}})],1)],1)]),_c('div',{staticClass:"d-flex flex-column mb-2 align-self-end",staticStyle:{"margin-left":"160px"}},[_c('div',{staticClass:"d-flex"},[_c('h1',{staticClass:"ml-1 display-3 font-weight-bold",staticStyle:{"color":"white"}},[_vm._v(_vm._s(_vm.name))])]),_c('div',{staticClass:"d-flex"},[_c('v-icon',{attrs:{"small":"","color":"white"}},[_vm._v("mdi-map-marker")]),_c('span',{staticClass:"display-2",staticStyle:{"color":"white"}},[_vm._v("Libson, Portugal")])],1)]),_c('v-spacer'),_c('div',{staticClass:"d-flex"},[_c('div',{staticClass:"d-flex mr-8"},[_c('div',{staticClass:"d-flex flex-column align-center mx-6 mb-2"},[_c('h3',{staticClass:"display-3 font-weight-bold",staticStyle:{"color":"white"}},[_vm._v(_vm._s(_vm.countPassedTests))]),_c('span',{staticClass:"display-2",staticStyle:{"color":"white"}},[_vm._v("Passed tests")])]),_c('v-divider',{staticStyle:{"border-color":"white","min-height":"50%","max-height":"80%","margin-top":"8px"},attrs:{"vertical":""}}),_c('div',{staticClass:"d-flex flex-column align-center mx-6"},[_c('h3',{staticClass:"display-3 font-weight-bold",staticStyle:{"color":"white"}},[_vm._v("10")]),_c('span',{staticClass:"display-2",staticStyle:{"color":"white"}},[_vm._v("Friends")])]),_c('v-divider',{staticStyle:{"border-color":"white","min-height":"50%","max-height":"80%","margin-top":"8px"},attrs:{"vertical":""}}),_c('div',{staticClass:"d-flex flex-column align-center mx-6"},[_c('h3',{staticClass:"display-3 font-weight-bold",staticStyle:{"color":"white"}},[_vm._v("10")]),_c('span',{staticClass:"display-2",staticStyle:{"color":"white"}},[_vm._v("Rank")])])],1)])],1)])])],1),_c('v-row',{staticClass:"mt-4"},[_c('v-col',{attrs:{"cols":"12","md":"2"}},[_c('div',{staticClass:"user-nav mt-6"},[_c('v-list',{staticStyle:{"border-radius":"15px"},attrs:{"expand":"","nav":""}},[_c('v-subheader',{staticClass:"display-1 pl-2"},[_vm._v("Profile Widget")]),_c('v-list-item-group',{attrs:{"color":"primary"},model:{value:(_vm.listModel),callback:function ($$v) {_vm.listModel=$$v},expression:"listModel"}},_vm._l((_vm.computedItems),function(item,i){return _c('v-list-item',{key:i,attrs:{"link":""}},[_c('v-list-item-content',[_c('v-list-item-title',{staticClass:"display-2",domProps:{"textContent":_vm._s(item.title)}})],1)],1)}),1)],1)],1)]),_c('v-col',{attrs:{"cols":"12","md":"10"}},[_c('v-row',[_c('v-col',{attrs:{"cols":"12","md":"6"}},[_c('coreStatsCard',{attrs:{"color":"transparent","colorPlace":"#ff4484","icon":"","place":_vm.correctnessRank.toString(),"placePercentage":_vm.correctnessPercentageRank.toString(),"title":"Your average accuracy per test","value":_vm.averageAccuracyPerTest + "%","sub-icon":"mdi-clock-outline","sub-text":("Updated: " + _vm.updatedTime)}})],1),_c('v-col',{attrs:{"cols":"12","md":"6"}},[_c('coreStatsCard',{attrs:{"color":"transparent","colorPlace":"#2ca2ff","place":_vm.timingRank.toString(),"placePercentage":_vm.timingPercentageRank.toString(),"icon":"","title":"Average time spent on each test","value":_vm.averageTimeSpentOnEachTest.toString(),"sub-icon":"mdi-tag","sub-text":("Updated: " + _vm.updatedTime)}})],1)],1)],1)],1)],1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/lk/user/index.vue?vue&type=template&id=9fce510a&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// EXTERNAL MODULE: ./components/core/coreCard.vue + 4 modules
var coreCard = __webpack_require__(324);

// EXTERNAL MODULE: ./components/core/coreStatsCard.vue + 4 modules
var coreStatsCard = __webpack_require__(374);

// EXTERNAL MODULE: ./store/index.ts + 1 modules
var store = __webpack_require__(5);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/lk/user/index.vue?vue&type=script&lang=ts&





let uservue_type_script_lang_ts_User = class User extends external_vue_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.listModel = 0;
    this.items = [{
      icon: 'mdi-view-dashboard',
      title: 'Profile',
      to: '/lk/user'
    }, {
      icon: 'mdi-view-dashboard',
      title: 'Friends',
      to: '/lk/friends'
    }];
  }

  get timingRank() {
    if (store["auth"].user.timingRank) return store["auth"].user.timingRank['math_nocalc'];else return 0;
  }

  get correctnessRank() {
    if (store["auth"].user.correctnessRank) return store["auth"].user.correctnessRank['math_nocalc'];else return 0;
  }

  get timingPercentageRank() {
    if (store["auth"].user.timingRank) return store["auth"].user.timingRank['math_nocalc' + '_percent'];else return 0;
  }

  get correctnessPercentageRank() {
    if (store["auth"].user.correctnessRank) return store["auth"].user.correctnessRank['math_nocalc' + '_percent'];else return 0;
  }

  get updatedTime() {
    if (store["auth"].user.results !== undefined && store["auth"].user.results.length !== 0) {
      const time = store["auth"].user.results[store["auth"].user.results.length - 1].created_at;
      return this.$moment(time).fromNow();
    } else if (store["resultsStore"].resultPoll.length !== 0) {
      return this.$moment(store["resultsStore"].resultPoll[0].created_at).fromNow();
    }
  }

  get averageAccuracyPerTest() {
    if (store["auth"].user.correctness && store["auth"].user.correctness['math_nocalc']) {
      return Math.floor(store["auth"].user.correctness['math_nocalc' + '_percent']);
    } else {
      return 0;
    }
  }

  get averageTimeSpentOnEachTest() {
    if (store["auth"].user.timing && store["auth"].user.timing['math_nocalc']) {
      if (store["auth"].user.timing['math_nocalc'] > 3800) {
        return this.$moment.utc(Math.floor(store["auth"].user.timing['math_nocalc']) * 1000).format('h [hour] mm [min] ss [sec]');
      }

      return this.$moment.utc(Math.floor(store["auth"].user.timing['math_nocalc']) * 1000).format('mm [min] ss [sec]');
    } else {
      return 0;
    }
  }

  get name() {
    return store["auth"].user.name;
  }

  get countPassedTests() {
    if (store["auth"].user.results) {
      return store["auth"].user.results.length;
    }
  }

  get computedItems() {
    console.warn(this.items.map(this.mapItem));
    return this.items.map(this.mapItem);
  }

  mapItem(item) {
    return { ...item,
      children: item.children ? item.children.map(this.mapItem) : undefined,
      title: item.title
    };
  }

};
uservue_type_script_lang_ts_User = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'User',
  layout: 'dashboard',
  components: {
    CoreCard: coreCard["a" /* default */],
    coreStatsCard: coreStatsCard["a" /* default */]
  },

  fetch({
    redirect
  }) {
    if (!store["auth"].isAuthenticated && !store["auth"].isAnonimous) {
      redirect('/');
    }
  }

})], uservue_type_script_lang_ts_User);
/* harmony default export */ var uservue_type_script_lang_ts_ = (uservue_type_script_lang_ts_User);
// CONCATENATED MODULE: ./pages/lk/user/index.vue?vue&type=script&lang=ts&
 /* harmony default export */ var lk_uservue_type_script_lang_ts_ = (uservue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VAvatar/VAvatar.js
var VAvatar = __webpack_require__(96);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(343);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VContainer.js
var VContainer = __webpack_require__(314);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(301);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(50);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(72);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/VList.js
var VList = __webpack_require__(92);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/VListItem.js
var VListItem = __webpack_require__(51);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/index.js
var components_VList = __webpack_require__(13);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/VListItemGroup.js
var VListItemGroup = __webpack_require__(97);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VMain/VMain.js
var VMain = __webpack_require__(317);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(341);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VSpacer.js
var VSpacer = __webpack_require__(315);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSubheader/VSubheader.js
var VSubheader = __webpack_require__(342);

// CONCATENATED MODULE: ./pages/lk/user/index.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(480)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  lk_uservue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "94ddee24"
  
)

/* harmony default export */ var user = __webpack_exports__["default"] = (component.exports);

/* vuetify-loader */
















installComponents_default()(component, {VAvatar: VAvatar["a" /* default */],VCol: VCol["a" /* default */],VContainer: VContainer["a" /* default */],VDivider: VDivider["a" /* default */],VIcon: VIcon["a" /* default */],VImg: VImg["a" /* default */],VList: VList["a" /* default */],VListItem: VListItem["a" /* default */],VListItemContent: components_VList["a" /* VListItemContent */],VListItemGroup: VListItemGroup["a" /* default */],VListItemTitle: components_VList["c" /* VListItemTitle */],VMain: VMain["a" /* default */],VRow: VRow["a" /* default */],VSpacer: VSpacer["a" /* default */],VSubheader: VSubheader["a" /* default */]})


/***/ })

};;
//# sourceMappingURL=index.js.map