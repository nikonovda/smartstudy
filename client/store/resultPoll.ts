import { VuexModule, Module, Action, Mutation } from 'vuex-module-decorators'
import { auth } from '../store'
import axios from '../plugins/axios'
import { PollFull } from '../interfaces/interfaceStore'
@Module({
    name: 'resultPoll',
    stateFactory: true,
    namespaced: true
})
export default class ResultPoll extends VuexModule {
    pollStore: PollFull;
    pollStoreHistory: any = []
   
    @Mutation
    SET_POLL (payload) {
        this.pollStore = payload
        this.pollStoreHistory.push(payload)
    }

    @Action({ rawError: true })
    async getExportFile (id:string) {
        await axios.get(`/api/polls/${id}/exportResults`)
            .then( async (response) => {
                if (response.data.status === 'OK') {
                    let a = document.createElement("a")
                    a.href = response.data.url
                    a.click()
                }
            })
    }

    @Action({ rawError: true })
    async init (id: string) {
        let url: string
        if(auth.isAdmin || auth.isRoot) url = '/api/polls/' + id
        else url = '/api/polls/published/' + id
        return await axios.get(url)
                    .then((resp) => {
                        resp.data.sorted.forEach((item) => {
                            item.choised = []
                            item.choiseCount = [ 1, 1 ]
                        })
                        this.SET_POLL(resp.data)
                        return resp.data
                    })
    }
    get poll () {
        return this.pollStore
    }
}
