<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Result extends Model
{

	protected $fillable = [
		'user_id',
		'poll_id',
		'time',
		'answers'
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function poll()
	{
		return $this->belongsTo('App\Poll');
	}


	public function getCorrectCount()
	{
		$count = 0;
		foreach ($this->answers as $item) {
			$hop = Hop::find($item['hop_id']);
			if ($hop->correctAnswer[0] === $this->getOptionValue($item)) $count++;
		}
		return $count;
	}

	public function getRatioCorrect()
	{
		$count = 0;
		foreach ($this->answers as $item) {
			$hop = Hop::find($item['hop_id']);
			if ($hop->correctAnswer[0] === $this->getOptionValue($item)) $count++;
		}
		return $count / count($this->answers);
	}

	public function getTimeCount()
	{
		$time = 0;
		foreach ($this->answers as $item) {
			// dd($item);
			$time += $item['time'];
		}
		return $time;
	}

	protected function getOptionValue(array $answer)
	{
		return count($answer['options']) > 0
			? $answer['options'][0]['id']
			: null;
	}
}
