
export interface Hop {
  [key: string]: any
  _id: string
  content: string
  options: Array<Option>
  poll_id: string
  explanation: string
  image: string[]
  complexity: string
  correctAnswer: number[]
  tags: string[]
  created_at?: string
  updated_at?: string
}

export interface Option {
  id: number,
  attrId?: string,
  value: string,
  type: string,
  image: string,
}

