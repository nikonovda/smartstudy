<?php

namespace App\Http\Controllers;

use App\Services\Statistics\StatisticsService;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = auth()->user();
        $user->results->each(function ($item) {
            $item->poll->loadSortedHops();
        });

        $leaderbord = User::registered()->get()->map(function ($item) {
            return [
                '_id' => $item->_id,
                'rating' => $item->rating ? $item->rating : 0,
                'correctness' => $item->correctness ? $item->correctness : 0,
                'timing' => $item->timing ? $item->timing : 0,
                'updated_at' => $item->updated_at
            ];
        })->filter(function ($item) { return $item['rating'] > 0; });
        // dd($leaderbord);
        $leaderbord = $leaderbord->sortByDesc('updated_at');
        $user->rank = $leaderbord
            // ->sortByDesc('updated_at')
            ->sortByDesc('rating')
            ->values()
            ->search(function ($item) use ($user) {
                return $user->_id === $item['_id'];
            }) + 1;
        
        // dd($leaderbord);
        $rankStats = $leaderbord->map(function ($user) {
            if (!$user['correctness'] || !$user['timing']) return null;
            return collect([
                '_id' => $user['_id'],

                'correctnessNoCalc' => $user['correctness']['math_nocalc'],
                'correctnessCalc' => $user['correctness']['math_calc'],
                'correctnessPersonal' => $user['correctness']['personal'],
                'correctnessOlympics' => array_key_exists('olympics', $user['correctness']) ? $user['correctness']['olympics'] : 0,


                'timingNoCalc' => $user['timing']['math_nocalc'],
                'timingCalc' => $user['timing']['math_calc'],
                'timingPersonal' => $user['timing']['personal'],
                'timingOlympics' => array_key_exists('olympics', $user['timing']) ? $user['timing']['olympics'] : 0

            ]);
        })->filter(function ($user) {
            return $user !== 'null';
        });

        // dd($rankStats);
        $correctnessRank['math_nocalc'] = $rankStats
            ->sortByDesc('correctnessNoCalc')
            ->values()
            ->search(function ($item) use ($user) {
                return $user->_id === $item['_id'];
            }) + 1;
        $correctnessRank['math_calc'] = $rankStats
            ->sortByDesc('correctnessCalc')
            ->values()
            ->search(function ($item) use ($user) {
                return $user->_id === $item['_id'];
            }) + 1;
        $correctnessRank['personal'] = $rankStats
            ->sortByDesc('correctnessPersonal')
            ->values()
            ->search(function ($item) use ($user) {
                return $user->_id === $item['_id'];
            }) + 1;
        $correctnessRank['olympics'] = $rankStats
            ->sortByDesc('correctnessOlympics')
            ->values()
            ->search(function ($item) use ($user) {
                return $user->_id === $item['_id'];
            }) + 1;
        $percents = [];
        foreach ($correctnessRank as $key => $value) {
            $percents[$key . '_percent'] = round($value / $rankStats->count() * 100);
        }
        $user->correctnessRank = array_merge($correctnessRank, $percents);

        $timingRank['math_nocalc'] = $rankStats
            ->sortBy('timingNoCalc')
            ->values()
            ->search(function ($item) use ($user) {
                return $user->_id === $item['_id'];
            }) + 1;
        $timingRank['math_calc'] = $rankStats
            ->sortBy('timingCalc')
            ->values()
            ->search(function ($item) use ($user) {
                return $user->_id === $item['_id'];
            }) + 1;
        $timingRank['personal'] = $rankStats
            ->sortBy('timingPersonal')
            ->values()
            ->search(function ($item) use ($user) {
                return $user->_id === $item['_id'];
            }) + 1;
        $timingRank['olympics'] = $rankStats
            ->sortBy('timingOlympics')
            ->values()
            ->search(function ($item) use ($user) {
                return $user->_id === $item['_id'];
            }) + 1;

        $percents = [];
        foreach ($timingRank as $key => $value) {
            $percents[$key . '_percent'] = round($value / $rankStats->count() * 100);
        }
        $user->timingRank = array_merge($timingRank, $percents);

        // $user->completed_polls = $user->getCompletedPolls();
        $user->commonStatistics = (new StatisticsService)->allDepersonized($user);
        $user->statistics = (new StatisticsService)->byUser($user);
        $service = new StatisticsService();
        $user->averageStatistics = [
            'math_calc' => [
                'percentageOfCorrectAnswersOnEechThemeAverage' => $service->percentageOfCorrectAnswersOnEechThemeAverage('math_calc'),
                'averageTimeSpendToTopicValueUsers' => $service->averageTimeSpendToTopicValueUsers('math_calc'),
                'percentageCorrectAnwsersOfTopicAverage' => $service->percentageCorrectAnwsersOfTopicAverage('math_calc'),
                'predictiveTimeToResolveTests' => $service->predictiveTimeToResolveTests('math_calc', $user),
                'predictiveOfCorrectAnswers' => $service->predictiveOfCorrectAnswers('math_calc', $user),
            ],
            'math_nocalc' => [
                'percentageOfCorrectAnswersOnEechThemeAverage' => $service->percentageOfCorrectAnswersOnEechThemeAverage('math_nocalc'),
                'averageTimeSpendToTopicValueUsers' => $service->averageTimeSpendToTopicValueUsers('math_nocalc'),
                'percentageCorrectAnwsersOfTopicAverage' => $service->percentageCorrectAnwsersOfTopicAverage('math_nocalc'),
                'predictiveTimeToResolveTests' => $service->predictiveTimeToResolveTests('math_nocalc', $user),
                'predictiveOfCorrectAnswers' => $service->predictiveOfCorrectAnswers('math_nocalc', $user),
            ],
            'personal' => [
                'percentageOfCorrectAnswersOnEechThemeAverage' => $service->percentageOfCorrectAnswersOnEechThemeAverage('personal'),
                'averageTimeSpendToTopicValueUsers' => $service->averageTimeSpendToTopicValueUsers('personal'),
                'percentageCorrectAnwsersOfTopicAverage' => $service->percentageCorrectAnwsersOfTopicAverage('personal'),
                'predictiveTimeToResolveTests' => $service->predictiveTimeToResolveTests('personal', $user),
                'predictiveOfCorrectAnswers' => $service->predictiveOfCorrectAnswers('personal', $user),
            ],
            'olympics' => [
                'percentageOfCorrectAnswersOnEechThemeAverage' => $service->percentageOfCorrectAnswersOnEechThemeAverage('olympics'),
                'averageTimeSpendToTopicValueUsers' => $service->averageTimeSpendToTopicValueUsers('olympics'),
                'percentageCorrectAnwsersOfTopicAverage' => $service->percentageCorrectAnwsersOfTopicAverage('olympics'),
                'predictiveTimeToResolveTests' => $service->predictiveTimeToResolveTests('olympics', $user),
                'predictiveOfCorrectAnswers' => $service->predictiveOfCorrectAnswers('olympics', $user),
            ]
        ];
        return $user;
    }

    public function tempProfile()
    {
        $commonStatistics = (new StatisticsService)->all();
        $service = new StatisticsService();
        $averageStatistics = [
            'math_calc' => [
                'percentageOfCorrectAnswersOnEechThemeAverage' => $service->percentageOfCorrectAnswersOnEechThemeAverage('math_calc'),
                'averageTimeSpendToTopicValueUsers' => $service->averageTimeSpendToTopicValueUsers('math_calc'),
                'percentageCorrectAnwsersOfTopicAverage' => $service->percentageCorrectAnwsersOfTopicAverage('math_calc'),
                'predictiveTimeToResolveTests' => $service->predictiveTimeToResolveTests('math_calc'),
                'predictiveOfCorrectAnswers' => $service->predictiveOfCorrectAnswers('math_calc'),
            ],
            'math_nocalc' => [
                'percentageOfCorrectAnswersOnEechThemeAverage' => $service->percentageOfCorrectAnswersOnEechThemeAverage('math_nocalc'),
                'averageTimeSpendToTopicValueUsers' => $service->averageTimeSpendToTopicValueUsers('math_nocalc'),
                'percentageCorrectAnwsersOfTopicAverage' => $service->percentageCorrectAnwsersOfTopicAverage('math_nocalc'),
                'predictiveTimeToResolveTests' => $service->predictiveTimeToResolveTests('math_nocalc'),
                'predictiveOfCorrectAnswers' => $service->predictiveOfCorrectAnswers('math_nocalc'),
            ],
            'personal' => [
                'percentageOfCorrectAnswersOnEechThemeAverage' => $service->percentageOfCorrectAnswersOnEechThemeAverage('personal'),
                'averageTimeSpendToTopicValueUsers' => $service->averageTimeSpendToTopicValueUsers('personal'),
                'percentageCorrectAnwsersOfTopicAverage' => $service->percentageCorrectAnwsersOfTopicAverage('personal'),
                'predictiveTimeToResolveTests' => $service->predictiveTimeToResolveTests('personal'),
                'predictiveOfCorrectAnswers' => $service->predictiveOfCorrectAnswers('personal'),
            ],
            'olympics' => [
                'percentageOfCorrectAnswersOnEechThemeAverage' => $service->percentageOfCorrectAnswersOnEechThemeAverage('olympics'),
                'averageTimeSpendToTopicValueUsers' => $service->averageTimeSpendToTopicValueUsers('olympics'),
                'percentageCorrectAnwsersOfTopicAverage' => $service->percentageCorrectAnwsersOfTopicAverage('olympics'),
                'predictiveTimeToResolveTests' => $service->predictiveTimeToResolveTests('olympics'),
                'predictiveOfCorrectAnswers' => $service->predictiveOfCorrectAnswers('olympics'),
            ]
        ];
        return response()->json([ 'commonStatistics' => $commonStatistics, 'averageStatistics' => $averageStatistics ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
