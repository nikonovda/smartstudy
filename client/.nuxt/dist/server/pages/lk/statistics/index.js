exports.ids = [13];
exports.modules = {

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VCardActions; });
/* unused harmony export VCardSubtitle */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return VCardText; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return VCardTitle; });
/* harmony import */ var _VCard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(298);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);


const VCardActions = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__actions');
const VCardSubtitle = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__subtitle');
const VCardText = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__text');
const VCardTitle = Object(_util_helpers__WEBPACK_IMPORTED_MODULE_1__[/* createSimpleFunctional */ "i"])('v-card__title');

/* unused harmony default export */ var _unused_webpack_default_export = ({
  $_vuetify_subcomponents: {
    VCard: _VCard__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"],
    VCardActions,
    VCardSubtitle,
    VCardText,
    VCardTitle
  }
});

/***/ }),

/***/ 322:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(345);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("0db8c820", content, true, context)
};

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreCard.vue?vue&type=template&id=0a0f4055&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-card',_vm._b({staticClass:"v-card--material pa-3",class:_vm.classes},'v-card',_vm.$attrs,false),[_c('div',{staticClass:"d-flex grow flex-wrap"},[(_vm.avatar)?_c('v-avatar',{staticClass:"mx-auto v-card--material__avatar elevation-6",attrs:{"size":"128","color":"grey"}},[_c('v-img',{attrs:{"src":_vm.avatar}})],1):(_vm.$slots.image || _vm.place || _vm.icon || _vm.$slots.heading)?_c('v-sheet',{staticClass:"text-start v-card--material__heading mb-n6",class:{ 'pa-7': !_vm.$slots.image && !_vm.place },style:(_vm.place ? "display: flex;" : ""),attrs:{"color":_vm.color,"max-height":_vm.icon || _vm.place ? 90 : undefined,"width":_vm.icon || _vm.place ? '' : '100%',"elevation":_vm.place ? 0 : 6,"dark":""}},[(_vm.$slots.heading)?_vm._t("heading"):(_vm.$slots.image)?_vm._t("image"):(_vm.title && !_vm.icon)?_c('div',{staticClass:"display-1 font-weight-light",domProps:{"textContent":_vm._s(_vm.title)}}):(_vm.icon)?_c('v-icon',{attrs:{"size":"32"},domProps:{"textContent":_vm._s(_vm.icon)}}):(_vm.place)?[_c('div',{staticClass:"text-center",style:("padding: 15px 20px; background-color:" + _vm.colorPlace + "; border-radius: 4px; width: 115px")},[_c('span',{staticClass:"display-1"},[_vm._v("Rank:")]),_c('h1',{staticClass:"display-3"},[_vm._v(_vm._s(_vm.place))])]),_c('div',{staticClass:"text-center ml-4",style:("padding: 15px 20px; background-color:" + _vm.colorPlace + "; border-radius: 4px; width: 115px")},[_c('span',{staticClass:"display-1"},[_vm._v("Top:")]),_c('h1',{staticClass:"display-3"},[_vm._v(_vm._s(_vm.placePercentage + "%"))])])]:_vm._e(),(_vm.text)?_c('div',{staticClass:"headline font-weight-thin",domProps:{"textContent":_vm._s(_vm.text)}}):_vm._e()],2):_vm._e(),(_vm.$slots['after-heading'])?_c('div',{staticClass:"justify-center align-center",staticStyle:{"height":"calc(100% - 85px)"}},[_vm._t("after-heading")],2):(_vm.icon && _vm.title)?_c('div',{staticClass:"ml-4"},[_c('div',{staticClass:"card-title font-weight-light",domProps:{"textContent":_vm._s(_vm.title)}})]):_vm._e()],1),_vm._t("default"),(_vm.$slots.actions)?[_c('v-divider',{staticClass:"mt-2"}),_c('v-card-actions',{staticClass:"pb-0"},[_vm._t("actions")],2)]:_vm._e()],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/core/coreCard.vue?vue&type=template&id=0a0f4055&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreCard.vue?vue&type=script&lang=ts&


let coreCardvue_type_script_lang_ts_CoreCard = class CoreCard extends external_vue_property_decorator_["Vue"] {
  get classes() {
    return {
      'v-card--material--has-heading': this.hasHeading
    };
  }

  get hasHeading() {
    return Boolean(this.$slots.heading || this.title || this.icon);
  }

  get hasAltHeading() {
    return Boolean(this.$slots.heading || this.title && this.icon);
  }

};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "avatar", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: 'success'
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "color", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: 'success'
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "colorPlace", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "icon", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Boolean,
  default: false
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "image", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "text", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "title", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "place", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: ''
})], coreCardvue_type_script_lang_ts_CoreCard.prototype, "placePercentage", void 0);

coreCardvue_type_script_lang_ts_CoreCard = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'CoreCard'
})], coreCardvue_type_script_lang_ts_CoreCard);
/* harmony default export */ var coreCardvue_type_script_lang_ts_ = (coreCardvue_type_script_lang_ts_CoreCard);
// CONCATENATED MODULE: ./components/core/coreCard.vue?vue&type=script&lang=ts&
 /* harmony default export */ var core_coreCardvue_type_script_lang_ts_ = (coreCardvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VAvatar/VAvatar.js
var VAvatar = __webpack_require__(96);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(298);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(296);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(301);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(50);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VImg/VImg.js + 2 modules
var VImg = __webpack_require__(72);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSheet/VSheet.js
var VSheet = __webpack_require__(24);

// CONCATENATED MODULE: ./components/core/coreCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(344)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_coreCardvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "f4daf264"
  
)

/* harmony default export */ var coreCard = __webpack_exports__["a"] = (component.exports);

/* vuetify-loader */








installComponents_default()(component, {VAvatar: VAvatar["a" /* default */],VCard: VCard["a" /* default */],VCardActions: components_VCard["a" /* VCardActions */],VDivider: VDivider["a" /* default */],VIcon: VIcon["a" /* default */],VImg: VImg["a" /* default */],VSheet: VSheet["a" /* default */]})


/***/ }),

/***/ 334:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(371);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("91e81038", content, true, context)
};

/***/ }),

/***/ 335:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(373);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("09b645a2", content, true, context)
};

/***/ }),

/***/ 340:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VChip_VChip_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(349);
/* harmony import */ var _src_components_VChip_VChip_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VChip_VChip_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var _transitions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(32);
/* harmony import */ var _VIcon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(23);
/* harmony import */ var _mixins_colorable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(12);
/* harmony import */ var _mixins_groupable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(25);
/* harmony import */ var _mixins_themeable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(11);
/* harmony import */ var _mixins_toggleable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(15);
/* harmony import */ var _mixins_routable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(17);
/* harmony import */ var _mixins_sizeable__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(59);
/* harmony import */ var _util_console__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6);
// Styles

 // Components


 // Mixins






 // Utilities


/* @vue/component */

/* harmony default export */ __webpack_exports__["a"] = (Object(_util_mixins__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(_mixins_colorable__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], _mixins_sizeable__WEBPACK_IMPORTED_MODULE_9__[/* default */ "a"], _mixins_routable__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"], _mixins_themeable__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], Object(_mixins_groupable__WEBPACK_IMPORTED_MODULE_5__[/* factory */ "a"])('chipGroup'), Object(_mixins_toggleable__WEBPACK_IMPORTED_MODULE_7__[/* factory */ "b"])('inputValue')).extend({
  name: 'v-chip',
  props: {
    active: {
      type: Boolean,
      default: true
    },
    activeClass: {
      type: String,

      default() {
        if (!this.chipGroup) return '';
        return this.chipGroup.activeClass;
      }

    },
    close: Boolean,
    closeIcon: {
      type: String,
      default: '$delete'
    },
    disabled: Boolean,
    draggable: Boolean,
    filter: Boolean,
    filterIcon: {
      type: String,
      default: '$complete'
    },
    label: Boolean,
    link: Boolean,
    outlined: Boolean,
    pill: Boolean,
    tag: {
      type: String,
      default: 'span'
    },
    textColor: String,
    value: null
  },
  data: () => ({
    proxyClass: 'v-chip--active'
  }),
  computed: {
    classes() {
      return {
        'v-chip': true,
        ..._mixins_routable__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"].options.computed.classes.call(this),
        'v-chip--clickable': this.isClickable,
        'v-chip--disabled': this.disabled,
        'v-chip--draggable': this.draggable,
        'v-chip--label': this.label,
        'v-chip--link': this.isLink,
        'v-chip--no-color': !this.color,
        'v-chip--outlined': this.outlined,
        'v-chip--pill': this.pill,
        'v-chip--removable': this.hasClose,
        ...this.themeClasses,
        ...this.sizeableClasses,
        ...this.groupClasses
      };
    },

    hasClose() {
      return Boolean(this.close);
    },

    isClickable() {
      return Boolean(_mixins_routable__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"].options.computed.isClickable.call(this) || this.chipGroup);
    }

  },

  created() {
    const breakingProps = [['outline', 'outlined'], ['selected', 'input-value'], ['value', 'active'], ['@input', '@active.sync']];
    /* istanbul ignore next */

    breakingProps.forEach(([original, replacement]) => {
      if (this.$attrs.hasOwnProperty(original)) Object(_util_console__WEBPACK_IMPORTED_MODULE_10__[/* breaking */ "a"])(original, replacement, this);
    });
  },

  methods: {
    click(e) {
      this.$emit('click', e);
      this.chipGroup && this.toggle();
    },

    genFilter() {
      const children = [];

      if (this.isActive) {
        children.push(this.$createElement(_VIcon__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
          staticClass: 'v-chip__filter',
          props: {
            left: true
          }
        }, this.filterIcon));
      }

      return this.$createElement(_transitions__WEBPACK_IMPORTED_MODULE_2__[/* VExpandXTransition */ "b"], children);
    },

    genClose() {
      return this.$createElement(_VIcon__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
        staticClass: 'v-chip__close',
        props: {
          right: true,
          size: 18
        },
        on: {
          click: e => {
            e.stopPropagation();
            e.preventDefault();
            this.$emit('click:close');
            this.$emit('update:active', false);
          }
        }
      }, this.closeIcon);
    },

    genContent() {
      return this.$createElement('span', {
        staticClass: 'v-chip__content'
      }, [this.filter && this.genFilter(), this.$slots.default, this.hasClose && this.genClose()]);
    }

  },

  render(h) {
    const children = [this.genContent()];
    let {
      tag,
      data
    } = this.generateRouteLink();
    data.attrs = { ...data.attrs,
      draggable: this.draggable ? 'true' : undefined,
      tabindex: this.chipGroup && !this.disabled ? 0 : data.attrs.tabindex
    };
    data.directives.push({
      name: 'show',
      value: this.active
    });
    data = this.setBackgroundColor(this.color, data);
    const color = this.textColor || this.outlined && this.color;
    return h(tag, this.setTextColor(color, data), children);
  }

}));

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(131);
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _util_mergeData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);



 // no xs

const breakpoints = ['sm', 'md', 'lg', 'xl'];
const ALIGNMENT = ['start', 'end', 'center'];

function makeProps(prefix, def) {
  return breakpoints.reduce((props, val) => {
    props[prefix + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = def();
    return props;
  }, {});
}

const alignValidator = str => [...ALIGNMENT, 'baseline', 'stretch'].includes(str);

const alignProps = makeProps('align', () => ({
  type: String,
  default: null,
  validator: alignValidator
}));

const justifyValidator = str => [...ALIGNMENT, 'space-between', 'space-around'].includes(str);

const justifyProps = makeProps('justify', () => ({
  type: String,
  default: null,
  validator: justifyValidator
}));

const alignContentValidator = str => [...ALIGNMENT, 'space-between', 'space-around', 'stretch'].includes(str);

const alignContentProps = makeProps('alignContent', () => ({
  type: String,
  default: null,
  validator: alignContentValidator
}));
const propMap = {
  align: Object.keys(alignProps),
  justify: Object.keys(justifyProps),
  alignContent: Object.keys(alignContentProps)
};
const classMap = {
  align: 'align',
  justify: 'justify',
  alignContent: 'align-content'
};

function breakpointClass(type, prop, val) {
  let className = classMap[type];

  if (val == null) {
    return undefined;
  }

  if (prop) {
    // alignSm -> Sm
    const breakpoint = prop.replace(type, '');
    className += `-${breakpoint}`;
  } // .align-items-sm-center


  className += `-${val}`;
  return className.toLowerCase();
}

const cache = new Map();
/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_1___default.a.extend({
  name: 'v-row',
  functional: true,
  props: {
    tag: {
      type: String,
      default: 'div'
    },
    dense: Boolean,
    noGutters: Boolean,
    align: {
      type: String,
      default: null,
      validator: alignValidator
    },
    ...alignProps,
    justify: {
      type: String,
      default: null,
      validator: justifyValidator
    },
    ...justifyProps,
    alignContent: {
      type: String,
      default: null,
      validator: alignContentValidator
    },
    ...alignContentProps
  },

  render(h, {
    props,
    data,
    children
  }) {
    // Super-fast memoization based on props, 5x faster than JSON.stringify
    let cacheKey = '';

    for (const prop in props) {
      cacheKey += String(props[prop]);
    }

    let classList = cache.get(cacheKey);

    if (!classList) {
      classList = []; // Loop through `align`, `justify`, `alignContent` breakpoint props

      let type;

      for (type in propMap) {
        propMap[type].forEach(prop => {
          const value = props[prop];
          const className = breakpointClass(type, prop, value);
          if (className) classList.push(className);
        });
      }

      classList.push({
        'no-gutters': props.noGutters,
        'row--dense': props.dense,
        [`align-${props.align}`]: props.align,
        [`justify-${props.justify}`]: props.justify,
        [`align-content-${props.alignContent}`]: props.alignContent
      });
      cache.set(cacheKey, classList);
    }

    return h(props.tag, Object(_util_mergeData__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(data, {
      staticClass: 'row',
      class: classList
    }), children);
  }

}));

/***/ }),

/***/ 343:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(131);
/* harmony import */ var _src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VGrid_VGrid_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _util_mergeData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _util_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);



 // no xs

const breakpoints = ['sm', 'md', 'lg', 'xl'];

const breakpointProps = (() => {
  return breakpoints.reduce((props, val) => {
    props[val] = {
      type: [Boolean, String, Number],
      default: false
    };
    return props;
  }, {});
})();

const offsetProps = (() => {
  return breakpoints.reduce((props, val) => {
    props['offset' + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = {
      type: [String, Number],
      default: null
    };
    return props;
  }, {});
})();

const orderProps = (() => {
  return breakpoints.reduce((props, val) => {
    props['order' + Object(_util_helpers__WEBPACK_IMPORTED_MODULE_3__[/* upperFirst */ "F"])(val)] = {
      type: [String, Number],
      default: null
    };
    return props;
  }, {});
})();

const propMap = {
  col: Object.keys(breakpointProps),
  offset: Object.keys(offsetProps),
  order: Object.keys(orderProps)
};

function breakpointClass(type, prop, val) {
  let className = type;

  if (val == null || val === false) {
    return undefined;
  }

  if (prop) {
    const breakpoint = prop.replace(type, '');
    className += `-${breakpoint}`;
  } // Handling the boolean style prop when accepting [Boolean, String, Number]
  // means Vue will not convert <v-col sm></v-col> to sm: true for us.
  // Since the default is false, an empty string indicates the prop's presence.


  if (type === 'col' && (val === '' || val === true)) {
    // .col-md
    return className.toLowerCase();
  } // .order-md-6


  className += `-${val}`;
  return className.toLowerCase();
}

const cache = new Map();
/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_1___default.a.extend({
  name: 'v-col',
  functional: true,
  props: {
    cols: {
      type: [Boolean, String, Number],
      default: false
    },
    ...breakpointProps,
    offset: {
      type: [String, Number],
      default: null
    },
    ...offsetProps,
    order: {
      type: [String, Number],
      default: null
    },
    ...orderProps,
    alignSelf: {
      type: String,
      default: null,
      validator: str => ['auto', 'start', 'end', 'center', 'baseline', 'stretch'].includes(str)
    },
    tag: {
      type: String,
      default: 'div'
    }
  },

  render(h, {
    props,
    data,
    children,
    parent
  }) {
    // Super-fast memoization based on props, 5x faster than JSON.stringify
    let cacheKey = '';

    for (const prop in props) {
      cacheKey += String(props[prop]);
    }

    let classList = cache.get(cacheKey);

    if (!classList) {
      classList = []; // Loop through `col`, `offset`, `order` breakpoint props

      let type;

      for (type in propMap) {
        propMap[type].forEach(prop => {
          const value = props[prop];
          const className = breakpointClass(type, prop, value);
          if (className) classList.push(className);
        });
      }

      const hasColClasses = classList.some(className => className.startsWith('col-'));
      classList.push({
        // Default to .col if no other col-{bp}-* classes generated nor `cols` specified.
        col: !hasColClasses || !props.cols,
        [`col-${props.cols}`]: props.cols,
        [`offset-${props.offset}`]: props.offset,
        [`order-${props.order}`]: props.order,
        [`align-self-${props.alignSelf}`]: props.alignSelf
      });
      cache.set(cacheKey, classList);
    }

    return h(props.tag, Object(_util_mergeData__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(data, {
      class: classList
    }), children);
  }

}));

/***/ }),

/***/ 344:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(322);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 345:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-card--material__avatar{position:relative;top:-64px;margin-bottom:-32px}.v-card--material__heading{position:relative;top:-40px;transition:.3s ease;z-index:1}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 349:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(350);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(10).default("197fcea4", content, true)

/***/ }),

/***/ 350:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-chip:not(.v-chip--outlined).accent,.v-chip:not(.v-chip--outlined).error,.v-chip:not(.v-chip--outlined).info,.v-chip:not(.v-chip--outlined).primary,.v-chip:not(.v-chip--outlined).secondary,.v-chip:not(.v-chip--outlined).success,.v-chip:not(.v-chip--outlined).warning{color:#fff}.theme--light.v-chip{border-color:rgba(0,0,0,.12);color:rgba(0,0,0,.87)}.theme--light.v-chip:not(.v-chip--active){background:#e0e0e0}.theme--light.v-chip:hover:before{opacity:.04}.theme--light.v-chip--active:before,.theme--light.v-chip--active:hover:before,.theme--light.v-chip:focus:before{opacity:.12}.theme--light.v-chip--active:focus:before{opacity:.16}.theme--dark.v-chip{border-color:hsla(0,0%,100%,.12);color:#fff}.theme--dark.v-chip:not(.v-chip--active){background:#555}.theme--dark.v-chip:hover:before{opacity:.08}.theme--dark.v-chip--active:before,.theme--dark.v-chip--active:hover:before,.theme--dark.v-chip:focus:before{opacity:.24}.theme--dark.v-chip--active:focus:before{opacity:.32}.v-chip{align-items:center;cursor:default;display:inline-flex;line-height:20px;max-width:100%;outline:none;overflow:hidden;padding:0 12px;position:relative;text-decoration:none;transition-duration:.28s;transition-property:box-shadow,opacity;transition-timing-function:cubic-bezier(.4,0,.2,1);vertical-align:middle;white-space:nowrap}.v-chip:before{background-color:currentColor;bottom:0;border-radius:inherit;content:\"\";left:0;opacity:0;position:absolute;pointer-events:none;right:0;top:0}.v-chip .v-avatar{height:24px!important;min-width:24px!important;width:24px!important}.v-chip .v-icon{font-size:24px}.v-application--is-ltr .v-chip .v-avatar--left,.v-application--is-ltr .v-chip .v-icon--left{margin-left:-6px;margin-right:6px}.v-application--is-ltr .v-chip .v-avatar--right,.v-application--is-ltr .v-chip .v-icon--right,.v-application--is-rtl .v-chip .v-avatar--left,.v-application--is-rtl .v-chip .v-icon--left{margin-left:6px;margin-right:-6px}.v-application--is-rtl .v-chip .v-avatar--right,.v-application--is-rtl .v-chip .v-icon--right{margin-left:-6px;margin-right:6px}.v-chip:not(.v-chip--no-color) .v-icon{color:inherit}.v-chip .v-chip__close.v-icon{font-size:18px;max-height:18px;max-width:18px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.v-application--is-ltr .v-chip .v-chip__close.v-icon.v-icon--right{margin-right:-4px}.v-application--is-rtl .v-chip .v-chip__close.v-icon.v-icon--right{margin-left:-4px}.v-chip .v-chip__close.v-icon:active,.v-chip .v-chip__close.v-icon:focus,.v-chip .v-chip__close.v-icon:hover{opacity:.72}.v-chip .v-chip__content{align-items:center;display:inline-flex;height:100%;max-width:100%}.v-chip--active .v-icon{color:inherit}.v-chip--link:before{transition:opacity .3s cubic-bezier(.25,.8,.5,1)}.v-chip--link:focus:before{opacity:.32}.v-chip--clickable{cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.v-chip--clickable:active{box-shadow:0 3px 1px -2px rgba(0,0,0,.2),0 2px 2px 0 rgba(0,0,0,.14),0 1px 5px 0 rgba(0,0,0,.12)}.v-chip--disabled{opacity:.4;pointer-events:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.v-chip__filter{max-width:24px}.v-chip__filter.v-icon{color:inherit}.v-chip__filter.expand-x-transition-enter,.v-chip__filter.expand-x-transition-leave-active{margin:0}.v-chip--pill .v-chip__filter{margin-right:0 16px 0 0}.v-chip--pill .v-avatar{height:32px!important;width:32px!important}.v-application--is-ltr .v-chip--pill .v-avatar--left{margin-left:-12px}.v-application--is-ltr .v-chip--pill .v-avatar--right,.v-application--is-rtl .v-chip--pill .v-avatar--left{margin-right:-12px}.v-application--is-rtl .v-chip--pill .v-avatar--right{margin-left:-12px}.v-chip--label{border-radius:4px!important}.v-chip.v-chip--outlined{border-width:thin;border-style:solid}.v-chip.v-chip--outlined.v-chip--active:before{opacity:.08}.v-chip.v-chip--outlined .v-icon{color:inherit}.v-chip.v-chip--outlined.v-chip.v-chip{background-color:transparent!important}.v-chip.v-chip--selected{background:transparent}.v-chip.v-chip--selected:after{opacity:.28}.v-chip.v-size--x-small{border-radius:8px;font-size:10px;height:16px}.v-chip.v-size--small{border-radius:12px;font-size:12px;height:24px}.v-chip.v-size--default{border-radius:16px;font-size:14px;height:32px}.v-chip.v-size--large{border-radius:27px;font-size:16px;height:54px}.v-chip.v-size--x-large{border-radius:33px;font-size:18px;height:66px}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreChartCard.vue?vue&type=template&id=0a3012ae&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('coreCard',_vm._g(_vm._b({staticClass:"v-card--material-chart"},'coreCard',_vm.$attrs,false),_vm.$listeners),[_vm._t("title"),(_vm.type === "Line")?_c('line-chart',{attrs:{"chart-data":_vm.data,"options":_vm.options,"height":_vm.heigntChart}}):_vm._e(),(_vm.type === "Bar")?_c('bar-chart',{attrs:{"chart-data":_vm.data,"options":_vm.options,"height":_vm.heigntChart}}):_vm._e(),(_vm.type === "Time-Bar")?_c('time-bar-chart',{attrs:{"chart-data":_vm.data,"options":_vm.options,"height":_vm.heigntChart}}):_vm._e(),(_vm.type === "Time-Line")?_c('time-line-chart',{attrs:{"chart-data":_vm.data,"options":_vm.options,"height":_vm.heigntChart}}):_vm._e(),(_vm.type === "Radar")?_c('Radar',{attrs:{"chart-data":_vm.data,"options":_vm.options,"height":_vm.heigntChart}}):_vm._e(),_vm._t("reveal-actions",null,{"slot":"reveal-actions"}),_vm._t("actions",null,{"slot":"actions"})],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/core/coreChartCard.vue?vue&type=template&id=0a3012ae&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// EXTERNAL MODULE: ./components/core/coreCard.vue + 4 modules
var coreCard = __webpack_require__(324);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreChartCard.vue?vue&type=script&lang=ts&



let coreChartCardvue_type_script_lang_ts_CoreChartCard = class CoreChartCard extends external_vue_property_decorator_["Vue"] {};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Object,
  default: () => ({})
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "data", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Array,
  default: () => []
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "eventHandlers", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Object,
  default: () => ({})
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "options", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "ratio", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Array,
  default: () => []
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "responsiveOptions", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  required: true,
  validator: v => ['Bar', 'Line', 'Time-Bar', 'Time-Line', 'Radar'].includes(v)
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "type", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: Number,
  default: 100
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "heigntChart", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String
})], coreChartCardvue_type_script_lang_ts_CoreChartCard.prototype, "backgroundChart", void 0);

coreChartCardvue_type_script_lang_ts_CoreChartCard = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'CoreChartCard',
  inheritAttrs: false,
  components: {
    coreCard: coreCard["a" /* default */]
  }
})], coreChartCardvue_type_script_lang_ts_CoreChartCard);
/* harmony default export */ var coreChartCardvue_type_script_lang_ts_ = (coreChartCardvue_type_script_lang_ts_CoreChartCard);
// CONCATENATED MODULE: ./components/core/coreChartCard.vue?vue&type=script&lang=ts&
 /* harmony default export */ var core_coreChartCardvue_type_script_lang_ts_ = (coreChartCardvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// CONCATENATED MODULE: ./components/core/coreChartCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(372)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_coreChartCardvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "a027eb80"
  
)

/* harmony default export */ var coreChartCard = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(298);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/Card.vue?vue&type=script&lang=js&

/* harmony default export */ var Cardvue_type_script_lang_js_ = ({
  name: 'Card',
  extends: VCard["a" /* default */]
});
// CONCATENATED MODULE: ./components/core/Card.vue?vue&type=script&lang=js&
 /* harmony default export */ var core_Cardvue_type_script_lang_js_ = (Cardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// CONCATENATED MODULE: ./components/core/Card.vue
var render, staticRenderFns




/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_Cardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "48139ca2"
  
)

/* harmony default export */ var Card = __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(334);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreStatsCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 371:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-card--material-stats{display:flex;flex-wrap:wrap;position:relative}.v-card--material-stats>div:first-child{justify-content:space-between}.v-card--material-stats .v-card{border-radius:4px;flex:0 1 auto}.v-card--material-stats .v-card__text{display:inline-block;flex:1 0 calc(100% - 120px);position:absolute;top:0;right:0;width:100%}.v-card--material-stats .v-card__actions{flex:1 0 100%}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 372:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreChartCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(335);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreChartCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreChartCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreChartCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreChartCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_coreChartCard_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 373:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-card--material-chart p{color:#999}.v-card--material-chart .v-card--material__heading{max-height:185px}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreStatsCard.vue?vue&type=template&id=8d7c3be4&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('coreCard',_vm._g(_vm._b({staticClass:"v-card--material-stats",attrs:{"icon":_vm.icon},scopedSlots:_vm._u([{key:"after-heading",fn:function(){return [_c('div',{staticClass:"ml-auto text-right"},[_c('div',{staticClass:"display-2 grey--text font-weight-light",domProps:{"textContent":_vm._s(_vm.title)}}),_c('h3',{staticClass:"display-3 font-weight-light text--primary"},[_vm._v(_vm._s(_vm.value)+" "),_c('small',[_vm._v(" "+_vm._s(_vm.smallValue)+" ")])])])]},proxy:true}])},'coreCard',_vm.$attrs,false),_vm.$listeners),[_c('v-col',{staticClass:"px-0",attrs:{"cols":"12"}},[_c('v-divider')],1),_c('v-icon',{staticClass:"ml-2 mr-1",attrs:{"color":_vm.subIconColor,"size":"16"}},[_vm._v(_vm._s(_vm.subIcon))]),_c('span',{staticClass:"caption grey--text font-weight-light",class:_vm.subTextColor,domProps:{"textContent":_vm._s(_vm.subText)}})],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/core/coreStatsCard.vue?vue&type=template&id=8d7c3be4&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// EXTERNAL MODULE: ./components/core/Card.vue + 2 modules
var Card = __webpack_require__(361);

// EXTERNAL MODULE: ./components/core/coreCard.vue + 4 modules
var coreCard = __webpack_require__(324);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/core/coreStatsCard.vue?vue&type=script&lang=ts&




let coreStatsCardvue_type_script_lang_ts_ComponentName = class ComponentName extends external_vue_property_decorator_["Vue"] {};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  required: true
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "icon", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "subIcon", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "subIconColor", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "subTextColor", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "subText", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "title", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "value", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  default: undefined
})], coreStatsCardvue_type_script_lang_ts_ComponentName.prototype, "smallValue", void 0);

coreStatsCardvue_type_script_lang_ts_ComponentName = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'CoreStatsCard',
  inheritAttrs: false,
  components: {
    coreCard: coreCard["a" /* default */]
  },
  props: { ...Card["a" /* default */].props
  }
})], coreStatsCardvue_type_script_lang_ts_ComponentName);
/* harmony default export */ var coreStatsCardvue_type_script_lang_ts_ = (coreStatsCardvue_type_script_lang_ts_ComponentName);
// CONCATENATED MODULE: ./components/core/coreStatsCard.vue?vue&type=script&lang=ts&
 /* harmony default export */ var core_coreStatsCardvue_type_script_lang_ts_ = (coreStatsCardvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(343);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VDivider/VDivider.js
var VDivider = __webpack_require__(301);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(50);

// CONCATENATED MODULE: ./components/core/coreStatsCard.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(370)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  core_coreStatsCardvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "48bea601"
  
)

/* harmony default export */ var coreStatsCard = __webpack_exports__["a"] = (component.exports);

/* vuetify-loader */




installComponents_default()(component, {VCol: VCol["a" /* default */],VDivider: VDivider["a" /* default */],VIcon: VIcon["a" /* default */]})


/***/ }),

/***/ 437:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(438);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(10).default("3c0eddd7", content, true)

/***/ }),

/***/ 438:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".v-chip-group .v-chip{margin:4px 8px 4px 0}.v-chip-group .v-chip--active{color:inherit}.v-chip-group .v-chip--active.v-chip--no-color:after{opacity:.22}.v-chip-group .v-chip--active.v-chip--no-color:focus:after{opacity:.32}.v-chip-group .v-slide-group__content{padding:4px 0}.v-chip-group--column .v-slide-group__content{white-space:normal;flex-wrap:wrap;max-width:100%}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 439:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(475);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(10).default
module.exports.__inject__ = function (context) {
  add("4ef9e00c", content, true, context)
};

/***/ }),

/***/ 473:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _src_components_VChipGroup_VChipGroup_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(437);
/* harmony import */ var _src_components_VChipGroup_VChipGroup_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_components_VChipGroup_VChipGroup_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _VSlideGroup_VSlideGroup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(52);
/* harmony import */ var _mixins_colorable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(12);
/* harmony import */ var _util_mixins__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4);
// Styles
 // Extensions

 // Mixins

 // Utilities


/* @vue/component */

/* harmony default export */ __webpack_exports__["a"] = (Object(_util_mixins__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"])(_VSlideGroup_VSlideGroup__WEBPACK_IMPORTED_MODULE_1__[/* BaseSlideGroup */ "a"], _mixins_colorable__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"]).extend({
  name: 'v-chip-group',

  provide() {
    return {
      chipGroup: this
    };
  },

  props: {
    column: Boolean
  },
  computed: {
    classes() {
      return { ..._VSlideGroup_VSlideGroup__WEBPACK_IMPORTED_MODULE_1__[/* BaseSlideGroup */ "a"].options.computed.classes.call(this),
        'v-chip-group': true,
        'v-chip-group--column': this.column
      };
    }

  },
  watch: {
    column(val) {
      if (val) this.scrollOffset = 0;
      this.$nextTick(this.onResize);
    }

  },
  methods: {
    genData() {
      return this.setTextColor(this.color, { ..._VSlideGroup_VSlideGroup__WEBPACK_IMPORTED_MODULE_1__[/* BaseSlideGroup */ "a"].options.methods.genData.call(this)
      });
    }

  }
}));

/***/ }),

/***/ 474:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(439);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_6_oneOf_1_3_node_modules_vuetify_loader_lib_loader_js_ref_18_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 475:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(9);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "#statistics .theme--light.v-data-table tbody tr:hover:not(.v-data-table__expanded__content):not(.v-data-table__empty-wrapper){background:#fff}#statistics .v-slide-group__content{align-content:center;justify-content:center;place-content:center}#statistics .v-card--material-chart p{color:#999}#statistics .v-card--material__heading{max-height:450px;height:-webkit-fit-content;height:-moz-fit-content;height:fit-content}.v-tabs--icons-and-text>.v-tabs-bar{height:56px}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ 514:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/lk/statistics/index.vue?vue&type=template&id=888b2aa0&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-main',[_c('v-container',{attrs:{"id":"statistics","fluid":"","tag":"section"}},[_c('v-row',{staticStyle:{"place-content":"center"}},[_c('h2',{staticClass:"display-3"},[_vm._v("Aggregated Statistics")])]),_c('v-row',[(_vm.isAuthenticated)?_c('v-col',{attrs:{"cols":"12","lg":"6"}},[_c('coreStatsCard',{attrs:{"color":"transparent","colorPlace":"#ff4484","icon":"","place":_vm.correctnessRank.toString(),"placePercentage":_vm.correctnessPercentageRank.toString(),"title":"Your average accuracy per test","value":_vm.averageAccuracyPerTest + "%","sub-icon":"mdi-clock-outline","sub-text":("Updated: " + _vm.updatedTime)}})],1):_vm._e(),(_vm.isAuthenticated)?_c('v-col',{attrs:{"cols":"12","lg":"6"}},[_c('coreStatsCard',{attrs:{"color":"transparent","colorPlace":"#2ca2ff","place":_vm.timingRank.toString(),"placePercentage":_vm.timingPercentageRank.toString(),"icon":"","title":"Average time spent on each test","value":_vm.averageTimeSpentOnEachTest.toString(),"sub-icon":"mdi-tag","sub-text":("Updated: " + _vm.updatedTime)}})],1):_vm._e(),_c('v-col',{attrs:{"cols":"12"}},[_c('coreChartCard',{attrs:{"data":_vm.emailsSubscriptionChart.data,"options":_vm.emailsSubscriptionChart.options,"heigntChart":70,"color":"#ff4484","type":"Bar"},scopedSlots:_vm._u([{key:"title",fn:function(){return [_c('div',{staticClass:"my-2 text-center"},[_c('span',{staticClass:"display-2 font-weight-light"},[_vm._v("Percentage of correct answers")])])]},proxy:true},{key:"actions",fn:function(){return [_c('v-icon',{staticClass:"mr-1",attrs:{"small":""}},[_vm._v("mdi-clock-outline")]),_c('span',{staticClass:"caption grey--text font-weight-light"},[_vm._v("Updated "+_vm._s(_vm.updatedTime))])]},proxy:true}])})],1),_c('v-col',{attrs:{"cols":"12"}},[_c('coreChartCard',{attrs:{"data":_vm.averageTimeSpendToTopic.data,"options":_vm.averageTimeSpendToTopic.options,"color":"#2ca2ff","heigntChart":70,"type":"Time-Bar"},scopedSlots:_vm._u([{key:"title",fn:function(){return [_c('div',{staticClass:"my-2 text-center"},[_c('span',{staticClass:"display-2 font-weight-light"},[_vm._v("Average time spent on each topic")])])]},proxy:true},{key:"actions",fn:function(){return [_c('v-icon',{staticClass:"mr-1",attrs:{"small":""}},[_vm._v("mdi-clock-outline")]),_c('span',{staticClass:"caption grey--text font-weight-light"},[_vm._v("Updated "+_vm._s(_vm.updatedTime))])]},proxy:true}])})],1),(_vm.isAuthenticated)?_c('v-col',{attrs:{"cols":"12"}},[_c('div',{staticClass:"d-flex justify-center align-center"},[_c('span',{staticClass:"mr-3 display-1"},[_vm._v("Top")]),_c('v-chip-group',{attrs:{"mandatory":"","center-active":"","active-class":"primary--text"},on:{"change":function($event){return _vm.reloadRadar()}},model:{value:(_vm.percentageData),callback:function ($$v) {_vm.percentageData=$$v},expression:"percentageData"}},_vm._l((_vm.percentages),function(label){return _c('v-chip',{key:label},[_c('span',{staticClass:"display-1 font-weight-light"},[_vm._v(_vm._s(label)+"%")])])}),1),_c('span',{staticClass:"ml-2 display-1"},[_vm._v("for Others")])],1),_c('div',{staticClass:"mt-4 d-flex justify-center"},[_c('span',{staticClass:"display-2 font-weight-light"},[_vm._v("Your current percentile: top "),_c('b',[_vm._v(_vm._s(_vm.correctnessPercentageRank)+"% ")])])]),_c('div',{staticClass:"mt-4 d-flex justify-center"},[_c('span',{staticClass:"display-2 font-weight-light"},[_vm._v("Your future percentile: top "),_c('b',[_vm._v(_vm._s(_vm.futurePercentileCurrentUser)+"% ")])])]),_c('div',{staticClass:"mt-4 d-flex justify-center"},[_c('v-btn',{attrs:{"rounded":"","color":"info"},on:{"click":function($event){return _vm.resetPersonalTestData()}}},[_vm._v("Reset graph")])],1)]):_vm._e(),(_vm.isAuthenticated)?_c('v-col',{attrs:{"cols":"12"}},[(_vm.isReloadRadar)?_c('coreChartCard',{key:Math.random(),attrs:{"data":_vm.radarData.data,"options":_vm.radarData.options,"color":"#2ca2ff","heigntChart":210,"type":"Radar"},scopedSlots:_vm._u([{key:"title",fn:function(){return [_c('div',{staticClass:"my-2 text-center"},[_c('span',{staticClass:"display-2 font-weight-light"},[_vm._v("You vs. Others on each test")])])]},proxy:true},{key:"actions",fn:function(){return [_c('v-icon',{staticClass:"mr-1",attrs:{"small":""}},[_vm._v("mdi-clock-outline")]),_c('span',{staticClass:"caption grey--text font-weight-light"},[_vm._v("Updated "+_vm._s(_vm.updatedTime))])]},proxy:true}],null,false,2853954670)}):_vm._e()],1):_vm._e(),_c('v-col',{attrs:{"cols":"12"}},[_c('percentageOfCorrect',{attrs:{"nuxtChildKey":_vm.nuxtChildKey,"updatedTime":_vm.updatedTime,"labelsPercentageOfCorrectAnswers":_vm.labelsPercentageOfCorrectAnswers}}),_c('timeSpentOnEachTest',{attrs:{"nuxtChildKey":_vm.nuxtChildKey,"updatedTime":_vm.updatedTime,"labelsPercentageOfCorrectAnswers":_vm.labelsPercentageOfCorrectAnswers}})],1)],1)],1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/lk/statistics/index.vue?vue&type=template&id=888b2aa0&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// EXTERNAL MODULE: ./store/index.ts + 1 modules
var store = __webpack_require__(5);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/charts/percentageOfCorrect.vue?vue&type=template&id=82863638&lang=pug&
var percentageOfCorrectvue_type_template_id_82863638_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('coreChartCard',{attrs:{"data":_vm.percentageOfCorrectAnswers.data,"options":_vm.percentageOfCorrectAnswers.options,"color":"white","heigntChart":70,"type":"Line"},scopedSlots:_vm._u([{key:"title",fn:function(){return [_c('div',{staticClass:"my-2 text-center"},[_c('span',{staticClass:"display-2 font-weight-light"},[_vm._v("Percentage of correct answers")])])]},proxy:true},{key:"actions",fn:function(){return [_c('v-icon',{staticClass:"mr-1",attrs:{"small":""}},[_vm._v("mdi-clock-outline")]),_c('span',{staticClass:"caption grey--text font-weight-light"},[_vm._v("Updated "+_vm._s(_vm.updatedTime))])]},proxy:true}])})}
var percentageOfCorrectvue_type_template_id_82863638_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./components/charts/percentageOfCorrect.vue?vue&type=template&id=82863638&lang=pug&

// EXTERNAL MODULE: ./components/core/coreChartCard.vue + 4 modules
var coreChartCard = __webpack_require__(360);

// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__(306);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/charts/percentageOfCorrect.vue?vue&type=script&lang=ts&





let percentageOfCorrectvue_type_script_lang_ts_PercentageOfCorrect = class PercentageOfCorrect extends external_vue_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.percentageOfCorrectAnswers = {
      data: {
        labels: this.labelsPercentageOfCorrectAnswers,
        datasets: [{
          label: 'Percentage of correct answers on the last test',
          borderColor: '#d048b6',
          fill: true,
          backgroundColor: this.colorChart('#4848b0', '#4848b026'),
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: '#d048b6',
          pointBorderColor: 'rgba(255,255,255,0)',
          pointHoverBackgroundColor: '#d048b6',
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: this.dataPercentageOfCorrectAnswersLast
        }]
      },
      options: {
        responsive: true,
        hover: {
          animationDuration: 0
        },
        tooltips: {
          callbacks: {
            title: (tooltipItem, data) => {
              let items = store["pollsStore"].items.filter(item => item.section === this.nuxtChildKey);
              return items[tooltipItem[0].index].name;
            },
            label: (tooltipItem, data) => {
              let label = tooltipItem.value || 0;
              return data.datasets[tooltipItem.datasetIndex].label + ': ' + label + '%';
            }
          }
        },
        title: {
          display: false,
          text: 'Percentage of correct answers',
          fontSize: 26,
          fontColor: 'black'
        },
        animation: {
          duration: 1000,
          onComplete: () => {}
        },
        layout: {
          padding: {
            top: 0,
            left: 20,
            right: 20
          }
        },
        legend: {
          display: true,
          onClick: false,
          labels: {
            fontColor: 'black',
            fontSize: 15
          }
        },
        elements: {
          point: {
            radius: 4.5
          }
        },
        scales: {
          xAxes: [{
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: true
            },
            ticks: {
              fontColor: 'black',
              fontSize: 15
            }
          }],
          yAxes: [{
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: false
            },
            ticks: {
              stepSize: 20,
              max: 100,
              min: 0,
              fontColor: 'black',
              fontSize: 15
            }
          }]
        }
      }
    };
  }

  colorChart(color, colorWithOpacity) {
    let ctx = document.createElement("canvas").getContext("2d");
    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);
    gradientStroke.addColorStop(1, color);
    gradientStroke.addColorStop(0, colorWithOpacity);
    return gradientStroke;
  }

  get dataPercentageOfCorrectAnswersLast() {
    let series = [];
    let fromLocal = JSON.parse(localStorage.getItem('adaptive/polls/historyLS')) ? JSON.parse(localStorage.getItem('adaptive/polls/historyLS')) : [];
    const items = store["pollsStore"].items.filter(item => item.section === this.nuxtChildKey);
    let splitRes = [];

    for (const poll of items) {
      if (store["auth"].user.results) {
        const filteredOnPoll = store["auth"].user.results.filter(result => result.poll_id === poll._id && result.poll.section === this.nuxtChildKey);
        const lastOnTheme = Object(external_lodash_["findLast"])(filteredOnPoll);
        const result = lastOnTheme !== undefined ? this.getResultTest(lastOnTheme) : '0';
        splitRes = result.split('/');
      } else if (fromLocal.length !== 0) {
        const filteredOnPoll = fromLocal.filter(result => result._id === poll._id && poll.section === this.nuxtChildKey);

        if (filteredOnPoll) {
          const lastOnTheme = Object(external_lodash_["findLast"])(filteredOnPoll);
          const result = lastOnTheme !== undefined ? this.getResultTest({
            poll: lastOnTheme,
            answers: lastOnTheme.answers
          }) : '0';
          splitRes = result.split('/');
        }
      }

      if (!Math.floor(Number(splitRes[0]) / Number(splitRes[1]) * 100)) series.push(0);else series.push(Math.floor(Number(splitRes[0]) / Number(splitRes[1]) * 100));
    }

    return series;
  }

  getResultTest(result) {
    let countTrueAnswers = 0;
    let countHops = 0;

    if (result.poll) {
      countHops = result.poll.sorted.length;
      result.answers.forEach(answer => {
        let hop = result.poll.sorted.find(item => item._id === answer.hop_id);
        if (hop && answer.options[0]) hop.correctAnswer[0] === answer.options[0].id ? countTrueAnswers++ : '';
      });
      return countTrueAnswers + '/' + countHops;
    } else {
      countHops = result.sorted.length;
      result.answers.forEach(answer => {
        let hop = result.sorted.find(item => item._id === answer.hop_id);
        if (hop && answer.options[0]) hop.correctAnswer[0] === answer.options[0].id ? countTrueAnswers++ : '';
      });
      return countTrueAnswers + '/' + countHops;
    }
  }

};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  required: true
})], percentageOfCorrectvue_type_script_lang_ts_PercentageOfCorrect.prototype, "nuxtChildKey", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  required: true
})], percentageOfCorrectvue_type_script_lang_ts_PercentageOfCorrect.prototype, "updatedTime", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  required: true
})], percentageOfCorrectvue_type_script_lang_ts_PercentageOfCorrect.prototype, "labelsPercentageOfCorrectAnswers", void 0);

percentageOfCorrectvue_type_script_lang_ts_PercentageOfCorrect = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'percentageOfCorrect',
  components: {
    coreChartCard: coreChartCard["a" /* default */]
  }
})], percentageOfCorrectvue_type_script_lang_ts_PercentageOfCorrect);
/* harmony default export */ var percentageOfCorrectvue_type_script_lang_ts_ = (percentageOfCorrectvue_type_script_lang_ts_PercentageOfCorrect);
// CONCATENATED MODULE: ./components/charts/percentageOfCorrect.vue?vue&type=script&lang=ts&
 /* harmony default export */ var charts_percentageOfCorrectvue_type_script_lang_ts_ = (percentageOfCorrectvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(16);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(50);

// CONCATENATED MODULE: ./components/charts/percentageOfCorrect.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  charts_percentageOfCorrectvue_type_script_lang_ts_,
  percentageOfCorrectvue_type_template_id_82863638_lang_pug_render,
  percentageOfCorrectvue_type_template_id_82863638_lang_pug_staticRenderFns,
  false,
  injectStyles,
  null,
  "752f54c0"
  
)

/* harmony default export */ var percentageOfCorrect = (component.exports);

/* vuetify-loader */


installComponents_default()(component, {VIcon: VIcon["a" /* default */]})

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/charts/timeSpentOnEachTest.vue?vue&type=template&id=5c001903&lang=pug&
var timeSpentOnEachTestvue_type_template_id_5c001903_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('coreChartCard',{attrs:{"data":_vm.dataCompletedTasksChart.data,"options":_vm.dataCompletedTasksChart.options,"color":"#2ca2ffe3","heigntChart":70,"type":"Time-Line"},scopedSlots:_vm._u([{key:"title",fn:function(){return [_c('div',{staticClass:"my-2 text-center"},[_c('span',{staticClass:"display-2 font-weight-light"},[_vm._v("Time spent on each test")])])]},proxy:true},{key:"actions",fn:function(){return [_c('v-icon',{staticClass:"mr-1",attrs:{"small":""}},[_vm._v("mdi-clock-outline")]),_c('span',{staticClass:"caption grey--text font-weight-light"},[_vm._v("Updated "+_vm._s(_vm.updatedTime))])]},proxy:true}])})}
var timeSpentOnEachTestvue_type_template_id_5c001903_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./components/charts/timeSpentOnEachTest.vue?vue&type=template&id=5c001903&lang=pug&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./components/charts/timeSpentOnEachTest.vue?vue&type=script&lang=ts&





let timeSpentOnEachTestvue_type_script_lang_ts_PercentageOfCorrect = class PercentageOfCorrect extends external_vue_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.dataCompletedTasksChart = {
      data: {
        labels: this.labelsPercentageOfCorrectAnswers,
        datasets: [{
          label: 'Time spent on the last test',
          borderColor: '#e91e63',
          fill: true,
          backgroundColor: this.colorChart('#e91e63', '#e91e6326'),
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: '#e91e63',
          pointBorderColor: 'rgba(255,255,255,0)',
          pointHoverBackgroundColor: '#e91e63',
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: this.MainTimeToResolveTestsLast
        }]
      },
      options: {
        responsive: true,
        tooltips: {
          callbacks: {
            title: (tooltipItem, data) => {
              let items = store["pollsStore"].items.filter(item => item.section === this.nuxtChildKey);
              return items[tooltipItem[0].index].name;
            },
            label: (tooltipItem, data) => {
              let label = tooltipItem.value || 0;
              return data.datasets[tooltipItem.datasetIndex].label + ': ' + this.$moment.utc(label * 1000).format('mm:ss');
            }
          }
        },
        hover: {
          animationDuration: 0
        },
        animation: {
          duration: 1000,
          onComplete: () => {}
        },
        title: {
          display: false,
          text: 'Time spent on each test',
          fontColor: 'black',
          fontSize: 26
        },
        layout: {
          padding: {
            left: 20,
            right: 20
          }
        },
        legend: {
          display: true,
          onClick: false,
          labels: {
            fontColor: 'black',
            fontSize: 15
          }
        },
        elements: {
          point: {
            radius: 4.5
          }
        },
        scales: {
          xAxes: [{
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: true
            },
            ticks: {
              fontColor: 'black',
              fontSize: 15
            }
          }],
          yAxes: [{
            gridLines: {
              display: true,
              drawBorder: false,
              drawOnChartArea: false,
              drawTicks: false
            },
            ticks: {
              callback: value => {
                return this.$moment.utc(value * 1000).format('mm:ss');
              },
              display: false,
              min: 0,
              stepSize: 10,
              fontColor: 'black',
              fontSize: 15
            }
          }]
        }
      }
    };
  }

  get MainTimeToResolveTestsLast() {
    let series = [];
    const items = store["pollsStore"].items.filter(item => item.section === this.nuxtChildKey);
    let fromLocal = JSON.parse(localStorage.getItem('adaptive/polls/historyLS')) ? JSON.parse(localStorage.getItem('adaptive/polls/historyLS')) : [];
    let result = 0;

    for (const poll of items) {
      if (store["auth"].user.results) {
        const filteredOnPoll = store["auth"].user.results.filter(result => result.poll_id === poll._id && result.poll.section === this.nuxtChildKey);
        const lastOnTheme = Object(external_lodash_["findLast"])(filteredOnPoll);
        result = lastOnTheme !== undefined ? lastOnTheme.time : NaN;
      } else if (fromLocal.length !== 0) {
        const filteredOnPoll = fromLocal.filter(result => result._id === poll._id && poll.section === this.nuxtChildKey);
        const lastOnTheme = Object(external_lodash_["findLast"])(filteredOnPoll);
        let allTime = 0;
        if (lastOnTheme) lastOnTheme.answers.forEach(item => {
          allTime += item.time;
        });
        result = lastOnTheme !== undefined ? allTime / 1000 : NaN;
      }

      if (!result) series.push(0);else series.push(Math.round(result));
    }

    return series;
  }

  colorChart(color, colorWithOpacity) {
    let ctx = document.createElement("canvas").getContext("2d");
    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);
    gradientStroke.addColorStop(1, color);
    gradientStroke.addColorStop(0, colorWithOpacity);
    return gradientStroke;
  }

};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  required: true
})], timeSpentOnEachTestvue_type_script_lang_ts_PercentageOfCorrect.prototype, "nuxtChildKey", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  required: true
})], timeSpentOnEachTestvue_type_script_lang_ts_PercentageOfCorrect.prototype, "updatedTime", void 0);

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  required: true
})], timeSpentOnEachTestvue_type_script_lang_ts_PercentageOfCorrect.prototype, "labelsPercentageOfCorrectAnswers", void 0);

timeSpentOnEachTestvue_type_script_lang_ts_PercentageOfCorrect = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'percentageOfCorrect',
  components: {
    coreChartCard: coreChartCard["a" /* default */]
  }
})], timeSpentOnEachTestvue_type_script_lang_ts_PercentageOfCorrect);
/* harmony default export */ var timeSpentOnEachTestvue_type_script_lang_ts_ = (timeSpentOnEachTestvue_type_script_lang_ts_PercentageOfCorrect);
// CONCATENATED MODULE: ./components/charts/timeSpentOnEachTest.vue?vue&type=script&lang=ts&
 /* harmony default export */ var charts_timeSpentOnEachTestvue_type_script_lang_ts_ = (timeSpentOnEachTestvue_type_script_lang_ts_); 
// CONCATENATED MODULE: ./components/charts/timeSpentOnEachTest.vue



function timeSpentOnEachTest_injectStyles (context) {
  
  
}

/* normalize component */

var timeSpentOnEachTest_component = Object(componentNormalizer["a" /* default */])(
  charts_timeSpentOnEachTestvue_type_script_lang_ts_,
  timeSpentOnEachTestvue_type_template_id_5c001903_lang_pug_render,
  timeSpentOnEachTestvue_type_template_id_5c001903_lang_pug_staticRenderFns,
  false,
  timeSpentOnEachTest_injectStyles,
  null,
  "d660638c"
  
)

/* harmony default export */ var timeSpentOnEachTest = (timeSpentOnEachTest_component.exports);

/* vuetify-loader */


installComponents_default()(timeSpentOnEachTest_component, {VIcon: VIcon["a" /* default */]})

// EXTERNAL MODULE: ./components/core/coreCard.vue + 4 modules
var coreCard = __webpack_require__(324);

// EXTERNAL MODULE: ./components/core/coreStatsCard.vue + 4 modules
var coreStatsCard = __webpack_require__(374);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/lk/statistics/index.vue?vue&type=script&lang=ts&








let statisticsvue_type_script_lang_ts_Statistics = class Statistics extends external_vue_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.isReloadRadar = true;
    this.personalTestData = [];
    this.percentageData = 8;
    this.percentages = [1, 2, 3, 5, 10, 20, 30, 40, 50];
    this.chipData = 8;
    this.radarData = {
      data: {
        labels: this.labelsSubThemes,
        datasets: [{
          label: 'Correct % for you',
          borderColor: '#ff6384',
          backgroundColor: '#ffc5d154',
          pointBackgroundColor: '#ff6384',
          data: []
        }, {
          label: 'Correct % for others',
          borderColor: '#36a2eb',
          backgroundColor: '#79c1f14d',
          pointBackgroundColor: '#36a2eb',
          dragData: false,
          data: this.percentageOfCorrectAnswersOnEechThemeAverage // data: auth.user.averageStatistics[this.nuxtChildKey].percentageOfCorrectAnswersOnEechThemeAverage

        }]
      },
      options: {
        responsive: true,
        dragData: true,
        dragDataRound: -1,
        dragOptions: {
          showTooltip: true
        },
        // onDragStart: function(e) {    // когда человек поменял контур, просто посчитать процент, который был бы у такого контура, относительно остальных пользователей...
        // },
        onDrag: function (e, datasetIndex, index, value) {
          e.target.style.cursor = 'grabbing';
        },
        onDragEnd: (e, datasetIndex, index, value) => {
          e.target.style.cursor = 'default';
          let tmp = this.personalTestData.findIndex(item => item.label === this.labelsSubThemes[index]);

          if (tmp !== -1) {
            this.personalTestData[tmp] = {
              label: this.labelsSubThemes[index],
              value: value
            };
          } else {
            this.personalTestData.push({
              label: this.labelsSubThemes[index],
              value: value
            });
          }

          this.futurePercentileCurrentUser;
        },
        title: {
          display: false,
          text: 'You vs. Others on each tests',
          fontSize: 20,
          fontColor: 'black'
        },
        tooltips: {
          callbacks: {
            title: (tooltipItem, data) => {
              let str = '';
              tooltipItem.forEach(item => {
                str += data.labels[item.index] + ' & ';
              });
              return str.slice(0, -2);
            },
            label: (tooltipItem, data) => {
              let label = tooltipItem.value || 0;
              return data.datasets[tooltipItem.datasetIndex].label + ': ' + label + '%';
            }
          }
        },
        hover: {
          animationDuration: 500,
          onHover: function (e) {
            const point = this.getElementAtEvent(e);
            if (point.length) e.target.style.cursor = 'grab';else e.target.style.cursor = 'default';
          }
        },
        layout: {
          padding: {
            top: 0
          }
        },
        animation: {
          duration: 0
        },
        legend: {
          display: true,
          labels: {
            fontColor: 'black',
            fontSize: 16
          }
        },
        elements: {
          line: {
            tension: 0.0
          }
        },
        scale: {
          pointLabels: {
            fontSize: 13
          },
          ticks: {
            min: -10,
            stepSize: 10,
            max: 100,
            callback: function (label) {
              return Math.round(label) + '%';
            }
          }
        }
      }
    };
    this.emailsSubscriptionChart = {
      data: {
        labels: this.themes,
        datasets: [{
          label: 'All users',
          barPercentage: 0.8,
          categoryPercentage: 0.3,
          borderWidth: 1,
          backgroundColor: this.colorChart('#00f2c3', '#00f2c326'),
          borderDash: [],
          borderDashOffset: 0.0,
          // data: this.percentageCorrectAnwsersOfTopicAverage
          data: store["auth"].user.averageStatistics[this.nuxtChildKey].percentageCorrectAnwsersOfTopicAverage
        }, {
          label: 'You',
          barPercentage: 0.8,
          categoryPercentage: 0.3,
          borderWidth: 1,
          backgroundColor: this.colorChart('#4848b0', '#4848b026'),
          borderDash: [],
          data: this.percentageCorrectAnwsersOfTopic
        }]
      },
      options: {
        responsive: true,
        barValueSpacing: 30,
        title: {
          display: false,
          text: 'Percentage of correct answers',
          fontSize: 22,
          fontColor: 'black'
        },
        hover: {
          animationDuration: 0
        },
        layout: {
          padding: {
            top: 0
          }
        },
        animation: {
          duration: 1000,
          onComplete: () => {}
        },
        legend: {
          display: true,
          onClick: false,
          labels: {
            fontColor: 'black',
            fontSize: 15
          }
        },
        tooltips: {
          enabled: false
        },
        scales: {
          xAxes: [{
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: false
            },
            ticks: {
              fontColor: 'black',
              fontSize: 15
            }
          }],
          yAxes: [{
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: false,
              drawTicks: false
            },
            ticks: {
              display: false,
              stepSize: 2,
              min: 0,
              fontColor: 'black',
              fontSize: 15
            }
          }]
        }
      }
    };
    this.averageTimeSpendToTopic = {
      data: {
        labels: this.themes,
        datasets: [{
          label: 'All users',
          barPercentage: 0.8,
          categoryPercentage: 0.3,
          borderWidth: 1,
          backgroundColor: this.colorChart('#00f2c3', '#00f2c326'),
          borderDash: [],
          borderDashOffset: 0.0,
          // data: this.averageTimeSpendToTopicValueUsers
          data: store["auth"].user.averageStatistics[this.nuxtChildKey].averageTimeSpendToTopicValueUsers
        }, {
          label: 'You',
          barPercentage: 0.8,
          categoryPercentage: 0.3,
          borderWidth: 1,
          backgroundColor: this.colorChart('#4848b0', '#4848b026'),
          borderDash: [],
          borderDashOffset: 0.0,
          // data: [0,0,0,0]
          data: this.averageTimeSpendToTopicValue
        }]
      },
      options: {
        responsive: true,
        hover: {
          animationDuration: 0
        },
        title: {
          display: false,
          text: 'Average time spent on each topic',
          fontColor: 'black',
          fontSize: 22
        },
        layout: {
          padding: {
            top: 0
          }
        },
        animation: {
          duration: 1000,
          onComplete: () => {}
        },
        legend: {
          display: true,
          onClick: false,
          labels: {
            fontColor: 'black',
            fontSize: 15
          }
        },
        tooltips: {
          enabled: false
        },
        scales: {
          xAxes: [{
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: false
            },
            ticks: {
              fontColor: 'black',
              fontSize: 15
            }
          }],
          yAxes: [{
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: false,
              drawTicks: false
            },
            ticks: {
              display: false,
              stepSize: 2,
              min: 0,
              fontColor: 'black',
              fontSize: 15
            }
          }]
        }
      }
    };
  }

  colorChart(color, colorWithOpacity) {
    let ctx = document.createElement("canvas").getContext("2d");
    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);
    gradientStroke.addColorStop(1, color);
    gradientStroke.addColorStop(0, colorWithOpacity);
    return gradientStroke;
  }

  get futurePercentileCurrentUser() {
    if (this.personalTestData.length !== 0) {
      this.personalTestData.forEach(item => {
        const indexLabel = this.labelsSubThemes.findIndex(label => label === item.label);
      });
      let res = {};
      let sortedUserWithOutCurrentUser = Object.assign({}, this.sortedUsers);
      delete sortedUserWithOutCurrentUser[store["auth"].user._id];

      for (const user in sortedUserWithOutCurrentUser) {
        this.labelsSubThemes.forEach(theme => {
          res[user] === undefined ? res[user] = [store["auth"].user.commonStatistics.filter(answer => answer.subTag === theme && answer.section === this.nuxtChildKey && answer.user_id === user)] : res[user].push(store["auth"].user.commonStatistics.filter(answer => answer.subTag === theme && answer.section === this.nuxtChildKey && answer.user_id === user));
        });
      }

      let series = {};

      for (const user in res) {
        res[user].forEach(results => {
          const filtredTrueHop = results.filter(item => item.isCorrect);
          if (results.length === 0) series[user] === undefined ? series[user] = [0] : series[user].push(0);else series[user] === undefined ? series[user] = [Math.floor(filtredTrueHop.length / results.length * 100)] : series[user].push(Math.floor(filtredTrueHop.length / results.length * 100));
        });
      }

      for (const item in series) {
        series[item] = series[item].reduce((res, acc) => {
          return res += acc;
        }, 0);
      }

      let userSum = this.percentageofCorrectAnswersOnEechTheme.reduce((res, acc) => {
        return res += acc;
      }, 0);
      series[store["auth"].user._id] = userSum;
      let sortedKeys = Object.keys(series).sort(function (a, b) {
        return series[b] - series[a];
      });
      let sortedAndSliceStat = {};

      for (let i = 0; i < sortedKeys.length; i++) {
        sortedAndSliceStat[sortedKeys[i]] = series[sortedKeys[i]];
      }

      const place = sortedKeys.findIndex((item, index) => {
        return item === store["auth"].user._id;
      });
      let percentage = (place + 1) / sortedKeys.length * 100 < 1 ? 1 : Math.round((place + 1) / sortedKeys.length * 100);
      if (percentage > this.correctnessPercentageRank) percentage = this.correctnessPercentageRank;
      return percentage;
    } else {
      return this.correctnessPercentageRank;
    }
  }

  get percentageOfCorrectAnswersOnEechThemeAverage() {
    if (this.percentageData !== undefined && this.percentageData !== null) {
      if (store["auth"].user.commonStatistics) {
        let series = [];
        let keys = Object.keys(this.sortedUsers);
        let users = {};
        let percentPeople = Math.round(this.percentages[this.percentageData] / 100 * keys.length);
        if (percentPeople < 1) percentPeople = 1;
        console.warn(percentPeople);
        keys = keys.slice(0, percentPeople);
        console.warn(keys);
        if (this.sortedUsers[keys[keys.length - 1]] !== 0) users[keys[keys.length - 1]] = this.sortedUsers[keys[keys.length - 1]];else users[keys[keys.length - 2]] = this.sortedUsers[keys[keys.length - 2]];
        console.warn(users);

        for (const theme of this.labelsSubThemes) {
          let filteredOnTheme = [];

          for (const user in users) {
            filteredOnTheme.push(store["auth"].user.commonStatistics.filter(answer => answer.subTag === theme && answer.section === this.nuxtChildKey && answer.user_id === user));
          }

          filteredOnTheme = filteredOnTheme.flat();
          const filtredTrueHop = filteredOnTheme.filter(item => item.isCorrect);
          if (filteredOnTheme.length === 0) series.push(0);else series.push(Math.floor(filtredTrueHop.length / filteredOnTheme.length * 100));
        }

        return series;
      }
    } else {
      return store["auth"].user.averageStatistics[this.nuxtChildKey].percentageOfCorrectAnswersOnEechThemeAverage;
    }
  }

  get percentageofCorrectAnswersOnEechTheme() {
    const series = [];
    let fromLocal = JSON.parse(localStorage.getItem('adaptive/polls/historyLS')) ? JSON.parse(localStorage.getItem('adaptive/polls/historyLS')) : [];

    if (store["auth"].user.statistics !== undefined && store["auth"].user.statistics[this.nuxtChildKey]) {
      const flat = store["auth"].user.statistics[this.nuxtChildKey].flat();

      for (const theme of this.labelsSubThemes) {
        const filteredOnTheme = flat.filter(answer => answer.subTag === theme);
        const filtredTrueHop = filteredOnTheme.filter(item => item.isCorrect);
        if (filteredOnTheme.length === 0) series.push(0);else series.push(Math.floor(filtredTrueHop.length / filteredOnTheme.length * 100));
      }
    } else if (fromLocal.length !== 0) {
      let filteredOnTheme = [];

      for (const theme of this.themes) {
        let countTrue = 0;
        let countHops = 0;
        fromLocal ? fromLocal = fromLocal.filter(item => item.section === this.nuxtChildKey) : '';
        fromLocal.forEach(result => {
          if (result) {
            filteredOnTheme = result.sorted.map(hop => hop.subtags === theme ? hop : false);
            filteredOnTheme.forEach(hop => {
              let currentAnswer = result.answers ? result.answers.find(res => res.hop_id === hop._id) : false;

              if (hop && currentAnswer && currentAnswer.options[0]) {
                hop.correctAnswer[0] === currentAnswer.options[0].id ? countTrue++ : '';
                countHops++;
              }
            });
          }
        });
        if (filteredOnTheme.length === 0) series.push(0);else series.push(Math.floor(countTrue / countHops * 100));
      }
    }

    if (this.personalTestData.length !== 0) {
      this.personalTestData.forEach(test => {
        const index = this.labelsSubThemes.findIndex(item => {
          return item === test.label;
        });

        if (index !== -1) {
          series[index] = test.value;
        }
      });
    }

    return series;
  }

  get averageTimeSpendToTopicValue() {
    const series = [];
    let fromLocal = JSON.parse(localStorage.getItem('adaptive/polls/historyLS')) ? JSON.parse(localStorage.getItem('adaptive/polls/historyLS')) : [];

    if (store["auth"].user.statistics !== undefined && store["auth"].user.statistics[this.nuxtChildKey]) {
      const flat = store["auth"].user.statistics[this.nuxtChildKey].flat();

      for (const theme of this.themes) {
        const filteredOnTheme = flat.filter(answer => answer.tag === theme);
        const allTimeOnTheme = filteredOnTheme.reduce((acc, item) => {
          return item.time + acc;
        }, 0);
        if (filteredOnTheme.length === 0) series.push(0);else series.push(Math.round(allTimeOnTheme / filteredOnTheme.length));
      }
    } else if (fromLocal.length !== 0) {
      let filteredOnTheme = [];

      for (const theme of this.themes) {
        let allTimeOnTheme = 0;
        let countHopsOnTheme = 0;
        fromLocal ? fromLocal = fromLocal.filter(item => item.section === this.nuxtChildKey) : '';
        fromLocal.forEach(result => {
          // const poll = resultPollStore.pollStoreHistory.find(poll => poll._id === result.poll_id && poll.section === this.nuxtChildKey)
          if (result) {
            filteredOnTheme = result.sorted.map(hop => hop.tags === theme ? hop : false);
            filteredOnTheme.forEach(hop => {
              let currentAnswer = result.answers ? result.answers.find(res => res.hop_id === hop._id) : false;

              if (currentAnswer) {
                allTimeOnTheme += currentAnswer.time;
              }

              countHopsOnTheme++;
            });
          }
        });
        if (filteredOnTheme.length === 0) series.push(0);else series.push(Math.round(allTimeOnTheme / countHopsOnTheme));
      }
    }

    return series;
  }

  get percentageCorrectAnwsersOfTopic() {
    const series = [];
    let fromLocal = JSON.parse(localStorage.getItem('adaptive/polls/historyLS')) ? JSON.parse(localStorage.getItem('adaptive/polls/historyLS')) : [];

    if (store["auth"].user.statistics !== undefined && store["auth"].user.statistics[this.nuxtChildKey]) {
      const flat = store["auth"].user.statistics[this.nuxtChildKey].flat();

      for (const theme of this.themes) {
        const filteredOnTheme = flat.filter(answer => answer.tag === theme);
        const filtredTrueHop = filteredOnTheme.filter(item => item.isCorrect);
        if (filteredOnTheme.length === 0) series.push(0);else series.push(Math.floor(filtredTrueHop.length / filteredOnTheme.length * 100));
      }
    } else if (fromLocal.length !== 0) {
      let filteredOnTheme = [];

      for (const theme of this.themes) {
        let countTrue = 0;
        let countHops = 0;
        fromLocal ? fromLocal = fromLocal.filter(item => item.section === this.nuxtChildKey) : '';
        fromLocal.forEach(result => {
          // const poll = resultPollStore.pollStoreHistory.find(poll => poll._id === result.poll_id && poll.section === this.nuxtChildKey)
          if (result) {
            filteredOnTheme = result.sorted.map(hop => hop.tags === theme ? hop : false);
            filteredOnTheme.forEach(hop => {
              let currentAnswer = result.answers ? result.answers.find(res => res.hop_id === hop._id) : false;

              if (hop && currentAnswer && currentAnswer.options[0]) {
                hop.correctAnswer[0] === currentAnswer.options[0].id ? countTrue++ : '';
                countHops++;
              }
            });
          }
        });
        if (filteredOnTheme.length === 0) series.push(0);else series.push(Math.floor(countTrue / countHops * 100));
      }
    }

    return series;
  }

  get averageAccuracyPerTest() {
    if (store["auth"].user.correctness && store["auth"].user.correctness[this.nuxtChildKey]) {
      return Math.floor(store["auth"].user.correctness[this.nuxtChildKey + '_percent']);
    } else {
      return 0;
    }
  }

  get averageTimeSpentOnEachTest() {
    if (store["auth"].user.timing && store["auth"].user.timing[this.nuxtChildKey]) {
      if (store["auth"].user.timing[this.nuxtChildKey] > 3800) {
        return this.$moment.utc(Math.floor(store["auth"].user.timing[this.nuxtChildKey]) * 1000).format('h [hour] mm [min] ss [sec]');
      }

      return this.$moment.utc(Math.floor(store["auth"].user.timing[this.nuxtChildKey]) * 1000).format('mm [min] ss [sec]');
    } else {
      return 0;
    }
  }

  get placeOnLeaderboards() {
    if (store["auth"].user.rank) return store["auth"].user.rank.toString();else return '0';
  }

  get userRating() {
    if (store["auth"].user.rating) return store["auth"].user.rating.toString();else return '0';
  }

  get timingRank() {
    if (store["auth"].user.timingRank) return store["auth"].user.timingRank[this.nuxtChildKey];else return 0;
  }

  get correctnessRank() {
    if (store["auth"].user.correctnessRank) return store["auth"].user.correctnessRank[this.nuxtChildKey];else return 0;
  }

  get timingPercentageRank() {
    if (store["auth"].user.timingRank) return store["auth"].user.timingRank[this.nuxtChildKey + '_percent'];else return 0;
  }

  get correctnessPercentageRank() {
    if (store["auth"].user.correctnessRank) return store["auth"].user.correctnessRank[this.nuxtChildKey + '_percent'];else return 0;
  }

  get updatedTime() {
    if (store["auth"].user.results !== undefined && store["auth"].user.results.length !== 0) {
      const time = store["auth"].user.results[store["auth"].user.results.length - 1].created_at;
      return this.$moment(time).fromNow();
    } else if (store["resultsStore"].resultPoll.length !== 0) {
      return this.$moment(store["resultsStore"].resultPoll[0].created_at).fromNow();
    }
  }

  get labelsSubThemes() {
    let labels = [];
    this.themes.forEach(item => {
      this.subThemes[item].forEach(subItem => {
        labels.push(subItem);
      });
    });
    return labels;
  }

  get isAuthenticated() {
    return store["auth"].isAuthenticated;
  }

  get statusDrawer() {
    return store["app"].drawer;
  }

  get sortedUsers() {
    if (store["auth"].user.commonStatistics) {
      let stats = {};
      let res = [];
      store["auth"].user.commonStatistics.forEach(item => {
        if (item.user_id !== 'null' && item.user_id !== null && item.section === this.nuxtChildKey) {
          if (stats[item.user_id] === undefined) {
            stats[item.user_id] = item.isCorrect ? 1 : 0;
          } else {
            stats[item.user_id] += item.isCorrect ? 1 : 0;
          }
        }
      });
      let sortedKeys = [];
      console.warn(stats);
      sortedKeys = Object.keys(stats).sort(function (a, b) {
        return stats[b] - stats[a];
      });
      let sortedAndSliceStat = {}; // let percentPeople = Math.round(this.percentages[this.percentageData] / 100 * sortedKeys.length)
      // if(percentPeople < 1) percentPeople = 1

      for (let i = 0; i < sortedKeys.length; i++) {
        sortedAndSliceStat[sortedKeys[i]] = stats[sortedKeys[i]];
      }

      console.warn(sortedAndSliceStat);
      return sortedAndSliceStat;
    }
  }

  beforeMount() {
    this.radarData.data.datasets[0].data = this.percentageofCorrectAnswersOnEechTheme;
    this.getRadarData();
  }

  resetPersonalTestData() {
    this.isReloadRadar = false;
    this.personalTestData.splice(0, this.personalTestData.length);
    this.radarData.data.datasets[0].data = this.percentageofCorrectAnswersOnEechTheme;
    this.$nextTick(() => {
      this.isReloadRadar = true;
    });
  }

  changeChipIndex(index) {
    this.chipData = index;
  }

  changeChipData(val) {
    let index = -1;

    if (this.personalTestData.length !== 0) {
      index = this.personalTestData.findIndex(item => {
        return item.label === this.labelsSubThemes[this.chipData];
      });
    }

    if (index !== -1) {
      this.percentageData = Math.round(this.personalTestData[index].value / 10) - 1;
    } else {
      this.percentageData = Math.round(this.percentageofCorrectAnswersOnEechTheme[val] / 10) - 1;
    }

    this.percentageData <= 0 ? this.percentageData = 0 : '';
  }

  getRadarData() {
    this.radarData.data.datasets[1].data = this.percentageOfCorrectAnswersOnEechThemeAverage;
  }

  reloadRadar() {
    this.getRadarData();
  }

  get themes() {
    if (this.nuxtChildKey === 'math_nocalc' || this.nuxtChildKey === 'math_calc' || this.nuxtChildKey === 'personal') {
      return store["app"].themesSAT;
    } else if (this.nuxtChildKey === 'olympics') {
      return store["app"].themesOlympic;
    }
  }

  get subThemes() {
    if (this.nuxtChildKey === 'math_nocalc' || this.nuxtChildKey === 'math_calc' || this.nuxtChildKey === 'personal') {
      return store["app"].subThemesSAT;
    } else if (this.nuxtChildKey === 'olympics') {
      return store["app"].subThemesOlympic;
    }
  }

  get labelsPercentageOfCorrectAnswers() {
    if (store["pollsStore"].items) {
      let countPolls = [];
      const filteredPolls = store["pollsStore"].items.filter(item => {
        if (item.section === this.nuxtChildKey) {
          if (this.nuxtChildKey === 'personal') {
            return item.recipient_id !== null ? item.recipient_id === store["auth"].user._id : false;
          } else {
            return true;
          }
        }
      });
      filteredPolls.forEach((item, key) => {
        countPolls.push(key + 1);
      });
      return countPolls;
    }
  }

};

Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Prop"])({
  type: String,
  required: true
})], statisticsvue_type_script_lang_ts_Statistics.prototype, "nuxtChildKey", void 0);

statisticsvue_type_script_lang_ts_Statistics = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  layout: 'dashboard',
  name: 'Statistics',
  components: {
    coreCard: coreCard["a" /* default */],
    coreStatsCard: coreStatsCard["a" /* default */],
    coreChartCard: coreChartCard["a" /* default */],
    percentageOfCorrect: percentageOfCorrect,
    timeSpentOnEachTest: timeSpentOnEachTest
  },

  fetch({
    redirect
  }) {// if( !auth.isAuthenticated && !auth.isAnonimous ) {
    //   redirect('/')
    // }
  }

})], statisticsvue_type_script_lang_ts_Statistics);
/* harmony default export */ var statisticsvue_type_script_lang_ts_ = (statisticsvue_type_script_lang_ts_Statistics);
// CONCATENATED MODULE: ./pages/lk/statistics/index.vue?vue&type=script&lang=ts&
 /* harmony default export */ var lk_statisticsvue_type_script_lang_ts_ = (statisticsvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/VBtn.js
var VBtn = __webpack_require__(81);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VChip/VChip.js
var VChip = __webpack_require__(340);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VChipGroup/VChipGroup.js
var VChipGroup = __webpack_require__(473);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(343);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VContainer.js
var VContainer = __webpack_require__(314);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VMain/VMain.js
var VMain = __webpack_require__(317);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(341);

// CONCATENATED MODULE: ./pages/lk/statistics/index.vue



function statistics_injectStyles (context) {
  
  var style0 = __webpack_require__(474)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var statistics_component = Object(componentNormalizer["a" /* default */])(
  lk_statisticsvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  statistics_injectStyles,
  null,
  "69bfec86"
  
)

/* harmony default export */ var statistics = __webpack_exports__["default"] = (statistics_component.exports);

/* vuetify-loader */









installComponents_default()(statistics_component, {VBtn: VBtn["a" /* default */],VChip: VChip["a" /* default */],VChipGroup: VChipGroup["a" /* default */],VCol: VCol["a" /* default */],VContainer: VContainer["a" /* default */],VIcon: VIcon["a" /* default */],VMain: VMain["a" /* default */],VRow: VRow["a" /* default */]})


/***/ })

};;
//# sourceMappingURL=index.js.map