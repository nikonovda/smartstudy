import {Hop} from './interfaceHops'

export interface InterfacePoll {
  _id: string,
  name: string,
  description: string,
  isOpenResults: boolean,
  isOnlyAuth: boolean,
  published_at: string,
  completed_at: string,
  closed_at: string,
  updated_at: string,
  created_at: string,
  sort: Array<string>,
  sorted: Array<Hop>,
  isPublished: boolean,
  isActive: boolean,
  results: Array<Result>,
  type?: string,
  section: string,
  user_id: string,
  timeToSolution: string,
}

export interface Result {
  _id: string,
  poll_id: string,
  answers: Array<Answer>,
  updated_at: string,
  created_at: string
}

export interface Answer {
  hop_id: string,
  options: Array<OptionAnswer>
}

export interface OptionAnswer {
  id: Number,
  value: null | string
}

export interface RegisterChoise {
  id: string,
  payload: RegisterChoisePayload,
}

export interface Results {
  [_id: string]: RegisterChoisePayload
}

export interface RegisterChoisePayload {
  choised: Array<Number>,
  userValue: Boolean | string,
  time: number,
  wasShown: boolean
}

