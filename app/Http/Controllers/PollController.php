<?php

namespace App\Http\Controllers;

use App\Poll;
use Illuminate\Http\Request;

class PollController extends Controller
{
    /**
     * Возвращает коллекцию всех тестов.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Poll::all();
    }
}
