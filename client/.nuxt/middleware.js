const middleware = {}

middleware['anonymous'] = require('..\\middleware\\anonymous.ts')
middleware['anonymous'] = middleware['anonymous'].default || middleware['anonymous']

middleware['check_auth'] = require('..\\middleware\\check_auth.ts')
middleware['check_auth'] = middleware['check_auth'].default || middleware['check_auth']

middleware['isAdmin'] = require('..\\middleware\\isAdmin.ts')
middleware['isAdmin'] = middleware['isAdmin'].default || middleware['isAdmin']

middleware['isRoot'] = require('..\\middleware\\isRoot.ts')
middleware['isRoot'] = middleware['isRoot'].default || middleware['isRoot']

export default middleware
