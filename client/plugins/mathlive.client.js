import Vue from 'vue'

import MathLive from 'mathlive/dist/mathlive.mjs'
import MathField from 'mathlive/dist/vue-mathlive.mjs'

Vue.use(MathField, MathLive);
