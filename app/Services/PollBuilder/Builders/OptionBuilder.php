<?php

namespace App\Services\PollBuilder\Builders;

/**
 * 
 */
class OptionBuilder
{
	
	protected $source;

	public function make($source)
	{
		$this->source = $source;
		$model = new \StdClass();

		$model->id = $source['id'];
		$model->value = $source['value'];
		$model->image = $source['image'];

		return $model;
	}
}
