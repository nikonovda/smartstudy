<?php

namespace App;

use App\Hop;
use Carbon\Carbon;
use Jenssegers\Mongodb\Eloquent\Model;

class Poll extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::deleted(function($model) {
            $model->hops()->delete();
            $model->results()->delete();
        });
    }

	protected $fillable = [
		'name',
		'description',
		'type',
        // 'isOpenResults',
        // 'isOnlyAuth',
		'published_at',
        'closed_at',
        'section',
        'timeToSolution',

        'user_id',
        'updated_by',
        // 'group_id'
        'recipient_id'
	];
	protected $appends = [
		'isPublished',
        'isActive',
        'isPersonal'
	];
	protected $dates = [
		'published_at',
        'closed_at'
	];

    public function hops()
    {
        return $this->hasMany('App\Hop');
    }

    public function results()
    {
        return $this->hasMany('App\Result');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // Для кого создан персональный тест
    public function recipient()
    {
        return $this->belongsTo('App\User');
    }

    public function updatedByUser()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    // public function group()
    // {
    //     return $this->belongsTo('App\Group');
    // }

    public function getSortAttribute($value)
    {
    	return is_null($value)
    		? []
    		: $value;
    }
    public function getIsPublishedAttribute()
    {
        return $this->published_at <= Carbon::now();
    }
    public function getIsActiveAttribute()
    {
        return $this->closed_at > Carbon::now();
    }
    public function getIsPersonalAttribute()
    {
        return $this->recipient_id !== null;
    }


    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', Carbon::now());
    }

    public static function getForUser($id)
    {
        $poll = self::findOrFail($id);
        if (!$poll->isPublished) {
            return abort(404, 'Test not found.');
        }
        if ($poll->isOpenResults) {
            $poll->load('results');
        } else {
            $poll->results = [];
        }
        return $poll->loadSortedHops();
    }


    public static function newWithHops(array $data, array $hopsData)
    {
    	$poll = self::create($data);
        $hops = [];
        foreach ($hopsData as $hop) {
            $hops[] = Hop::make($hop);
        }
        $poll->hops()->saveMany($hops);

        $poll->sort = $poll->hops->pluck('_id')->toArray();
        $poll->save();
        return $poll->loadSortedHops();
    }

    public function updateWithHops(array $data, array $hopsData)
    {
        $this->update($data);

        $sort = [];
        foreach ($hopsData as $key => $hopData) {
            // Определяем немного криво, что хоп новый или старый
            // !! Переписать
            if (strlen((string)$hopData['_id']) > 3) {
                $hop = $this->hops->firstWhere('_id', $hopData['_id']);
                if ( is_null($hop) ) continue; 
                $hop->update($hopData);
                $id = $hopData['_id'];
            } else {
                $id = $this->hops()->save( Hop::make($hopData) )->_id;
            }
            $sort[] = $id;
        }

        $this->sort = $sort;
        $this->syncSortedHops();

        $this->save();
        return $this->loadSortedHops();
    }

    public function loadSortedHops()
    {
    	$this->sorted = Hop::sortedByArray($this->sort);
    	return $this;
    }

    public function syncSortedHops()
    {
        $this->dangerSyncHops($this->sort);
    }


    protected function dangerSyncHops(array $ids)
    {
        foreach ($this->hops as $model) {
            if (!in_array($model->_id, $ids)) {
                $model->delete();
            }
        }
    }



    public static function personalForUser(string $user_id)
    {
        return self::where('recipient_id', $user_id)->get();
    }
}
