<?php

namespace App\Http\Controllers;

use App\Services\PersonalPollBuilder\PersonalPollBuilder;
use Illuminate\Http\Request;

class PersonalTestController extends Controller
{
    public function makeTestByLastUserResult()
    {
    	// dd(auth()->user());
        // $id = (new PersonalPollBuilder)->byLastResultByUser(auth()->user());
        $id = (new PersonalPollBuilder)->bySomeLastResults(auth()->user(), 3, 'math_nocalc');
        return response()->json([ 'status' => 'OK', '_id' => $id ]);
    }
}
