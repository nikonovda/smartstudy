<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Result;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AuthController extends Controller
{
	public function __construct()
	{
	    // $this->middleware('role:root', ['only' => ['registerAnonimousFinally']]);
	}

	public function local()
    {
        return response()->json(['user' => auth()->user()]);
    }
    public function oauth()
    {
        return response()->json(auth()->user());
    }

    /**
     * Регистрация пользователя
     * 
     * @return [type] [description]
     */
	public function register(Request $request)
	{
		if ($request->headers->has('authorization')) {
			$redirect = Request::create('/api/registerAnonimousFinally', 'POST', $request->all());
			foreach ($request->headers as $key => $value) {
				$redirect->headers->set($key, $value);
			}
			return app()->handle($redirect);
		}

		$user = User::create([
			'name' => request('name'),
			'lastname' => request('lastname'),
			'middlename' => request('middlename'),
			'email' => request('email'),
			'password' => bcrypt(request('password')),
			'roles' => [],
			'rating' => 0,
			'activated_at' => Carbon::now()
		]);

		if($request->has('results_id')) {
			foreach ($request->results_id as $value)
			{
				$result = Result::find($value)->user()->associate($user);
				$result->save();
			}
		}

		return response()->json(['status' => 201]);
	}

    /**
     * Регистрация анонимного пользователя как настоящего
     * 
     * @return [type] [description]
     */
	public function registerAnonimousFinally(Request $request)
	{
		if (!auth()->user()) abort(422, 'Anonimous user does not exists. Please check your access token.');

		if (!!auth()->user()->password) abort(422, 'User already exists.');
		
		auth()->user()->update([
			'name' => request('name'),
			'lastname' => request('lastname'),
			'middlename' => request('middlename'),
			'email' => request('email'),
			'password' => bcrypt(request('password')),
			'roles' => [],
		]);

		return response()->json(['status' => 201]);
	}


    /**
     * Регистрация анонимного пользователя
     * 
     * @return [type] [description]
     */
	public function registerAnonimous(Request $request)
	{
		if ($request->headers->has('authorization')) {
	        return response()->json([
	            'message' => 'User already authorized.',
	            'status' => 422,
	            'type' => 'already_authorized'
	        ], 422);
		}

		$user = User::create([
			'name' => 'Anonimous',
			'lastname' => null,
			'middlename' => null,
			'email' => uniqid() . '@anonimous.com',
			'password' => null,
			'roles' => [],
			'rating' => 0,
			'activated_at' => Carbon::now()
		]);

		$token = $user->createToken('Token Name')->accessToken;

		return response()->json([
			'token_type' => 'Bearer',
			'access_token' => $token
		]);
	}


	public function login()
	{
		$user = User::whereEmail(request('username'))->first();

	    if (!$user) {
	        return response()->json([
	            'message' => 'Wrong email.',
	            'status' => 422,
	            'type' => 'wrong_email'
	        ], 422);
	    }

		if (!Hash::check(request('password'), $user->password)) {
	        return response()->json([
	            'message' => 'Wrong password.',
	            'status' => 422,
	            'type' => 'wrong_password'
	        ], 422);
	    }

		$client = DB::table('oauth_clients')
			->where('password_client', true)
			->first();


	    if (!$client) {
	        return response()->json([
	            'message' => 'Laravel Passport is not setup properly.',
	            'status' => 500,
	            'type' => 'oauth_error'
	        ], 500);
	    }
			// return $client["_id"]->$oid;

		$data = [
			'grant_type' => 'password',
			'client_id' => $client["_id"],
			'client_secret' => $client["secret"],
			'username' => request('username'),
			'password' => request('password'),
		];

		$request = Request::create('/oauth/token', 'POST', $data);

		return app()->handle($request);
	}

	public function logout()
	{
		$accessToken = auth()->user()->token();

	    $refreshToken = DB::table('oauth_refresh_tokens')
	        ->where('access_token_id', $accessToken->id)
	        ->update([
	            'revoked' => true
	        ]);

	    $accessToken->revoke();

	    return response()->json(['status' => 200]);
	}
}
