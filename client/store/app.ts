import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import axios from '../plugins/axios'

@Module({
  name: 'app',
  stateFactory: true,
  namespaced: true
})
export default class App extends VuexModule {
  drawer: boolean = true

  themesSAT: Array<string> = [
    'Heart of Algebra',
    'Passport to Advanced Math',
    'Problem Solving and Data Analysis',
    'Additional Topics'
  ]

  subThemesSAT = {
    'Heart of Algebra': [
      'Solving linear equations',
      "Interpreting linear functions",
      "Linear inequality word problems",
      "Linear equation word problems",
      "Graphing linear equations",
      "Linear function word problems",
      'Systems of linear inequalities word problems',
      "Solving systems of linear equations"
    ],
    'Passport to Advanced Math': [
      "Solving quadratic equations",
      "Interpreting nonlinear expressions",
      "Quadratic and exponential word problems",
      "Radicals and rational exponents",
      "Operations with rational expressions and polynomials",
      "Polynomial factors and graphs",
      "Linear and quadratic systems",
      "Solving equations with radicals",
      "Isolating quantities",
      "Functions and graphs",
      "Solving and graphing quadratic equations",
      "Functions",
    ],
    'Problem Solving and Data Analysis': [
      "Percents",
      "Table data",
      "Ratios, rates, and proportions",
      "Key features of graphs",
      "Data collection and conclusions",
    ],
    'Additional Topics': [
      "Volume word problems",
      "Congruence and similarity",
      "Angles and arc lengths in a circle",
      "Angles, arc lengths, and trig functions",
      "Basic geometry",
      "Complex numbers",
    ]
  }

  themesOlympic: Array<string> = [
    'Counting/Combinatorics',
    'Number Theory',
    'Algebra',
    'Geometry'
  ]

  subThemesOlympic = {
    'Counting/Combinatorics': [
      'Counting word problems',
      'Combinatorics',
      'Probability',
      'Arithmetic word problems',
      'Arithmetic',
      'Sequences',
    ],
    'Number Theory': [
      'Number Theory',
    ],
    'Algebra': [
      'Basic Algebra',
      'Exponents',
      'Roots of polynomial equations',
      'Algebra word problems',
    ],
    'Geometry': [
      'Geometry of circles',
      'Geometry of polygons',
      '3D Geometry',
    ]
  }
  tab = 0

  @Mutation
  SET_DRAWER (payload) {
    this.drawer = payload
  }

  @Mutation
  SET_TAB (payload) {
    this.tab = payload
  }

}
