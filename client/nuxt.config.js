// import colors from 'vuetify/es5/util/colors'
require('dotenv').config()

import localforage from 'localforage';
// require('localforage')

export default {
  mode: 'universal',

  router: {
    middleware: 'check_auth'
  },

  /*
  ** Headers of the page
  */
  head: {
    title: 'Adaptive Education',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      // { src: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-AMS_HTML', defer: true }
    ],
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#00E676' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/main.css',
  ],
  cache: true,
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/silentbox',
    {
      src: '~/plugins/chart', mode: 'client'
    },
    '~/plugins/mathEditor',
    '~/plugins/katex',
    { src: '~/plugins/froala', mode: 'client'}
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    ['@nuxt/typescript-build', {
      ignoreNotFoundWarnings: false
    }],
    ['@nuxtjs/vuetify' ],
    [ '@nuxtjs/dotenv', { path: './' } ],
    '@nuxtjs/moment',
    '@nuxtjs/localforage'
  ],

  localforage: {
    driver: [localforage.LOCALSTORAGE],
    instances: [{
      driver: [localforage.LOCALSTORAGE],
      name: 'adaptive',
      storeName: 'polls'
    }]
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/proxy',
    'nuxt-sweetalert2',
    'nuxt-socket-io'
  ],

  io: {
    sockets: [ // Required
      { // At least one entry is required
        name: 'issues',
        url: 'http://localhost:3000',
        default: true,
        vuex: {  },
        // namespaces: { /* see section below */ }
      },
    //   { name: 'work', url: 'http://somedomain1:3000' },
    //   { name: 'car', url: 'http://somedomain2:3000' },
    //   { name: 'tv', url: 'http://somedomain3:3000' },
    //   { name: 'test', url: 'http://localhost:4000' }
    ]
  },

  proxy: {
    '/api': process.env.API_URL
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    preset: "~/preset",
    treeShake: true,
    extractCSS: true
    // theme: {
    // dark: true,
    // themes: {
    // dark: {
    // primary: colors.blue.darken2,
    // accent: colors.grey.darken3,
    // secondary: colors.amber.darken3,
    // info: colors.teal.lighten1,
    // warning: colors.amber.base,
    // error: colors.deepOrange.accent4,
    // success: colors.green.accent3
    // }
    // }
    // }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    postcss: {
      plugins: {
        'autoprefixer': { grid: true },
      },
    },
    resolve: {
      alias: {
        '~': '.'
      }
    },
    transpile: ['vuex-module-decorators']
  },


  server: {
    port: process.env.SERVER_PORT,
    host: process.env.SERVER_HOST
  }
}
