exports.ids = [16];
exports.modules = {

/***/ 523:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader??ref--1-oneOf-0-0!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/logout/index.vue?vue&type=template&id=d98072f8&scoped=true&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c("div")}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/logout/index.vue?vue&type=template&id=d98072f8&scoped=true&lang=pug&

// EXTERNAL MODULE: external "tslib"
var external_tslib_ = __webpack_require__(1);

// EXTERNAL MODULE: external "vue-property-decorator"
var external_vue_property_decorator_ = __webpack_require__(8);

// EXTERNAL MODULE: ./store/index.ts + 1 modules
var store = __webpack_require__(5);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!./node_modules/vuetify-loader/lib/loader.js??ref--18-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/logout/index.vue?vue&type=script&lang=ts&



let logoutvue_type_script_lang_ts_Logout = class Logout extends external_vue_property_decorator_["Vue"] {
  beforeMount() {
    store["auth"].logout().then(() => {
      localStorage.removeItem('AnswerData');
      localStorage.removeItem('polls');
      localStorage.removeItem('historyLS');
      this.$router.push('/');
    }).catch(() => {
      localStorage.removeItem('AnswerData');
      localStorage.removeItem('polls');
      localStorage.removeItem('historyLS');
      this.$router.push('/');
    });
  }

};
logoutvue_type_script_lang_ts_Logout = Object(external_tslib_["__decorate"])([Object(external_vue_property_decorator_["Component"])({
  name: 'Logout',
  layout: 'stepLayout'
})], logoutvue_type_script_lang_ts_Logout);
/* harmony default export */ var logoutvue_type_script_lang_ts_ = (logoutvue_type_script_lang_ts_Logout);
// CONCATENATED MODULE: ./pages/logout/index.vue?vue&type=script&lang=ts&
 /* harmony default export */ var pages_logoutvue_type_script_lang_ts_ = (logoutvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(14);

// CONCATENATED MODULE: ./pages/logout/index.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_logoutvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "d98072f8",
  "7594a380"
  
)

/* harmony default export */ var logout = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=index.js.map