<?php

use Illuminate\Database\Seeder;

class PollsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
	    factory(App\Poll::class, 6)->create()->each(function ($poll) {
	        $poll->hops()->save(factory(App\Hop::class)->make());
	    });
    }
}
