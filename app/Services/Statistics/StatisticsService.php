<?php

namespace App\Services\Statistics;

use App\Hop;
use App\Poll;
use App\Result;
use App\User;
use Illuminate\Support\Collection;

/**
 * 
 */
class StatisticsService
{
	protected $cache = [];

	protected function isCached($key)
	{
		return array_key_exists($key, $this->cache);
	}
	protected function cache($key, $value)
	{
		$this->cache[$key] = $value;
	}
	protected function getCached($key)
	{
		return $this->cache[$key];
	}




	public function byUser(User $user)
	{
		$results = $user->results;
		$hops = Hop::all();
		
		$arr = [];
		foreach ($results as $result) {
			$section = $result->poll->recipient_id === $user->_id ? 'personal' : $result->poll->section;
			$passage = [];
			foreach ($result->answers as $key => $answer) {
				$hop_id = $answer['hop_id'];
				$hop = $hops->find($hop_id);
				
				if (!$hop) continue;

				$complexity = $hop->complexity;
				$option_id = count($answer['options']) > 0 ? $answer['options'][0]['id'] : null;

				$isCorrect = in_array($option_id, $hop->correctAnswer);
				$time = array_key_exists('time', $answer) ? $answer['time'] : 0;

				$item = [
					'poll_id' => $result->poll_id,
					'result_id' => $result->_id,
					'isCorrect' => $isCorrect,
					'time' => $time,
					'complexity' => $complexity,
					'tag' => $this->tmpGetTag($hop->tags),
					'subTag' => $this->tmpGetTag($hop->subTags),
					'created_at' => $result->created_at
				];
				$passage[] = $item;
			}
			$arr[$section][] = $passage;
		}
		return $arr;
	}

	/**
	 * Return flat array of all user's results (hops)
	 * 
	 * @param  User   $user [description]
	 * @return [type]       [description]
	 */
	public function byUserCommon(User $user)
	{
		$results = $user->results;
		$hops = Hop::all();
		
		$arr = [];
		foreach ($results as $result) {
			$section = $result->poll->recipient_id === $user->_id ? 'personal' : $result->poll->section;
			// $passage = [];
			foreach ($result->answers as $key => $answer) {
				$hop_id = $answer['hop_id'];
				$hop = $hops->find($hop_id);
				
				if (!$hop) continue;

				$complexity = $hop->complexity;
				$option_id = count($answer['options']) > 0 ? $answer['options'][0]['id'] : null;

				$isCorrect = in_array($option_id, $hop->correctAnswer);
				$time = array_key_exists('time', $answer) ? $answer['time'] : 0;

				$item = [
					'section' => $section,
					'poll_id' => $result->poll_id,
					'result_id' => $result->_id,
					'isCorrect' => $isCorrect,
					'time' => $time,
					'complexity' => $complexity,
					'tag' => $this->tmpGetTag($hop->tags),
					'subTag' => $this->tmpGetTag($hop->subTags),
					'created_at' => $result->created_at
				];
				$arr[] = $item;
				// $passage[] = $item;

			}
			// $arr[] = $passage;
		}
		return $arr;
	}


	public function all() // ?
	{
		if ($this->isCached(__FUNCTION__)) return $this->getCached(__FUNCTION__);

		$polls = Poll::with('hops', 'results')->get();

		$res = collect([]);
		foreach ($polls as $poll) {
			foreach ($poll->results as $result) {
				foreach ($result->answers as $key => $answer) {
					$hop = $poll->hops->find($answer['hop_id']);
					if (!$hop) continue;

					$option_id = count($answer['options']) > 0 ? $answer['options'][0]['id'] : null;
					$isCorrect = in_array($option_id, $hop->correctAnswer);
					$time = array_key_exists('time', $answer) ? $answer['time'] : 0;


					$tag = $this->tmpGetTag($hop->tags);

					$res[] = [
						'hop_id' => $hop->_id,
						'poll_id' => $poll->_id,
						'section' => $poll->section,
						'user_id' => $result->user_id,
						'recipient_id' => $poll->recipient_id,
						'isCorrect' => $isCorrect,
						'time' => $time,
						'complexity' => $hop->complexity,
						'tag' => $this->tmpGetTag($hop->tags),
						'subTag' => $this->tmpGetTag($hop->subTags),
						'created_at' => $result->created_at
					];
				}
			}
		}
		$this->cache(__FUNCTION__, $res);

		return $res;
	}

	public function allDepersonized(User $user)
	{
		return $this->depersonize($this->all(), $user);
		// return $this->all()->map(function ($item) use ($user) {
		// 	if ($item['user_id'] !== $user->_id) $item['user_id'] = md5($item['user_id']);
		// 	if ($item['recipient_id'] !== $user->_id) $item['recipient_id'] = null;
		// 	return $item;
		// });
	}


	public function percentageOfCorrectAnswersOnEechThemeAverage(string $section)
	{
		$stats = $this->allBySection($section);
		$series = collect([]);

		foreach ($this->getSubTagsList($section) as $tag) {
			$filteredByTag = $stats->filter(function($item) use ($tag) {
				return $item['subTag'] === $tag;
			});
			$correctCount = $filteredByTag->filter(function($item) {
				return $item['isCorrect'];
			})->count();

			$res = $correctCount > 0
				? round( $correctCount / $filteredByTag->count() * 100 )
				: 0;

			$series->push($res);
		}
		return $series;
	}


	public function averageTimeSpendToTopicValueUsers(string $section)
	{
		$stats = $this->allBySection($section);
		$series = collect([]);

		foreach ($this->getTagsList($section) as $tag) {
			$filteredByTag = $stats->filter(function ($item) use ($tag) {
				return $item['tag'] === $tag;
			});
			// return $filteredByTag;
			$fullTime = $filteredByTag->reduce(function ($acc, $item) {
				return $item['time'] > 10000
					? $acc
					: $acc + $item['time'];
			}, 0);


			$res = $fullTime > 0
				? round( $fullTime / $filteredByTag->count() )
				: 0;

			$series->push($res);
		}
		return $series;
	}

	public function percentageCorrectAnwsersOfTopicAverage(string $section)
	{
		$stats = $this->allBySection($section);
		$series = collect([]);

		foreach ($this->getTagsList($section) as $tag) {
			$filteredByTag = $stats->filter(function($item) use ($tag) {
				return $item['tag'] === $tag;
			});
			$correctCount = $filteredByTag->filter(function($item) {
				return $item['isCorrect'];
			})->count();

			$res = $correctCount > 0
				? round( $correctCount / $filteredByTag->count() * 100 )
				: 0;

			$series->push($res);
		}
		return $series;
	}


	public function predictiveTimeToResolveTests(string $section, User $user = null, int $count_hops = 20)
	{
		$offset = 25;
		$series = collect([]);
		$polls = Poll::published()->whereSection($section)->get();


		foreach ($polls as $key => $poll) {
			$stats = $this->all()->filter(function ($item) use ($poll) {
				return $item['poll_id'] === $poll->_id;
			});
			$fullTime = $stats->reduce(function ($acc, $item) {
				return $item['time'] > 10000
					? $acc
					: $acc + $item['time'];
			}, 0);

			$res = $fullTime > 0
				? round( $fullTime / $stats->count() * $count_hops )
				: 0;
			if ($user !== null) {
				$lastResult = $user->results()->wherePollId($poll->_id)->get()->reverse()->first();
				if ($lastResult && $lastResult->time > 0 && $lastResult->time <= $res ) {
					$res = $lastResult->time > $offset
						? $lastResult->time - $offset
						: 0;
				}
			}
	
			$series->push($res);
		}
		return $series;
	}


	public function predictiveOfCorrectAnswers(string $section, User $user = null, int $count_hops = 20)
	{
		$offset = 10;
		$series = collect([]);
		$polls = Poll::published()->whereSection($section)->get();


		foreach ($polls as $key => $poll) {
			$stats = $this->all()->filter(function ($item) use ($poll) {
				return $item['poll_id'] === $poll->_id;
			});
			$correctCount = $stats->filter(function($item) {
				return $item['isCorrect'];
			})->count();

			$res = $correctCount > 0
				? round( $correctCount / $stats->count() * 100 )
				: 0;
			if($user !== null){
				$lastResult = $user->results()->wherePollId($poll->_id)->get()->reverse()->first();
				// dd($lastResult->getCorrectCount());
	
				$correctnessLastResult = $lastResult
					? $lastResult->getCorrectCount() / $poll->hops->count() * 100
					: 0;
	
	
				if ($correctnessLastResult >= $res) {
					$res = $correctnessLastResult + $offset;
					if ($res > 100) $res = 100;
				}
			}
	
			$series->push($res);
		}
		return $series;
	}



	protected function allBySection(string $section)
	{
		return $this->all()->filter(function($item) use ($section) {
			return $item['section'] === $section;
		});
	}



	protected function tmpGetTag($value)
	{
		if (is_string($value)) return $value;
		if (is_array($value) && count($value) > 0) return $value[0];
		return '';
	}


	protected function depersonize(Collection $data, User $user)
	{
		return $data->map(function ($item) use ($user) {
			if ($item['user_id'] !== $user->_id) $item['user_id'] = $item['user_id'] !== null ? md5($item['user_id']) : 'null';
			if ($item['recipient_id'] !== $user->_id) $item['recipient_id'] = null;
			return $item;
		});
	}


	protected function getTagsList(string $section = 'math_calc')
	{
		if (!in_array($section, ['math_calc', 'math_nocalc', 'personal', 'olympics'])) return [];

		$section = $section === 'olympics' ? 'olympics' : 'sat';
		$themes = [
			'sat' => [
				'Heart of Algebra',
				'Passport to Advanced Math',
				'Problem Solving and Data Analysis',
				'Additional Topics'
			],
			'olympics' => [
				'Counting/Combinatorics',
				'Number Theory',
				'Algebra',
				'Geometry'
			]
    	];
		return $themes[$section];
	}


	protected function getSubTagsList(string $section = 'math_calc')
	{
		if (!in_array($section, ['math_calc', 'math_nocalc', 'personal', 'olympics'])) return [];

		$section = $section === 'olympics' ? 'olympics' : 'sat';

		$themes = [
			'sat' => [

				'Solving linear equations',
      	'Interpreting linear functions',
      	'Linear inequality word problems',
      	'Linear equation word problems',
      	'Graphing linear equations',
      	'Linear function word problems',
      	'Systems of linear inequalities word problems',
      	'Solving systems of linear equations',

				'Solving quadratic equations',
      	'Interpreting nonlinear expressions',
      	'Quadratic and exponential word problems',
      	'Radicals and rational exponents',
      	'Operations with rational expressions and polynomials',
      	'Polynomial factors and graphs',
      	'Linear and quadratic systems',
      	'Solving equations with radicals',
      	'Isolating quantities',
      	'Functions and graphs',
      	'Solving and graphing quadratic equations',
				'Functions',

				'Percents',
				'Table data',
				'Ratios, rates, and proportions',
				'Key features of graphs',
				'Data collection and conclusions',

				'Volume word problems',
				'Congruence and similarity',
				'Angles and arc lengths in a circle',
				'Angles, arc lengths, and trig functions',
				'Basic geometry',
				'Complex numbers',
	    	],
	    	'olympics' => [
				'Counting word problems',
				'Combinatorics',
				'Probability',
				'Arithmetic word problems',
				'Arithmetic',
				'Sequences',

				'Number Theory',

				'Basic Algebra',
				'Exponents',
				'Roots of polynomial equations',
				'Algebra word problems',

				'Geometry of circles',
				'Geometry of polygons',
				'3D Geometry',
	    	],
		];
		return $themes[$section];
	}
}
