import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import axios from '../plugins/axios'
import Vue from 'vue'

@Module({
  name: 'issues',
  stateFactory: true,
  namespaced: true
})
export default class Issues extends VuexModule {

  issues = []


  @Mutation
  ADD_NEW_MESSAGE (payload) {
    let index = this.issues.findIndex( issue => issue._id === payload.id )
    if (index !== -1 ) {
      Vue.delete(this.issues, index)
      Vue.set(this.issues, index, payload)
      // this.issues[index] = payload
    }
  }

  @Mutation
  ADD_NEW_ISSUE (payload) {
    this.issues.push(payload)
  }

  @Mutation
  SET_ISSUES (payload) {
    this.issues = payload
  }


  @Action({ rawError: true })
  getIssues () {
    axios.get('/issues').then(response => {
      console.warn(response);
    })
  }



}
