

export interface PollFull {
  // [key: string]: any,
  _id: string,
  closed_at?: string,
  created_at?: string,
  description?: string,
  hops?: Array<string>,
  isOpenResults?: boolean
  isOnlyAuth?: boolean
  name?: string
  sort: string[]
  sorted: Sorted[]
  published_at?: string
  type?: string
  isPublished?: boolean,
  isActive?: boolean,
  section?: string,
  user_id?: string,
  timeToSolution?: string
}

export interface Sorted {
  _id: string
  choised: number[]
  complexity:90.3744495027047
  content: string
  correctAnswer: number[]
  created_at: string
  explanation: string
  image: Array<string>
  options: Options
  poll_id: string
  subTags: string
  tags: string
  updated_at: string
}

export interface Hops {
  [key: string]: HopStore
}

export interface HopBase {
  [key: string]: any,
  _id?: string,
  choiseCount?: Array<number>,
  choised?: Array<number>,
  content?: string,
  view?: string
  condition?: string
}

export interface HopStore extends HopBase {
  options?: number[]
}

export interface HopData extends HopBase {
  options?: Option[]
}

export interface Options {
  [key: string]: Option
}

export interface Option {
  [key: string]: any
  id: number,
  type: string,
  value: string
}

export interface HopIds {
  [Symbol.iterator]: any
  [key: string]: any
}

export interface OptionIds {
  [Symbol.iterator]: any
  [key: string]: any
}


export interface HopMutation {
  [key: string]: any
  _id: string
  content: string
  view: string
  required?: Boolean
  choiseCount: Array<number>
  choised: Array<number>
  options: Array<OptionMutatuion>
  poll_id: string
  created_at?: string
  updated_at?: string
}

export interface OptionMutatuion {
  id: number
  attrId?: string
  value: string
  type: string
  userValue: Boolean
}

export interface ResultPoll {
  [key: number]: number
  _id?: string
  answers?: Answer[]
  created_at?: string
  poll_id?: string,
  updated_at?: string
  time: number
}

export interface Answer {
  hop_id: string
  options: Option[]
  time: number
}

export interface Option {
  id: number
  value: null | string
}