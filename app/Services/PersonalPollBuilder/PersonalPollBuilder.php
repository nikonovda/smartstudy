<?php

namespace App\Services\PersonalPollBuilder;

use App\Poll;
use App\Result;
use App\Services\PollBuilder\PollMapper;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use phpDocumentor\Reflection\Types\Integer;

/**
 * 
 */
class PersonalPollBuilder
{
	protected $targetCount = [
		'math_calc' => 20,
		'math_nocalc' => 20
	];

	public function byLastResultByUser(User $user)
	{
        $result = $user->results()->with('poll')->orderBy('created_at', 'desc')->first();
        if (!$result) return false;

        $result->poll->loadSortedHops();

        $c = $this->getCoefficient($result);

        $hops = $this->getHopList($c, $result->poll->section, $user);
        // dd($hops);

        $poll = $this->makePoll($user, $hops, $result->poll->section);
        // dd($poll);

        $mapper = new PollMapper($poll);
        $poll_id = $mapper->save();

        return $poll_id;
	}

	public function bySomeLastResults(User $user, int $count, string $section)
	{
				$results = $user->results()->with('poll')->orderBy('created_at', 'desc')->get();
				$results = $results->filter(function ($item) use ($section) {
					return $item->poll->section === $section;
				})->take($count);
				if ($results->count() === 0) return false;
				$coefficients = [];
				foreach ($results as $result)
				{
					$result->poll->loadSortedHops();
					$coefficients[] = $this->getCoefficient($result);
				}
				$res = [];
				foreach ($coefficients as $i => $coefficient)
				{
					foreach ($coefficient as $key => $value)
					{
						if(array_key_exists( $key, $res)) $res[$key] += $value;
						else $res[$key] = $value;
					}
				}
				$res = collect($res);

				$hops = $this->getHopList($res, $section, $user);

				$poll = $this->makePoll($user, $hops, $section);

				$mapper = new PollMapper($poll);
				$poll_id = $mapper->save();
        return $poll_id;
	}

	public function getCoefficient(Result $result)
	{
		$subTags = [];
		foreach ($result->poll->sorted as $item) {
			$subTags[] = $item->subTags;
		}
		$subTags = array_unique($subTags);
		$answers = $result->answers;
		$errors = $result->poll->sorted->filter(function ($item, $i) use ($answers) {
			if(!array_key_exists(0, $answers[$i]['options'])) return false;
			return $item->correctAnswer[0] !== $answers[$i]['options'][0]['id'];
		});

		$c = array_fill_keys($subTags, 0);
		// dd($errors);
		foreach ($errors as $item) {
			// dump($item->subTags);
			$c[$item->subTags] = $c[$item->subTags] + 1;
		}
		// dd($c);
		return collect($c);
	}

	public function getHopList(Collection $coefficient, string $section, User $user)
	{
		$coefficient = $coefficient->filter(function ($item) {
			return $item > 0;
		});
		$polls = Poll::where('section', $section)
			->where('recipient_id', null)
			->with('hops')
			->get();

		$except = $user->results->map(function ($item) {
			return $item->poll_id;
		})->unique();
		
		$polls = $polls->filter(function ($item) use ($except)
		{
			return !$except->contains($item->_id);
		});

		$hops = $polls->reduce(function ($acc, $item) {
			if (gettype($acc) === 'array') $acc = collect($acc);
			return $acc->merge($item->hops);
		}, []);

		$fullError = $coefficient->reduce(function ($acc, $item) { return $acc + $item; }, 0);

		$taggedHops = [];
		$list = [];
		$diff = [];
		foreach ($coefficient as $key => $value) {
			$count = $value / $fullError * $this->targetCount[$section];
			$arr = [];
			foreach ($hops as $item) {
				if ($key === $item->subTags) {
					$arr[] = $item;
				}
			}
			$taggedHops[$key] = collect($arr)->shuffle();

			if ($taggedHops[$key]->count() >= $count) {
				$list[] = $taggedHops[$key]->take($count);
				$taggedHops[$key] = $taggedHops[$key]->slice($count);
			} else {
				// dd('twert');
				$diff[$key] = $count - $taggedHops[$key]->count();
				$list[] = $taggedHops[$key]->all();
				$taggedHops[$key] = collect([]);
			}
		}

		if (count($diff) > 0) {
			$fullDiff = 0;
			foreach ($diff as $value) {
				$fullDiff += $value;
			}
			$commonTaggedHops = collect([]);
			foreach ($taggedHops as $value) {
				$commonTaggedHops->merge($value->values());
			}
			// dd($commonTaggedHops);

			if ($commonTaggedHops->count() >= $fullDiff) {
				$list[] = $commonTaggedHops->take($fullDiff);
			} else {
				$list[] = $commonTaggedHops->all();
			}
		}

		$flatList = collect([]);
		foreach ($list as $value) {
			$flatList = $flatList->merge(collect($value));
		}
		// dd($flatList);
		
		if ($flatList->count() < $this->targetCount[$section]) {
			$fullDiff = $this->targetCount[$section] - $flatList->count();
			$ids = $flatList->pluck('_id');
			// dd($ids);
			foreach ($hops as $hop) {
				if (!$ids->contains($hop->_id)) {
					$flatList->push($hop);
					$fullDiff--;
				}
				if ($fullDiff === 0) break;
			}
		}
		return $flatList;
	}

	public function makePoll(User $user, Collection $hops, string $section)
	{
		$number = $user->personalPolls()->count() + 1;
		$username = !!$user->name ? $user->name : 'anonimous user';
		$description = 'Automatically generated SAT test in section ' . $section . ' for ' . $username;
		
		$hops = $hops->map(function ($item, $i) {
			$item->origin_id = $item->_id;
			$item->poll_id = null;
			$item->_id = $i;
			return $item;
		});

		$data = [
			'recipient' => $user->_id,
            'name' => 'Personal test #' . $number,
            'description' => $description,
            'type' => 'SAT',
            'published_at' => Carbon::now(),
            'closed_at' => Carbon::now()->addYears(1),
            'section' => 'personal',
            'timeToSolution' => '03:00:00',
            'hops' => $hops->shuffle()->toArray()
		];

		return $data;
	}
}
