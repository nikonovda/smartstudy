import Axios from 'axios'

import { getUserFromLocalStorage } from '../utils/auth'

let token = process.server ? null : getUserFromLocalStorage()

// export default axios.create({
//   baseURL: process.server ? process.env.API_URL : ''
// })

const axios = Axios.create({
  baseURL: process.server ? process.env.API_URL : ''
})

if(token) axios.defaults.headers.common['Authorization'] = 'Bearer ' + token

export default axios