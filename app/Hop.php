<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class Hop extends Model
{

	protected $fillable = [
		'content',
		// 'view',
		// 'required',
		// 'choiseCount',
		// 'choised',
		'options',
        'image',
        'correctAnswer',
        'tags',
        'subTags',
        'complexity',
        'explanation',

        'origin_id'
	];

    public function poll()
    {
    	return $this->belongsTo('App\Poll');
    }

    public function origin()
    {
        return $this->belongsTo('App\Hop');
    }

    public function getTagsAttribute($value)
    {
        return $this->tmpGetTag($value);
    }

    public function getSubTagsAttribute($value)
    {
        return $this->tmpGetTag($value);
    }

    protected function tmpGetTag($value)
    {
        if (is_array($value) && count($value) > 0) return $value[0];
        if (is_string($value)) return $value; 
        return '';
    }

    public static function listByArray(array $ids = [])
    {
    	if ( count($ids) === 0 ) return collect([]);
    	
    	$query = self::query();
    	foreach ($ids as $key => $index) {
    		$query->orWhere('_id', $index);
    	}
    	return $query->get();
    }

    public static function sortedByArray(array $ids = [])
    {
    	return self::listByArray($ids)->sortBy(function($item, $key) use ($ids) {
    		return array_search($item->_id, $ids);
    	})->values();
    }
}
