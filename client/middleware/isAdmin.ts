export default function ({ store, redirect }) {
  if(!store.getters['auth/isAdmin'] && !store.getters['auth/isRoot']) {
    redirect('/')
  }
}