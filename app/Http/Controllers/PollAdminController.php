<?php

namespace App\Http\Controllers;

use App\Exports\PollResultsExport;
use App\Hop;
use App\Http\Requests\PollRequest;
use App\Poll;
use App\Services\PollBuilder\PollMapper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class PollAdminController extends Controller
{
    /**
     * DEPRECATED Возвращает список тестов группы пользователя.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $group = auth()->user()->group;
        return $group
            ? $group->polls
            : abort(404, 'Group not found.');
    }


    /**
     * DEPRECATED Собирает и сохраняет новый тест.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PollRequest $request)
    {
        $source = $request->all();
        $source['user'] = auth()->user();
        $source['updatedByUser'] = auth()->user();
        $source['group'] = auth()->user()->group;

        $id = (new PollMapper($source))->save();

        return response()->json([ 'status' => 'OK', '_id' => $id ]);
    }

    /**
     * Возвращает домен теста.
     *
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function show(Poll $poll)
    {
        $poll->load('results');
        return $poll->loadSortedHops();
    }

    /**
     * Собирает и обновляет тест.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function update(PollRequest $request, Poll $poll)
    {
        $source = $request->all();
        $source['updatedByUser'] = auth()->user();
        
        $id = (new PollMapper($source))->save();
        return response()->json([ 'status' => 'OK', '_id' => $id ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function destroy(Poll $poll)
    {
        $poll->delete();
        return response()->json([ 'status' => 'OK' ]);
    }

    /**
     * Формирует и сохраняет на сервере файл XLSX из результатов переданного теста.
     * Возвращает ссылку на роут для скачивания.
     * 
     * @param  Poll   $poll [description]
     * @return [type]       [description]
     */
    public function export(Poll $poll)
    {
        $filename = 'results-' . $poll->_id . '-' . Carbon::now() . '.xlsx';

        if (Excel::store(new PollResultsExport($poll), $filename, 'exports')) {
           return response()->json([ 'status' => 'OK', 'url' => '/api/download?filename='. $filename ]); 
        }
        return response()->json([ 'status' => 'error']);
    }


    /**
     * Отдает для скачивания файл, созданный при экспорте. Название файла передается в GET-параметре filename.
     * После скачивания файл удаляется.
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function onceLoadFile(Request $request)
    {
        $path = \Storage::disk('exports')->path($request->filename);
        return response()->download($path)->deleteFileAfterSend(true);
    }
}
